#!/usr/bin/env bash
# A small script for switching current working dataset.

rm ./python/datasets/current
rm ./figures/current
rm ./results/regression/current
rm ./results/classification/current

ln --symbolic ./$1 ./python/datasets/current
ln --symbolic ./$1 ./figures/current
ln --symbolic ./$1 ./results/regression/current
ln --symbolic ./$1 ./results/classification/current
