#!/bin/env python
from distutils.core import setup, Extension
from os import environ

sgraph = Extension('sgraph',
    sources = ['sgraph.c'],
    libraries = ['RNA'],
    extra_compile_args = ['-O2', '-fopenmp'],
    extra_link_args = ['-fopenmp'],
    #library_dirs = ['/home/nfs/alexeygritsenk/env.el7/sys_enhance/lib64/']
    library_dirs = ['/opt/insy/env.el7/sys_enhance/lib64/'])

setup(name = 'StructureGraph',
      version = '1.0',
      description = 'A C extension containing a set of tools used for RNA structure graphs.',
      author = 'Alexey A. Gritsenko',
      author_email = 'a.gritsenko@tudelft.nl',
      long_description = 'A C implementation of some of the tools used within the IRES Predictor project to predict and process RNA structures.',
      include_dirs = ['/opt/insy/env.el7/sys_enhance/include/python2.7/', '/opt/insy/env.el7/sys_enhance/lib64/python2.7/site-packages/numpy/core/include/numpy/', '/opt/insy/env.el7/sys_enhance/include/', '/opt/insy/env.el7/sys_enhance/include/ViennaRNA'],
      ext_modules = [sgraph])
