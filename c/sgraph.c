#include <Python.h>
#include <string.h>
#include <stdio.h>
#include "part_func.h"
#include "fold.h"
#include "fold_vars.h"
#include "utils.h"
#include "subopt.h"
#include "params.h"

#include "arrayobject.h"

static PyObject *sgraph_get_structure_pairwise_distances(PyObject *self, PyObject *args)
{
    const unsigned char *structure;
    int n, i, j, k;
    int *stack;
    int stack_size;
    int dims[2];
    int *dist;
    int infinity;
    PyArrayObject *dist_np;
    
    if (!PyArg_ParseTuple(args, "s", &structure))
        return NULL;
    
    n = strlen(structure);
    stack_size = 0;
    stack = (int *) malloc(n * sizeof(int));
    dims[0] = dims[1] = n;
    dist_np = (PyArrayObject *) PyArray_FromDims(2, dims, NPY_INT);
    dist = (int *)dist_np->data;
    
    infinity = n * n + 1;
    for (i = 0; i < n; i++)
        for (j = 0; j< n; j++)
            dist[i * n + j] = infinity;
    
    for (i = 0; i < n; i++)
    {
        dist[i * n + i] = 0;
        if (structure[i] == '(')
            stack[stack_size++] = i;
        else if (structure[i] == ')')
        {
            int j = stack[--stack_size];
            dist[i * n + j] = dist[j * n + i] = 1;
        }
    }
    
    for (i = 0; i < n - 1; i++)
        dist[i * n + (i + 1)] = dist[(i + 1) * n + i] = 1;

    for (k = 0; k < n; k++)
        for (i = 0; i < n; i++)
            for (j = 0; j < n; j++)
            {
                int new_dist = dist[i * n + k] + dist[k * n + j];
                if (dist[i * n + j] > new_dist)
                    dist[i * n + j] = new_dist;
            }
    
    free(stack);
    return Py_BuildValue("N", dist_np);
}

/*
 * This is a striped version of the function used in RNAsubopt from the ViennaRNA package.
 * I count not get the Python interface to enumerate the structures, so had to resort to copy-pasting things into a C module.
 * */
static PyObject *sgraph_sample_structures(PyObject *self, PyObject *args)
{
    const unsigned char *sequence;
    int n_back;

    if (!PyArg_ParseTuple(args, "si", &sequence, &n_back))
        return NULL;
   
    double betaScale = 1.;
    paramT *P = NULL;
    int do_backtrack = 1;
    int fold_constrained = 0;
    int circular = 0;
    model_detailsT  md;

    set_model_details(&md);
    P = get_scaled_parameters(temperature, md);

    st_back = 1;
    int length;
    double mfe, kT;
    char *ss, *structure, *rec_sequence;
    pf_paramT *pf_parameters;
    rec_sequence = sequence;
    length = strlen((char *)rec_sequence);
    structure = (char *) malloc((unsigned) strlen(sequence) + 1);
    ss = (char *) malloc((unsigned) length + 1);
    strncpy(ss, structure, length);
    mfe = fold_par(rec_sequence, ss, P, fold_constrained, circular);
    kT = (betaScale * ((temperature + K0) * GASCONST)) / 1000.; /* in Kcal */
    pf_scale = exp(-(1.03 * mfe) / kT / length);
    strncpy(ss, structure, length);
   
    pf_parameters = get_boltzmann_factors(temperature, betaScale, md, pf_scale);
    /* ignore return value, we are not interested in the free energy */
    (void) pf_fold_par(rec_sequence, ss, pf_parameters, do_backtrack, fold_constrained, circular);
    PyListObject *list = PyList_New(n_back);
    int i;
    for (i = 0; i < n_back; i++) {
        char *s;
        PyObject *python_string;
        s = pbacktrack(rec_sequence);
        python_string = Py_BuildValue("s", s);
        PyList_SetItem(list, i, python_string);
        //printf("%s\n", s);
        free(s);
    }
    free(structure);
    free(ss);
    free_pf_arrays();
    free(pf_parameters);
    return Py_BuildValue("O", list);
};

static PyMethodDef SgraphMethods[] = {
    {"sample_structures",  sgraph_sample_structures, METH_VARARGS, "Sample a give number structures from the ensemble based on their probabilities (given by the Boltzmann equation)."}, 
   {"get_structure_pairwise_distances", sgraph_get_structure_pairwise_distances, METH_VARARGS, "A C function used to run the Floyd-Warshall algorithm on the RNA structure graph."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

PyMODINIT_FUNC initsgraph(void)
{
    (void) Py_InitModule("sgraph", SgraphMethods);
    import_array(); // NumPy
}
