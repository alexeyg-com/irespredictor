# Introduction
This repository provides the source code and data accompanying the manuscript
"Sequence features of viral and human Internal Ribosome Entry Sites predictive of their activity"
by A.A. Gritsenko, S. Weingarten-Gabbay, S. Elias-Kirma, R. Nir, D. de Ridder and E. Segal

Last updated: May 2017

# Directory structure
* `/data/` contains raw data used for the analysis
* `/figures/` contains figures generated for the manuscript, as well as additional figures used for supplementary analyses
* `/python/` contains the main code for the analyses presented in the manuscript
* `/results/` is designated for storing analyses results
* `/scripts/` contains auxiliary scripts for grid computing
* `/tools/` contains third-party tools used for analyses
* `/c/` contains python module source code use for auxiliary analyses

