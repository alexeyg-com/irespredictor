'''
A module implementing necessary routines for constructing figures/tables/etc required for the (final) paper.

8-03-2015
Alexey Gritsenko
'''

import lib.tsl
import lib.tools
import lib.reader
import lib.persistent
import lib.prepare
import lib.gbrf
import lib.regression
import lib.plotter
import lib.interpretation
from lib.SimpleNamespace import SimpleNamespace

import itertools
import math
import scipy.stats
import copy
from ipyparallel import Client
import os
import numpy as np
import sklearn.metrics as metrics
import pylab
import matplotlib.pyplot as plot
import matplotlib.colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

EPS_IMPORTANCES = 1e-2

def stars(p) :
    '''
    Return a number of stars of the given p-value
    '''
    if p < 0.0001 :
        return '****'
    elif p < 0.001 :
        return '***'
    elif p < 0.01 :
        return '**'
    elif p < 0.05 :
        return '*'
    else:
        return '-'

def output_stats(seqs) :
    '''
    Output sequences statistics.
    :param seqs : sequences for which the stats should be output.
    '''
    libraries = lib.tools.get_sequence_lib_set(seqs)
    locations = ['CDS', '5UTR', '3UTR']
    virus_types = list(set(lib.persistent.virus_types.values()))
    for library in libraries :
        lib_seqs = lib.tools.get_sequences_by_lib(seqs, library)
        n_total, n_measured, n_filtered, n_filtered_nan, n_positive, n_positive_nan = get_sequences_stats(lib_seqs)
        print '%s & & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) \\tabularnewline'  % (library, n_total, float(n_total) / n_total * 100, n_measured, float(n_measured) / n_total * 100, n_filtered, float(n_filtered) / n_total * 100, n_filtered_nan, float(n_filtered_nan) / n_total * 100, n_positive, float(n_positive) / n_total * 100, n_positive_nan, float(n_positive_nan) / n_total * 100)
        n_library_total = n_total
        for location in locations :
            location_seqs = lib.tools.get_sequences_by_location(lib_seqs, location)
            n_total, n_measured, n_filtered, n_filtered_nan, n_positive, n_positive_nan = get_sequences_stats(location_seqs)
            if n_total == 0 :
                continue
            print ' & %s & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) \\tabularnewline' % (location, n_total, float(n_total) / n_library_total * 100, n_measured, float(n_measured) / n_library_total * 100, n_filtered, float(n_filtered) / n_library_total * 100, n_filtered_nan, float(n_filtered_nan) / n_library_total * 100, n_positive, float(n_positive) / n_library_total * 100, n_positive_nan, float(n_positive_nan) / n_library_total * 100)
        
        for virus_type in virus_types :
            virus_seqs = lib.tools.get_sequences_by_virus_type(lib_seqs, virus_type)
            n_total, n_measured, n_filtered, n_filtered_nan, n_positive, n_positive_nan = get_sequences_stats(virus_seqs)
            if n_total == 0 :
                continue
            print ' & %s & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) \\tabularnewline' % (virus_type, n_total, float(n_total) / n_library_total * 100, n_measured, float(n_measured) / n_library_total * 100, n_filtered, float(n_filtered) / n_library_total * 100, n_filtered_nan, float(n_filtered_nan) / n_library_total * 100, n_positive, float(n_positive) / n_library_total * 100, n_positive_nan, float(n_positive_nan) / n_library_total * 100)

def output_stats_for_groups(seqs) :
    '''
    Output sequences statistics.
    :param seqs : sequences for which the stats should be output.
    '''
    groups = lib.tools.predefined_groups.keys()
    groups.sort()
    for group in groups :
        group_seqs = lib.tools.get_group_sequences(seqs, group)
        n_total, n_measured, n_filtered, n_filtered_nan, n_positive, n_positive_nan = get_sequences_stats(group_seqs)
        print '%s & & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) & $%d$ ($%.2f$\\%%) \\tabularnewline'  % (group, n_total, float(n_total) / n_total * 100, n_measured, float(n_measured) / n_total * 100, n_filtered, float(n_filtered) / n_total * 100, n_filtered_nan, float(n_filtered_nan) / n_total * 100, n_positive, float(n_positive) / n_total * 100, n_positive_nan, float(n_positive_nan) / n_total * 100)

def get_sequences_stats(seqs) :
    '''
    Get statistics for a given library.
    :param seqs : a list of sequences for which we should get the stats.
    '''
    measured = [seq.ires_activity for seq in seqs if not math.isnan(seq.ires_activity)]
    seqs_filtered = lib.tools.filter_sequences(seqs)
    seqs_filtered_positive, _ = lib.tools.get_positive_sequences(seqs_filtered)
    seqs_filtered_nan = lib.tools.filter_sequences(seqs, remove_nan_splicing = True)
    seqs_filtered_positive_nan, _ = lib.tools.get_positive_sequences(seqs_filtered_nan)
    return len(seqs), len(measured), len(seqs_filtered), len(seqs_filtered_nan), len(seqs_filtered_positive), len(seqs_filtered_positive_nan)

def _make_kmer_set(seqs, kmer_length = 5) :
    '''
    Create a set of kmers of a given length from a given list of oligos.
    :param seqs : a list of oligos that should be used for creating the set.
    :param kmer_length : length of the kmers that should be created.
    '''
    kmers = set()
    for seq in seqs :
        seq = seq.sequence
        seq_length = len(seq)
        for i in range(seq_length - kmer_length + 1) :
            kmer = seq[i : i + kmer_length]
            kmers.add(kmer)
    return kmers

def plot_kmer_fraction(datasets = None, max_kmer_length = 10) :
    '''
    Plot the fraction of all possible kmers that is actually present in our datasets.
    :param datasets : a list of datasets that should be considered in the plot.
    :param max_kmer_length : maximum length of the kmer to be considered.
    '''
    if datasets is None :
        datasets = lib.reader.read_pickle('../results/regression/current/selected-dataset-names.out')
    f, ax = plot.subplots(1, 1)
    x = range(1, max_kmer_length + 1)
    for dataset_name in datasets :
        print '[i] Processing: %s' % dataset_name
        seqs = lib.persistent.datasets[dataset_name]
        found_fraction = np.zeros((max_kmer_length, ))
        for kmer_length in range(max_kmer_length) :
            print '    [i] Current k-mer length = %d' % (kmer_length + 1)
            kmers = _make_kmer_set(seqs, kmer_length + 1)
            found_count = len(kmers)
            total_count = 4 ** (kmer_length + 1)
            del kmers
            found_fraction[kmer_length] = float(found_count) / total_count
        ax.plot(x, found_fraction, alpha = 0.5, color = 'k', linewidth = 1.5)
        #print dataset_name, found_fraction
    ax.set_xlabel('$k$-mer length', fontsize = 13)
    ax.set_ylabel('Used fraction', fontsize = 13)
    ax.set_xlim(0, max_kmer_length + 1)
    ax.set_ylim(0, 1.05)
    f.set_size_inches(7, 5, forward = True)
    f.tight_layout()
    plot.show()

def plot_tree_scores(datasets, results = [('without-nan-splicing', '', 'log, 5k'), ('without-nan-splicing', 'sqrt', 'sqrt, 5k'), ('without-nan-splicing', 'sqrt_eran-1k', 'Eran, 1k')], kmer_length = 4, feature_type = 'independent', score = 'r2') :
    '''
    Plot GBRF regression tree train and test scores.
    :param datasets : a list of datasets that should be considered.
    :param results : a list of tuples (configuration, experiment, name) that should be considered.
    :param kmer_length : length of kmer that should be considered.
    :param feature_type : type of features that should be considered.
    :param score : type of score that should be considered.
    '''
    n_datasets = len(datasets)
    if n_datasets <= 4 :
        n_rows, n_cols = 1, n_datasets
    elif n_datasets <= 12 :
        n_rows, n_cols = 3, 4
    elif n_datasets <= 20 :
        n_rows, n_cols = 5, 4
    elif n_datasets <= 24 :
        n_rows, n_cols = 4, 6
    elif n_datasets <= 36 :
        n_rows, n_cols = 6, 6
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    colors = ['r', 'g', 'b', 'k', 'y', 'c', 'm']
    line_types = ['-', '--', '.-']
    n_colors, n_line_types = len(colors), len(line_types)
    for dataset_name in datasets :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        counter = 0
        max_x = 0
        for configuration_name, experiment, name in results :
            file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
            if experiment != '' :
                filename = '%s/%s-max_kmer_%d-%s-final.out' % (file_path, feature_type, kmer_length, experiment)
            else :
                filename = '%s/%s-max_kmer_%d-final.out' % (file_path, feature_type, kmer_length)
            if os.path.exists(filename) :
                result = lib.reader.read_pickle(filename)
                dataset_size = np.mean(result.dataset_size, dtype = np.int)
                if result.has_key('estimator_recording_frequency') :
                    frequency = result.estimator_recording_frequency
                else :
                    frequency = 100
                train_score = copy.deepcopy(result.train_score[score])
                test_score = copy.deepcopy(result.test_score[score])
                n_folds = train_score.shape[0]
                for i in range(n_folds) :
                    if result.has_key('used_n_estimators') :
                        used_n_estimators = result.used_n_estimators[i]
                    else :
                        used_n_estimators = train_score.shape[1] * frequency
                    used_n_estimators = int(used_n_estimators / frequency)
                    train_score[i, used_n_estimators: ] = float('nan')
                    test_score[i, used_n_estimators: ] = float('nan')
                mean_train = np.nanmean(train_score, axis = 0)
                std_train = np.nanstd(train_score, axis = 0)
                mean_test = np.nanmean(test_score, axis = 0)
                std_test = np.nanstd(test_score, axis = 0)
                x = range(frequency, len(mean_test) * frequency + 1, frequency)
                max_x = max(max_x, np.max(x))
                #cur.errorbar(x, mean_score, yerr = std_score, label = '%s' % name, linewidth = 1.5)
                line_type_i = counter / n_colors
                color_i = counter % n_colors
                cur.plot(x, mean_train, label = '%s train' % name, linewidth = 1.5, color = colors[color_i], linestyle = line_types[line_type_i])
                counter += 1
                line_type_i = counter / n_colors
                color_i = counter % n_colors
                cur.plot(x, mean_test, label = '%s test' % name, linewidth = 1.5, color = colors[color_i], linestyle = line_types[line_type_i])
                counter += 1
        cur.set_xlim(0, max_x + 1)
        display_name = lib.plotter.get_dataset_display_name(dataset_name)
        cur.set_title('%s : %s' % (display_name, dataset_size), fontsize = 10)
        if row == 0 and col == 0 :
            cur.legend(ncol = 3, prop = {'size' : 8}, loc = 'center')
        if col == 0 :
            cur.set_ylabel('$R^2$', fontsize = 13)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('$k$-mer length', fontsize = 13)
        else :
            cur.set_xticklabels([])
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    if n_datasets <= 3 :
        f.set_size_inches(18, 5, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)

    f.tight_layout()
    plot.show()

_results_without_nan_splicing =[('without-nan-splicing', '', 'log, 5k'), ('without-nan-splicing', 'sqrt', 'sqrt, 5k'), ('without-nan-splicing', 'sqrt-1k', 'sqrt, 1k'), ('without-nan-splicing', 'sqrt-0.5k', 'sqrt, 0.5k'), ('without-nan-splicing', 'sqrt_eran-1k', 'Eran, 1k'), ('without-nan-splicing', 'sqrt_eran', 'Eran, 0.5k'), ('without-nan-splicing', 'sqrt-intercept', 'sqrt, 5k, intercept'), ('without-nan-splicing', 'sqrt-n_estimators', 'sqrt, 0.5k, N estimators'), ('without-nan-splicing', 'sqrt-n_estimators-intercept', 'sqrt, 0.5k, N estimators, intercept')]

_results_without_nan_splicing_trimmed_activity =[('without-nan-splicing-trimmed_activity', 'sqrt-n_estimators', 'sqrt, 0.5k, N estimators, trim'), ('without-nan-splicing-trimmed_activity', 'sqrt-3k', 'sqrt, 3k, trim'), ('without-nan-splicing-trimmed_activity', 'sqrt-3k-n_estimators', 'sqrt, 3k, N estimators, trim'), ('without-nan-splicing-trimmed_activity', 'sqrt_eran-3k', 'Eran, 3k, trim'), ('without-nan-splicing-trimmed_activity', 'sqrt_eran-3k-n_estimators', 'Eran, 3k, N estimators, trim'), ('without-nan-splicing-trimmed_activity', 'log-5k', 'log, 5k, trim'), ('without-nan-splicing-trimmed_activity', 'sqrt_eran_refined-3k', 'Eran R, 3k, trim'), ('without-nan-splicing-trimmed_activity', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, trim')]

_results_without_nan_splicing_trimmed_activity_thresholds = [('without-nan-splicing-trimmed_activity', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, trim'), ('without-nan-splicing-trimmed_activity0.95-positive206', 'sqrt_eran_refined2-3k-equalize', 'Eran R2, 3k, Equalize, trim 0.95, +206.29'), ('without-nan-splicing-trimmed_activity0.95-positive206', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, trim 0.95, +206.29'), ('without-nan-strict-trimed_activity0.995-positive206', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, Strict, trim 0.995, +206.29'), ('without-nan-splicing-trimmed_activity0.995-positive206', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, trim 0.995, +206.29'), ('without-nan-splicing-trimmed_activity0.995-positive206', 'sqrt_eran_refined2-3k-equalize', 'Eran R2, 3k, Equalize, trim 0.995, +206.29')]

_results_without_nan_splicing_trimmed_activity_final = [('without-nan-splicing-trimmed_activity0.995-positive206', 'sqrt_eran_refined2-1k-all_orf-minimum_fraction_0.1', 'Eran R2, 1k, trim 0.995, +206.29'), ('without-nan-splicing-trimmed_activity0.995-positive206', 'sqrt_eran_refined2-2k-all_orf-minimum_fraction_0.1', 'Eran R2, 1k-2k, trim 0.995, +206.29'), ('without-nan-splicing-trimmed_activity0.995-positive206', 'sqrt_eran_refined2-3k-all_orf-minimum_fraction_0.1', 'Eran R2, 1k-3k, trim 0.995, +206.29'), ('without-nan-splicing-trimmed_activity0.995-positive206', 'sqrt_eran_refined2-5k-all_orf-minimum_fraction_0.1', 'Eran R2, 1k-5k, trim 0.995, +206.29')]

def plot_outer_cv_results(datasets, results = _results_without_nan_splicing,  max_kmer_length = 5, feature_type = 'independent') :
    '''
    Plot regression results for multiple datasets.
    :param datasets : a list of datasets that should be considered.
    :param results : a list of tuples (configuration, experiment, name) that should be considered.
    :param max_kmer_length : maximum length of the k-mers to be considered.
    :param feature_type : type of features that should be considered.
    '''
    n_datasets = len(datasets)
    if n_datasets <= 4 :
        n_rows, n_cols = 1, n_datasets
    elif n_datasets <= 6 :
        n_rows, n_cols = 2, 3
    elif n_datasets <= 12 :
        n_rows, n_cols = 3, 4
    elif n_datasets <= 20 :
        n_rows, n_cols = 5, 4
    elif n_datasets <= 24 :
        n_rows, n_cols = 4, 6
    elif n_datasets <= 36 :
        n_rows, n_cols = 6, 6
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(1, max_kmer_length + 1)
    colors = ['r', 'g', 'b', 'k', 'y', 'c', 'm']
    line_types = ['-', '--', ':']
    n_colors, n_line_types = len(colors), len(line_types)
    for dataset_name in datasets :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        counter = 0
        dataset_size = float('NaN')
        for configuration_name, experiment, name in results :
            mean_score = np.zeros((max_kmer_length, )) * float('nan')
            std_score = np.zeros((max_kmer_length, )) * float('nan')
            for kmer_length in range(1, max_kmer_length + 1) :
                file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
                if experiment != '' :
                    filename = '%s/%s-max_kmer_%d-%s-final.out' % (file_path, feature_type, kmer_length, experiment)
                else :
                    filename = '%s/%s-max_kmer_%d-final.out' % (file_path, feature_type, kmer_length)
                if os.path.exists(filename) :
                    result = lib.reader.read_pickle(filename)
                    dataset_size = np.mean(result.dataset_size, dtype = np.int)
                    select_score = 'r2'
                    mean_score[kmer_length - 1] = np.mean(result.test_score[select_score][:, -1])
                    std_score[kmer_length - 1] = np.std(result.test_score[select_score][:, -1])
                    #mean_score[kmer_length - 1] = result.overall_test_score[select_score]
                else :
                    print filename
            #cur.errorbar(x, mean_score, yerr = std_score, label = '%s' % name, linewidth = 1.5)
            line_type_i = counter / n_colors
            color_i = counter % n_colors
            cur.plot(x, mean_score, label = '%s' % name, linewidth = 1.5, color = colors[color_i], linestyle = line_types[line_type_i])
            counter += 1
        cur.set_xlim(0, kmer_length + 1)
        display_name = lib.plotter.get_dataset_display_name(dataset_name)
        cur.set_title('%s : %s' % (display_name, dataset_size), fontsize = 10)
        if row == 0 and col == 0 :
            cur.legend(ncol = 2, prop = {'size' : 8}, loc = 'lower center')
        if col == 0 :
            cur.set_ylabel('$R^2$', fontsize = 13)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('$k$-mer length', fontsize = 13)
        else :
            cur.set_xticklabels([])
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    if n_datasets <= 3 :
        f.set_size_inches(18, 5, forward = True)
    elif n_datasets <= 6 :
        f.set_size_inches(18, 10, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)

    f.tight_layout()
    plot.show()

def plot_grid_search_preferences(datasets, results = ('without-nan-splicing-trimmed_activity', 'sqrt-3k'), min_kmer_length = 1, max_kmer_length = 5, feature_type = 'independent', aggregator = 'mean') :
    '''
    Plot preferences of results from grid search.
    :param datasets : a list of datasets that should be considered when choosing best parameters.
    :param results : a tuple of (configuration, experiment).
    :param min_kmer_length : minimum length of a k-mer.
    :param max_kmer_length : maximum length of a k-mer.
    :param aggregator : the aggregator to use when summarizing results for a particular setting.
    '''
    width = 1.0
    colors = ['r', 'b', 'g', 'y', 'b']
    n_datasets = len(datasets)
    n_kmers = max_kmer_length - min_kmer_length + 1
    n_rows = n_datasets
    n_cols = 4
    f, ax = plot.subplots(n_rows, n_cols, sharex = 'col', sharey = 'row')
    configuration_name, experiment = results
    for row, dataset_name in enumerate(datasets) :
        for kmer_length in range(min_kmer_length, max_kmer_length + 1) :
            file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
            if experiment != '' :
                filename = '%s/%s-max_kmer_%d-%s.out' % (file_path, feature_type, kmer_length, experiment)
            else :
                filename = '%s/%s-max_kmer_%d.out' % (file_path, feature_type, kmer_length)
            if not os.path.exists(filename) :
                raise Exception('Results file not found: %s' % filename)
            result_folds = lib.reader.read_pickle(filename)
            n_folds = len(result_folds)
            n_learning_rates, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample = len(result_folds[0].setting_learning_rate), len(result_folds[0].setting_max_depth), len(result_folds[0].setting_min_samples_leaf), len(result_folds[0].setting_max_features), len(result_folds[0].setting_subsample)
            score_acc = np.zeros((n_folds, n_learning_rates, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample), dtype = np.float16)
            for fold, result in enumerate(result_folds) :
                setting_learning_rate = result.setting_learning_rate
                setting_max_depth = result.setting_max_depth
                setting_min_samples_leaf = result.setting_min_samples_leaf
                setting_subsample = result.setting_subsample
                score = result.test_score['r2']
                score = score[:, :, :, :, :, :, -1]
                score = np.mean(score, axis = 0)
                score_acc[fold, :, :, :, :, :] = score
            if aggregator == 'mean' :
                score_acc = np.mean(score_acc, axis = 0)
            elif aggregator == 'max' :
                score_acc = np.max(score_acc, axis = 0)

            cur = ax[row, 0] if n_rows > 1 else ax[0]
            setting_scores = np.zeros((n_learning_rates, ))
            x = np.array(range(n_learning_rates)) * (n_kmers + 1) * width + width / 2.0
            for i in range(n_learning_rates) :
                setting_scores[i] = np.mean(score_acc[i, :, :, :, :])
            cur.bar(x + (kmer_length - min_kmer_length) * width, setting_scores, width, label = '$k=%d$' % kmer_length, alpha = 0.45, color = colors[kmer_length - min_kmer_length])
            cur.set_xticks(x + n_kmers / 2.0 * width)
            cur.set_xlim(0, np.max(x) + width / 2.0 + n_kmers * width)
            cur.set_xticklabels(setting_learning_rate)
            display_name = lib.plotter.get_dataset_display_name(dataset_name)
            cur.set_ylabel('$R^2$\n%s' % display_name)
            if row == 0 :
                cur.set_title('Learning rate')
                cur.legend(ncol = 5, prop = {'size' : 8}, loc = 'lower center')

            cur = ax[row, 1] if n_rows > 1 else ax[1]
            setting_scores = np.zeros((n_max_depth, ))
            x = np.array(range(n_max_depth)) * (n_kmers + 1) * width + width / 2.0
            for i in range(n_max_depth) :
                setting_scores[i] = np.mean(score_acc[:, i, :, :, :])
            cur.bar(x + (kmer_length - min_kmer_length) * width, setting_scores, width, label = '$k=%d$' % kmer_length, alpha = 0.45, color = colors[kmer_length - min_kmer_length])
            cur.set_xticks(x + n_kmers / 2.0 * width)
            cur.set_xlim(0, np.max(x) + width / 2.0 + n_kmers * width)
            cur.set_xticklabels(setting_max_depth)
            if row == 0 :
                cur.set_title('Max depth')

            cur = ax[row, 2] if n_rows > 1 else ax[2]
            setting_scores = np.zeros((n_min_samples_leaf, ))
            x = np.array(range(n_min_samples_leaf)) * (n_kmers + 1) * width + width / 2.0
            for i in range(n_min_samples_leaf) :
                setting_scores[i] = np.mean(score_acc[:, :, i, :, :])
            cur.bar(x + (kmer_length - min_kmer_length) * width, setting_scores, width, label = '$k=%d$' % kmer_length, alpha = 0.45, color = colors[kmer_length - min_kmer_length])
            cur.set_xticks(x + n_kmers / 2.0 * width)
            cur.set_xlim(0, np.max(x) + width / 2.0 + n_kmers * width)
            cur.set_xticklabels(setting_min_samples_leaf)
            if row == 0 :
                cur.set_title('Min samples leaf')

            cur = ax[row, 3] if n_rows > 1 else ax[3]
            setting_scores = np.zeros((n_subsample, ))
            x = np.array(range(n_subsample)) * (n_kmers + 1) * width + width / 2.0
            for i in range(n_subsample) :
                setting_scores[i] = np.mean(score_acc[:, :, :, :, i])
            cur.bar(x + (kmer_length - min_kmer_length) * width, setting_scores, width, label = '$k=%d$' % kmer_length, alpha = 0.45, color = colors[kmer_length - min_kmer_length])
            cur.set_xticks(x + n_kmers / 2.0 * width)
            cur.set_xlim(0, np.max(x) + width / 2.0 + n_kmers * width)
            cur.set_xticklabels(setting_subsample)
            if row == 0 :
                cur.set_title('Subsample')

    if n_datasets <= 3 :
        f.set_size_inches(18, 10, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)
    f.tight_layout()
    plot.show()

def plot_max_features_outer_cv_results(datasets, results = ('without-nan-splicing-trimmed_activity', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, trimmed'), max_features_settings = ['log2', 'sqrt', 10, 20, 30, 40, 50, 60, 70, 80, 90, 0.001, 0.002, 0.004, 0.008, 0.016, 0.032], feature_types = [('kmers', 'Count') , ('independent-windows', 'Count windows (20, 10)')], kmer_length = 4) :
    '''
    Plot regression results for multiple datasets and for various window sizes.
    :param datasets : a list of datasets that should be considered.
    :param results : a tuple (configuration, experiment, name) that should be considered.
    :param max_features_settings : a list of max feature settings that should be considered.
    :param feature_types : a list of (feature type, feature type name) tuples that 
    should be considered.
    :param kmer_length : length of the k-mer that should be considered.
    '''
    width = 1.0
    colors = ['r', 'b', 'g', 'y', 'b']
    n_datasets = len(datasets)
    if n_datasets <= 4 :
        n_rows, n_cols = 1, n_datasets
    elif n_datasets <= 12 :
        n_rows, n_cols = 3, 4
    elif n_datasets <= 20 :
        n_rows, n_cols = 5, 4
    elif n_datasets <= 24 :
        n_rows, n_cols = 4, 6
    elif n_datasets <= 36 :
        n_rows, n_cols = 6, 6
    n_max_features = len(max_features_settings)
    n_feature_types = len(feature_types)
    x = np.array(range(n_max_features)) * width * (n_feature_types + 1.5)
    f, ax = plot.subplots(n_rows, n_cols, sharex = True)
    row, col = 0, 0
    for dataset_name in datasets :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        configuration_name, experiment, name = results
        dataset_size = float('nan')
        counter = 0
        for feature_type, feature_type_name in feature_types :
            mean_score = np.zeros((n_max_features, )) * float('nan')
            std_score = np.zeros((n_max_features, )) * float('nan')
            for i, max_features in enumerate(max_features_settings) :
                file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
                if experiment != '' :
                    filename = '%s/%s-max_kmer_%d-%s-max_features_%s-final.out' % (file_path, feature_type, kmer_length, experiment, str(max_features))
                else :
                    filename = '%s/%s-max_kmer_%d-max_features_%s-final.out' % (file_path, feature_type, kmer_length, str(max_features))
                if os.path.exists(filename) :
                    result = lib.reader.read_pickle(filename)
                    dataset_sizes = result.dataset_size
                    dataset_size = np.mean(dataset_sizes, dtype = np.int)
                    mean_score[i] = np.mean(result.test_score['r2'][:, -1])
                    std_score[i] = np.std(result.test_score['r2'][:, -1])
            cur.bar(x + counter * width, mean_score, width, alpha = 0.45, color = colors[counter], label = feature_type_name)
            cur.set_xticks(x + n_feature_types / 2.0 * width)
            cur.set_xlim(0, np.max(x) + width / 2.0 + n_feature_types * width)
            cur.set_xticklabels(max_features_settings, rotation = 'vertical')
            display_name = lib.plotter.get_dataset_display_name(dataset_name)
            cur.set_title('%s : %s' % (display_name, dataset_size), fontsize = 10)
            cur.set_ylabel('$R^2$')
            if row == 0 and col == 0 :
                cur.legend(ncol = 3, prop = {'size' : 8}, loc = 'lower center')
            if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
                cur.set_xlabel('Max. features setting', fontsize = 13)
            else :
                cur.set_xticklabels([])
            pass
            counter += 1
        col += 1
        if col >= n_cols :
            row += 1
            col = 0

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    if n_datasets <= 3 :
        f.set_size_inches(18, 5, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)

    f.tight_layout()
    plot.show()

def plot_minimum_fraction_outer_cv_results(datasets, results = ('without-nan-splicing-trimmed_activity', 'sqrt_eran_refined2-3k', 'Eran R2, 3k, trimmed'), miminum_fraction_setting = [0.0, 0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064, 0.128, 0.256, 0.512],  max_kmer_length = 5, feature_type = 'independent') :
    '''
    Plot regression results for multiple datasets.
    :param datasets : a list of datasets that should be considered.
    :param results : a tuple (configuration, experiment, name) that should be considered.
    :param minimum_fraction_setting : a list of minimum fraction settings that should be considered.
    :param max_kmer_length : maximum length of the k-mers to be considered.
    :param feature_type : type of features that should be considered.
    '''
    n_datasets = len(datasets)
    if n_datasets <= 4 :
        n_rows, n_cols = 1, n_datasets
    elif n_datasets <= 12 :
        n_rows, n_cols = 3, 4
    elif n_datasets <= 20 :
        n_rows, n_cols = 5, 4
    elif n_datasets <= 24 :
        n_rows, n_cols = 4, 6
    elif n_datasets <= 36 :
        n_rows, n_cols = 6, 6
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(1, max_kmer_length + 1)
    colors = ['r', 'g', 'b', 'k', 'y', 'c', 'm']
    line_types = ['-', '--', ':']
    n_colors, n_line_types = len(colors), len(line_types)
    configuration_name, experiment, name = results
    for dataset_name in datasets :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        counter = 0
        for minimum_fraction in miminum_fraction_setting :
            mean_score = np.zeros((max_kmer_length, )) * float('nan')
            std_score = np.zeros((max_kmer_length, )) * float('nan')
            for kmer_length in range(1, max_kmer_length + 1) :
                file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
                if experiment != '' :
                    filename = '%s/%s-max_kmer_%d-%s-minimum_fraction_%s-final.out' % (file_path, feature_type, kmer_length, experiment, str(minimum_fraction))
                else :
                    filename = '%s/%s-max_kmer_%d-minimum_fraction_%s-final.out' % (file_path, feature_type, kmer_length, str(minimum_fraction))
                if os.path.exists(filename) :
                    result = lib.reader.read_pickle(filename)
                    dataset_size = np.mean(result.dataset_size, dtype = np.int)
                    mean_score[kmer_length - 1] = np.mean(result.test_score['r2'])
                    std_score[kmer_length - 1] = np.std(result.test_score['r2'])
            #cur.errorbar(x, mean_score, yerr = std_score, label = '%s' % name, linewidth = 1.5)
            line_type_i = counter / n_colors
            color_i = counter % n_colors
            cur.plot(x, mean_score, label = 'min frac %s' % str(minimum_fraction), linewidth = 1.5, color = colors[color_i], linestyle = line_types[line_type_i])
            counter += 1
        cur.set_xlim(0, kmer_length + 1)
        display_name = lib.plotter.get_dataset_display_name(dataset_name)
        cur.set_title('%s : %s' % (display_name, dataset_size), fontsize = 10)
        if row == 0 and col == 0 :
            cur.legend(ncol = 3, prop = {'size' : 8}, loc = 'lower center')
        if col == 0 :
            cur.set_ylabel('$R^2$', fontsize = 13)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('$k$-mer length', fontsize = 13)
        else :
            cur.set_xticklabels([])
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    f.suptitle(name)
    if n_datasets <= 3 :
        f.set_size_inches(18, 5, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)

    f.tight_layout()
    f.subplots_adjust(top = 0.9)
    plot.show()

def plot_variance_from_outer_cv_folds(inner_results, can_choose_n_estimators = False, use_aux = False, scoring = 'r2', cv_folds = None) :
    '''
    Compute Gradient-Boosted Regression Trees based on inner-fold CV results.
    :param inner_results : inner CV results.
    :param can_choose_n_estimators : a boolean flag determining whether the regressor is allowed to use its own number of estimators.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param scoring : scoring function to be used.
    :param cv_folds : CV folds to be used.
    '''
    dataset_name = inner_results[0].dataset_name
    features = inner_results[0].features
    learn_intercept = inner_results[0].learn_intercept
    log2 = inner_results[0].log2
    n_estimators = inner_results[0].n_estimators
    estimator_recording_frequency = inner_results[0].estimator_recording_frequency
    
    if isinstance(cv_folds, SimpleNamespace) :
        cv_folds = cv_folds.outer_folds
        n_folds = len(cv_folds)
    elif isinstance(cv_folds,  list) :
        n_folds = len(cv_folds)
    elif isinstance(cv_folds, int) :
        n_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        ires_values = np.array([seq.ires_activity for seq in dataset])
        cv_folds = crossvalidation.stratified_KFold(ires_values, n_folds)
    else :
        raise Exception('Unable to parse CV folds')
        cv_folds = None

    n_rows, n_cols = 3, 4
    f, ax = plot.subplots(n_rows, n_cols, sharey = True)
    row, col = 0, 0
    
    n_recorded_estimators = int(n_estimators / estimator_recording_frequency)
    if n_estimators % lib.gbrf.ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    all_true_y, all_predicted_y = [], []
    print '[i] Processing dataset: %s' % dataset_name
    for fold in range(n_folds) :
        print '   [i] Calculating for fold %d' % (fold + 1)
        inner_result = inner_results[fold]
        scores = np.mean(inner_result.test_score['r2'], axis = 0)
        if can_choose_n_estimators :
            index = np.nanargmax(scores)
            i, j, k, l, m, n = np.unravel_index(index, scores.shape)
        else :
            scores = scores[:, :, :, :, :, -1]
            index = np.nanargmax(scores)
            i, j, k, l, m = np.unravel_index(index, scores.shape)
            n = n_recorded_estimators - 1
        learning_rate = inner_result.setting_learning_rate[i]
        max_depth = inner_result.setting_max_depth[j]
        min_samples_leaf = inner_result.setting_min_samples_leaf[k]
        max_features = inner_result.setting_max_features[l]
        subsample = inner_result.setting_subsample[m]
        best_n_estimators = (n + 1) * estimator_recording_frequency
        train_index, test_index = cv_folds[fold]
        if use_aux :
            Xs = lib.regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = fold)
        else :
            Xs = lib.regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
        X, y = lib.regression.build_regression_matrix(Xs, dataset_name, log2 = log2)
        del Xs
        fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = lib.gbrf.train_gbrf_cv_fold(X, y, train_index, test_index, learn_intercept = learn_intercept, n_estimators = best_n_estimators, min_samples_leaf =      min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, max_depth = max_depth, scoring = [scoring])
        n_features = X.shape[1]
        del X, y
        all_true_y.extend(fold_true_y)
        all_predicted_y.extend(fold_predicted_y)
        
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        cur.scatter(fold_true_y, fold_predicted_y, alpha = 0.45, color = 'k')
        cur.set_title('N. features: %d\nDataset size: %d (%d)\nScore: %.2f (%.2f)' % (n_features, len(train_index), len(test_index), fold_test_score[scoring][-1], fold_train_score[scoring][-1]), fontsize = 10)
        #if row == 0 and col == 0 :
        #    cur.legend(ncol = 3, prop = {'size' : 8}, loc = 'lower center')
        if col == 0 :
            cur.set_ylabel('Predicted activity', fontsize = 13)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_folds :
            cur.set_xlabel('True activity', fontsize = 13)
        else :
            cur.set_xticklabels([])
        cur.text(0.01, 0.1, 'Rate: %s\nmin. samp.: %d\nmax. feat.: %s\nsubsamp: %s\nN est.: %d' % (str(learning_rate), min_samples_leaf, str(max_features), str(subsample), best_n_estimators), fontsize = 8, transform = cur.transAxes)
        col += 1
        if col >= n_cols :
            row += 1
            col = 0

    shift = 0.1
    min_x, max_x = np.min(all_true_y) - shift, np.max(all_true_y) + shift
    min_y, max_y = np.min(all_predicted_y) - shift, np.max(all_predicted_y) + shift
    min_xy = min(min_x, min_y)
    max_xy = max(max_x, max_y)
    row, col = 0, 0
    for fold in range(n_folds) :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        #cur.set_xlim(min_x, max_x)
        #cur.set_ylim(min_y, max_y)
        cur.set_xlim(min_xy, max_xy)
        cur.set_ylim(min_xy, max_xy)
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    if scoring == 'r2' :
        overall_test_score = metrics.r2_score(all_true_y, all_predicted_y)
    elif scoring == 'pearson' :
        coef_r, pvalue = scipy.stats.pearsonr(all_true_y, all_predicted_y)
        overall_test_score = coef_r
    elif scoring == 'spearman' :
        coef_r, pvalue = scipy.stats.spearmanr(all_true_y, all_predicted_y)
        overall_test_score = coef_r
    else :
        raise Exception('Unknown scoring function requested.')

    display_name = lib.plotter.get_dataset_display_name(dataset_name)
    f.suptitle(display_name)
    size = 3.5
    f.set_size_inches(n_cols * size * 0.9, n_rows * size, forward = True)

    f.tight_layout()
    f.subplots_adjust(top = 0.90)
    plot.show()

def plot_feature_importance_histogram(datasets, results = ('without-nan-splicing-trimmed_activity0.995-positive206', 'independent_independent-windows_positional-windows-max_kmer_3-sqrt_eran_refined2-1k-all_orf-minimum_fraction_0.1', 'Eran R2, 1k, ORF all, min. frac. 0.1, $k=3$')) :
    '''
    Plot feature importance histogram for given datasets.
    :param datasets : a list of datasets for which the feature importance histogram should be plotted.
    :param results : definition of the results that should be used to make this plot.
    '''
    n_bins = 80
    n_datasets = len(datasets)
    if n_datasets <= 4 :
        n_rows, n_cols = 1, n_datasets
    elif n_datasets <= 6 :
        n_rows, n_cols = 2, 3
    elif n_datasets <= 12 :
        n_rows, n_cols = 3, 4
    elif n_datasets <= 20 :
        n_rows, n_cols = 5, 4
    elif n_datasets <= 24 :
        n_rows, n_cols = 4, 6
    elif n_datasets <= 36 :
        n_rows, n_cols = 6, 6
    f, ax = plot.subplots(n_rows, n_cols, sharex = True)
    row, col = 0, 0
    for dataset_name in datasets :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        configuration_name, experiment, name = results
        file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
        filename = '%s/%s-feature_interpretation.out' % (file_path, experiment)
        dataset_size = None
        total_features, total_unique_features = None, None
        if os.path.exists(filename) :
            #print 'Reading: %s' % filename
            result = lib.reader.read_pickle(filename)
            importances = lib.tools.get_feature_importances(result)
            unique_features = set([key.split('-')[1] for key in importances])
            total_features = len(importances)
            total_unique_features = len(unique_features)
            n_folds = len(result.feature_names)
            for feature in importances :
                cur_importance = copy.deepcopy(importances[feature])
                count = len(cur_importance)
                for i in range(count, n_folds) :
                    cur_importance.append(0.0)
                cur_importance = np.array(cur_importance)
                cur_importance[np.isnan(cur_importance)] = 0.0
                importances[feature] = np.mean(cur_importance)
                if math.isnan(importances[feature]) :
                    print dataset_name, cur_importance
            dataset_size = np.mean(result.dataset_size, dtype = np.int)
            importances = importances.values()
            n, bins_plotted, patches = cur.hist(importances, n_bins, normed = False, histtype = 'stepfilled')
            pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
        else :
            print 'Missing: %s' % filename
        display_name = lib.plotter.get_dataset_display_name(dataset_name)
        cur.set_title('%s : %s, %d/%d feat' % (display_name, dataset_size, total_unique_features, total_features), fontsize = 10)
        cur.set_xlim(0, 1.0)
        
        if col == 0 :
            cur.set_ylabel('Feature count', fontsize = 13)
        
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    if n_datasets <= 3 :
        f.set_size_inches(18, 5, forward = True)
    elif n_datasets <= 6 :
        f.set_size_inches(18, 10, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)
    
    f.tight_layout()
    plot.show()

def make_window_feature_plots(result, type = 'count', oligo_length = 174, min_importance = 0.01, min_folds = 10, min_windows = 4) :
    '''
    Make window feature plot for a given result.
    :param result : result for which window feature importances should be plotted.
    :param type : type of features to be plotted (count or presence).
    :param oligo_length : length of the oligo.
    :param min_importance : minimum importance that should be considered. Only window with this importance are counted as passing selection threshold.
    :param min_folds : minimum number of folds. Only windows that are present in at least so many CV folds are counted as passing selection threshold.
    :param min_windows : minimum number of windows passing the selection thresholds required to select the k-mer feature.
    '''
    n_folds = len(result.feature_names)
    presence_window, count_window, count = lib.tools.process_importances(result)
    if type == 'count' :
        importances = count_window
    elif type == 'presence' :
        importances = presence_window
    selected_kmers = []
    for kmer in importances :
        n_passed_windows = 0
        for start, stop in importances[kmer] :
            window = (start, stop)
            window_n_folds = len(importances[kmer][window])
            average_importance = np.sum(importances[kmer][window]) / n_folds
            if average_importance >= min_importance and window_n_folds >= min_folds :
                n_passed_windows += 1
        if n_passed_windows >= min_windows :
            selected_kmers.append(kmer)
    print '[i] K-mers passing threshold: %d' % len(selected_kmers)
    n_kmers = len(selected_kmers)
    print n_kmers
    if n_kmers <= 8 :
        n_rows, n_cols = 2, 4
    elif n_kmers <= 16 :
        n_rows, n_cols = 4, 4
    elif n_kmers <= 20 :
        n_rows, n_cols = 5, 4
    elif n_kmers <= 32 :
        n_rows, n_cols = 8, 4
    elif n_kmers <= 42 :
        n_rows, n_cols = 7, 6
    elif n_kmers <= 45 :
        n_rows, n_cols = 7, 7
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(1, oligo_length + 1)
    for kmer in selected_kmers :
        print '    [i] Processing k-mer: %s' % kmer
        oligo = np.zeros((oligo_length, ))
        covered = np.zeros((oligo_length, ), dtype = np.int)
        for window, window_importances in importances[kmer].iteritems() :
            start, stop = window
            window_importances = copy.deepcopy(window_importances)
            for i in range(len(window_importances), n_folds) :
                window_importances.append(0.0)
            window_importances = np.array(window_importances)
            window_importances[np.isnan(window_importances)] = 0.0
            oligo[start : stop] += np.mean(window_importances)
            covered[start : stop] += 1
        covered[covered == 0] = 1
        oligo /= covered
       
        cur = ax[row, col]
        cur.plot(x, oligo)
        cur.set_xlim(1, oligo_length)
        cur.set_ylim(0, 1.0)
        cur.set_title('%s %s' % (kmer, type))
        if col == 0 :
            cur.set_ylabel('Importance', fontsize = 13)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_kmers :
            cur.set_xlabel('Location', fontsize = 13)
            locs = [int(loc) for loc in cur.get_xticks()]
            cur.set_xticklabels(locs, rotation = 90)
        else :
            cur.set_xticklabels([])
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(18, 14, forward = True)
    f.tight_layout()
    plot.show()

def plot_final_predictor_roc_performance(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["All sequence", "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], kmer_sizes = [4, 4, 4, 4, 4, 4], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 1, 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), metric = 'roc', plot_gbrf=True, save_filename=None):
    '''
    Plot ROC and PR curves for the selected results.
    :param [str] dataset_names: a list of dataset names for which performance should be plotted.
    :param [str] dataset_labels: a list of dataset labels to use for the datasets.
    :param [int] kmer_sizes: selected k-mer lengths for each of the datasets.
    :param (str, str, str) results: a description of the results that should be plotted (configuration, features, experiment)
    :param str metric: metric ('roc' or 'pr') to plot.
    :param bool plot_gbrf: should we plot gbrf or rf?
    '''
    n_datasets = len(dataset_names)
    if n_datasets <= 6:
        n_rows, n_cols = 2, 3
    else :
        n_rows, n_cols = 3, 3
    f, ax = plot.subplots(n_rows, n_cols)
   
    def put_text(ax, x, y, text, dest=0.5, pos='left'):
        delta_x, delta_y = 0.15, 0.025
        n_points, ind = len(x), 0
        dist = +float('inf')
        for i in xrange(n_points - 1):
            j = i + 1
            di, dj = abs(dest - x[i]), abs(dest - x[j])
            if di + dj < dist:
                ind = i
                dist = di + dj
        i, j = ind, ind + 1
        di, dj = abs(dest - x[i]), abs(dest - x[j])
        if di + dj == 0:
            di, dj = 1.0, 1.0
        wi, wj = di / (di + dj), dj / (di + dj)
        cur_x, cur_y = x[i] * wi + x[j] * wj, y[i] * wi + y[j] * wj
        va, ha = 'top', 'right'
        if 'left' in pos:
            cur_x -= delta_x
            ha = 'right'
        elif 'right' in pos:
            cur_x += delta_x
            ha = 'left'
        if 'bottom' in pos:
            cur_y -= delta_y
            va = 'top'
        elif 'top' in pos:
            cur_y += delta_y
            va = 'bottom'
        ax.text(cur_x, cur_y, text, fontsize=10, va=va, ha=ha)

    fg_color, bg_color, alpha = 'b', 'r', 0.45
    row, col = 0, 0
    for dataset_name, dataset_label, kmer_length in zip(dataset_names, dataset_labels, kmer_sizes) :
        print dataset_name
        if n_rows == 1:
            cur = ax[col]
        else :
            cur = ax[row, col]
        configuration_name, feature_names, min_kmer_length, experiment = results

        x, y = np.zeros((10, )) * float('nan'), np.zeros((10, )) * float('nan')
        random_x, random_y = np.zeros((10, )) * float('nan'), np.zeros((10, )) * float('nan')
        folder = 'gbrf' if plot_gbrf else 'svr'
        file_path = '../results/regression/%s/%s/%s' % (configuration_name, dataset_name, folder)
        filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename) :
            result = lib.reader.read_pickle(filename)
        else :
            print '[!] Missing: %s' % filename

        filename = '../results/regression/%s/%s/crossvalidation_folds.out' % (configuration_name, dataset_name)
        if os.path.exists(filename) :
            cv_folds = lib.reader.read_pickle(filename)
        else :
            print '[!] Missing: %s' % filename

        true_values, predicted_values = lib.regression.make_cv_predictions(result, cv_folds.outer_folds)
        graph_label, x_label, y_label = '', '', ''
        pos_auc, pos_random_auc = 'top left', 'bottom right'
        if metric == 'roc':
            x, y = lib.regression.get_roc_curve(true_values, predicted_values)
            random_x, random_y = [0, 1], [0, 1]
            graph_label, random_graph_label, x_label, y_label = 'Model ROC', 'Random ROC', 'False Positive Rate', 'True Positive Rate'
            pos_auc, pos_random_auc = 'top left', 'bottom right'
        elif metric == 'pr':
            y, x = lib.regression.get_pr_curve(true_values, predicted_values)
            random_y, random_x = lib.regression.get_random_pr_curve(dataset_name)
            graph_label, random_graph_label, x_label, y_label = 'Model PR-curve', 'Random PR-curve', 'Recall', 'Precision'
            pos_auc, pos_random_auc = 'top right', 'bottom left'
        else:
            print '[!] Unknown metric: %s' % metric
        
        cur.plot(x, y, linewidth=1.5, color=fg_color, alpha=alpha, label=graph_label)
        cur.plot(random_x, random_y, linewidth=1.5, color=bg_color, alpha=alpha, linestyle='--', label=random_graph_label)

        auc, random_auc = lib.regression.get_auc(x, y), lib.regression.get_auc(random_x, random_y)
        put_text(cur, x, y, 'AUC %.2f' % auc, pos=pos_auc)
        put_text(cur, random_x, random_y, 'AUC %.2f' % random_auc, pos=pos_random_auc)
        
        cur.locator_params(nbins=6, axis='y')
        cur.set_title(dataset_label, fontweight='bold', fontsize=12)
        
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('bottom')

        cur.set_xlim(0, 1)
        cur.set_ylim(0, 1)
        
        if col == 0 and row == 0:
            cur.legend(prop = {'size' : 10}, loc='lower right')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel(x_label, fontsize=13)
        else:
            cur.set_xticklabels([])
        if col == 0:
            cur.set_ylabel(y_label, fontsize=13)
        col += 1
        if col >= n_cols:
            col = 0
            row += 1
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    if n_datasets <= 6 :
        f.set_size_inches(13, 5, forward = True)
    else :
        f.set_size_inches(13, 7.5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

# mod
    
def plot_final_predictor_performance(datasets, results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), min_kmer_length = 1, max_kmer_length = 5, metric = 'r2'):
    '''
    Make bar plots to show performance of final predictors for the given datasets.
    :param datasets : a list of datasets for which performance should be shown.
    :param results : a description of the results that should be plotted (configuration, features, experiment)
    :param min_kmer_length : minimum length of k-mer to be plotted.
    :param max_kmer_length : maximum length of k-mer to be plotted.
    '''
    n_datasets = len(datasets)
    if n_datasets <= 4 :
        n_rows, n_cols = 1, n_datasets
    elif n_datasets <= 12 :
        n_rows, n_cols = 3, 4
    elif n_datasets <= 20 :
        n_rows, n_cols = 5, 4
    elif n_datasets <= 24 :
        n_rows, n_cols = 4, 6
    elif n_datasets <= 36 :
        n_rows, n_cols = 6, 6
    
    f, ax = plot.subplots(n_rows, n_cols)
    width = 0.8
    row, col = 0, 0
    configuration_name, feature_names, experiment = results
    x = np.array(range(max_kmer_length)) * width + width / 2.0
    for dataset_name in datasets :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        performance = np.zeros((max_kmer_length, )) * float('NaN')
        for kmer_length in range(1, max_kmer_length + 1) :
            file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
            #filename = '%s/%s-max_kmer_%d-%s-feature_interpretation.out' % (file_path, feature_names, kmer_length, experiment)
            if min_kmer_length is not None :
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
            else :
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, kmer_length, kmer_length, experiment)
            if os.path.exists(filename) :
                result = lib.reader.read_pickle(filename)
                dataset_sizes = result.dataset_size
                dataset_size = np.mean(dataset_sizes, dtype = np.int)
                performance[kmer_length - 1] = result.overall_test_score[metric]
            else :
                dataset_size = None
                print '[i] Missing: %s' % filename
        cur.bar(x, performance, width = width, alpha = 0.45)
        if metric == 'auc-pr' :
            random_pr_auc = np.mean(lib.regression.get_random_pr_auc(dataset_name))
            cur.plot([0, np.max(x) + width + width / 2.0], [random_pr_auc] * 2, color = 'r', linewidth = 1.5)
        elif metric == 'auc-roc' :
            cur.plot([0, np.max(x) + width + width / 2.0], [0.5] * 2, color = 'r', linewidth = 1.5)
        display_name = lib.plotter.get_dataset_display_name(dataset_name)
        cur.set_title('%s : %s' % (display_name, dataset_size), fontsize = 16)
        cur.set_xlim(0, np.max(x) + width / 2.0 + width)
        #_, ymax = cur.get_ylim()
        #cur.set_ylim(0, ymax)
        if col == 0 :
            cur.set_ylabel(metric.upper(), fontsize = 16)
        if row == 0 and col == 0 :
            cur.legend(ncol = 3, prop = {'size' : 8}, loc = 'lower center')
        cur.set_xticks(x + width / 2.0)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xticklabels(range(1, max_kmer_length + 1))
            cur.set_xlabel('$k$-mer length', fontsize = 16)
        else :
            cur.set_xticklabels([])
        col += 1
        if col >= n_cols :
            row += 1
            col = 0

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    if n_datasets <= 3 :
        f.set_size_inches(18, 5, forward = True)
    else :
        f.set_size_inches(18, 14, forward = True)

    f.tight_layout()
    plot.show()

def plot_feature_ires_activity_scatter(dataset_name, feature_name = 'count-T-RF_all-window_10_30', feature_label = 'T count in [10, 30]', features = [{'feature_type' : 'independent', 'max_motif_length' : 4, 'other_reading_frames' : False}, {'feature_type' : 'independent-windows', 'max_motif_length' : 4, 'other_reading_frames' : False}], fold = None, use_aux = True) :
    Xs, feature_names = lib.regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = fold)
    feature_names = [name for name_group in feature_names for name in name_group]
    X, y = lib.regression.build_regression_matrix(Xs, dataset_name, log2 = True)
    feature_index = np.where(np.array(feature_names) == feature_name)[0]
    if len(feature_index) == 0 :
        print '[-] Feature %s not found!' % (feature_name)
        return None
    feature_index = feature_index[0]
    feature_vector = X[:, feature_index].toarray()
    feature_vector = feature_vector[:, 0]
    coef_r, pvalue = scipy.stats.spearmanr(feature_vector, y)
    #coef_r, pvalue = scipy.stats.pearsonr(feature_vector, y)
    print '[i] Coef: %.2f, p-value: %g [correlation]' % (coef_r, pvalue)
    feature_vector_rank, y_rank = scipy.stats.rankdata(feature_vector), scipy.stats.rankdata(y)
    f, ax = plot.subplots(1, 1)
    ax.scatter(feature_vector_rank, y_rank, color = 'k', alpha = 0.30, s = 10)
    #ax.scatter(feature_vector, y, color = 'k', alpha = 0.30, s = 10)
    ax.set_xlabel('%s, rank' % feature_label, fontsize = 16)
    ax.set_ylabel('IRES activity, rank', fontsize = 16)
    x = np.array(range(int(np.min(feature_vector_rank)), int(np.max(feature_vector_rank))))
    slope, intercept, coef_r, p_value, _ = scipy.stats.linregress(feature_vector_rank, y_rank)
    print '[i] Coef: %.2f, p-value: %g, y = %.2f * x + %.2f [regression]' % (coef_r, pvalue, slope, intercept)
    #x = np.array(range(int(np.min(feature_vector)) - 1, int(np.max(feature_vector)) + 1))
    y_pred = slope * x + intercept
    ax.plot(x, y_pred, linewidth = 1.5, color = 'b')
    ax.set_xlim(np.min(feature_vector_rank) - 1, np.max(feature_vector_rank) + 1)
    ax.set_ylim(np.min(y_rank) - 1, np.max(y_rank) + 1)
    f.set_size_inches(7, 5, forward = True)
    f.tight_layout()
    plot.show()

def plot_ires_activity_replicates(seqs, log2 = True) :
    '''
    Make a scatter plot of two IRES activity replicates.
    :param seqs : a list of sequences for which the scatter plot should be made.
    :param log2 : whether log2 IRES activity should be plotted.
    '''
    activity1 = np.array([seq.ires_activity_replicate1 for seq in seqs])
    activity2 = np.array([seq.ires_activity_replicate2 for seq in seqs])
    f, ax = plot.subplots(1, 1)
    if log2 :
        activity1, activity2 = np.log2(activity1), np.log2(activity2)
    ax.scatter(activity1, activity2, color = 'k', alpha = 0.45)
    if not log2 :
        ax.set_xlabel('Replicate 1', fontsize = 13)
        ax.set_ylabel('Replicate 2', fontsize = 13)
    else :
        ax.set_xlabel('Replicate 1, $\\log_2$', fontsize = 13)
        ax.set_ylabel('Replicate 2, $\\log_2$', fontsize = 13)

    coef_r, p = scipy.stats.spearmanr(activity1, activity2)
    print '[i] Spearman r = %.2f (p-value %g)' % (coef_r, p)
    indices = np.logical_and(~np.isnan(activity1), ~np.isnan(activity2))
    coef_r, p = scipy.stats.pearsonr(activity1[indices], activity2[indices])
    print '[i] Pearson  r = %.2f (p-value %g)' % (coef_r, p)
    f.set_size_inches(7, 5, forward = True)
    f.tight_layout()
    plot.show()

def plot_dataset_composition_piechart(datasets = ['human_native_5utr', 'human_native_cds', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_negative'], labels = ["Human $5'$ UTR", 'Human CDS', "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses', '(-) ssRNA viruses'], save_filename=None) :
    '''
    '''
    import brewer2mpl
    colors1 = brewer2mpl.get_map('Blues', 'sequential', 5).mpl_colors
    colors2 = brewer2mpl.get_map('Set2', 'qualitative', 8).mpl_colors
    del colors2[2]
    del colors2[0]
    colors = colors1[1:4] + colors2
    total_counts = []
    positive_counts = []
    for dataset in datasets :
        ds = lib.persistent.datasets[dataset]
        dataset_size = len(ds)
        n_positive = len([seq for seq in ds if seq.ires_activity > lib.tools.MINIMAL_IRES_ACTIVITY])
        total_counts.append(dataset_size)
        positive_counts.append(n_positive)

    global cnt
    cnt = 0
    def my_autopct(value) :
        global cnt
        str = '%d / %d' % (positive_counts[cnt], total_counts[cnt])
        cnt += 1
        print str
        return str

    f, ax = plot.subplots(1, 1)
    #real_labels = ['%s\n%d / %d' % (label, positive_count, total_count) for label, positive_count, total_count in zip(labels, positive_counts, total_counts)]
    real_labels = ['%d / %d' % (positive_count, total_count) for positive_count, total_count in zip(positive_counts, total_counts)]
    position_delta = [(0,0), (0, 0), (-1, -0.3), (-1.3, -1.3), (-0.3, -2.9), (0, 0), (1.0, 2.4)]
    position_angle = [0, 0, 20, 0, 0, 0, 0]
    patches, texts, autotexts = ax.pie(total_counts, labels = labels, autopct = my_autopct, shadow = False, startangle = 90, colors=colors)
    for text in texts :
        text.set_fontsize(13)
    for text, dC, angle in zip(autotexts, position_delta, position_angle) :
        text.set_fontsize(11)
        x, y = text.get_position()
        dx, dy = dC
        x += dx * 0.1
        y += dy * 0.1
        text.set_position((x, y))
        text.set_rotation(angle)
    ax.axis('equal')
    f.set_size_inches(8, 5, forward = True)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

#def plot_dataset_feature_combination_performance(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = [('kmers', 'Global counts'), ('kmers-windows', 'Positional counts'), ('kmers_kmers-windows', 'Global & positional counts')], additional_features = [('kmers_presence', 'Global presence'), ('kmers_presence-windows', 'Positional presence'), ('kmers_presence_kmers_presence-windows', 'Global & positional presence')], min_kmer_length = 1, max_kmer_length = 5, marks = [4, 4, 4, 4, 4], mark_features = 'kmers_kmers-windows', plot_gbrf = True) :

def plot_dataset_feature_combination_performance(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = [('kmers', 'Global'), ('kmers-windows', 'Positional'), ('kmers_kmers-windows', 'Global & positional')], additional_features = [('kmers_presence', None), ('kmers_presence-windows', None), ('kmers_presence_kmers_presence-windows', None)], min_kmer_length = 1, max_kmer_length = 5, marks = [4, 4, 4, 4, 4], mark_features = 'kmers_kmers-windows', plot_gbrf = True, chosen_metric='r2', metric_label='$R^2$', save_filename=None) :

#def plot_dataset_feature_combination_performance(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = [('accessible_kmers', 'Global'), ('accessible_kmers-windows', 'Positional'), ('accessible_kmers_accessible_kmers-windows', 'Global & positional')], additional_features = [], min_kmer_length = 1, max_kmer_length = 5, marks = [-1] * 6, mark_features = 'kmers_kmers-windows', plot_gbrf = True) :
    '''
    '''
    n_datasets = len(dataset_names)
    if n_datasets <= 6 :
        n_rows, n_cols = 2, 3
    else :
        n_rows, n_cols = 3, 3
    f, ax = plot.subplots(n_rows, n_cols)
    colors = ['b', 'g', 'r']
    
    x = np.array(range(1, max_kmer_length + 1), dtype = np.int)
    row, col = 0, 0
    for dataset_name, dataset_label, mark_kmer in zip(dataset_names, dataset_labels, marks) :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        configuration_name, experiment = results
        k = 0
        for feature_names, feature_labels in features :
            performance = np.zeros((max_kmer_length, )) * float('NaN')
            for kmer_length in range(max_kmer_length) :
                folder = 'gbrf' if plot_gbrf else 'svr'
                file_path = '../results/regression/%s/%s/%s' % (configuration_name, dataset_name, folder)
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length + 1, experiment)
                if os.path.exists(filename) :
                    result = lib.reader.read_pickle(filename)
                    performance[kmer_length] = result.overall_test_score[chosen_metric]
                else :
                    print '[!] Missing: %s' % filename
            cur.plot(x, performance, alpha = 0.45, marker = 'x', label = feature_labels, linewidth = 1.5, color = colors[k])
            if feature_names == mark_features and mark_kmer is not None :
                print '[i] %s + %s (k=%d) : %.2f' % (dataset_name, feature_names, mark_kmer, performance[mark_kmer - 1])
                cur.scatter([x[mark_kmer - 1]] , [performance[mark_kmer - 1]], s = 120, marker = 'o', edgecolors = 'k', facecolors = 'none', alpha = 0.75)
            k += 1
        
        k = 0
        for feature_names, feature_labels in additional_features :
            performance = np.zeros((max_kmer_length, )) * float('NaN')
            for kmer_length in range(max_kmer_length) :
                folder = 'gbrf' if plot_gbrf else 'svr'
                file_path = '../results/regression/%s/%s/%s' % (configuration_name, dataset_name, folder)
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length + 1, experiment)
                if os.path.exists(filename) :
                    result = lib.reader.read_pickle(filename)
                    performance[kmer_length] = result.overall_test_score[chosen_metric]
                else :
                    print '[!] Missing: %s' % filename
            cur.plot(x, performance, alpha = 0.45, marker = 'x', label = feature_labels, linewidth = 1.5, linestyle = '--', color = colors[k])
            if feature_names == mark_features and mark_kmer is not None :
                print '[i] %s + %s (k=%d) : %.2f' % (dataset_name, feature_names, mark_kmer, performance[mark_kmer - 1])
                cur.scatter([x[mark_kmer - 1]] , [performance[mark_kmer - 1]], s = 120, marker = 'o', edgecolors = 'k', facecolors = 'none', alpha = 0.75)
            k += 1

        cur.locator_params(nbins = 6, axis = 'y')
        cur.set_xlim(0.75, max_kmer_length + 0.25)
        cur.set_title(dataset_label, fontweight = 'bold', fontsize = 12)
        
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('bottom')
        
        if col == 0 and row == 0 :
            cur.legend(prop = {'size' : 10}, loc = 'best')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('$k$-mer length', fontsize = 13)
        else :
            cur.set_xticklabels([])
        if col == 0 :
            cur.set_ylabel(metric_label, fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    if n_datasets <= 6 :
        f.set_size_inches(13, 5, forward = True)
    else :
        f.set_size_inches(13, 7.5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

def _get_kmer_by_index(index) :
    '''
    '''
    index_to_nucleotide = {0 : 'A', 1 : 'C', 2 : 'T', 3 : 'G'}
    
    kmer_length = 1
    while index >= 4 ** kmer_length :
        index -= 4 ** kmer_length
        kmer_length += 1
    
    nucleotides = []
    for i in range(kmer_length) :
        nuc_index = index % 4
        index /= 4
        nucleotides.append(index_to_nucleotide[nuc_index])
    return ''.join(nucleotides)

def auto_classify_kmer_groups(max_kmer_length = 4) :
    '''
    '''

    n_kmers = 0
    for i in range(1, max_kmer_length + 1) :
        n_kmers += 4 ** i
    
    #group_names = ['poly-U', 'pyrimidine tract', 'poly-A', 'purine tract', 'C/U-rich', 'G/A-rich', 'CG dinuc', 'other']
    group_names = ['poly-U', 'pyrimidines', 'C/U-rich', 'poly-A', 'purines', 'G/A-rich', 'CG', 'other']
    groups = {group_name : [] for group_name in group_names[: -1]}

    for index in range(n_kmers) :
        kmer = _get_kmer_by_index(index)
        nucs = np.array([nuc for nuc in kmer])
        unique_nucs = list(set(nucs))
        count_ag = np.sum(np.logical_or(nucs == 'A', nucs == 'G'), dtype = np.int)
        count_ct = np.sum(np.logical_or(nucs == 'C', nucs == 'T'), dtype = np.int)
       
        assigned = False
        if len(unique_nucs) == 1 :
            if unique_nucs[0] == 'T' :
                groups['poly-U'].append(kmer)
            elif unique_nucs[0] == 'A' :
                groups['poly-A'].append(kmer)
            elif unique_nucs[0] == 'C' :
                groups['pyrimidines'].append(kmer)
            elif unique_nucs[0] == 'G' :
                groups['purines'].append(kmer)
            assigned = True
        elif len(unique_nucs) == 2 :
            if (unique_nucs[0] == 'C' and unique_nucs[1] == 'T') or (unique_nucs[0] == 'T' and unique_nucs[1] == 'C') :
                groups['pyrimidines'].append(kmer)
                assigned = True
            elif (unique_nucs[0] == 'A' and unique_nucs[1] == 'G') or (unique_nucs[0] == 'G' and unique_nucs[1] == 'A') :
                groups['purines'].append(kmer)
                assigned = True
            #elif 'CG' in kmer :
            #    groups['CG'].append(kmer)
            #    assgiend = True
        
        if not assigned :
            #if 'CG' in kmer :
            #    groups['CG'].append(kmer)
            #    assgiend = True
            if 1 == 0 :
                pass
            else :
                if count_ag > count_ct :
                    groups['G/A-rich'].append(kmer)
                elif count_ag < count_ct :
                    groups['C/U-rich'].append(kmer)
                else :
                    pass
                    #groups[7].append(kmer)
    
    groups = [groups.get(group_name, []) for group_name in group_names[: -1]]
    return groups, group_names

def plot_count_feature_importance_for_datasets(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], is_counted = [False, True, True, True, True, True], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [4, 4, 4, 4, 4, 4], min_kmer_length = 1, min_n_folds = 10, min_importance = 0.1, min_n_datasets = 2, max_n_datasets = 100, plot_marks = False, selected_groups = None, label_location = 'left', save_filename = None) :
    '''
    Plot classified partial dependence for a number of datasets.
    :param dataset_names : a list of datasets for which PDP should plotted.
    :param dataset_labels : a list of dataset labels.
    :param results : a description of the results to be used for plotting partial dependence.
    :param chosen_kmers : a list of k-mer lengths chosen for the results.
    :param min_kmer_length : minimum kmer length for the chosen results.
    :param min_n_folds : only plot those features that are present in at least so many CV folds.
    :param min_importance : only plot feature with importance above this number.
    :param min_n_datasets : only plot features shared by at least so many datasets.
    :param max_n_datasets : only plot features shared by no more that this number of datasets.
    '''
    groups, group_names = auto_classify_kmer_groups()
    
    configuration_name, feature_names, experiment = results
    importances, pdp, assignment = {}, {}, {}
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            dataset_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            dataset_result = None
            break
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            dataset_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            dataset_pdp = None
            break
        dataset_importances = lib.tools.get_feature_importances(dataset_result, normalize = True)
        dataset_importances = lib.tools.average_importances(dataset_importances)
        importances[dataset_name] = {key : value for key, value in dataset_importances.iteritems() if not 'window' in key}
        pdp[dataset_name] = dataset_pdp
        assignment[dataset_name] = lib.interpretation.classify_partial_dependence_using_derivative(dataset_pdp)
   
    n_datasets = len(dataset_names)
    overall_importances = {}
    for dataset_name, is_dataset_counted in zip(dataset_names, is_counted) : # filter features in datasets
        cur_assignment = assignment[dataset_name]
        cur_importances = importances[dataset_name]
        n_pre_filtering = len(cur_importances)
        #cur_importances = {key : value for key, value in cur_importances.iteritems() if value[0] >= min_importance and value[1] >= min_n_folds and cur_assignment[key] is not None}
        importances[dataset_name] = cur_importances
        n_post_filtering = len(cur_importances)
        print '[i] %s : %d / %d' % (dataset_name, n_post_filtering, n_pre_filtering)
        for feature, value in cur_importances.iteritems() :
            if not feature in overall_importances :
                overall_importances[feature] = []
            if value[0] >= min_importance and value[1] >= min_n_folds and cur_assignment[feature] is not None and is_dataset_counted :
                overall_importances[feature].append(value[0])
    print ''
    print '[i] Union features: %d' % len(overall_importances)
    for i in range(n_datasets) :
        count = len([feature for feature, value in overall_importances.iteritems() if len(value) >= i + 1])
        print '    [i] %d datasets : %d' % (i + 1, count)

    selected_features = [feature for feature, value in overall_importances.iteritems() if len(value) >= min_n_datasets and len(value) <= max_n_datasets]

    mean_importances = {feature_name : np.mean(overall_importances[feature_name]) for feature_name in selected_features}
    def feature_group_cmp(feat_a, feat_b) :
        orig_feat_a, orig_feat_b = feat_a, feat_b
        feat_a, feat_b = feat_a.split('-')[1], feat_b.split('-')[1]
        n_groups = len(groups)
        group_a, group_b = n_groups, n_groups
        for i in range(n_groups) :
            if feat_a in groups[i] :
                group_a = i
            if feat_b in groups[i] :
                group_b = i

        if group_a == group_b :
            diff = mean_importances[orig_feat_b] - mean_importances[orig_feat_a]
            if diff < 0 :
                return -1
            elif diff > 0 :
                return +1
            return 0
        else :
            return group_a - group_b

    _selected_features = []
    n_groups = len(groups)
    for feature_name in selected_features :
        feat = feature_name.split('-')[1]
        group_name = group_names[-1]
        for i in range(n_groups) :
            if feat in groups[i] :
                group_name = group_names[i]
        if selected_groups is None or group_name in selected_groups :
            _selected_features.append(feature_name)
    selected_features = _selected_features

    selected_features.sort(cmp = feature_group_cmp, reverse = False)
    n_selected_features = len(selected_features)
    feature_group = [None] * n_selected_features
    for i, feature_name in enumerate(selected_features) :
        kmer = feature_name.split('-')[1]
        n_groups = len(groups)
        group_ind = n_groups 
        for j in range(n_groups) :
            if kmer in groups[j] :
                group_ind = j
        feature_group[i] = group_ind

    f, ax = plot.subplots(1, 1)
    labels = [feature_name.split('-')[1] for feature_name in selected_features]
    for i, label in enumerate(labels) :
        label = np.array([char for char in label])
        label[label == 'T'] = 'U'
        label = ''.join(label)
        labels[i] = label

    #matrix = np.zeros((n_datasets, n_selected_features)) * float('nan')
    matrix = np.zeros((n_selected_features, n_datasets))
    marks = []
    for i, dataset_name in enumerate(dataset_names) :
        cur_assignment, cur_pdp, cur_importances = assignment[dataset_name], pdp[dataset_name], importances[dataset_name]
        for j, feature_name in enumerate(selected_features) :
            if feature_name in cur_pdp and feature_name in cur_importances and feature_name in cur_assignment :
                cur_segment_observed, cur_segment, cur_feature_pdp = cur_pdp[feature_name]
                val = cur_importances[feature_name][0]
                n_folds = cur_importances[feature_name][1]
                if cur_assignment[feature_name] == 'Dec' :
                    val = -val
                matrix[j][i] = val
                if n_folds < min_n_folds or abs(val) < min_importance :
                    mark = (i + 0.5, j + 0.5)
                    marks.append(mark)

    x = np.array(range(n_datasets)) + 0.5
    ax.set_xticks(x)
    ax2 = ax.twinx()
    actual_labels = list(reversed(labels))
    y = np.array(range(n_selected_features)) + 0.5
    ax.set_yticks(y)
    ax2.set_yticks(y)
    if label_location == 'left' :
        ax.set_yticklabels(actual_labels, ha = 'right', va = 'center', fontsize = 13)
        ax2.set_yticks([])
    else :
        ax.set_yticks([])
        ax2.set_yticklabels(actual_labels, ha = 'left', va = 'center', fontsize = 13)

    for tick in ax.yaxis.get_major_ticks() : 
        tick.tick1On = False 
        tick.tick2On = False 
    for tick in ax2.yaxis.get_major_ticks() : 
        tick.tick1On = False 
        tick.tick2On = False 
    ax.set_xticklabels(dataset_labels, rotation = 60, ha = 'right', va = 'top')
    for tick in ax.xaxis.get_major_ticks() : 
        tick.tick1On = False 
        tick.tick2On = False 
    ax.set_xlim(0, n_datasets)
    ax.set_ylim(0, n_selected_features)
    ax2.set_ylim(0, n_selected_features)
    vmin, vmax = -1, +1
    matrix = np.ma.array(matrix, mask = np.isnan(matrix))
    plot.cm.RdBu.set_bad('w', alpha = 1.0)
    im = ax.imshow(matrix, interpolation = 'nearest', aspect = 'auto', cmap = plot.cm.RdBu, vmin = vmin, vmax = vmax, extent = [0, n_datasets, 0, n_selected_features], origin = 'upper')
    if plot_marks :
        ax.scatter([mark[0] for mark in marks], [n_selected_features - mark[1] for mark in marks], s = 10, marker = 'x', color = 'k')
    #for i in range(1, n_datasets) :
    #    ax.axvline(i, color = 'k', linewidth = 0.8)
    offset, start = 0.3, 0
    for i in range(1, n_selected_features) :
        if feature_group[i] != feature_group[i - 1] :
            ax.axhline(n_selected_features - i, color = 'k', linewidth = 0.5)

        if feature_group[i] != feature_group[i - 1] or i == n_selected_features - 1 :
            stop = i if i != n_selected_features - 1 else n_selected_features
            mid = (start + stop) / 2.0
            if label_location == 'left' :
                line = ax.plot([-1.4, -1.4], [n_selected_features - start - offset, n_selected_features - stop + offset], linewidth = 2, color = 'k')
                ax.text(-1.5 , n_selected_features - mid, group_names[feature_group[i - 1]], va = 'center', ha = 'right', fontsize = 13, fontweight = 'bold', rotation = 90)
            else :
                x_offset = 1.4 + n_datasets
                line = ax.plot([x_offset, x_offset], [n_selected_features - start - offset, n_selected_features - stop + offset], linewidth = 2, color = 'k')
                ax.text(x_offset + 0.1 , n_selected_features - mid, group_names[feature_group[i - 1]], va = 'center', ha = 'left', fontsize = 13, fontweight = 'bold', rotation = 90)
            line[0].set_clip_on(False)
            start = stop

    original_height, original_n_rows = 15.0, 52
    bottom_fraction, top_fraction = 0.13, 0.1
    height_per_row, top_height, bottom_height = original_height * (1.0 - bottom_fraction - top_fraction) / original_n_rows, original_height * bottom_fraction, original_height * top_fraction
    bottom_fraction = (original_height - top_height - height_per_row * n_selected_features) / original_height
    #f.set_size_inches(4.5 / 8.0 * (n_datasets + 2), 15, forward = True)
    #f.set_size_inches(4.5 / 8.0 * (n_datasets + 2), 18, forward = True)
    f.set_size_inches(4.5 / 8.0 * (n_datasets + 2), original_height, forward = True)
    f.tight_layout()
    if label_location == 'left' :
        f.subplots_adjust(left = 0.25, hspace = 0.00, right = 0.95, bottom = bottom_fraction, top = 1.0 - top_fraction)
        cbar_ax = f.add_axes([0.25, 0.935, 0.7, 0.02])
    else :
        f.subplots_adjust(left = 0.05, hspace = 0.00, right = 0.75, bottom = bottom_fraction, top = 1.0 - top_fraction)
        cbar_ax = f.add_axes([0.065, 0.935, 0.7, 0.02])
    cbar = f.colorbar(im, cax = cbar_ax, ticks = [vmin, vmax], orientation = 'horizontal')
    cbar.set_label('Feature effect')
    cbar.ax.get_xaxis().labelpad = -50
    if save_filename is None :
        plot.show()
    else :
        plot.savefig(save_filename)

def check_correlation_between_importance_and_pdp_span(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [3, 3, 2, 3, 4, 4], min_kmer_length = 1, min_n_folds = 10) :
    '''
    '''
    configuration_name, feature_names, experiment = results
    importances, pdp = {}, {}
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            dataset_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            dataset_result = None
            break
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            dataset_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            dataset_pdp = None
            break
        dataset_importances = lib.tools.get_feature_importances(dataset_result, normalize = True)
        dataset_importances = lib.tools.average_importances(dataset_importances)
        importances[dataset_name] = {key : value for key, value in dataset_importances.iteritems() if not 'window' in key}
        pdp[dataset_name] = dataset_pdp
   
    n_datasets = len(dataset_names)
    for dataset_name in dataset_names :
        feature_names = set(importances[dataset_name].keys()) & set(pdp[dataset_name].keys())
        feature_names = [feature_name for feature_name in feature_names if importances[dataset_name][feature_name][1] >= min_n_folds]
        values = [importances[dataset_name][feature_name][0] for feature_name in feature_names]
        spans = [np.max(pdp[dataset_name][feature_name][2]) - np.min(pdp[dataset_name][feature_name][2]) for feature_name in feature_names]
        coef_r, pvalue = scipy.stats.pearsonr(values, spans)
        print '[i] %s : Pearson r = %.3f (p = %g)' % (dataset_name, coef_r, pvalue)

def plot_positional_features_importance(dataset_name, kmer_length = 3, results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), min_kmer_length = 1, sequence_length = 300, atg_offset = -270, min_importance = 0.1, min_n_folds = 10, min_n_windows = 2) :
    '''
    Plot positional feature importances (with effect directionality) for a given dataset.
    :param dataset_name : name of the dataset, for which importance should be plotted.
    :param kmer_length : length of the kmer, for which importance should be plotted.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param min_importance : minimal importance for selecting a window.
    :param min_n_folds : minimal number of folds for selecting a window.
    :param min_n_windows : minimal number of selected windows required to select a feature.
    '''
    configuration_name, feature_names, experiment = results
    file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
    filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
    if os.path.exists(filename_result) :
        result = lib.reader.read_pickle(filename_result)
    else :
        print '[!] Missing (result): %s' % filename_result
        return
    filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
    if os.path.exists(filename_pdp) :
        pdp = lib.reader.read_pickle(filename_pdp)
    else :
        print '[!] Missing (PDP): %s' % filename_pdp
        return
    importances = lib.tools.get_feature_importances(result, normalize = True)
    importances = lib.tools.average_importances(importances)
    assignment = lib.interpretation.classify_partial_dependence_using_derivative(pdp)
    windows_by_kmer = {}
    n_useable_windows = {}

    for feature_name, feature_importance in importances.iteritems() :
        if not 'window' in feature_name :
            continue
        args = feature_name.split('-')
        kmer, window = args[1], args[3]
        args = window.split('_')
        start, stop = int(args[1]), int(args[2])
        feature_importance, n_folds = feature_importance
        if not kmer in windows_by_kmer :
            windows_by_kmer[kmer] = []
            n_useable_windows[kmer] = 0
        item = ((start, stop), feature_importance, assignment[feature_name])
        windows_by_kmer[kmer].append(item)
        if n_folds >= min_n_folds and feature_importance >= min_importance :
            n_useable_windows[kmer] += 1
    n_interesting_kmers = 0
    for n_windows in n_useable_windows.values() :
        if n_windows >= min_n_windows :
            n_interesting_kmers += 1
    print '[i] Total k-mers: %d' % len(n_useable_windows)
    print '[i] Interesting: %d' % n_interesting_kmers

    if n_interesting_kmers <= 12 :
        n_rows, n_cols = 3, 4
    elif n_interesting_kmers <= 16 :
        n_rows, n_cols = 4, 4
    elif n_interesting_kmers <= 20 :
        n_rows, n_cols = 5, 4
    elif n_interesting_kmers <= 24 :
        n_rows, n_cols = 6, 4

    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    for kmer in windows_by_kmer :
        if n_useable_windows[kmer] < min_n_windows :
            continue
        cur = ax[row][col]
        window_importances = np.zeros((sequence_length, ))
        window_counts = np.zeros((sequence_length, ), dtype = np.int)
        for window, importance, assignment in windows_by_kmer[kmer] :
            start, stop = window
            effect = importance
            if assignment == 'Dec' :
                effect = -effect
            window_importances[start:stop] += effect
            window_counts[start:stop] += 1
        window_counts[window_counts == 0] = 1
        window_importances /= window_counts
        cur.plot(x, window_importances, linewidth = 1.3, color = 'b', alpha = 0.45)
        cur.axhline(0, color = 'k', linewidth = 0.8, linestyle = '--')
        cur.set_xlim(atg_offset, atg_offset + sequence_length - 1)
        vmax = np.nanmax(np.abs(window_importances))
        cur.set_ylim(-vmax * 1.05 , +vmax * 1.05)
        cur.set_title(kmer, fontweight = 'bold', fontsize = 11)

        col += 1
        if col == n_cols :
            col = 0
            row += 1

    while row < n_rows and col < n_cols :
        cur = ax[row, col]
        f.delaxes(cur)
        col += 1
        if col == n_cols :
            col = 0
            row += 1

    f.suptitle('Dataset: %s' % dataset_name, fontsize = 13, fontweight = 'bold')
    f.set_size_inches(15, 10, forward = True)
    f.tight_layout()
    f.subplots_adjust(top = 0.93)
    plot.show()

def plot_positional_features_importance_for_datasets(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], kmer_length = 3, results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [3, 2, 3, 4, 4], min_kmer_length = 1, sequence_length = 300, atg_offset = -270, min_importance = 0.1, min_n_folds = 10, min_n_windows = 3, min_n_datasets = 2) :
    '''
    Plot positional feature importances (with effect directionality) for a list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param kmer_length : length of the kmer, for which importance should be plotted.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param min_importance : minimal importance for selecting a window.
    :param min_n_folds : minimal number of folds for selecting a window.
    :param min_n_windows : minimal number of selected windows required to select a feature.
    '''
    configuration_name, feature_names, experiment = results
    importances, pdp, assignment = {}, {}, {}
    windows_by_kmer, n_useable_windows = {}, {}
    datasets_by_interesting_kmer = {}
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            cur_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            return
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            cur_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            return
        cur_assignment = lib.interpretation.classify_partial_dependence_using_derivative(cur_pdp)
        cur_importances = lib.tools.get_feature_importances(cur_result, normalize = True)
        cur_importances = lib.tools.average_importances(cur_importances)
        importances[dataset_name] = cur_importances
        pdp[dataset_name] = cur_pdp
        assignment[dataset_name] = cur_assignment
        
        cur_windows_by_kmer = {}
        cur_n_useable_windows = {}
        for feature_name, feature_importance in cur_importances.iteritems() :
            if not 'window' in feature_name :
                continue
            args = feature_name.split('-')
            kmer, window = args[1], args[3]
            args = window.split('_')
            start, stop = int(args[1]), int(args[2])
            feature_importance, n_folds = feature_importance
            if not kmer in cur_windows_by_kmer :
                cur_windows_by_kmer[kmer] = []
                cur_n_useable_windows[kmer] = 0
            item = ((start, stop), feature_importance, cur_assignment[feature_name])
            cur_windows_by_kmer[kmer].append(item)
            if n_folds >= min_n_folds and feature_importance >= min_importance :
                cur_n_useable_windows[kmer] += 1
        windows_by_kmer[dataset_name] = cur_windows_by_kmer
        n_useable_windows[dataset_name] = cur_n_useable_windows

        cur_interesting_kmers = [kmer for kmer in cur_windows_by_kmer if cur_n_useable_windows[kmer] >= min_n_windows]

        for kmer in cur_interesting_kmers :
            if not kmer in datasets_by_interesting_kmer :
                datasets_by_interesting_kmer[kmer] = []
            datasets_by_interesting_kmer[kmer].append(dataset_name)

        print '[i] Dataset: %s' % dataset_name
        print '    [i] Total k-mers: %d' % len(cur_n_useable_windows)
        print '    [i] Interesting: %d' % len(cur_interesting_kmers)
        print '    ', cur_interesting_kmers
        if 'CTT' in n_useable_windows : 
            print n_useable_windows['CTT'], cur_windows_by_kmer['CTT']
        

    print datasets_by_interesting_kmer['CTT']

    selected_kmers = [kmer for kmer, dataset_list in datasets_by_interesting_kmer.iteritems() if len(dataset_list) >= min_n_datasets]
    n_interesting_kmers = len(selected_kmers)

    if n_interesting_kmers <= 12 :
        n_rows, n_cols = 3, 4
    elif n_interesting_kmers <= 16 :
        n_rows, n_cols = 4, 4
    elif n_interesting_kmers <= 20 :
        n_rows, n_cols = 5, 4
    elif n_interesting_kmers <= 24 :
        n_rows, n_cols = 6, 4

    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    for kmer in selected_kmers :
        cur = ax[row][col]
        overall_vmax = -float('inf')
        for dataset_name, dataset_label in zip(dataset_names, dataset_labels) :
            cur_windows_by_kmer = windows_by_kmer[dataset_name]
            window_importances = np.zeros((sequence_length, ))
            window_counts = np.zeros((sequence_length, ), dtype = np.int)
            window_list = cur_windows_by_kmer.get(kmer, [])
            for window, importance, assignment in window_list :
                start, stop = window
                effect = importance
                if assignment == 'Dec' :
                    effect = -effect
                window_importances[start:stop] += effect
                window_counts[start:stop] += 1
            window_counts[window_counts == 0] = 1
            window_importances /= window_counts
            if len(window_list) > 0 :
                cur.plot(x, window_importances, linewidth = 1.3, label = dataset_label)
            vmax = np.nanmax(np.abs(window_importances))
            overall_vmax = max(overall_vmax, vmax)
        cur.axhline(0, color = 'k', linewidth = 0.8, linestyle = '--')
        #cur.set_xlim(atg_offset, atg_offset + sequence_length - 1)
        cur.set_xlim(-200, 0)
        cur.set_ylim(-overall_vmax * 1.05 , +overall_vmax * 1.05)
        cur.set_title(kmer, fontweight = 'bold', fontsize = 11)

        if col == 3 and row == 1 :
            cur.legend(ncol = 2, prop = {'size' : 8}, loc = 'best')

        col += 1
        if col == n_cols :
            col = 0
            row += 1

    while row < n_rows and col < n_cols :
        cur = ax[row, col]
        f.delaxes(cur)
        col += 1
        if col == n_cols :
            col = 0
            row += 1

    f.set_size_inches(15, 10, forward = True)
    f.tight_layout()
    plot.show()

def plot_regression_performance(result) :
    '''
    Plot regression performance (R2, Pearson r, Spearman rho) for a given regression result.
    :param result : result, for which performance should be plotted.
    '''
    width = 0.5
    labels = ['$R^2$', 'Pearson $r$', 'Spearman $\\rho$', 'AUC-PR', 'AUC-ROC']
    train_error, test_error = [], []
    train_error_std, test_error_std = [], []
    for metric in ['r2', 'pearson', 'spearman', 'auc-pr', 'auc-roc'] :
        cur_train_error = np.mean(result.train_score[metric], axis = 0)
        cur_test_error = np.mean(result.test_score[metric], axis = 0)
        cur_train_error_std = np.std(result.train_score[metric], axis = 0)
        cur_test_error_std = np.std(result.test_score[metric], axis = 0)
        train_error.append(cur_train_error[-1])
        test_error.append(cur_test_error[-1])
        train_error_std.append(cur_train_error_std[-1])
        test_error_std.append(cur_test_error_std[-1])
    f, ax = plot.subplots(1, 1)
    x = np.array(range(5)) * 2.4 * width + width / 10.0
    ax.bar(x, train_error, width = width, color = 'g', alpha = 0.45, label = 'Train score')
    ax.bar(x + width, test_error, width = width, color = 'b', alpha = 0.45, label = 'Test score')
    ax.errorbar(x + width / 2.0, train_error, yerr = train_error_std, ecolor = 'g', color = None, fmt = None, elinewidth = 2.2)
    ax.errorbar(x + width + width / 2.0, test_error, yerr = test_error_std, ecolor = 'b', color = None, fmt = None, elinewidth = 2.2)
    ax.set_xlim(0, np.max(x) + width * 2 + width / 10.0)
    ax.legend(loc='best')
    ax.set_ylabel('Performance', fontsize = 13)
    ax.set_xticks(x + width)
    ax.set_xticklabels(labels, fontsize = 13)
    for tick in ax.xaxis.get_major_ticks() : 
        tick.tick1On = False 
        tick.tick2On = False 
    f.tight_layout()
    plot.show()

def plot_svr_rf_performance_comparison_for_datasets(desc_svr, desc_rf, dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], save_filename=None):
    METRICS = ['r2', 'pearson', 'spearman', 'auc-roc', 'auc-pr']
    LABELS = ['$R^2$', 'Pearson $r$', 'Spearman $\\rho$', 'AUC-ROC', 'AUC-PR']
    N_METRICS = len(METRICS)
    
    def read_results(dataset_names,
                     configuration_name,
                     feature_names,
                     experiment,
                     chosen_kmers,
                     min_kmer_length=1,
                     read_gbrf=True):
        dataset_results = {}
        for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
            folder = 'gbrf' if read_gbrf else 'svr'
            file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
            filename_result = '%s/%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, folder, feature_names, min_kmer_length, kmer_length, experiment)
            if os.path.exists(filename_result) :
                cur_result = lib.reader.read_pickle(filename_result)
            else :
                print '[!] Missing (result): %s' % filename_result
                return
            
            train_error, test_error = {}, {}
            train_error_std, test_error_std = {}, {}
            for metric in METRICS :
                cur_train_error = np.mean(cur_result.train_score[metric], axis = 0)
                cur_test_error = np.mean(cur_result.test_score[metric], axis = 0)
                cur_train_error_std = np.std(cur_result.train_score[metric], axis = 0)
                cur_test_error_std = np.std(cur_result.test_score[metric], axis = 0)
                if isinstance(cur_train_error, np.ndarray):
                    cur_train_error = cur_train_error[-1]
                    cur_test_error = cur_test_error[-1]
                    cur_train_error_std = cur_train_error_std[-1]
                    cur_test_error_std = cur_test_error_std[-1]
                    
                train_error[metric] = cur_train_error
                test_error[metric] = cur_test_error
                train_error_std[metric] = cur_train_error_std
                test_error_std[metric] = cur_test_error_std
            
            dataset_results[dataset_name] = (train_error,
                                             test_error,
                                             train_error_std,
                                             test_error_std)
        return dataset_results
    
    svr = read_results(dataset_names,
                       desc_svr['configuration_name'],
                       desc_svr['feature_names'],
                       desc_svr['experiment'],
                       desc_svr['chosen_kmers'],
                       desc_svr['min_kmer_length'],
                       read_gbrf=False)
    
    rf =  read_results(dataset_names,
                       desc_rf['configuration_name'],
                       desc_rf['feature_names'],
                       desc_rf['experiment'],
                       desc_rf['chosen_kmers'],
                       desc_rf['min_kmer_length'],
                       read_gbrf=True)
    
    n_datasets = len(dataset_names)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    width = 0.50
    row, col = 0, 0
    
    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
    colors = bmap.mpl_colors[2:]

    for cur_dataset_name, cur_label in zip(dataset_names, dataset_labels) :
        cur = ax[row, col]
        x = np.array(range(N_METRICS)) * 2.4 * width + width / 10.0
        
        svr_error = [svr[cur_dataset_name][1][metric_name] for metric_name in METRICS]
        rf_error = [rf[cur_dataset_name][1][metric_name] for metric_name in METRICS]
        svr_error_std = [svr[cur_dataset_name][3][metric_name] for metric_name in METRICS]
        rf_error_std = [rf[cur_dataset_name][3][metric_name] for metric_name in METRICS]
        
        cur.bar(x, svr_error, width = width, color = colors[0], edgecolor = 'none', label = 'SVR')
        cur.bar(x + width, rf_error, width = width, color = colors[1], edgecolor = 'none', label = 'RF')
        cur.set_xlim(0, np.max(x) + width * 2 + width / 10.0)
        cur.set_xticks(x + width)
        cur.set_ylim(0, 1)
        for tick in cur.xaxis.get_major_ticks() : 
            tick.tick1On = False 
            tick.tick2On = False 
    
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        yticks = cur.get_yticks()
        _ymin, _ymax = cur.get_ylim()
        for tick in yticks :
            cur.axhline(tick, linewidth = 1, color = 'w')
        cur.set_ylim(_ymin, _ymax)
    
        cur.errorbar(x + width / 2.0, svr_error, yerr = svr_error_std, ecolor = colors[0], color = None, fmt = None, elinewidth = 2.2)
        cur.errorbar(x + width + width / 2.0, rf_error, yerr = rf_error_std, ecolor = colors[1], color = None, fmt = None, elinewidth = 2.2)
        
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xticklabels(LABELS, fontsize = 13, rotation=20)
        else :
            cur.set_xticklabels([])
        if col == 0 and row == 0 :
            box = cur.get_position()
            cur.legend(ncol = 1, prop = {'size': 12}, loc = (box.x0 * 2.5, box.y0 * 1.2))
        if col == 0 :
            cur.set_ylabel('Performance', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace=0.2)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()
    
def plot_regression_performance_for_datasets(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [4, 4, 4, 4, 4, 4], min_kmer_length = 1, plot_gbrf = True, save_filename=None) :
    '''
    Plot regression performance (R2, Pearson r, Spearman rho) for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param kmer_length : length of the kmer, for which importance should be plotted.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    '''
    n_datasets = len(dataset_names)
    configuration_name, feature_names, experiment = results
    dataset_results = []
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        folder = 'gbrf' if plot_gbrf else 'svr'
        filename_result = '%s/%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, folder, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            cur_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            return
        dataset_results.append(cur_result)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    width = 0.50
    labels = ['$R^2$', 'Pearson $r$', 'Spearman $\\rho$', 'AUC-ROC', 'AUC-PR']
    row, col = 0, 0
    
    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
    colors = bmap.mpl_colors

    for cur_result, cur_label in zip(dataset_results, dataset_labels) :
        cur = ax[row, col]
        train_error, test_error = [], []
        train_error_std, test_error_std = [], []
        for metric in ['r2', 'pearson', 'spearman', 'auc-roc', 'auc-pr'] :
            cur_train_error = np.mean(cur_result.train_score[metric], axis = 0)
            cur_test_error = np.mean(cur_result.test_score[metric], axis = 0)
            cur_train_error_std = np.std(cur_result.train_score[metric], axis = 0)
            cur_test_error_std = np.std(cur_result.test_score[metric], axis = 0)
            if isinstance(cur_train_error, np.ndarray):
                train_error.append(cur_train_error[-1])
                test_error.append(cur_test_error[-1])
                train_error_std.append(cur_train_error_std[-1])
                test_error_std.append(cur_test_error_std[-1])
            else:
                train_error.append(cur_train_error)
                test_error.append(cur_test_error)
                train_error_std.append(cur_train_error_std)
                test_error_std.append(cur_test_error_std)
        x = np.array(range(5)) * 2.4 * width + width / 10.0
        
        cur.bar(x, train_error, width = width, color = colors[0], edgecolor = 'none', label = 'Train')
        cur.bar(x + width, test_error, width = width, color = colors[1], edgecolor = 'none', label = 'Test')
        cur.set_xlim(0, np.max(x) + width * 2 + width / 10.0)
        cur.set_xticks(x + width)
        cur.set_ylim(0, 1)
        for tick in cur.xaxis.get_major_ticks() : 
            tick.tick1On = False 
            tick.tick2On = False 
    
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        yticks = cur.get_yticks()
        _ymin, _ymax = cur.get_ylim()
        for tick in yticks :
            cur.axhline(tick, linewidth = 1, color = 'w')
        cur.set_ylim(_ymin, _ymax)
    
        cur.errorbar(x + width / 2.0, train_error, yerr = train_error_std, ecolor = colors[0], color = None, fmt = None, elinewidth = 2.2)
        cur.errorbar(x + width + width / 2.0, test_error, yerr = test_error_std, ecolor = colors[1], color = None, fmt = None, elinewidth = 2.2)
        
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xticklabels(labels, fontsize = 13, rotation=20)
        else :
            cur.set_xticklabels([])
        if col == 0 and row == 0 :
            box = cur.get_position()
            cur.legend(ncol = 1, prop = {'size': 12}, loc = (box.x0 * 2.5, box.y0 * 1.2))
        if col == 0 :
            cur.set_ylabel('Performance', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace=0.2)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

def plot_scatterplot_for_datasets(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [4, 4, 4, 4, 4, 4], min_kmer_length = 1, plot_gbrf = True, x_max = 14, show_mse=False, save_filename=None) :
    '''
    Plot regression performance (R2, Pearson r, Spearman rho) for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param kmer_length : length of the kmer, for which importance should be plotted.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    :param show_mse : whether the MSE label should be shown.
    '''
    from scipy.stats import gaussian_kde
    
    n_datasets = len(dataset_names)
    configuration_name, feature_names, experiment = results
    dataset_results, dataset_folds = [], []
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        folder = 'gbrf' if plot_gbrf else 'svr'
        filename_result = '%s/%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, folder, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            cur_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            return
        filename_cv = '%s/crossvalidation_folds.out' % file_path
        if os.path.exists(filename_cv) :
            cur_folds = lib.reader.read_pickle(filename_cv)
        else :
            print '[!] Missing (CV folds): %s' % filename_cv
            return
        dataset_results.append(cur_result)
        dataset_folds.append(cur_folds)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    for cur_dataset_name, cur_result, cur_label, cur_folds in zip(dataset_names, dataset_results, dataset_labels, dataset_folds) :
        cur = ax[row, col]
        n_folds = len(cur_result.regressors)
        features = cur_result.features
        use_aux = cur_result.use_aux
        log2 = cur_result.log2
        all_true_y, all_predicted_y = [], []
        for fold in range(n_folds):
            train_index, test_index = cur_folds.outer_folds[fold]
            if use_aux :
                Xs, feature_names = lib.regression.build_regression_matrix_list(cur_dataset_name, features, use_aux = use_aux, fold = fold)
            else :
                Xs, feature_names = lib.regression.build_regression_matrix_list(cur_dataset_name, features, use_aux = use_aux, fold = train_index)
            X, y = lib.regression.build_regression_matrix(Xs, cur_dataset_name, log2 = log2)
            del Xs
            X, y = X[test_index, :], y[test_index]
            if not plot_gbrf:
                X_c1, X_c2, y_c1, y_c2 = cur_result.scaling[fold]
            regressor = cur_result.regressors[fold]
            if plot_gbrf:
                predicted_y = regressor.predict(X.toarray())
            else:
                from lib.svr import scale_up, rescale_X
                predicted_y = lib.svr.scale_up(regressor.predict(lib.svr.rescale_X(X, X_c1, X_c2)), y_c1, y_c2)
            all_true_y.extend(y)
            all_predicted_y.extend(predicted_y)
            del X, y
        
        all_true_y, all_predicted_y = np.array(all_true_y), np.array(all_predicted_y)
        
        xy = np.vstack([all_true_y, all_predicted_y])
        z = gaussian_kde(xy)(xy)
        cur.scatter(all_true_y, all_predicted_y, c = z, edgecolor = '', alpha = 1)
        
        if show_mse:
            from sklearn.metrics import mean_squared_error
            mse = mean_squared_error(all_true_y, all_predicted_y)
            text = 'MSE: %.2f' % mse
            cur.text(0.1, 0.9, text, fontsize=13, transform=cur.transAxes)
            #print np.var(all_true_y)
        
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('True activity, $\\log_2$', fontsize = 14)
        if col == 0 :
            cur.set_ylabel('Predicted activity, $\\log_2$', fontsize = 13)
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        y_min, y_max = cur.get_ylim()
        y_min, y_max = int(y_min), int(y_max)
        x_min, _ = cur.get_xlim()
        cur.set_xlim(x_min, x_max)
        y_ticks = range(y_min, y_max + 1)
        cur.set_yticks(y_ticks)
        
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('bottom')
        
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.3)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

def plot_structure_correlations_for_datasets(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], sequence_length = 330, atg_offset = -270, max_kmer_length = 3) :
    '''
    Plot structure correlations for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param max_kmer_length : maximum length of the kmer to be plotted.
    '''
    n_datasets = len(dataset_names)
    dataset_correlations = []
    for dataset_name in dataset_names :
        filename = './datasets/current/%s/structure-windows20_step10.out' % dataset_name
        if os.path.exists(filename) :
            cur_correlations = lib.reader.read_pickle(filename)
        else :
            print '[!] Missing (correlations): %s' % filename
            return
        dataset_correlations.append(cur_correlations)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    for cur_correlations, cur_label in zip(dataset_correlations, dataset_labels) :
        cur = ax[row, col]
        corr_max = np.zeros((sequence_length, ), dtype = np.float) * float('nan')
        corr_min = np.zeros((sequence_length, ), dtype = np.float) * float('nan')
        for struct_kmer, window, _, coef_r, pvalue, _ in cur_correlations :
            if len(struct_kmer) > max_kmer_length :
                continue
            start, stop = window
            for i in range(start, stop) :
                corr_max[i] = np.nanmax([corr_max[i], coef_r])
                corr_min[i] = np.nanmin([corr_min[i], coef_r])
        cur.plot(x, corr_max, color = 'b')
        cur.plot(x, corr_min, color = 'r')
        cur.set_xlim(atg_offset, atg_offset + sequence_length)
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            pass
        else :
            cur.set_xticklabels([])
        if col == 0 and row == 0 :
            cur.legend(ncol = 1, prop = {'size' : 12}, loc = 'best')
        if col == 0 :
            cur.set_ylabel('Correlation', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    plot.show()

def _atomize_window_list(window_list) :
    '''
    Separate window list into non-overlapping windows.
    :param window_list : list of windows to be separated.
    '''
    overall_list = []
    for window, importance, assignment, n_folds, dataset_name in window_list :
        effect = importance
        if assignment == 'Dec' :
            effect = -effect
        window_start, window_stop = window
        window_middle = (window_start + window_stop) / 2
        overall_list.append(((window_start, window_middle), effect, n_folds))
        overall_list.append(((window_middle, window_stop), effect, n_folds))
    overall_list.sort(key = lambda x : x[0])
    n_windows = len(overall_list)
    final_list = []
    i = 0
    while i < n_windows :
        window = overall_list[i][0]
        j = i + 1
        while j < n_windows and window == overall_list[j][0] :
            j += 1
        effect = np.mean([item[1] for item in overall_list[i : j]])
        n_folds = np.mean([item[2] for item in overall_list[i : j]], dtype = np.float)
        final_list.append((window, effect, n_folds))
        i = j
    return final_list

def _plot_consensus_islands(ax, x, effect, color = 'b', linewidth = 1.3) :
    '''
    Plot consensus effect islands on a given axis.
    :param ax : axis on which the islands should be plotted.
    :param x : position list.
    :param effect : effect list
    :param color : color of the islands.
    :param linewidth : with of the line.
    '''
    def sign(x) :
        if x < -1e-4 :
            return -1
        elif x > +1e-4 :
            return +1
        return 0

    n = len(x)
    i = 0
    while i < n :
        while i < n and effect[i] == 0 :
            i += 1
        j = i + 1
        while j < n and sign(effect[j]) == sign(effect[j - 1]) :
            j += 1
        if j < n and sign(effect[j]) * sign(effect[j - 1]) >= 0 :
            j += 1
        if i > 0 and (i < n and effect[i] != 0) :
            i -= 1
        if i < n :
            ax.plot(x[i : j], effect[i : j], color = color, linewidth = linewidth)
        i = j

def plot_positional_features_importance_for_datasets_shadow(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], kmer_length = 3, results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [3, 3, 2, 3, 4, 4], min_kmer_length = 1, sequence_length = 300, atg_offset = -270, min_importance = 0.1, min_n_folds = 10, min_n_windows = 1, atomic_support = 3, max_atomic_support = float('inf')) :
    '''
    Plot positional feature importances (with effect directionality) for a list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param kmer_length : length of the kmer, for which importance should be plotted.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param min_importance : minimal importance for selecting a window.
    :param min_n_folds : minimal number of folds for selecting a window.
    :param min_n_windows : minimal number of selected windows required to select a feature.
    '''
    #groups = [['T', 'TT', 'TTT'], ['CTT', 'TCT', 'TCC', 'TTC', 'TTC', 'CTC'], ['CT', 'TC', 'C'], ['G'], ['CAG', 'AG'], ['GG', 'GC', 'TA']]
    groups = [['T', 'TT', 'TTT'], ['TCT', 'TTC', 'CTT', 'CT', 'TC'], ['G', 'GG', 'TA'], ['C'], ['CAG', 'AG'], ['A', 'GA', 'CG']]
    configuration_name, feature_names, experiment = results
    importances, pdp, assignment = {}, {}, {}
    windows_by_kmer = {}
    atomic_windows_by_kmer = {}
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            cur_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            return
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            cur_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            return
        cur_assignment = lib.interpretation.classify_partial_dependence_using_derivative(cur_pdp)
        cur_importances = lib.tools.get_feature_importances(cur_result, normalize = True)
        cur_importances = lib.tools.average_importances(cur_importances)
        importances[dataset_name] = cur_importances
        pdp[dataset_name] = cur_pdp
        assignment[dataset_name] = cur_assignment
        
        cur_windows_by_kmer = {}
        cur_n_useable_windows = {}
        for feature_name, feature_importance in cur_importances.iteritems() :
            if not 'window' in feature_name :
                continue
            args = feature_name.split('-')
            kmer, window = args[1], args[3]
            args = window.split('_')
            start, stop = int(args[1]), int(args[2])
            feature_importance, n_folds = feature_importance
            if not kmer in cur_windows_by_kmer :
                cur_windows_by_kmer[kmer] = []
                cur_n_useable_windows[kmer] = 0
            item = ((start, stop), feature_importance, cur_assignment[feature_name], n_folds, dataset_name)
            cur_windows_by_kmer[kmer].append(item)

            if not kmer in atomic_windows_by_kmer :
                atomic_windows_by_kmer[kmer] = {}
        windows_by_kmer[dataset_name] = cur_windows_by_kmer

        for kmer in cur_windows_by_kmer :
            window_list = cur_windows_by_kmer.get(kmer, [])
            atomic_window_list = _atomize_window_list(window_list)
            for atomic_window, atomic_effect, atomic_n_folds in atomic_window_list :
                if not atomic_window in atomic_windows_by_kmer[kmer] :
                    atomic_windows_by_kmer[kmer][atomic_window] = []
                atomic_windows_by_kmer[kmer][atomic_window].append((atomic_effect, atomic_n_folds, dataset_name))
        
        cur_interesting_kmers = cur_windows_by_kmer.keys()
        print '[i] Dataset: %s' % dataset_name
        print '    [i] Total k-mers: %d' % len(cur_n_useable_windows)
        print '    ', cur_interesting_kmers

    selected_kmers = []
    for kmer in atomic_windows_by_kmer :
        n_passing_windows = 0
        n_passing_windows_max = 0
        window_list = atomic_windows_by_kmer[kmer]
        for window, entries in window_list.iteritems() :
            
            verdict = False

            entries = [entry for entry in entries if entry[1] >= min_n_folds]
            positive_entries = [entry for entry in entries if entry[0] > 0]
            negative_entries = [entry for entry in entries if entry[0] < 0]
            positive_entries.sort(key = lambda x : +x[0])
            negative_entries.sort(key = lambda x : -x[0])
            values, values_max = [], []
            if len(positive_entries) >= atomic_support :
                value = positive_entries[-atomic_support][0]
                values.append(value)
            if len(negative_entries) >= atomic_support :
                value = negative_entries[-atomic_support][0]
                values.append(value)
            
            if len(positive_entries) >= max_atomic_support and not math.isinf(max_atomic_support) :
                value = positive_entries[-max_atomic_support][0]
                values_max.append(value)
            if len(negative_entries) >= max_atomic_support and not math.isinf(max_atomic_support) :
                value = negative_entries[-max_atomic_support][0]
                values_max.append(value)
            values = np.abs(np.array(values))
            values_max = np.abs(np.array(values_max))
            if np.any(values >= min_importance) :
                n_passing_windows += 1
                verdict = True
            if np.any(values_max >= min_importance) :
                n_passing_windows_max += 1
            if kmer == 'CAG' :
                print verdict, window, entries
        if kmer == 'CAG' :
            print n_passing_windows, n_passing_windows_max, min_n_windows
        if n_passing_windows >= min_n_windows and n_passing_windows_max < min_n_windows :
            selected_kmers.append(kmer)

    n_interesting_kmers = len(selected_kmers)
    print '[i] Interesting kmers: %d' % n_interesting_kmers

    if n_interesting_kmers <= 12 :
        n_rows, n_cols = 6, 4
    elif n_interesting_kmers <= 16 :
        n_rows, n_cols = 4, 4
    elif n_interesting_kmers <= 20 :
        n_rows, n_cols = 5, 4
    elif n_interesting_kmers <= 24 :
        n_rows, n_cols = 6, 4
    elif n_interesting_kmers <= 30 :
        n_rows, n_cols = 6, 5
    elif n_interesting_kmers <= 35 :
        n_rows, n_cols = 7, 5

    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    zero_y = np.zeros((sequence_length, ), dtype = np.float)
    kmer_overall_max = {}

    for kmer in selected_kmers :
        overall_vmax = -float('inf')
        for dataset_name, dataset_label in zip(dataset_names, dataset_labels) :
            cur_windows_by_kmer = windows_by_kmer[dataset_name]
            window_importances = np.zeros((sequence_length, ))
            window_counts = np.zeros((sequence_length, ), dtype = np.int)
            window_list = cur_windows_by_kmer.get(kmer, [])
            for window, importance, assignment, n_folds, dataset_name in window_list :
                start, stop = window
                effect = importance
                if assignment == 'Dec' :
                    effect = -effect
                window_importances[start:stop] += effect
                window_counts[start:stop] += 1
            window_counts[window_counts == 0] = 1
            window_importances /= window_counts
            vmax = np.nanmax(np.abs(window_importances))
            overall_vmax = max(overall_vmax, vmax)
        kmer_overall_max[kmer] = overall_vmax

    def _kmer_group_cmp(a, b) :
        '''
        Compare two k-mers a and b based on their groups.
        :param a, b : kmers to be compared.
        '''
        n_groups = len(groups)
        a_gid, b_gid = n_groups, n_groups
        for id, group in enumerate(groups) :
            if a in group :
                a_gid = id
            if b in group :
                b_gid = id
        if a_gid < b_gid :
            return -1
        elif a_gid > b_gid :
            return +1
        elif kmer_overall_max[a] < kmer_overall_max[b] :
            return +1
        else :
            return -1

    selected_kmers.sort(cmp = _kmer_group_cmp)

    for kmer in selected_kmers :
        cur = ax[row][col]
        for dataset_name, dataset_label in zip(dataset_names, dataset_labels) :
            cur_windows_by_kmer = windows_by_kmer[dataset_name]
            window_importances = np.zeros((sequence_length, ))
            window_counts = np.zeros((sequence_length, ), dtype = np.int)
            window_list = cur_windows_by_kmer.get(kmer, [])
            for window, importance, assignment, n_folds, dataset_name in window_list :
                start, stop = window
                effect = importance
                if assignment == 'Dec' :
                    effect = -effect
                window_importances[start:stop] += effect
                window_counts[start:stop] += 1
            window_counts[window_counts == 0] = 1
            window_importances /= window_counts
            if len(window_list) > 0 :
                cur.fill_between(x, window_importances, zero_y, color = '0.1', alpha = 0.15)
        overall_vmax = kmer_overall_max[kmer]
        cur.axhline(0, color = 'k', linewidth = 1.0, linestyle = '--')
        cur.axhline(min_importance, color = 'b', linewidth = 1.0, linestyle = '--')
        cur.axhline(-min_importance, color = 'r', linewidth = 1.0, linestyle = '--')
        cur.set_xlim(-200, 0)
        cur.set_ylim(-overall_vmax * 1.05, +overall_vmax * 1.05)
        yticks = [-overall_vmax * 1.05, 0, +overall_vmax * 1.05]
        cur.set_yticks(yticks)
        cur.set_yticklabels(['%.2f' % tick for tick in yticks])
        cur.set_title(kmer, fontweight = 'bold', fontsize = 11)
        
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_interesting_kmers :
            cur.set_xlabel('Position', fontsize = 13)
            cur.set_xticks([-200, -150, -100, -50, 0])
        else :
            cur.set_xticklabels([])

        if col == 0 :
            cur.set_ylabel('Feature effect', fontsize = 13)

        col += 1
        if col == n_cols :
            col = 0
            row += 1

    rewrite = {'native' : 'N', 'viral_native_dsRNA_all' : 'D', 'viral_native_ssRNA_positive' : 'S', 'viral_native_RTV' : 'R', 'human_native_5utr' : '5', 'human_native_3utr' : '3'}

    row, col = 0, 0
    for kmer in selected_kmers :
        cur_atomic_windows = atomic_windows_by_kmer[kmer]
        positive_consensus, negative_consensus = [], []
        for window in cur_atomic_windows :
            window_list = cur_atomic_windows[window]
            window_list = [entry for entry in window_list if entry[1] >= min_n_folds]
            positive_entries = [entry for entry in window_list if entry[0] > 0]
            negative_entries = [entry for entry in window_list if entry[0] < 0]
            positive_entries.sort(key = lambda x : +x[0])
            negative_entries.sort(key = lambda x : -x[0])
            if len(positive_entries) >= atomic_support :
                consensus_datasets = [item[2] for item in positive_entries[:-(atomic_support - 1)]]
                positive_consensus.append((window, positive_entries[-atomic_support][0], consensus_datasets))
            if len(negative_entries) >= atomic_support :
                consensus_datasets = [item[2] for item in negative_entries[:-(atomic_support - 1)]]
                negative_consensus.append((window, negative_entries[-atomic_support][0], consensus_datasets))
        
        cur = ax[row][col]
        positive_importances, negative_importances = np.zeros((sequence_length, )), np.zeros((sequence_length, ))
        for window, effect, names in positive_consensus :
            start, stop = window
            positive_importances[start : stop] = effect
            #names = [rewrite[name] for name in names]
            #cur.text((start + stop) / 2.0 + atg_offset, effect, ' '.join(names))
        for window, effect, _ in negative_consensus :
            start, stop = window
            negative_importances[start : stop] = effect
        _plot_consensus_islands(cur, x, positive_importances, color = 'b', linewidth = 1.3)
        _plot_consensus_islands(cur, x, negative_importances, color = 'r', linewidth = 1.3)
        col += 1
        if col == n_cols :
            col = 0
            row += 1

    while row < n_rows and col < n_cols :
        cur = ax[row, col]
        f.delaxes(cur)
        col += 1
        if col == n_cols :
            col = 0
            row += 1

    f.set_size_inches(19, 10, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.30)
    plot.show()

def plot_ddG_accessibility_for_datasets(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], sequence_length = 330, atg_offset = -270, window_settings = [(10, 5), (20, 10), (30, 10), (40, 10), (50, 10), (60, 10), (70, 10), (80, 10)]) :
    '''
    Plot accessibility correlations for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param window_settings : settings for windows (window_size, window_step)
    '''
    colors = ['r', 'b', 'g', 'k']
    styles = ['-', ':', '-.', '--']

    n_colors, n_styles = len(colors), len(styles)

    n_datasets = len(dataset_names)
    dataset_correlations = []
    for dataset_name in dataset_names :
        cur_dataset_correlations = {}
        for window_size, window_step in window_settings :
            filename = './datasets/current/%s/ddG-open-windows%d_step%d.out' % (dataset_name, window_size, window_step)
            if os.path.exists(filename) :
                cur_correlations = lib.reader.read_pickle(filename)
            else :
                print '[!] Missing (correlations): %s' % filename
                return
            window = (window_size, window_step)
            cur_dataset_correlations[window] = cur_correlations
        dataset_correlations.append(cur_dataset_correlations)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    for cur_correlations, cur_label in zip(dataset_correlations, dataset_labels) :
        cur = ax[row, col]
        index = 0
        for window_size, window_step in window_settings :
            window = (window_size, window_step)
            corr = np.zeros((sequence_length, ), dtype = np.float)
            corr_cnt = np.zeros((sequence_length, ), dtype = np.int)
            for window, coef_r, pvalue, _ in cur_correlations[window] :
                start, stop = window
                for i in range(start, stop) :
                    corr[i] += coef_r
                    corr_cnt[i] += 1
            corr /= corr_cnt
            cur_color = colors[index % n_colors]
            cur_style = styles[index / n_colors]
            cur.plot(x, corr, label = '(%d, %d)' % (window_size, window_step), color = cur_color, linestyle = cur_style)
            index += 1
        cur.set_xlim(atg_offset, atg_offset + sequence_length)
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            pass
        else :
            cur.set_xticklabels([])
        if col == 0 and row == 0 :
            cur.legend(ncol = 3, prop = {'size' : 7}, loc = 'best')
        if col == 0 :
            cur.set_ylabel('Correlation', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    plot.show()

def plot_accessibility_ddG_regression_results_for_datasets(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), window_settings = [(10, 5), (20, 10), (30, 10), (40, 10), (50, 10), (60, 10), (70, 10), (80, 10)]) :
    '''
    Plot accessibility regression results for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param window_settings : settings for windows (window_size, window_step)
    '''
    n_settings = len(window_settings)
    n_datasets = len(dataset_names)
    configuration_name, experiment = results
    dataset_results = []
    for dataset_name in dataset_names :
        cur_dataset_results= {}
        file_path = '../results/regression/%s/%s/gbrf' % (configuration_name, dataset_name)
        for window_size, window_step in window_settings :
            filename = '%s/ddG_size%d_step%d-%s-final.out' % (file_path, window_size, window_step, experiment)
            if os.path.exists(filename) :
                cur_results = lib.reader.read_pickle(filename)
            else :
                print '[!] Missing (results): %s' % filename
                return
            window = (window_size, window_step)
            cur_dataset_results[window] = cur_results
        dataset_results.append(cur_dataset_results)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    width = 0.8
    x = np.array(range(n_settings)) * width + width / 2.0
    labels = ['(%d, %d)' % (window_size, window_step) for window_size, window_step in window_settings]
    for cur_dataset_results, cur_label in zip(dataset_results, dataset_labels) :
        cur = ax[row, col]
        performance = np.zeros((n_settings, ), dtype = np.float)
        index = 0
        for window_size, window_step in window_settings :
            window = (window_size, window_step)
            cur_result = cur_dataset_results[window]
            performance[index] = cur_result.overall_test_score['r2']
            index += 1
        cur.bar(x, performance, color = 'b', alpha = 0.45)
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        cur.set_xlim(0, np.max(x) + width + width / 2.0)
        cur.set_xticks(x + width / 2.0)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xticklabels(labels, rotation = 90)
        else :
            cur.set_xticklabels([])
        if col == 0 and row == 0 :
            cur.legend(ncol = 3, prop = {'size' : 7}, loc = 'best')
        if col == 0 :
            cur.set_ylabel('$R^2$', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    plot.show()

def generate_context_samples(dataset, seed_sequence, left_extension = 6, right_extension = 6, start_region = 0, end_region = None) :
    '''
    Generates context samples for a given seed sequence and region.
    :param dataset : a list of sequences from which context samples should be generated.
    :param seed_sequence : seed sequence that should be used to create the motif.
    :param left_extension : how much the seed should be extended on the left.
    :param right_extension : how much the seed should be extended on the right.
    :param start_region : beginning of the region where the seed should be located.
    :param stop_region : end of the region where the seed should be located.
    '''
    samples = []
    if end_region is None :
        end_region = len(dataset[0].sequence)
    for seq in dataset :
        sequence = seq.sequence
        pos = sequence.find(seed_sequence, start_region, end_region)
        while pos > 0 and pos < end_region :
            context_start, context_stop = pos - left_extension, pos + len(seed_sequence) + right_extension
            context = sequence[context_start : context_stop]
            name = 'Seq%d_pos%d-%d' % (seq.index, context_start, context_stop)
            samples.append((name, context))
            pos = sequence.find(seed_sequence, pos + 1, end_region)
    return samples

def generate_sequence_enrichment(dataset_names, seed_sequence, top_percent = 0.1, left_extension = 6, right_extension = 6, start_region = 0, end_region = None) :
    '''
    Generates context samples for a given seed sequence, region and a list of datasets.
    :param dataset_names : names of datasets, which should be scanned for the presence of a motif.
    :param seed_sequence : seed sequence that should be used to create the motif.
    :param top_percent : only use the given fraction of samples with highest IRES activity when looking for an enrichment.
    :param left_extension : how much the seed should be extended on the left.
    :param right_extension : how much the seed should be extended on the right.
    :param start_region : beginning of the region where the seed should be located.
    :param stop_region : end of the region where the seed should be located.
    '''
    if isinstance(dataset_names, basestring) :
        dataset_names = [dataset_names]
    union_ds = {}
    for dataset_name in dataset_names :
        ds = lib.persistent.datasets[dataset_name]
        for seq in ds :
            union_ds[seq.index] = seq
    del ds
    if end_region is None :
        end_region = len(seq.sequence)
    union_ds = union_ds.values()
    count_all = len(union_ds)
    negative_union_ds = [seq for seq in union_ds if seq.ires_activity == lib.tools.MINIMAL_IRES_ACTIVITY]
    union_ds = [seq for seq in union_ds if seq.ires_activity > lib.tools.MINIMAL_IRES_ACTIVITY]
    union_ds.sort(key = lambda seq : -seq.ires_activity)
    count_positive, count_negative = len(union_ds), len(negative_union_ds)
    count_top_percent = int(count_positive * top_percent) + 1
    union_ds = union_ds[:count_top_percent]
    print '    [i] Total: %d' % count_all
    print '    [i] Positive: %d, top %.2f percent: %d' % (count_positive, top_percent * 100, count_top_percent)
    print '    [i] Negative: %d' % count_negative

    foreground_samples = generate_context_samples(union_ds, seed_sequence, left_extension, right_extension, start_region, end_region)
    background_samples = generate_context_samples(negative_union_ds, seed_sequence, left_extension, right_extension, start_region, end_region) 
    print '    [i] FG: %d, BG: %d' % (len(foreground_samples), len(background_samples))

    if len(foreground_samples) > 1 and len(background_samples) > 1 :
        try :
            logo = lib.tsl.make_logo(foreground_samples, background_samples)
        except :
            logo = None
    else :
        logo = None
    return foreground_samples, background_samples, logo

def generate_sequence_enrichment_for_parameters(dataset_names, seed_sequence, top_percents = [0.05, 0.1, 0.15, 0.2, 0.25, 0.5, 1.0], regions = [(240, 270), (240, 250), (240, 260), (250, 270), (250, 260), (260, 270)], left_extension = 6, right_extension = 6) :
    n_retries = 5
    results = []
    for top_percent in top_percents :
        for start_region, end_region in regions :
            print '[i] Processing top percent = %.2f, region (%d, %d)' % (top_percent, start_region, end_region)
            fg, bg, logo = generate_sequence_enrichment(dataset_names, seed_sequence, top_percent = top_percent, left_extension = left_extension, right_extension = right_extension, start_region = start_region, end_region = end_region)
            result = SimpleNamespace()
            result.dataset_names = dataset_names
            result.seed_sequence = seed_sequence
            result.top_percent = top_percent
            result.start_region = start_region
            result.end_region = end_region
            result.left_extension = left_extension
            result.right_extension = right_extension
            result.logo = logo
            result.n_fg = len(fg)
            result.n_bg = len(bg)
            results.append(result)
    return results

def plot_generated_sequence_enrichments(results) :
    '''
    Plot motif logos generated by 'generate_sequence_enrichment_for_parameters'.
    :param results : a list of results to be plotted.
    '''
    results = [result for result in results if result.logo is not None]
    n_results = len(results)
    print '[i] Got %d results' % n_results
    
    if n_results <= 10 :
        n_rows, n_cols = 2, 5
    elif n_results <= 20 :
        n_rows, n_cols = 4, 5
    elif n_results <= 30 :
        n_rows, n_cols = 5, 6
    elif n_results <= 35 :
        n_rows, n_cols = 5, 7
    elif n_results <= 40 :
        n_rows, n_cols = 6, 7
    
    row, col = 0, 0
    f, ax = plot.subplots(n_rows, n_cols)
    for result in results :
        cur = ax[row][col]
        cur.imshow(result.logo, aspect = 'auto')
        title = 'FG/BG: %d/%d\n%.2f%% (%d, %d)' % (result.n_fg, result.n_bg, 100 * result.top_percent, result.start_region, result.end_region) 
        cur.set_title(title, fontsize = 9)
        cur.set_axis_off()
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(20, 15, forward = True)
    f.tight_layout()
    f.subplots_adjust(top = 0.95)
    plot.show()

def select_sequences_with_seed(dataset_names, seed_sequence, top_percent = 0.1, start_region = 0, end_region = None, positive_only = True) :
    '''
    Generates context samples for a given seed sequence, region and a list of datasets.
    :param dataset_names : names of datasets, which should be scanned for the presence of a motif.
    :param seed_sequence : seed sequence that should be used to create the motif.
    :param top_percent : only use the given fraction of samples with highest IRES activity when looking for an enrichment.
    :param start_region : beginning of the region where the seed should be located.
    :param stop_region : end of the region where the seed should be located.
    '''
    if isinstance(dataset_names, basestring) :
        dataset_names = [dataset_names]
    union_ds = {}
    for dataset_name in dataset_names :
        ds = lib.persistent.datasets[dataset_name]
        for seq in ds :
            union_ds[seq.index] = seq
    del ds
    if end_region is None :
        end_region = len(seq.sequence)
    union_ds = union_ds.values()
    count_all = len(union_ds)
    if positive_only :
        union_ds = [seq for seq in union_ds if seq.ires_activity > lib.tools.MINIMAL_IRES_ACTIVITY]
    union_ds.sort(key = lambda seq : -seq.ires_activity)
    count_positive = len(union_ds)
    count_top_percent = int(count_positive * top_percent) + 1
    union_ds = union_ds[:count_top_percent]
    print '    [i] Total: %d' % count_all
    print '    [i] Positive: %d, top %.2f percent: %d' % (count_positive, top_percent * 100, count_top_percent)
   
    foreground_samples, background_samples = [], []
    for seq in union_ds :
        sequence = seq.sequence
        sequence = sequence[start_region : end_region]
        if seed_sequence in sequence :
            foreground_samples.append(seq)
        else :
            background_samples.append(seq)

    print '    [i] FG: %d, BG: %d' % (len(foreground_samples), len(background_samples))

    return foreground_samples, background_samples

def plot_splicing_scores_for_sequences(set_a, set_b, labels = ['+ CAG', '- CAG'], ylabel = 'Splicing score', title = 'Splicing in dsRNA viruses') :
    '''
    '''
    n_a, n_b = len(set_a), len(set_b)
    data = [[a.splicing_score for a in set_a], [b.splicing_score for b in set_b]]
    z, p = scipy.stats.mannwhitneyu(data[0], data[1])
    p_value = p * 2
    s = stars(p_value)
    print '[i] P-value: %g, %s' % (p_value, s)

    f, ax = plot.subplots(1, 1)
    bp = ax.boxplot(data, widths = [0.6, 0.6])

    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
    colors = bmap.mpl_colors

    for i in range(0, len(bp['boxes'])):
       bp['boxes'][i].set_color(colors[i])
       # we have two whiskers!
       bp['whiskers'][i * 2].set_color(colors[i])
       bp['whiskers'][i * 2 + 1].set_color(colors[i])
       bp['whiskers'][i * 2].set_linewidth(2)
       bp['whiskers'][i * 2 + 1].set_linewidth(2)
       # top and bottom fliers
       # (set allows us to set many parameters at once)
       bp['fliers'][i].set(markerfacecolor = colors[i],
                       marker = 'o', alpha = 0.75, markersize = 6,
                       markeredgecolor = 'none')
       bp['medians'][i].set_color('black')
       bp['medians'][i].set_linewidth(3)
       # and 4 caps to remove
       for c in bp['caps']:
           c.set_linewidth(0)

       box = bp['boxes'][i]
       box.set_linewidth(0)
       boxX = []
       boxY = []
       for j in range(5):
           boxX.append(box.get_xdata()[j])
           boxY.append(box.get_ydata()[j])
           boxCoords = zip(boxX,boxY)
           boxPolygon = matplotlib.patches.Polygon(boxCoords, facecolor = colors[i], linewidth=0)
           ax.add_patch(boxPolygon)

    ax.xaxis.set_ticks_position('none')
    y_max = np.max(np.concatenate((data[0], data[1])))
    y_min = np.min(np.concatenate((data[0], data[1])))
    _ymin, _ymax = ax.get_ylim()
    ax.set_ylim(_ymin, _ymax * 1.2)
    ax.annotate("", xy = (1, y_max * 1.05), xycoords = 'data', xytext = (2, y_max * 1.05), textcoords = 'data', arrowprops = dict(arrowstyle = "-", ec = '#aaaaaa', connectionstyle = "bar, fraction=0.2"))
    ax.text(1.5, y_max * 1.05 + abs(y_max - y_min) * 0.12, stars(p_value), horizontalalignment = 'center', verticalalignment = 'center')
        
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')

    ax.set_xticklabels(labels, rotation = 0)
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    ax.set_ylabel(ylabel, fontsize = 13)
    f.set_size_inches(4, 4.7, forward = True)
    f.tight_layout()
    plot.show()

def plot_gc_content(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], trim_left = 66, trim_right = 52, save_filename=None) :
    title = 'GC content'
    ylabel = '%GC'
    #title = 'GpC content'
    composition = {}
    for dataset_name, dataset_label in zip(dataset_names, dataset_labels) :
        cur_composition = []
        seqs = lib.persistent.datasets[dataset_name]
        for seq in seqs :
            seq = seq.sequence
            length = len(seq)
            seq = seq[trim_left:length - trim_right]
            length = len(seq)
            #cnt = 0
            #for i in range(length - 1) :
            #    if seq[i] == 'G' and seq[i + 1] == 'C' :
            #        cnt += 1
            chars = np.array([c for c in seq])
            cg = np.logical_or(chars == 'C', chars == 'G')
            #cg = chars == 'G'
            cg_count = np.sum(cg, dtype=np.int)
            #for i in range(len(seq) - 1) :
            #    if seq[i] == 'G' and seq[i + 1] == 'G' :
            #        cg_count += 1
            freq = cg_count / float(length) * 100.0
            #freq = cnt / (float(length) - 1)
            cur_composition.append(freq)
        composition[dataset_name] = cur_composition
    
    for dataset_name in dataset_names :
        comp = np.mean(composition[dataset_name])
        comp_std = np.std(composition[dataset_name])
        print '%s : %.2f +/- %.2f%%' % (dataset_name, comp, comp_std)
    n_datasets = len(dataset_names)
    for i in range(n_datasets) :
        ds_a = dataset_names[i]
        for j in range(i + 1, n_datasets) :
            ds_b = dataset_names[j]
            #res = scipy.stats.ttest_ind(composition[ds_a], composition[ds_b], equal_var = False)
            res = scipy.stats.median_test(composition[ds_a], composition[ds_b])
            res = scipy.stats.kruskal(composition[ds_a], composition[ds_b])
            res = scipy.stats.ranksums(composition[ds_a], composition[ds_b])
            #res = scipy.stats.mannwhitneyu(composition[ds_a], composition[ds_b])
            print '%s vs %s : ' % (ds_a, ds_b), res
    data = [composition[dataset_name] for dataset_name in dataset_names]
    
    import brewer2mpl
    colors1 = brewer2mpl.get_map('Blues', 'sequential', 5).mpl_colors
    colors2 = brewer2mpl.get_map('Set2', 'qualitative', 8).mpl_colors
    del colors2[2]
    del colors2[0]
    colors = [colors2[-1]] + colors1[1:4] + colors2
    del colors[1]
    
    f, ax = plot.subplots(1, 1)
    bp = ax.boxplot(data)
    ax.set_xticklabels(dataset_labels, rotation = 60)
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    ax.set_ylabel(ylabel)
    
    for i in range(0, len(bp['boxes'])):
       bp['boxes'][i].set_color(colors[i])
       # we have two whiskers!
       bp['whiskers'][i * 2].set_color(colors[i])
       bp['whiskers'][i * 2 + 1].set_color(colors[i])
       bp['whiskers'][i * 2].set_linewidth(2)
       bp['whiskers'][i * 2 + 1].set_linewidth(2)
       # top and bottom fliers
       # (set allows us to set many parameters at once)
       bp['fliers'][i].set(markerfacecolor = colors[i],
                       marker = 'o', alpha = 0.75, markersize = 6,
                       markeredgecolor = 'none')
       bp['medians'][i].set_color('black')
       bp['medians'][i].set_linewidth(3)
       # and 4 caps to remove
       for c in bp['caps']:
           c.set_linewidth(0)

       box = bp['boxes'][i]
       box.set_linewidth(0)
       boxX = []
       boxY = []
       for j in range(5):
           boxX.append(box.get_xdata()[j])
           boxY.append(box.get_ydata()[j])
           boxCoords = zip(boxX,boxY)
           boxPolygon = matplotlib.patches.Polygon(boxCoords, facecolor = colors[i], linewidth=0)
           ax.add_patch(boxPolygon)
    
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('none')
    f.set_size_inches(7, 4.7, forward = True)
    f.tight_layout()
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

def plot_unique_features_partial_dependence(chosen_dataset_name = 'viral_native_RTV', chosen_feature_list = ['CC', 'CTCC', 'CCCC', 'TCCC', 'TCCT', 'CCTC', 'CTCT', 'TTCC', 'CCCT'], dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [4, 4, 4, 4, 4], min_kmer_length = 1, min_n_folds = 10, min_importance = 0.1, title = 'Retroviruses unique features') :
    '''
    Plot unique features for a given dataset.
    :param dataset_name : name of the dataset, for which features should be plotted.:
    :param feature_list : a list of features that should be plotted.
    '''
    configuration_name, feature_names, experiment = results
    importances, pdp, assignment = {}, {}, {}
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            dataset_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            dataset_result = None
            break
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            dataset_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            dataset_pdp = None
            break
        dataset_importances = lib.tools.get_feature_importances(dataset_result, normalize = True)
        dataset_importances = lib.tools.average_importances(dataset_importances)
        importances[dataset_name] = {key : value for key, value in dataset_importances.iteritems() if not 'window' in key}
        pdp[dataset_name] = dataset_pdp
        assignment[dataset_name] = lib.interpretation.classify_partial_dependence_using_derivative(dataset_pdp)
  
    dataset_selected_features = {}

    n_datasets = len(dataset_names)
    overall_importances = {}
    for dataset_name in dataset_names : # filter features in datasets
        dataset_selected_features[dataset_name] = []
        cur_assignment = assignment[dataset_name]
        cur_importances = importances[dataset_name]
        n_pre_filtering = len(cur_importances)
        importances[dataset_name] = cur_importances
        n_post_filtering = len(cur_importances)
        print '[i] %s : %d / %d' % (dataset_name, n_post_filtering, n_pre_filtering)
        for feature, value in cur_importances.iteritems() :
            if not feature in overall_importances :
                overall_importances[feature] = []
            if value[0] >= min_importance and value[1] >= min_n_folds and cur_assignment[feature] is not None :
                overall_importances[feature].append(value[0])
                dataset_selected_features[dataset_name].append(feature)
    print ''
    print '[i] Union features: %d' % len(overall_importances)
    for i in range(n_datasets) :
        count = len([feature for feature, value in overall_importances.iteritems() if len(value) >= i + 1])
        print '    [i] %d datasets : %d' % (i + 1, count)

    unique_features = [feature for feature, value in overall_importances.iteritems() if len(value) == 1]
    selected_features = [feature for feature in unique_features if feature in dataset_selected_features[chosen_dataset_name]]
    chosen_features = [feature for feature in selected_features if feature.split('-')[1] in chosen_feature_list]
    print '[i] Unique features: ', len(unique_features)
    print '[i] Selected features: ', len(selected_features)
    print '[i] Chosen features: ', chosen_features

    pairs = [(importances[chosen_dataset_name][feature_name][0], feature_name) for feature_name in chosen_features]
    pairs.sort(key = lambda x : -x[0])
    n_pairs = len(pairs)
    effects = np.zeros((n_pairs, ), dtype = np.float)
    plot_feature_names = []
    i = 0
    for importance, feature_name in pairs :
        ass = assignment[chosen_dataset_name][feature_name]
        if ass == 'Dec' :
            importance = -importrance
        effects[i] = importance
        feature_name = feature_name.split('-')[1]
        feature_name = ''.join([c if c != 'T' else 'U' for c in feature_name])
        plot_feature_names.append(feature_name)
        i += 1
    width = 0.5
    x = np.array(range(n_pairs)) * width * 1.3 + width / 2.0
    f, ax = plot.subplots(1, 1)
    cmap = plot.cm.RdBu
    norm = matplotlib.colors.Normalize(vmin = -1, vmax = +1)
    ax.set_xticks(x + width / 2.0)
    ax.set_xticks([])
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    for _x, _y, feature_name in zip(x, effects, plot_feature_names) :
        cur_color = cmap(norm(_y))
        ax.bar([_x], [_y], width, color = cur_color, ec = cur_color)
        _x += width / 2.0
        _y += 0.01
        ax.text(_x, _y, feature_name, ha = 'center', fontweight = 'bold')
    ax.set_xlim(0, np.max(x) + width / 2.0 + width)
    ax.set_ylim(0, 0.74)
    #ax.set_xticklabels(plot_feature_names, rotation = 90)
    ax.set_xlabel('Feature', fontsize = 13)
    ax.set_ylabel('Feature effect', fontsize = 13)
    for tick in ax.xaxis.get_major_ticks() : 
        tick.tick1On = False 
        tick.tick2On = False 
    
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    yticks = ax.get_yticks()
    _ymin, _ymax = ax.get_ylim()
    for tick in yticks :
        ax.axhline(tick, linewidth = 1, color = 'w')
    ax.set_ylim(_ymin, _ymax)
    
    #f.set_size_inches(7, 3.5, forward = True)
    f.set_size_inches(4.0, 3.5, forward = True)
    f.tight_layout()
    plot.show()

def plot_positional_features_importance_for_datasets_consensus(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], is_counted = [False, True, True, True, True, True], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [4, 4, 4, 4, 4, 4], min_kmer_length = 1, sequence_length = 300, atg_offset = -270, min_importance = 0.1, min_n_folds = 10, min_n_windows = 1, atomic_support = 3, max_atomic_support = float('inf'), plot_marks = False, page = 0) :
    '''
    Plot positional feature importances (with effect directionality) for a list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param min_importance : minimal importance for selecting a window.
    :param min_n_folds : minimal number of folds for selecting a window.
    :param min_n_windows : minimal number of selected windows required to select a feature.
    '''
    groups = [['T', 'TT', 'TTT', 'TTTT'], ['TCT', 'TTC', 'CTT', 'CT', 'TC'], ['G', 'TA'], ['C'], ['CAG'], ['AG', 'GA', 'GG']]
    groups += [['TCTT', 'TTTC'], ['C', 'CC', 'CCC', 'CCT', 'TCC', 'CTCC', 'CTC'], ['A', 'AA'], ['AT', 'GTT', 'TGT']]
    #groups += [['CC', 'CCC', 'CCT', 'ACC', 'CTCC', 'CCTC', 'TCC', 'CTC'], ['ATT', 'TAT', 'TTA', 'GTT', 'TGT'], ['GTC', 'TGC'], ['AAG', 'AAT'], ['AGC', 'TAG', 'AGA', 'CTA']]
   
    n_datasets = len(dataset_names)
    configuration_name, feature_names, experiment = results
    importances, assignment = {}, {}
    for dataset_name, kmer_length in zip(dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            cur_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            return
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            cur_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            return
        cur_assignment = lib.interpretation.classify_partial_dependence_using_derivative(cur_pdp)
        cur_importances = lib.tools.get_feature_importances(cur_result, normalize = True)
        cur_importances = lib.tools.average_importances(cur_importances)
        importances[dataset_name] = cur_importances
        assignment[dataset_name] = cur_assignment

    is_dataset_counted = {dataset_name : is_cur_counted for dataset_name, is_cur_counted in zip(dataset_names, is_counted)}

    windows_by_kmer = {}
    for dataset_name in dataset_names : 
        cur_importances, cur_assignment = importances[dataset_name], assignment[dataset_name]
        cur_windows_by_kmer, cur_n_useable_windows = {}, {}
        for feature_name, feature_importance in cur_importances.iteritems() :
            if not 'window' in feature_name :
                continue
            args = feature_name.split('-')
            kmer, window = args[1], args[3]
            args = window.split('_')
            start, stop = int(args[1]), int(args[2])
            feature_importance, n_folds = feature_importance
            if not kmer in cur_windows_by_kmer :
                cur_windows_by_kmer[kmer] = []
                cur_n_useable_windows[kmer] = 0
            item = ((start, stop), feature_importance, cur_assignment[feature_name], n_folds, dataset_name)
            cur_windows_by_kmer[kmer].append(item)
        windows_by_kmer[dataset_name] = cur_windows_by_kmer
        
        cur_interesting_kmers = cur_windows_by_kmer.keys()
        print '[i] Dataset: %s' % dataset_name
        print '    [i] Total k-mers: %d' % len(cur_n_useable_windows)
        print '    ', cur_interesting_kmers

    atomic_windows_by_kmer = {}
    for dataset_name in dataset_names :
        cur_windows_by_kmer = windows_by_kmer[dataset_name]
        for kmer in cur_windows_by_kmer :
            if not kmer in atomic_windows_by_kmer :
                atomic_windows_by_kmer[kmer] = {}
            cur_atomic_windows = atomic_windows_by_kmer[kmer]
            window_list = cur_windows_by_kmer.get(kmer, [])
            atomic_window_list = _atomize_window_list(window_list)
            for atomic_window, atomic_effect, atomic_n_folds in atomic_window_list :
                if not atomic_window in atomic_windows_by_kmer[kmer] :
                    atomic_windows_by_kmer[kmer][atomic_window] = []
                cur_atomic_windows[atomic_window].append((atomic_effect, atomic_n_folds, dataset_name))
    del windows_by_kmer

    selected_kmers = []
    for kmer in atomic_windows_by_kmer :
        n_passing_windows, n_passing_windows_max = 0, 0
        window_list = atomic_windows_by_kmer[kmer]
        for window, entries in window_list.iteritems() :
            entries = [entry for entry in entries if entry[1] >= min_n_folds and is_dataset_counted[entry[-1]]]
            #entries = [entry for entry in entries if entry[1] >= min_n_folds]
            positive_entries = [entry for entry in entries if entry[0] > 0]
            negative_entries = [entry for entry in entries if entry[0] < 0]
            positive_entries.sort(key = lambda x : +x[0])
            negative_entries.sort(key = lambda x : -x[0])
            values, values_max = [], []
            if len(positive_entries) >= atomic_support :
                value = positive_entries[-atomic_support][0]
                values.append(value)
            if len(negative_entries) >= atomic_support :
                value = negative_entries[-atomic_support][0]
                values.append(value)
            
            if len(positive_entries) >= max_atomic_support and not math.isinf(max_atomic_support) :
                value = positive_entries[-max_atomic_support][0]
                values_max.append(value)
            if len(negative_entries) >= max_atomic_support and not math.isinf(max_atomic_support) :
                value = negative_entries[-max_atomic_support][0]
                values_max.append(value)
            values = np.abs(np.array(values))
            values_max = np.abs(np.array(values_max))
            if np.any(values >= min_importance) :
                n_passing_windows += 1
            if np.any(values_max >= min_importance) :
                n_passing_windows_max += 1
        if n_passing_windows >= min_n_windows and n_passing_windows_max < min_n_windows :
            selected_kmers.append(kmer)

    n_interesting_kmers = len(selected_kmers)
    print '[i] Interesting kmers: %d' % n_interesting_kmers
    
    positive_consensus, negative_consensus = {}, {}
    kmer_overall_max = {}
    for kmer in selected_kmers :
        cur_atomic_windows = atomic_windows_by_kmer[kmer]
        kmer_positive_consensus, kmer_negative_consensus = [], []
        vmax = -float('inf')
        for window in cur_atomic_windows :
            window_list = cur_atomic_windows[window]
            window_max = np.max([abs(entry[0]) for entry in window_list])
            window_list = [entry for entry in window_list if entry[1] >= min_n_folds and is_dataset_counted[entry[-1]]]
            positive_entries = [entry for entry in window_list if entry[0] > 0]
            negative_entries = [entry for entry in window_list if entry[0] < 0]
            positive_entries.sort(key = lambda x : +x[0])
            negative_entries.sort(key = lambda x : -x[0])
            if len(positive_entries) >= atomic_support :
                consensus_datasets = [item[2] for item in positive_entries[:-(atomic_support - 1)]]
                kmer_positive_consensus.append((window, positive_entries[-atomic_support][0], consensus_datasets))
            if len(negative_entries) >= atomic_support :
                consensus_datasets = [item[2] for item in negative_entries[:-(atomic_support - 1)]]
                kmer_negative_consensus.append((window, negative_entries[-atomic_support][0], consensus_datasets))
            vmax = max(vmax, window_max)
        positive_consensus[kmer], negative_consensus[kmer] = kmer_positive_consensus, kmer_negative_consensus
        kmer_overall_max[kmer] = vmax

    def _kmer_group_cmp(a, b) :
        '''
        Compare two k-mers a and b based on their groups.
        :param a, b : kmers to be compared.
        '''
        n_groups = len(groups)
        a_gid, b_gid = n_groups, n_groups
        for id, group in enumerate(groups) :
            if a in group :
                a_gid = id
            if b in group :
                b_gid = id
        if a_gid < b_gid :
            return -1
        elif a_gid > b_gid :
            return +1
        elif kmer_overall_max[a] < kmer_overall_max[b] :
            return +1
        else :
            return -1
    selected_kmers.sort(cmp = _kmer_group_cmp)

    #selected_kmers = selected_kmers[24:]
    #n_interesting_kmers = len(selected_kmers)

    n_pages = 1
    if n_interesting_kmers <= 16 :
        n_rows, n_cols = 9, 4
    else :
        n_pages = n_interesting_kmers / 16
        if n_interesting_kmers % 16 != 0 :
            n_pages += 1
        print '[!] Plotting page: %d' % (page + 1)
        selected_kmers = selected_kmers[page * 16 : (page + 1) * 16]
        selected_kmers = selected_kmers[8:]
        n_interesting_kmers = len(selected_kmers)
        n_rows, n_cols = 9, 4

    def _resize_y_axis(y_min, y_max) :
        MAX_DIFF = 0.4
        if y_min != 0 and y_max != 0 :
            diff = abs(abs(y_min) - abs(y_max)) / (abs(y_min) + abs(y_max))
            while diff > MAX_DIFF :
                if abs(y_min) < abs(y_max) :
                    y_min *= 1.1
                else :
                    y_max *= 1.1
                diff = abs(abs(y_min) - abs(y_max)) / (abs(y_min) + abs(y_max))
        return y_min, y_max

    import matplotlib.gridspec as gridspec
    import matplotlib.ticker as mtick
    gs_parent = gridspec.GridSpec(n_rows / 2, n_cols)
    gs_parent.update(left = 0.09, right = 0.99, top = 0.99, bottom = 0.01, wspace = 0.20, hspace = 0.25)
    #f, ax = plot.subplots(n_rows, n_cols)
    f = plot.figure()
    ax = np.zeros((n_rows, n_cols), dtype = np.object)
    for row in range(n_rows / 2) :
        for col in range(n_cols) :
            gs_child = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec = gs_parent[row, col], hspace = 0.12, height_ratios = [1.3, 2])
            ax_top, ax_bottom = plot.Subplot(f, gs_child[0]), plot.Subplot(f, gs_child[1])
            f.add_subplot(ax_top)
            f.add_subplot(ax_bottom)
            ax[2 * row, col], ax[2 * row + 1, col] = ax_top, ax_bottom
    plot.cm.RdBu.set_bad('w', alpha = 1.0)
    vmin, vmax = -1, +1
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)

    dataset_name_to_index = {}
    for i, dataset_name in enumerate(dataset_names) :
        dataset_name_to_index[dataset_name] = i

    for kmer in selected_kmers :
        cur_consensus = ax[row][col]
        cur_heatmap = ax[row + 1][col]
        matrix = np.zeros((n_datasets, sequence_length), dtype = np.float)
        marks = []
        cur_atomic_windows = atomic_windows_by_kmer[kmer]
        for window, window_list in cur_atomic_windows.iteritems() :
            start, stop = window
            for effect, n_folds, dataset_name in window_list :
                index = dataset_name_to_index[dataset_name]
                matrix[index, start : stop] = effect
                #if n_folds < min_n_folds or abs(effect) < min_importance :
                if n_folds < min_n_folds :
                    mark = ((start + stop) / 2.0 + atg_offset, index + 0.5)
                    marks.append(mark)
        cur_heatmap.imshow(matrix, interpolation = 'nearest', aspect = 'auto', cmap = plot.cm.RdBu, vmin = vmin, vmax = vmax, origin = 'lower', extent = [atg_offset, sequence_length + atg_offset, 0, n_datasets])
        cur_heatmap.set_ylim(0, n_datasets)
        if plot_marks :
            cur_heatmap.scatter([mark[0] for mark in marks], [mark[1] for mark in marks], s = 10, marker = 'x', color = 'k')
        
        #cur_heatmap.set_title(kmer, fontweight = 'bold', fontsize = 12)
        if row / 2 == n_rows - 1 or (row / 2 + 1) * n_cols + col + 1 > n_interesting_kmers and page == n_pages - 1 :
            cur_heatmap.set_xlabel('Position', fontsize = 13)
            cur_heatmap.set_xticks([-200, -150, -100, -50, 0])
        else :
            cur_heatmap.set_xticklabels([])

        if col == 0 :
            y = np.array(range(n_datasets)) + 0.5
            cur_heatmap.set_yticks(y)
            cur_heatmap.set_yticklabels(dataset_labels)
            for tick in cur_heatmap.yaxis.get_major_ticks() : 
                tick.tick1On = False 
                tick.tick2On = False
                tick.label.set_fontsize(12)
        else :
            cur_heatmap.set_yticks([])
        cur_heatmap.set_xlim(-200, 0)

        cur_consensus.spines['right'].set_visible(False)
        cur_consensus.spines['top'].set_visible(False)
        cur_consensus.yaxis.set_ticks_position('left')
        cur_consensus.xaxis.set_ticks_position('bottom')
        cur_consensus.patch.set_alpha(0.0)
        cur_consensus.spines['bottom'].set_position('zero')
        cur_consensus.set_xticks([-200, -150, -100, -50, 0])
        cur_consensus.set_xticklabels([])
        cur_consensus.set_xlim(-200, 0)
        cur_consensus.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2f'))

        kmer_positive_consensus, kmer_negative_consensus = positive_consensus[kmer], negative_consensus[kmer]
        positive_importances, negative_importances = np.zeros((sequence_length, )), np.zeros((sequence_length, ))
        for window, effect, names in kmer_positive_consensus :
            start, stop = window
            positive_importances[start : stop] = effect
        for window, effect, _ in kmer_negative_consensus :
            start, stop = window
            negative_importances[start : stop] = effect
        has_positive = np.max(positive_importances) > EPS_IMPORTANCES
        has_negative = np.max(np.abs(negative_importances)) > EPS_IMPORTANCES
        y_min, y_max = 0, 0
        if has_positive :
            _plot_consensus_islands(cur_consensus, x, positive_importances, color = 'b', linewidth = 1.3)
            y_max = None
        if has_negative :
            _plot_consensus_islands(cur_consensus, x, negative_importances, color = 'r', linewidth = 1.3)
            y_min = None

        if col == 0 :
            cur_consensus.set_ylabel('Effect', fontsize = 13)

        _y_min, _y_max = cur_consensus.get_ylim()
        if y_min is None :
            y_min = _y_min
        if y_max is None :
            y_max = _y_max
        y_min, y_max = _resize_y_axis(y_min, y_max)
        cur_consensus.set_ylim(y_min, y_max)
        yticks = [y_min, y_max]
        if not 0 in yticks :
            yticks.append(0)
        cur_consensus.set_yticks(yticks)
        
        title_at_bottom = False
        if has_positive and not has_negative :
            cur_consensus.tick_params(axis = 'x', direction = 'in')
        elif not has_positive and has_negative :
            cur_consensus.tick_params(axis = 'x', direction = 'out')
            title_at_bottom = True
        else :
            cur_consensus.tick_params(axis = 'x', direction = 'inout')
            title_at_bottom = abs(y_min) > abs(y_max)
        
        y_pos = 0.85
        if title_at_bottom :
            y_pos = 1.0 - y_pos
        label = np.array([char for char in kmer])
        label[label == 'T'] = 'U'
        label = ''.join(label)
        cur_consensus.text(0.05, y_pos, label, fontsize = 12, fontweight = 'bold', transform = cur_consensus.transAxes, va = 'top' if title_at_bottom else 'bottom')

        col += 1
        if col == n_cols :
            col = 0
            row += 2

    while row < n_rows and col < n_cols :
        if row < n_rows :
            cur = ax[row, col]
            try :
                f.delaxes(cur)
            except :
                pass
        if row + 1 < n_rows :
            cur = ax[row + 1, col]
            try :
                f.delaxes(cur)
            except :
                pass
        col += 1
        if col == n_cols :
            col = 0
            row += 2

    f.set_size_inches(19, 10, forward = True)
    # f.tight_layout()
    #f.subplots_adjust(bottom = 0.55)
    plot.show()

def count_sequence_reading_frames(dataset_names, seed_sequence, top_percent = 0.1, start_region = 0, end_region = None) :
    '''
    Generates context samples for a given seed sequence, region and a list of datasets.
    :param dataset_names : names of datasets, which should be scanned for the presence of a motif.
    :param seed_sequence : seed sequence that should be used to create the motif.
    :param top_percent : only use the given fraction of samples with highest IRES activity when looking for an enrichment.
    :param start_region : beginning of the region where the seed should be located.
    :param stop_region : end of the region where the seed should be located.
    '''
    if isinstance(dataset_names, basestring) :
        dataset_names = [dataset_names]
    union_ds = {}
    for dataset_name in dataset_names :
        ds = lib.persistent.datasets[dataset_name]
        for seq in ds :
            union_ds[seq.index] = seq
    del ds
    if end_region is None :
        end_region = len(seq.sequence)
    union_ds = union_ds.values()
    count_all = len(union_ds)
    negative_union_ds = [seq for seq in union_ds if seq.ires_activity == lib.tools.MINIMAL_IRES_ACTIVITY]
    union_ds = [seq for seq in union_ds if seq.ires_activity > lib.tools.MINIMAL_IRES_ACTIVITY]
    union_ds.sort(key = lambda seq : -seq.ires_activity)
    count_positive, count_negative = len(union_ds), len(negative_union_ds)
    count_top_percent = int(count_positive * top_percent) + 1
    union_ds = union_ds[:count_top_percent]
    print '    [i] Total: %d' % count_all
    print '    [i] Positive: %d, top %.2f percent: %d' % (count_positive, top_percent * 100, count_top_percent)
    print '    [i] Negative: %d' % count_negative

    foreground_samples = generate_context_samples(union_ds, seed_sequence, 0, 0, start_region, end_region)
    background_samples = generate_context_samples(negative_union_ds, seed_sequence, 0, 0, start_region, end_region) 
    print '    [i] FG: %d, BG: %d' % (len(foreground_samples), len(background_samples))

    fg_rf, bg_rf = np.zeros((3, ), dtype = np.float), np.zeros((3, ), dtype = np.float)
    fg_starts, bg_starts = [], []
    for name, seq in foreground_samples :
        pos = name.find('pos')
        name = name[pos + 3:].split('-')
        start = int(name[0])
        fg_rf[start % 3] += 1
        fg_starts.append(start)
        #fg_starts[start] = fg_starts.get(start, 0) + 1
    
    for name, seq in background_samples :
        pos = name.find('pos')
        name = name[pos + 3:].split('-')
        start = int(name[0])
        #bg_rf[start % 3] += 1
        #bg_starts[start] = bg_starts.get(start, 0) + 1
        bg_starts.append(start)
    return fg_rf, bg_rf, fg_starts, bg_starts

def generate_sequence_reading_frames_for_parameters(dataset_names, seed_sequence, top_percents = [0.1, 0.15, 0.25, 0.5, 1.0], regions = [(240, 250), (240, 260), (250, 260), (230, 240), (230, 250), (230, 260)]) :
    n_retries = 5
    results = []
    for top_percent in top_percents :
        for start_region, end_region in regions :
            print '[i] Processing top percent = %.2f, region (%d, %d)' % (top_percent, start_region, end_region)
            fg_rf, bg_rf, fg_starts, bg_starts = count_sequence_reading_frames(dataset_names, seed_sequence, top_percent = top_percent, start_region = start_region, end_region = end_region)
            result = SimpleNamespace()
            result.dataset_names = dataset_names
            result.seed_sequence = seed_sequence
            result.top_percent = top_percent
            result.start_region = start_region
            result.end_region = end_region
            result.n_fg = len(fg_starts)
            result.n_bg = len(bg_starts)
            result.fg_starts = fg_starts
            result.bg_starts = bg_starts
            result.fg_rf = fg_rf
            result.bg_rf = bg_rf
            results.append(result)
    return results

def plot_sequence_kmer_positions(dataset_name = 'viral_native_dsRNA_all', seed_sequence = 'CAG', window = (-20, 0), top_percent = 1.0, atg_offset = -270, title = 'CAG position in dsRNA viruses') :
    '''
    '''
    start_region, end_region = window
    start_region -= atg_offset
    end_region -= atg_offset
    _, _, fg_starts, bg_starts = count_sequence_reading_frames([dataset_name], seed_sequence, top_percent = top_percent, start_region = start_region, end_region = end_region)
    min_value, max_value = np.min(fg_starts + bg_starts), np.max(fg_starts + bg_starts)
    min_value += atg_offset
    max_value += atg_offset
    array_length = max_value - min_value + 1
    fg_counts, bg_counts = np.zeros((array_length, ), dtype = np.float), np.zeros((array_length, ), dtype = np.float)
    for start in fg_starts :
        start += atg_offset
        fg_counts[start - min_value] += 1
    for start in bg_starts :
        start += atg_offset
        bg_counts[start - min_value] += 1
    bg_counts /= np.sum(bg_counts)
    fg_counts /= np.sum(fg_counts)

    width = 0.8
    x = np.array(range(array_length)) * 3.2 * width + width / 2.0 + min_value
    
    import brewer2mpl
    bmap = brewer2mpl.get_map('Pastel1', 'qualitative', 7)
    colors = bmap.mpl_colors
    
    f, ax = plot.subplots(1, 1)
    ax.bar(x, fg_counts, color = colors[0], label = 'Positive, $N=%d$' % len(fg_starts), ec = 'none')
    ax.bar(x + width, bg_counts, color = colors[1], label = 'Negative, $N=%d$' % len(bg_starts), ec = 'none')
    ax.set_xlim(min_value, np.max(x) + 2 * width + width / 2.0)
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    ax.legend(prop = {'size' : 13}, loc = 'best')
    ax.set_ylabel('Frequency', fontsize = 13)
    ax.set_xlabel('Position', fontsize = 13)
    ax.set_xticks(x + width)
    ax.set_xticklabels(np.array(range(array_length)) + min_value)
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('left')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    yticks = ax.get_yticks()
    _ymin, _ymax = ax.get_ylim()
    for tick in yticks :
        ax.axhline(tick, linewidth = 1, color = 'w')
    ax.set_ylim(_ymin, _ymax)

    f.set_size_inches(7, 4.7, forward = True)
    f.tight_layout()
    plot.show()

def plot_sequence_reading_frame_results(results, take_mod = False, atg_offset = -270) :
    '''
    Results for which RFs should be plotted.
    :param results : a list of results to be plotted.
    '''
    results = [result for result in results if result.n_fg > 0 and result.n_bg > 0]
    n_results = len(results)
    print '[i] Results: %d' % n_results
    if n_results <= 12 :
        n_rows, n_cols = 3, 4
    elif n_results <= 20 :
        n_rows, n_cols = 5, 4
    elif n_results <= 24 :
        n_rows, n_cols = 4, 6
    elif n_results <= 30 :
        n_rows, n_cols = 5, 6
    elif n_results <= 36 :
        n_rows, n_cols = 6, 6
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    for result in results :
        cur = ax[row, col]
        fg_starts, bg_starts = result.fg_starts, result.bg_starts
        min_value, max_value = np.min(fg_starts + bg_starts), np.max(fg_starts + bg_starts)
        min_value += atg_offset
        max_value += atg_offset
        array_length = max_value - min_value + 1
        if take_mod :
            array_length = 3
            min_value, max_value = 0, 2
        fg_counts, bg_counts = np.zeros((array_length, ), dtype = np.float), np.zeros((array_length, ), dtype = np.float)
        for start in fg_starts :
            start += atg_offset
            if take_mod :
                start %= 3
            fg_counts[start - min_value] += 1
        for start in bg_starts :
            start += atg_offset
            if take_mod :
                start %= 3
            bg_counts[start - min_value] += 1
        bg_counts /= np.sum(bg_counts)
        fg_counts /= np.sum(fg_counts)

        width = 0.8
        x = np.array(range(array_length)) * 3.2 * width + width / 2.0 + min_value
        cur.bar(x, fg_counts, color = 'b', alpha = 0.45, label = 'Positive')
        cur.bar(x + width, bg_counts, color = 'r', alpha = 0.45, label = 'Negative')
        cur.set_xlim(min_value, np.max(x) + 2 * width + width / 2.0)
        title = 'FG/BG: %d/%d\n%.2f%% (%d, %d)' % (result.n_fg, result.n_bg, 100 * result.top_percent, result.start_region, result.end_region) 
        cur.set_title(title, fontsize = 9)
        if row == 0 and col == 0 :
            cur.legend()
        if col == 0 :
            cur.set_ylabel('Frequency', fontsize = 13)
        else :
            cur.set_yticks([])
        cur.set_xticks(x + width)
        cur.set_xticklabels(np.array(range(array_length)) + min_value, rotation = 90, fontsize = 8)
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    f.set_size_inches(18, 14, forward = True)
    f.tight_layout()
    f.subplots_adjust(bottom = 0.04, hspace = 0.5)
    plot.show()

def plot_title(title = 'Sequence around CAG in dsRNA viruses') :
    '''
    '''
    f, ax = plot.subplots(1, 1)
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    ax.set_axis_off()
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    f.set_size_inches(7, 4.7, forward = True)
    f.tight_layout()
    plot.show()

def get_permutation_correlation_coefs(x, y) :
    '''
    '''
    length = len(x)
    indices = np.array(range(length))
    coefs = []
    for perm in itertools.permutations(y) :
        perm = list(perm)
        coef_r, _ = scipy.stats.pearsonr(x, perm)
        coefs.append(coef_r)
    return np.array(coefs)

def process_multiplicity_experiment(selected_type = 'Rbm3', selected_background = 'Spacer1', n_total_sites = 9) :
    '''
    Calculate multiplicity experiment values (mean per n_sites, regression coefficient and p-values).
    :param selected_type : type of the selected experiment.
    :param selected_background : background of the selected experiment.
    :param n_total_sites : max number of sites per oligo.
    '''
    activity_per_site = np.zeros((n_total_sites, ), dtype = np.object)
    activity_per_site_rep1 = np.zeros((n_total_sites, ), dtype = np.object)
    activity_per_site_rep2 = np.zeros((n_total_sites, ), dtype = np.object)
    for i in range(n_total_sites) :
        activity_per_site[i] = []
        activity_per_site_rep1[i] = []
        activity_per_site_rep2[i] = []
    seqs = lib.persistent.datasets['mult_all_comb']
    for seq in seqs :
        infos = lib.tools.get_sequence_combination_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                n_sites = info['n_sites']
                activity_per_site[n_sites].append(seq.ires_activity)
                activity_per_site_rep1[n_sites].append(seq.ires_activity_replicate1)
                activity_per_site_rep2[n_sites].append(seq.ires_activity_replicate2)
                break

    for n_sites in range(n_total_sites) :
        #activity_per_site[n_sites] = np.log2(np.mean(activity_per_site[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        #activity_per_site_rep1[n_sites] = np.log2(np.mean(activity_per_site_rep1[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        #activity_per_site_rep2[n_sites] = np.log2(np.mean(activity_per_site_rep2[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        activity_per_site[n_sites] = np.mean(np.log2(activity_per_site[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        activity_per_site_rep1[n_sites] = np.mean(np.log2(activity_per_site_rep1[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        activity_per_site_rep2[n_sites] = np.mean(np.log2(activity_per_site_rep2[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)

    x = np.array(range(n_total_sites))
    indices = ~np.isnan(np.array(activity_per_site, dtype = np.float))
    slope, intercept, r, pvalue, _ = scipy.stats.linregress(x[indices], activity_per_site[indices])
    y = x * slope + intercept
    coef_r, pvalue = scipy.stats.pearsonr(x[indices], activity_per_site[indices])

    indices_rep1 = ~np.isnan(np.array(activity_per_site_rep1, dtype = np.float))
    indices_rep2 = ~np.isnan(np.array(activity_per_site_rep2, dtype = np.float))
    x_replicates = np.concatenate((x[indices_rep1], x[indices_rep2]))
    activity_replicates = np.concatenate((activity_per_site_rep1[indices_rep1], activity_per_site_rep2[indices_rep2]))
    slope_replicates, intercept_replicates, r_replicates, pvalue_replicates, _ = scipy.stats.linregress(x_replicates, activity_replicates)
    y_replicates = x_replicates * slope_replicates + intercept_replicates
    coef_r_replicates, pvalue_replicates = scipy.stats.pearsonr(x_replicates, activity_replicates)

    #result = {'activity_per_site' : activity_per_site, 'n_sites' : n_sites, 'type' : selected_type, 'background' : selected_background, 'x' : x, 'y' : y, 'slope' : slope, 'intercept' : intercept, 'coef_r' : coef_r, 'pvalue' : pvalue}
    result = {'activity_per_site' : activity_per_site, 'activity_per_site_rep1' : activity_per_site_rep1, 'activity_per_site_rep2' : activity_per_site_rep2, 'n_sites' : n_sites, 'type' : selected_type, 'background' : selected_background, 'x' : x, 'x_replicates' : x_replicates, 'y' : y, 'y_replicates' : y_replicates, 'slope' : slope, 'slope_replicates' : slope_replicates, 'intercept' : intercept, 'intercept_replicates' : intercept_replicates, 'coef_r' : coef_r, 'coef_r_replicates' : coef_r_replicates, 'pvalue' : pvalue, 'pvalue_replicates' : pvalue_replicates}
    return result

def plot_multiplicity_experiments(types = ['TEV', 'Rbm3', 'Gtx_133_141', 'Poliovirus_t_2', 'Random_ICS1_23', 'Random_ICS2_17_2'], backgrounds = ['HBB', 'Spacer1'], n_total_sites = 9, plot_replicates = True) :
    '''
    '''
    n_backgrounds, n_types = len(backgrounds), len(types)
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    for col, background in enumerate(backgrounds) :
        for row, type in enumerate(types) :
            if n_rows > 1 and n_cols > 1 :
                cur = ax[row, col]
            elif n_rows == 1 and n_cols == 1 :
                cur = ax
            elif n_rows == 1 :
                cur = ax[col]
            elif n_cols == 1 :
                cur = ax[row]
            result = process_multiplicity_experiment(type, background, n_total_sites)
            if not plot_replicates :
                s = stars(result['pvalue'])
                cur.scatter(result['x'], result['activity_per_site'], s = 20)
                cur.plot(result['x'], result['y'], linewidth = 1.2, color = 'r')
                cur.text(0.03, 0.80, 'Pearson $r = %.2f^\\mathrm{%s}$' % (result['coef_r'], s), fontsize = 14, transform = cur.transAxes, va = 'bottom')
            else :
                s = stars(result['pvalue_replicates'])
                cur.scatter(result['x'], result['activity_per_site_rep1'], s = 20, color = 'b')
                cur.scatter(result['x'], result['activity_per_site_rep2'], s = 20, color = 'r')
                cur.plot(result['x_replicates'], result['y_replicates'], linewidth = 1.2, color = 'r')
                cur.text(0.03, 0.80, 'Pearson $r = %.2f^\\mathrm{%s}$' % (result['coef_r_replicates'], s), fontsize = 14, transform = cur.transAxes, va = 'bottom')
            if row == 0 :
                cur.set_title('%s' % background, fontsize = 12)
                cur.set_xticks([])
            elif row == n_rows - 1 :
                cur.set_xticks(result['x'])
            else :
                cur.set_xticks([])
            if col == 0 :
                cur.set_ylabel('%s' % type, fontsize = 10)
    f.set_size_inches(9, 14, forward = True)
    f.tight_layout()
    plot.show()
    return result

def process_joint_multiplicity_experiment(selected_type = 'Rbm3', selected_background = 'Spacer1') :
    '''
    Calculate multiplicity experiment values (mean per n_sites, regression coefficient and p-values).
    :param selected_type : type of the selected experiment.
    :param selected_background : background of the selected experiment.
    :param n_total_sites : max number of sites per oligo.
    '''
    seqs = lib.persistent.datasets['mult_all_comb']
    for seq in seqs :
        infos = lib.tools.get_sequence_combination_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                n_sites = info['n_sites']
                yield (0, n_sites, seq.ires_activity_replicate1)
                yield (1, n_sites, seq.ires_activity_replicate2)
                break

def _average_multiplicity_sites(data) : 
    '''
    '''
    groups = {}
    for x, y in data :
        group_list = groups.get(x, [])
        group_list.append(y)
        groups[x] = group_list
    for x, ys in groups.iteritems() :
        #y = np.mean(np.log2(ys)) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        y = np.log2(np.mean(ys)) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        yield x, y

def plot_joint_multiplicity_experiments(types = ['TEV', 'Rbm3', 'Gtx_133_141', 'Poliovirus_t_2', 'Random_ICS1_23', 'Random_ICS2_17_2'], backgrounds = ['HBB', 'Spacer1'], n_total_sites = 8, plot_replicates = True) :
    '''
    '''
    import pandas as pd
    import statsmodels.stats.anova as anova
    from statsmodels.formula.api import ols
    n_types, n_backgrounds = len(types), len(backgrounds)
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    xticks = range(n_total_sites + 1)
    for row, type in enumerate(types) :
        all_sites = []
        for background_ind, background in enumerate(backgrounds) :
            result = process_joint_multiplicity_experiment(type, background)
            current_sites = [((replicate, n_sites, background_ind), activity) for replicate, n_sites, activity in result if not math.isnan(activity)]
            all_sites += current_sites
        all_sites = _average_multiplicity_sites(all_sites)
        all_sites = list(all_sites)

        x, activity = zip(*all_sites)
        replicate, n_sites, background = zip(*x)
        data = {'replicate' : replicate, 'n_sites' : n_sites, 'background' : background, activity : 'activity'}
        replicate, n_sites, background, activity = np.array(replicate), np.array(n_sites), np.array(background), np.array(activity)
        df = pd.DataFrame(data)
        model = ols('activity ~ n_sites + background', data = df).fit()
        pvalue_background = model.f_test('background = 0').pvalue
        pvalue_n_sites = model.f_test('n_sites = 0').pvalue
        pvalue = model.f_test('background = n_sites = 0').pvalue
        print 'Type: %s' % type
        print model.summary()
        rep1_indices = replicate == 0
        rep2_indices = replicate == 1
        _ymin, _ymax = +float('inf'), -float('inf')
        for col, background_name in enumerate(backgrounds) :
            data_predict = {'n_sites' : xticks, 'background' : [col] * (n_total_sites + 1)}
            prediction = model.predict(data_predict)
            background_indices = background == col
            
            cur = ax[row, col]
            cur.scatter(n_sites[rep1_indices & background_indices], activity[rep1_indices & background_indices], color = 'b', s = 20) 
            cur.scatter(n_sites[rep2_indices & background_indices], activity[rep2_indices & background_indices], color = 'r', s = 20)
            cur.plot(xticks, prediction, color = 'k', linewidth = 1.2)
            cur.set_xticks(xticks)
            cur.set_xlim(-0.5, n_total_sites + 0.5)
            if row == 0 :
                cur.set_title('%s' % background_name, fontsize = 12)
                cur.set_xticks([])
            elif row != n_rows - 1 :
                cur.set_xticklabels([])
            if col == 0 :
                cur.set_ylabel('%s' % type, fontsize = 10)
            cur.text(0.03, 0.80, 'n_sites = %g\nbackground = %g\noverall = %g' % (pvalue_n_sites, pvalue_background, pvalue), fontsize = 9, transform = cur.transAxes, va = 'top')
            if col != 0 :
                cur.set_yticklabels([])
            __ymin, __ymax = cur.get_ylim()
            _ymin, _ymax = min(_ymin, __ymin), max(_ymax, __ymax)
        for col, background_name in enumerate(backgrounds) :
            cur = ax[row, col]
            cur.set_ylim(_ymin, _ymax)
    f.set_size_inches(9, 14, forward = True)
    f.tight_layout()
    plot.show()

def plot_joint_multiplicity_experiments_paper(types = ['TEV', 'Random_ICS2_17_2'], backgrounds = ['HBB', 'Spacer1'], type_labels = ['TEV', 'ICS2 17 2'], background_labels = ['HBB', 'Spacer'], n_total_sites = 8, plot_replicates = True) :
    '''
    '''
    import pandas as pd
    import statsmodels.stats.anova as anova
    from statsmodels.formula.api import ols
    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 8)
    colors = bmap.mpl_colors
    n_types, n_backgrounds = len(types), len(backgrounds)
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    xticks = range(n_total_sites + 1)
    for row, type in enumerate(types) :
        all_sites = []
        for background_ind, background in enumerate(backgrounds) :
            result = process_joint_multiplicity_experiment(type, background)
            current_sites = [((replicate, n_sites, background_ind), activity) for replicate, n_sites, activity in result if not math.isnan(activity)]
            all_sites += current_sites
        all_sites = _average_multiplicity_sites(all_sites)
        all_sites = list(all_sites)

        x, activity = zip(*all_sites)
        replicate, n_sites, background = zip(*x)
        data = {'replicate' : replicate, 'n_sites' : n_sites, 'background' : background, activity : 'activity'}
        replicate, n_sites, background, activity = np.array(replicate), np.array(n_sites), np.array(background), np.array(activity)
        df = pd.DataFrame(data)
        model = ols('activity ~ n_sites + background', data = df).fit()
        pvalue_background = model.f_test('background = 0').pvalue
        pvalue_n_sites = model.f_test('n_sites = 0').pvalue
        pvalue = model.f_test('background = n_sites = 0').pvalue
        print 'Type: %s' % type
        print model.summary()
        rep1_indices = replicate == 0
        rep2_indices = replicate == 1
        _ymin, _ymax = +float('inf'), -float('inf')
        for col, background_name in enumerate(backgrounds) :
            data_predict = {'n_sites' : xticks, 'background' : [col] * (n_total_sites + 1)}
            prediction = model.predict(data_predict)
            background_indices = background == col
            
            cur = ax[row, col]
            cur.scatter(n_sites[rep1_indices & background_indices], activity[rep1_indices & background_indices], color = colors[0], s = 20, label = 'Replicate 1') 
            cur.scatter(n_sites[rep2_indices & background_indices], activity[rep2_indices & background_indices], color = colors[1], s = 20, label = 'Replicate 2')
            cur.plot(xticks, prediction, color = 'k', linewidth = 1.2)
            cur.set_xlim(-0.5, n_total_sites + 0.5)
            cur.spines['right'].set_visible(False)
            cur.spines['top'].set_visible(False)
            cur.yaxis.set_ticks_position('left')
            cur.xaxis.set_ticks_position('bottom')
            cur.set_xticks(xticks)
            if row == 0 :
                cur.set_title('%s' % background_labels[col], fontweight = 'bold', fontsize = 12)
                if col == n_cols - 1 :
                    cur.legend(loc = 'best', prop = {'size' : 12})
            if row != n_rows - 1 :
                cur.set_xticklabels([])
            else :
                cur.set_xlabel('Site count', fontsize = 13)
            if col == n_cols - 1 :
                cur_twin = cur.twinx()
                cur_twin.set_yticklabels([])
                cur_twin.spines['right'].set_visible(False)
                cur_twin.spines['top'].set_visible(False)
                cur_twin.yaxis.set_ticks_position('none')
                cur_twin.xaxis.set_ticks_position('none')
                cur_twin.set_ylabel('%s' % type_labels[row], fontweight = 'bold', fontsize = 12, rotation = 270, va = 'bottom')
            elif col == 0 :
                cur.set_ylabel('Avg. IRES activity\n$\\log_2$ fold change', fontsize = 13)
                #cur.text(0.03, 0.80, 'n_sites = %g\nbackground = %g\noverall = %g' % (pvalue_n_sites, pvalue_background, pvalue), fontsize = 9, transform = cur.transAxes, va = 'top')
            if col != 0 :
                cur.set_yticklabels([])
            __ymin, __ymax = cur.get_ylim()
            _ymin, _ymax = min(_ymin, __ymin), max(_ymax, __ymax)
        for col, background_name in enumerate(backgrounds) :
            cur = ax[row, col]
            cur.set_ylim(_ymin, _ymax)
    f.set_size_inches(7, 5, forward = True)
    f.tight_layout()
    plot.show()

def process_position_experiment(selected_type = 'Rbm3', selected_background = 'Spacer1', min_n_sites = 0, max_n_sites = 10) :
    '''
    Calculate position experiment values (mean per n_sites, regression coefficient and p-values).
    :param selected_type : type of the selected experiment.
    :param selected_background : background of the selected experiment.
    '''
    x, positions = set(), set()
    activity_per_site = {}
    seqs = lib.persistent.datasets['mult_all_comb']
    for seq in seqs :
        infos = lib.tools.get_sequence_combination_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                sites = info['site_ids']
                if info['n_sites'] < min_n_sites or info['n_sites'] > max_n_sites :
                    continue
                site_positions = info['site_positions']
                for id, position in zip(sites, site_positions) :
                    x.add(id)
                    positions.add(position)
                    if not id in activity_per_site :
                        activity_per_site[id] = []
                    activity_per_site[id].append(seq.ires_activity)
                break
    for site in activity_per_site :
        #activity_per_site[site] = np.log2(np.mean(activity_per_site[site])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        activity_per_site[site] = np.mean(np.log2(activity_per_site[site])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        #n_total = len(activity_per_site[site])
        #n_positive = np.sum(activity_per_site[site] > lib.tools.MINIMAL_IRES_ACTIVITY, dtype = np.int)
        #activity_per_site[site] = n_positive / float(n_total)
    
    x, positions = list(x), list(positions)
    x.sort()
    positions.sort()
    positions, activities = np.array(positions), np.array([activity_per_site[site] for site in x])
    x = np.array(x)

    result = {'type' : selected_type, 'background' : selected_background, 'x' : x, 'positions' : positions, 'activity_per_site' : activity_per_site, 'activities' : activities}
    return result

def plot_position_experiments(types = ['TEV', 'Rbm3', 'Gtx_133_141', 'Poliovirus_t_2', 'Random_ICS1_23', 'Random_ICS2_17_2'], backgrounds = ['HBB', 'Spacer1'], min_n_sites = 0, max_n_sites = 10, atg_offset = -270, left_extension = 84) :
    '''
    '''
    n_backgrounds, n_types = len(backgrounds), len(types)
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    for col, background in enumerate(backgrounds) :
        for row, type in enumerate(types) :
            cur = ax[row, col]
            result = process_position_experiment(type, background, min_n_sites, max_n_sites)
            positions = result['positions'].copy()
            x, activities = result['x'], result['activities']
            positions = np.array(positions) + left_extension - 1 + atg_offset 
            cur.set_xticks(x)
            cur.set_xticklabels(positions)
            cur.plot(x, activities, color = 'b', alpha = 0.45, linewidth = 1.2)
            cur.scatter(x, activities)
            if row == 0 :
                cur.set_title('%s' % background, fontsize = 12)
                cur.set_xticks(x)
                cur.set_xticklabels([])
            elif row == n_rows - 1 :
                cur.set_xticks(x)
            else :
                cur.set_xticks(x)
                cur.set_xticklabels([])
            if col == 0 :
                cur.set_ylabel('%s' % type, fontsize = 10)

    f.set_size_inches(9, 14, forward = True)
    f.tight_layout()
    plot.show()

def process_distance_experiment(selected_type = 'TEV', selected_background = 'HBB') :
    '''
    '''
    positions = set()
    seqs = lib.persistent.datasets['distance']
    for seq in seqs :
        infos = lib.tools.get_sequence_distance_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                site_positions = info['site_positions']
                for position in site_positions :
                    positions.add(position)
                break
    positions = np.array(sorted(list(positions)))
    n_positions = len(positions)
    position_to_index = {position : i for i, position in enumerate(positions)}
    position_activities = np.zeros((n_positions, ), dtype = np.object)
    for i in range(n_positions) :
        position_activities[i] = []
    distance_activities, distance_activity_counts = {}, {}

    for seq in seqs :
        infos = lib.tools.get_sequence_distance_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                value = np.log2(seq.ires_activity) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
                site_positions = info['site_positions']
                if info['n_sites'] == 2 :
                    site_a, site_b = site_positions[0], site_positions[1]
                    distance = abs(site_a - site_b) + 1
                    if distance not in distance_activities :
                        distance_activities[distance] = []
                        distance_activity_counts[distance] = 0
                    distance_activities[distance].append(value)
                    distance_activity_counts[distance] += 1
                for position in site_positions :
                    index = position_to_index[position]
                    position_activities[index].append(value)
    
    position_activity_counts = np.zeros((n_positions, ), dtype = np.int)
    for i in range(n_positions) :
        position_activity_counts[i] = len(position_activities[i])
        position_activities[i] = np.mean(position_activities[i])
    
    result = {'positions' : positions, 'n_positions' : n_positions, 'position_activities' : position_activities, 'distance_activities' : distance_activities, 'distance_activity_counts' : distance_activity_counts, 'position_activity_counts' : position_activity_counts}
    return result

def plot_position_experiments_from_distance_data(types = ['TEV', 'Rbm3', 'Gtx_133_141', 'Poliovirus_t_2', 'Random_ICS1_23', 'Random_ICS2_17_2'], backgrounds = ['HBB', 'Spacer1'], atg_offset = -270, left_extension = 84) :
    '''
    '''
    n_backgrounds, n_types = len(backgrounds), len(types)
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    for col, background in enumerate(backgrounds) :
        for row, type in enumerate(types) :
            cur = ax[row, col]
            result = process_distance_experiment(type, background)
            positions = result['positions'].copy()
            positions = np.array(positions) + left_extension - 1 + atg_offset
            activities = result['position_activities']
            cur.plot(positions, activities, color = 'b', alpha = 0.45, linewidth = 1.2)
            cur.scatter(positions, activities)
            cur.set_xticks(positions)
            cur.set_xticklabels(positions, rotation = 90, fontsize = 8)
            if row == 0 :
                cur.set_title('%s' % background, fontsize = 12)
            if col == 0 :
                cur.set_ylabel('%s' % type, fontsize = 10)

    f.set_size_inches(15, 14, forward = True)
    f.tight_layout()
    plot.show()

def plot_distance_experiments(types = ['TEV', 'Rbm3', 'Gtx_133_141', 'Poliovirus_t_2', 'Random_ICS1_23', 'Random_ICS2_17_2'], backgrounds = ['HBB', 'Spacer1']) :
    '''
    '''
    n_backgrounds, n_types = len(backgrounds), len(types)
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    for col, background in enumerate(backgrounds) :
        for row, type in enumerate(types) :
            cur = ax[row, col]
            result = process_distance_experiment(type, background)
            activities = result['distance_activities']
            distances = sorted(activities.keys())
            activities = [activities[distance] for distance in distances]
            cur.plot(distances, activities, color = 'b', alpha = 0.45, linewidth = 1.2)
            cur.scatter(distances, activities)
            cur.set_xticks(distances)
            cur.set_xticklabels(distances, rotation = 90, fontsize = 8)
            cur.set_xlim(np.min(distances) - 1, np.max(distances) + 1)
            if row == 0 :
                cur.set_title('%s' % background, fontsize = 12)
            elif row == n_rows - 1 :
                cur.set_xlabel('Site pair distance', fontsize = 13)
            if col == 0 :
                cur.set_ylabel('%s' % type, fontsize = 10)

    f.set_size_inches(9, 14, forward = True)
    f.tight_layout()
    f.subplots_adjust(bottom = 0.06)
    plot.show()

def process_background_experiment(selected_type = 'Rbm3', selected_background = 'Spacer1') :
    '''
    Calculate multiplicity experiment values (mean per n_sites, regression coefficient and p-values).
    :param selected_type : type of the selected experiment.
    :param selected_background : background of the selected experiment.
    :param n_total_sites : max number of sites per oligo.
    '''
    n_total_sites, n_observations = 0, 0
    seqs = lib.persistent.datasets['backgrounds']
    for seq in seqs :
        infos = lib.tools.get_sequence_background_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                site_ids = info['site_ids']
                for id in site_ids :
                    if n_total_sites < id :
                        n_total_sites = id
                n_observations += 1
                break

    n_total_sites += 1
    activity_per_site, activity_per_position = np.zeros((n_total_sites, ), dtype = np.object), np.zeros((n_total_sites, ), dtype = np.object)
    activity_observations_per_site, activity_observations_per_position = np.zeros((n_total_sites, ), dtype = np.int), np.zeros((n_total_sites, ), dtype = np.int)
    for i in range(n_total_sites) :
        activity_per_site[i], activity_per_position[i] = [], []
    for seq in seqs :
        infos = lib.tools.get_sequence_background_info(seq)
        for info in infos :
            if info['type'] == selected_type and info['background'] == selected_background :
                n_sites = info['n_sites']
                activity_per_site[n_sites].append(seq.ires_activity)
                activity_observations_per_site[n_sites] += 1
                for id in info['site_ids'] :
                    id -= 1
                    activity_per_position[id].append(seq.ires_activity)
                    activity_observations_per_position[id] += 1
                break
    
    for n_sites in range(n_total_sites) :
        activity_per_site[n_sites] = np.log2(np.mean(activity_per_site[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        activity_per_position[n_sites] = np.log2(np.mean(activity_per_position[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        #activity_per_site[n_sites] = np.mean(np.log2(activity_per_site[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)
        #activity_per_position[n_sites] = np.mean(np.log2(activity_per_position[n_sites])) - np.log2(lib.tools.MINIMAL_IRES_ACTIVITY)

    x = np.array(range(n_total_sites))
    indices = ~np.isnan(np.array(activity_per_site, dtype = np.float))
    slope, intercept, r, pvalue, _ = scipy.stats.linregress(x[indices], activity_per_site[indices])
    y = x * slope + intercept
    coef_r, pvalue = scipy.stats.pearsonr(x[indices], activity_per_site[indices])

    result = {'activity_per_site' : activity_per_site, 'n_sites' : n_sites, 'type' : selected_type, 'background' : selected_background, 'x' : x, 'y' : y, 'slope' : slope, 'intercept' : intercept, 'coef_r' : coef_r, 'pvalue' : pvalue, 'n_observations' : n_observations, 'activity_observations_per_site' : activity_observations_per_site, 'activity_per_position' : activity_per_position, 'activity_observations_per_positions' : activity_observations_per_position}
    return result 

def plot_bppm_accessibility_for_datasets(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], sequence_length = 330, atg_offset = -270, window_size = 20, window_step = 10, filename = None) :
    '''
    Plot accessibility correlations for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param window_size : size of the window to measure accessibility for.
    :param window_step : step, by which the window should be moved.
    '''

    def get_correlation_vector(cur_correlations, sequence_length) :
        '''
        Returns a vector of correlations for the given list of correlations.
        '''
        corr = np.zeros((sequence_length, ), dtype = np.float)
        corr_cnt = np.zeros((sequence_length, ), dtype = np.int)
        for window, coef_r, pvalue, _ in cur_correlations :
            start, stop = window
            for i in range(start, stop) :
                corr[i] += coef_r
                corr_cnt[i] += 1
        corr /= corr_cnt
        return corr

    n_datasets = len(dataset_names)
    dataset_correlations = []
    for dataset_name in dataset_names :
        cur_folds = lib.reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
        cur_dataset_correlations = []
        for train_indices, test_indices in cur_folds.outer_folds :
            fold_correlations = lib.structure.look_for_bppm_features(dataset_name, window_step = window_step, window_size = window_size, cutoff = 1.1, indices = train_indices)
            fold_correlations = get_correlation_vector(fold_correlations, sequence_length)
            cur_dataset_correlations.append(fold_correlations)
        dataset_correlations.append(cur_dataset_correlations)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    for cur_correlations, cur_label in zip(dataset_correlations, dataset_labels) :
        cur = ax[row, col]
        corr_mean = np.mean(cur_correlations, axis = 0)
        corr_std = np.std(cur_correlations, axis = 0)
        cur.plot(x, corr_mean, color = 'b', alpha = 0.45, linewidth = 2)
        #cur.fill_between(x, corr_mean - corr_std, corr_mean + corr_std, alpha = 0.25, linewidth = 0, color = 'b')
        cur.set_xlim(atg_offset, atg_offset + sequence_length)
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        cur.axhline(0, linewidth = 1.1, color = 'k', linestyle = '--')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('Position', fontsize = 13)
        else :
            cur.set_xticklabels([])
        if col == 0 and row == 0 :
            cur.legend(ncol = 3, prop = {'size' : 7}, loc = 'best')
        if col == 0 :
            cur.set_ylabel('Spearman, $\\rho$', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    if filename is None :
        plot.show()
    else :
        f.savefig(filename)

def plot_bppm_pairs_for_datasets(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], sequence_length = 330, atg_offset = -270, window_size = 20, window_step = 10) :
    '''
    Plot BPPm pair correlations for a given list of datasets.
    :param dataset_names : a list of dataset names that should be plotted.
    :param dataset_labels : a list of dataset labels.
    :param sequence_length : length of the sequence. Used for positioning k-mer features.
    :param atg_offset : offset for positioning x-coordinates.
    :param window_size : size of the window to measure accessibility for.
    :param window_step : step, by which the window should be moved.
    '''

    def get_correlation_matrix(cur_correlations, sequence_length) :
        '''
        Returns a vector of correlations for the given list of correlations.
        '''
        corr = np.zeros((sequence_length, sequence_length), dtype = np.float)
        corr_cnt = np.zeros((sequence_length, sequence_length), dtype = np.int)
        for window, coef_r, pvalue, _ in cur_correlations :
            start_row, stop_row, start_col, stop_col = window
            corr[start_row : stop_row, start_col : stop_col] += coef_r
            corr_cnt[start_row : stop_row, start_col : stop_col] += 1
        corr /= corr_cnt
        return corr

    n_datasets = len(dataset_names)
    dataset_correlations = []
    for dataset_name in dataset_names :
        cur_folds = lib.reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
        cur_dataset_correlations = []
        for train_indices, test_indices in cur_folds.outer_folds :
            fold_correlations = lib.structure.look_for_bppm_pair_features(dataset_name, window_step = window_step, window_size = window_size, cutoff = 1.1, indices = train_indices)
            fold_correlations = get_correlation_matrix(fold_correlations, sequence_length)
            cur_dataset_correlations.append(fold_correlations)
        dataset_correlations.append(cur_dataset_correlations)
    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    x = range(atg_offset, atg_offset + sequence_length)
    _ma = -1
    for cur_correlations, cur_label in zip(dataset_correlations, dataset_labels) :
        corr_mean = np.mean(cur_correlations, axis = 0)
        _ma = max(np.nanmax(np.abs(corr_mean)), _ma)
    vmin, vmax = -_ma, +_ma
    for cur_correlations, cur_label in zip(dataset_correlations, dataset_labels) :
        cur = ax[row, col]
        corr_mean = np.mean(cur_correlations, axis = 0)
        corr_mean = corr_mean.T
        #corr_std = np.std(cur_correlations, axis = 0)
        im = cur.imshow(corr_mean, interpolation = 'nearest', aspect = 'auto', cmap = plot.cm.RdGy, vmin = vmin, vmax = vmax, extent = [atg_offset, atg_offset + sequence_length, atg_offset + sequence_length, atg_offset], origin = 'upper')
        #cur.fill_between(x, corr_mean - corr_std, corr_mean + corr_std, alpha = 0.25, linewidth = 0, color = 'b')
        cur.set_xlim(atg_offset, atg_offset + sequence_length)
        cur.set_ylim(atg_offset + sequence_length, atg_offset)
        cur.set_title(cur_label, fontweight = 'bold', fontsize = 12)
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('bottom')
        cur.tick_params(axis = 'x', direction = 'none')
        cur.tick_params(axis = 'y', direction = 'none')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('Position', fontsize = 13)
        else :
            cur.set_xticklabels([])
        if col == 0  :
            cur.set_ylabel('Position', fontsize = 13)
        else :
            cur.set_yticklabels([])
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
   
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2, right = 0.90)
    cbar_ax = f.add_axes([0.93, 0.07, 0.02, 0.86])
    cbar = f.colorbar(im, cax = cbar_ax, ticks = [vmin, vmax], orientation = 'vertical')
    cbar.ax.set_yticklabels(['%.2f' % tick for tick in [vmin, vmax]], rotation = 90)
    #cbar.ax.set_yticks([vmin, vmax])
    #cbar.ax.spines['right'].set_visible(True)
    #cbar.ax.yaxis.set_ticks_position('right')
    cbar.set_label('Spearman $\\rho$', fontsize = 13)
    plot.show()

def plot_dataset_feature_effect_correlations(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh', 'kmers_kmers-windows'), min_kmer_length = 1, kmer_length = 4, min_n_folds = 10, min_importance = 0.1, only_common = True, include_global = True, include_positional = True) :
    '''
    '''
    importances, assignment = {}, {}
    n_datasets = len(dataset_names)
    for dataset_name in dataset_names :
        configuration_name, experiment, feature_names = results
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename) :
            try :
                cur_result = lib.reader.read_pickle(filename)
            except :
                '[!] Failed to read: %s' % filename
                return
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            cur_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            return
        cur_assignment = lib.interpretation.classify_partial_dependence_using_derivative(cur_pdp)
        cur_importances = lib.tools.get_feature_importances(cur_result, normalize = True)
        cur_importances = lib.tools.average_importances(cur_importances)
        importances[dataset_name] = cur_importances
        assignment[dataset_name] = cur_assignment
        del cur_result
    
    effects = {}
    for dataset_name in dataset_names :
        cur_effect = {}
        cur_assignment = assignment[dataset_name]
        for feature_name, importance in importances[dataset_name].iteritems() :
            importance, n_folds = importance
            #if importance < min_importance or n_folds < min_n_folds :
            #    continue
            if 'window' in feature_name and not include_positional :
                continue
            if not 'window' in feature_name and not include_global :
                continue
            effect = -importance if cur_assignment[feature_name]  == 'Dec' else +importance
            cur_effect[feature_name] = effect
        effects[dataset_name] = cur_effect

    n_datasets = len(dataset_names)
    f, ax = plot.subplots(n_datasets - 1, n_datasets - 1)
    for i, dataset_name_a in enumerate(dataset_names) :
        if i < n_datasets - 1 :
            for j in range(1, i + 1) :
                cur = ax[i][j - 1]
                f.delaxes(cur)

        for j, dataset_name_b in enumerate(dataset_names[i + 1 : ]) :
            j += i
            cur = ax[i][j]
            dataset_a_features = [feature_name for feature_name in effects[dataset_name_a].keys() if importances[dataset_name_a][feature_name][0] >= min_importance and importances[dataset_name_a][feature_name][1] >= min_n_folds]
            dataset_b_features = [feature_name for feature_name in effects[dataset_name_b].keys() if importances[dataset_name_b][feature_name][0] >= min_importance and importances[dataset_name_b][feature_name][1] >= min_n_folds]
            if only_common :
                common_features = set(dataset_a_features) & set(dataset_b_features)
            else :
                common_features = set(dataset_a_features) | set(dataset_b_features)
            common_features = list(common_features)
            array_a = [effects[dataset_name_a].get(feature_name, 0.0) for feature_name in common_features]
            array_b = [effects[dataset_name_b].get(feature_name, 0.0) for feature_name in common_features]
            cur.scatter(array_a, array_b, alpha = 0.45, color = 'k')
            
            coef_r, pvalue = scipy.stats.pearsonr(array_a, array_b)
            n_features = len(common_features)
            cur.text(0.03, 0.03, 'Pearson $r = %.2f$' % coef_r, fontsize = 14, transform = cur.transAxes, va = 'bottom')
            extremum = 1.07
            cur.set_xlim(-extremum, extremum)
            cur.set_ylim(-extremum, extremum)
            print '%25s vs %25s : N = %3d, coef_r = %.2f, p-value = %g' % (dataset_name_a, dataset_name_b, n_features, coef_r, pvalue)
            if j == n_datasets - 2 :
                cur_twin = cur.twinx()
                cur_twin.set_ylim(-extremum, extremum)
                cur_twin.set_ylabel(dataset_labels[i], fontsize = 12, fontweight = 'bold')
                cur_twin.set_yticklabels([])
                cur_twin.set_yticks([])
            if j != i :
                cur.set_yticks([-extremum, 0, extremum])
                cur.set_yticklabels([])
            else :
                cur.set_yticks([-extremum, 0, extremum])
                cur.set_yticklabels(['-1', '0', '1'])
                cur.set_ylabel('Feature\neffect', fontsize = 13)
            if i == 0 :
                cur.set_title(dataset_labels[j + 1], fontsize = 12, fontweight = 'bold')
            if i != j :
                cur.set_xticks([-extremum, 0, extremum])
                cur.set_xticklabels([])
            else :
                cur.set_xlabel('Feature\neffect', fontsize = 13)
                cur.set_xticks([-extremum, 0, extremum])
                cur.set_xticklabels(['-1', '0', '1'])

    
    f.set_size_inches(9, 9, forward = True)
    f.tight_layout()
    f.subplots_adjust(wspace = 0.2, hspace = 0.2)
    plot.show()
    
def plot_scatterplot_for_all_sequences(chosen_dataset_name = 'native', dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmer = 4, min_kmer_length = 1, plot_gbrf = True, x_max = 14) :
    '''
    Plot regression performance (R2, Pearson r, Spearman rho) for a given list of datasets.
    :param chosen_dataset_name : a name of the dataset to be processed.
    :param dataset_names : a list of dataset names that should be plotted.
    :param results : description of the results that should be plotted.
    :param min_kmer_length : minimum length of the kmer to be plotted.
    '''
    n_datasets = len(dataset_names)
    configuration_name, feature_names, experiment = results
    dataset_results, dataset_folds = [], []

    file_path = '../results/regression/%s/%s' % (configuration_name, chosen_dataset_name)
    folder = 'gbrf' if plot_gbrf else 'rf'
    filename_result = '%s/%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, folder, feature_names, min_kmer_length, chosen_kmer, experiment)
    if os.path.exists(filename_result) :
        result = lib.reader.read_pickle(filename_result)
    else :
        print '[!] Missing (result): %s' % filename_result
        return
    filename_cv = '%s/crossvalidation_folds.out' % file_path
    if os.path.exists(filename_cv) :
        folds = lib.reader.read_pickle(filename_cv)
    else :
        print '[!] Missing (CV folds): %s' % filename_cv
        return

    n_rows, n_cols = 2, 3
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0

    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 8)
    colors = bmap.mpl_colors
    del colors[0]
    colors.insert(1, colors[-1])
    del colors[-1]
    
    cur = ax[row, col]
    n_folds = len(result.regressors)
    features = result.features
    use_aux = result.use_aux
    log2 = result.log2
    
    data = {}
    ds = lib.persistent.datasets[chosen_dataset_name]
    for fold in range(n_folds) :
        train_index, test_index = folds.outer_folds[fold]
        if use_aux :
            Xs, feature_names = lib.regression.build_regression_matrix_list(chosen_dataset_name, features, use_aux = use_aux, fold = fold)
        else :
            Xs, feature_names = lib.regression.build_regression_matrix_list(chosen_dataset_name, features, use_aux = use_aux, fold = train_index)
        X, y = lib.regression.build_regression_matrix(Xs, chosen_dataset_name, log2 = log2)
        del Xs
        X, y = X[test_index, :], y[test_index]
        X = X.toarray()
        regressor = result.regressors[fold]
        predicted_y = regressor.predict(X)
        for index, _true_y, _predicted_y in zip(test_index, y, predicted_y) :
            data[ds[index].index] = (_true_y, _predicted_y)
        del X, y
    
    for k, dataset_name in enumerate(dataset_names) :
        ds = lib.persistent.datasets[dataset_name]
        ids = [seq.index for seq in ds]
        all_true_y, all_predicted_y = zip(*[data[id] for id in ids])
        #cur.scatter(all_true_y, all_predicted_y, color = colors[k], alpha = 0.25)
        cur.scatter(all_true_y, all_predicted_y, color = 'k', alpha = 0.25)
    
    if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
        cur.set_xlabel('True activity, $\\log_2$', fontsize = 14)
    if col == 0 :
        cur.set_ylabel('Predicted activity, $\\log_2$', fontsize = 13)
    y_min, y_max = cur.get_ylim()
    y_min, y_max = int(y_min), int(y_max)
    x_min, _ = cur.get_xlim()
    cur.set_xlim(x_min, x_max)
    y_ticks = range(y_min, y_max + 1)
    cur.set_yticks(y_ticks)
    
    cur.spines['right'].set_visible(False)
    cur.spines['top'].set_visible(False)
    cur.yaxis.set_ticks_position('left')
    cur.xaxis.set_ticks_position('bottom')
    
    col += 1
    if col >= n_cols :
        col = 0
        row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.3)
    plot.show()

def plot_pyrimidines_features_partial_dependence(chosen_dataset_names = ['viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], chosen_dataset_labels = ['dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], chosen_feature_list = ['C', 'CC', 'CCC', 'CCCC', 'CCCT', 'CCTC', 'CTCC', 'TCCC'] + ['TTCC', 'TCTC', 'TCCT', 'CTCT', 'CCTT', 'CTTC'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), chosen_kmers = [4, 4, 4], min_kmer_length = 1, min_n_folds = 10, min_importance = 0.1, title = 'C-rich pyrimidines', save_filename=None) :
    '''
    Plot unique features for a given dataset.
    :param dataset_name : name of the dataset, for which features should be plotted.:
    :param feature_list : a list of features that should be plotted.
    '''
    import brewer2mpl
    colors1 = brewer2mpl.get_map('Blues', 'sequential', 5).mpl_colors
    colors2 = brewer2mpl.get_map('Set2', 'qualitative', 8).mpl_colors
    del colors2[2]
    del colors2[0]
    colors = colors1[1:4] + colors2
    
    configuration_name, feature_names, experiment = results
    importances, pdp, assignment = {}, {}, {}
    for dataset_name, kmer_length in zip(chosen_dataset_names, chosen_kmers) :
        file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
        filename_result = '%s/gbrf/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_result) :
            dataset_result = lib.reader.read_pickle(filename_result)
        else :
            print '[!] Missing (result): %s' % filename_result
            dataset_result = None
            break
        filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
        if os.path.exists(filename_pdp) :
            dataset_pdp = lib.reader.read_pickle(filename_pdp)
        else :
            print '[!] Missing (PDP): %s' % filename_pdp
            dataset_pdp = None
            break
        dataset_importances = lib.tools.get_feature_importances(dataset_result, normalize = True)
        dataset_importances = lib.tools.average_importances(dataset_importances)
        importances[dataset_name] = {key : value for key, value in dataset_importances.iteritems() if not 'window' in key}
        pdp[dataset_name] = dataset_pdp
        assignment[dataset_name] = lib.interpretation.classify_partial_dependence_using_derivative(dataset_pdp)
 
    selected_features = set()
    for dataset_name in chosen_dataset_names :
        for feature_name in importances[dataset_name].keys() :
            selected_features.add(feature_name)

    n_datasets = len(chosen_dataset_names)
    chosen_features = [feature for feature in selected_features if feature.split('-')[1] in chosen_feature_list]
    chosen_features.sort(key = lambda x : chosen_feature_list.index(x.split('-')[1]))
    print '[i] Chosen features: ', chosen_features
    n_features = len(chosen_features)

    width = 0.5
    x = np.array(range(n_features)) * width * (n_datasets + 0.7) + width / 2.0
    f, ax = plot.subplots(1, 1)
    ax.set_xticks(x + width * n_datasets / 2.0)
    chosen_feature_list = [''.join([c if c != 'T' else 'U' for c in label]) for label in chosen_feature_list]
    ax.set_xticklabels(chosen_feature_list, rotation = 60, ha = 'center', va = 'top')
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    
    color_offset = 3

    labeled = {dataset_name : False for dataset_name in chosen_dataset_names}
    for j, dataset_name in enumerate(chosen_dataset_names) :
        effects = np.zeros((n_features, ), dtype = np.float)
        is_robust = np.ones((n_features, ), dtype = np.bool)
        plot_feature_names = []
        i = 0
        for feature_name in chosen_features :
            importance = importances[dataset_name].get(feature_name, (0.0, 0.0))
            importance, n_folds = importance
            if n_folds < min_n_folds :
                is_robust[i] = False
            ass = assignment[dataset_name].get(feature_name, 'Dec')
            if ass == 'Dec' :
                importance = -importance
            effects[i] = importance
            feature_name = feature_name.split('-')[1]
            feature_name = ''.join([c if c != 'T' else 'U' for c in feature_name])
            plot_feature_names.append(feature_name)
            i += 1
        
        for _x, _y, _is_robust, feature_name in zip(x + j * width, effects, is_robust, plot_feature_names) :
            cur_color = colors[j + color_offset]
            if not _is_robust :
                ax.bar([_x], [_y], width, color = cur_color, ec = 'w', hatch = '//')
            else :
                dataset_label = chosen_dataset_labels[j]
                if not labeled[dataset_name] :
                    ax.bar([_x], [_y], width, color = cur_color, ec = 'w', label = dataset_label)
                    labeled[dataset_name] = True
                else :
                    ax.bar([_x], [_y], width, color = cur_color, ec = 'w')
            _x += width / 2.0
            _y += 0.01
            #ax.text(_x, _y, feature_name, ha = 'center', fontweight = 'bold')
    ax.legend(loc = 'best', prop = {'size' : 12})
    ax.set_xlim(0, np.max(x) + width * n_datasets + width / 2.0)
    ax.set_ylim(0, 0.74)
    ax.set_xlabel('Feature', fontsize = 13)
    ax.set_ylabel('Feature effect', fontsize = 13)
    
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    yticks = ax.get_yticks()
    _ymin, _ymax = ax.get_ylim()
    for tick in yticks :
        ax.axhline(tick, linewidth = 1, color = 'w')
    for tick in ax.xaxis.get_major_ticks() : 
        tick.tick1On = False 
        tick.tick2On = False 
    ax.set_ylim(_ymin, _ymax)
    
    f.set_size_inches(7, 3.5, forward = True)
    f.tight_layout()
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

def process_dazzler_alignments_global(alignments, identity_threshold = 0.9, dataset_name = 'native', analyse_folds = True) :
    '''
    '''
    ds = lib.persistent.datasets[dataset_name]
    id2seq = {seq.index : seq for seq in ds}
    graph = {}
    for alg in alignments :
        identity = alg['identity']
        if identity >= identity_threshold :
            i, j = alg['index_i'], alg['index_j']
            if i in id2seq and j in id2seq :
                if not i in graph :
                    graph[i] = []
                if not j in graph :
                    graph[j] = []
                graph[i].append(j)
                graph[j].append(i)
    
    visited = set()

    def dfs(v, dfs_set = None) :
        if v in visited :
            return 0
        visited.add(v)
        if dfs_set is not None :
            dfs_set.add(v)
        count = 1
        for u in graph.get(v, []) :
            count += dfs(u, dfs_set)
        return count

    n_total_nodes, n_duplicated_nodes = 0, 0

    duplicate_paths = []
    for seq in ds :
        i = seq.index
        if not i in visited :
            dfs_set = set()
            count = dfs(i, dfs_set)
            n_total_nodes += count
            if count > 1 :
                n_duplicated_nodes += count
                duplicate_paths.append(list(dfs_set))
    n_duplicate_paths, n_duplicate_paths_different_activity = len(duplicate_paths), 0
    n_duplicate_paths_positive_activity, n_duplicate_paths_negative_activity = 0, 0
    for path in duplicate_paths :
        positive, negative = False, False
        for node in path :
            activity = id2seq[node].ires_activity
            if activity == lib.tools.MINIMAL_IRES_ACTIVITY :
                negative = True
            else :
                positive = True
        if negative and positive :
            n_duplicate_paths_different_activity += 1
        elif positive :
            n_duplicate_paths_positive_activity += 1
        elif negative :
            n_duplicate_paths_negative_activity += 1

    print '[i] Total: %3d (%.2f%%)' % (n_total_nodes, float(n_total_nodes) / n_total_nodes * 100)
    print '[i] Duplicated: %3d (%.2f%%)' % (n_duplicated_nodes, float(n_duplicated_nodes) / n_total_nodes * 100)
    print '[i] Duplicate paths: %3d (%.2f%%)' % (n_duplicate_paths, float(n_duplicate_paths) / max(n_duplicate_paths, 1) * 100)
    print '[i] Duplicate paths with different activity: %3d (%.2f%%)' % (n_duplicate_paths_different_activity, float(n_duplicate_paths_different_activity) / max(n_duplicate_paths, 1) * 100)
    print '[i] Duplicate paths with positive activity:  %3d (%.2f%%)' % (n_duplicate_paths_positive_activity, float(n_duplicate_paths_positive_activity) / max(n_duplicate_paths, 1) * 100)
    print '[i] Duplicate paths with negative activity:  %3d (%.2f%%)' % (n_duplicate_paths_negative_activity, float(n_duplicate_paths_negative_activity) / max(n_duplicate_paths, 1) * 100)
    
    if analyse_folds :
        print ''
        print '[i] Cross-validation folds:'
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        folds = lib.reader.read_pickle(cv_filename)
        fold, n_folds = 0, len(folds.outer_folds)
        for train_indices, test_indices in folds.outer_folds :
            fold +=1
            train_indices = set([ds[ind].index for ind in train_indices])
            test_indices = set([ds[ind].index for ind in test_indices])
            n_train_samples, n_test_samples = len(train_indices), len(test_indices)
            n_split_paths = 0
            n_split_test_nodes, n_split_train_nodes = 0, 0
            for path in duplicate_paths :
                in_train, in_test = False, False
                n_train_nodes, n_test_nodes = 0, 0
                for node in path :
                    if node in train_indices :
                        in_train = True
                        n_train_nodes += 1
                    if node in test_indices :
                        in_test = True
                        n_test_nodes += 1
                if in_train and in_test :
                    n_split_paths += 1
                    n_split_test_nodes += n_test_nodes
                    n_split_train_nodes += n_train_nodes
            print '   [i] Fold %2d/%2d (sizes %4d/%4d) %2d (%2.f%%) split paths -> %2d (%.2f%%) and %2d (%.2f%%) train/test samples' % (fold, n_folds, n_train_samples, n_test_samples, n_split_paths, float(n_split_paths) / max(n_duplicate_paths, 1) * 100, n_split_train_nodes, float(n_split_train_nodes) / n_train_samples * 100, n_split_test_nodes, float(n_split_test_nodes) / n_test_samples * 100)

def plot_partial_dependence_examples(dataset_name = 'viral_native_dsRNA_all', chosen_features = ['count-T-RF_all', 'count-AAAA-RF_all', 'count-CAG-RF_all-window_250_270'], chosen_feature_labels = ['Count of U', 'Count of AAAA', 'Count of CAG in [-20, 0]'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'kmers_kmers-windows', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), min_kmer_length = 1, kmer_length = 4) :
    '''
    '''
    configuration_name, feature_names, experiment = results
    file_path = '../results/regression/%s/%s' % (configuration_name, dataset_name)
    filename_pdp = '%s/partial_dependence/%s-min_kmer_%d-max_kmer_%d-%s-partial_dependence.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
    if os.path.exists(filename_pdp) :
        pdp = lib.reader.read_pickle(filename_pdp)
    else :
        print '[!] Missing (PDP): %s' % filename_pdp
        pdp = None
        return 
    n_chosen_features = len(chosen_features)
    n_rows, n_cols = 1, n_chosen_features
    f, ax = plot.subplots(n_rows, n_cols)
    for i, feature_name in enumerate(chosen_features) :
        cur = ax[i]
        _, segment, pdp_values = pdp[feature_name]
        start, stop = segment
        x = np.array(range(start, stop))
        cur.plot(x, pdp_values, linewidth = 1.5, color = 'k')
        title = chosen_feature_labels[i]
        cur.set_xlim(start, stop - 1)
        cur.set_xlabel('Feature value', fontsize = 13)
        cur.set_title(title, fontsize = 12, fontweight = 'bold')
        #for tick in cur.yaxis.get_major_ticks() : 
        #    tick.label.set_fontsize(12)
        #for tick in cur.xaxis.get_major_ticks() : 
        #    tick.label.set_fontsize(12)
        if i == 0 :
            cur.set_ylabel('Partial dependence', fontsize = 13)
        xax = cur.get_xaxis()
        xax.set_major_locator(pylab.MaxNLocator(integer=True))
    
    f.set_size_inches(13, 3.5, forward = True)
    f.tight_layout()
    plot.show()

def _get_independent_columns(X, tolerance = lib.tools.EPS) :
    '''
    '''
    tolerance = 1
    import scipy.linalg
    _, R = scipy.linalg.qr(X, mode = 'economic')
    n_rows, n_cols = R.shape
    diag = [R[i, i] for i in range(min(n_rows, n_cols))]
    diag = np.array(diag)
    diag = np.abs(diag)
    indices = diag > tolerance
    return X[:, indices]

def perform_canonical_correlation_analysis(dataset_name = 'viral_native_dsRNA_all', feature_set_a = [{'feature_type' : 'kmers', 'min_motif_length' : 1, 'max_motif_length' : 4, 'minimum_fraction' : 0.1, 'cutoff' : 0.05, 'correction_method' : 'fdr_bh'}], feature_set_b = [{'feature_type' : 'kmers-windows', 'min_motif_length' : 1, 'max_motif_length' : 4, 'minimum_fraction' : 0.1, 'cutoff' : 0.05, 'correction_method' : 'fdr_bh'}], use_aux = True) :
    '''
    '''
    Xs, _ = lib.regression.build_regression_matrix_list(dataset_name, feature_set_a, use_aux = use_aux)
    X, _ = lib.regression.build_regression_matrix(Xs, dataset_name, log2 = True)
    del Xs
    Ys, _ = lib.regression.build_regression_matrix_list(dataset_name, feature_set_b, use_aux = use_aux)
    Y, _ = lib.regression.build_regression_matrix(Ys, dataset_name, log2 = True)
    del Ys
    X, Y = X.toarray(), Y.toarray()
    print '[i] Built two matrices:'
    print '    [i] X: ', X.shape
    print '    [i] Y: ', Y.shape
    X, Y = _get_independent_columns(X), _get_independent_columns(Y)
    print '[i] Reduced dimensionality:'
    print '    [i] A: ', X.shape
    print '    [i] B: ', Y.shape

    X_n_vars, Y_n_vars = X.shape[1], Y.shape[1]
    C = np.corrcoef(X.transpose(), Y.transpose())
    Rxx, Ryy = C[: X_n_vars, : X_n_vars], C[X_n_vars :, X_n_vars :]
    Rxy, Ryx = C[: X_n_vars, X_n_vars :], C[X_n_vars :, : X_n_vars]
    R = np.dot(np.dot(np.linalg.inv(Ryy), Ryx), np.dot(np.linalg.inv(Rxx), Rxy))

    W, V = np.linalg.eig(R)
    ind = np.abs(W) < lib.tools.EPS
    W[ind] = 0
    V[:, ind] = 0
    ind = np.argsort(-W)
    W, V = W[ind], V[:, ind]
    #V[np.abs(V) < lib.tools.EPS] = 0
    if np.any(np.abs(W.imag) > lib.tools.EPS) or np.any(np.abs(V.imag) > lib.tools.EPS) :
        raise Exception('Imaginary eigenvalues or eigenvectors found.')
    W, V = W.real, V.real

    W = np.sqrt(W)
    Vy = V
    Vx = np.dot(np.dot(np.linalg.inv(Rxx), Rxy), Vy)
    #Vx[np.abs(Vx) < lib.tools.EPS] = 0

    Tx, Ty = np.dot(X, Vx), np.dot(Y, Vy)
    for i in range(10) :
        coef_r, pvalue = scipy.stats.pearsonr(Tx[:, i], Ty[:, i])
        print '%d : r = %.2f vs w = %.2f' % (i + 1, coef_r, W[i])

    return X, Y, W, Vx, Vy
    
    '''
    import sklearn.cross_decomposition
    max_n_components = 10
    for n_components in range(1, max_n_components + 1) :
        cca = sklearn.cross_decomposition.CCA(n_components, tol = 1e-10)
        cca.fit(Xa, Xb)
        trans_a, trans_b = cca.transform(Xa, Xb)
        corr_list = []
        for i in range(n_components) :
            coef_r, pvalue = scipy.stats.pearsonr(trans_a[:, i], trans_b[:, i])
            corr_list.append(coef_r ** 2)
        print n_components, trans_a.shape, trans_b.shape, corr_list
    return cca, Xa, Xb, trans_a, trans_b
    '''

def check_multiplicity_data(dataset_name = 'mult_all_comb') :
    '''
    '''
    lengths = {'Random_ICS2_17_2' : len('ACCGGCCGT'), 'TEV' : len('TACTCCC'), 'Gtx_133_141' : len('CCGGCGGGT'), 'Random_ICS1_23' : len('CACGGAATCGAGAA'), 'Poliovirus_t_2' : len('CGTCAATTCCTTTA'), 'Rbm3' : len('TAATTTTTTC')}
    offset = 83
    ds = lib.persistent.datasets[dataset_name]
    for seq in ds :
        sequence = seq.sequence
        infos = lib.tools.get_sequence_combination_info(seq)
        for info in infos :
            length = lengths.get(info['type'], 20)
            sites = [sequence[offset + start : offset + start + length] for start in info['site_positions']]
            u_sites = set(sites)
            n_unique_sites = len(u_sites)
            if not ((n_unique_sites == 0 and info['n_sites'] == 0) or n_unique_sites == 1) :
                print '[!] Non-unique (index %d) : %s + %s -> %s' % (seq.index, info['type'], info['background'], sites)
    return seq

def plot_dataset_structure_feature_combination_performance(dataset_names = ['native', 'human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ['All sequences', "Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = [('bppm', 'Accessibility'), ('bppm_pair', 'Region interactions'), ('bppm_bppm_pair', 'Accessibility &\nRegion interactions'), ('kmers_kmers-windows', 'Global & positional counts'), ('bppm_kmers_kmers-windows', 'Global & positional counts &\nAccessibility'), ('bppm_pair_kmers_kmers-windows', 'Global & positional counts &\nRegion interactions'), ('bppm_bppm_pair_kmers_kmers-windows', 'Global & positional counts &\nAccessibility &\nRegion interactions')], min_kmer_length = 1, max_kmer_length = 4, window_setup = (10, 5), plot_gbrf = True) :
    '''
    '''
    n_datasets = len(dataset_names)
    if n_datasets <= 6 :
        n_rows, n_cols = 2, 3
    else :
        n_rows, n_cols = 3, 3
    f, ax = plot.subplots(n_rows, n_cols)
    
    width = 1.0
    window_size, window_step = window_setup

    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
    colors = bmap.mpl_colors
    
    n_features = len(features)
    x = np.array(range(n_features)) * width * 1.3 + width / 2.0
    row, col = 0, 0
    for dataset_name, dataset_label in zip(dataset_names, dataset_labels) :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        configuration_name, experiment = results
        performance = np.zeros((n_features, )) * float('NaN')
        k = 0
        for feature_names, feature_labels in features :
            folder = 'gbrf' if plot_gbrf else 'rf'
            file_path = '../results/regression/%s/%s/%s' % (configuration_name, dataset_name, folder)

            using_bppm = 'bppm' in feature_names
            using_kmers = 'kmers' in feature_names

            if not using_bppm and using_kmers :
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, max_kmer_length, experiment)
            elif using_bppm and not using_kmers :
                filename = '%s/%s-window%d_%d-%s-final.out' % (file_path, feature_names, window_size, window_step, experiment)
            elif using_bppm and using_kmers :
                filename = '%s/%s-window%d_%d-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, window_size, window_step, min_kmer_length, max_kmer_length, experiment)

            if os.path.exists(filename) :
                result = lib.reader.read_pickle(filename)
                performance[k] = result.overall_test_score['r2']
            else :
                print '[!] Missing: %s' % filename
            k += 1
        
        cur.bar(x, performance, width, color = colors[0], edgecolor = 'none')
        cur.tick_params(axis = 'x', direction = 'none')
        cur.locator_params(nbins = 6, axis = 'y')
        cur.set_xlim(0, np.max(x) + width + width / 2.0)
        cur.set_title(dataset_label, fontweight = 'bold', fontsize = 12)
        
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('bottom')
       
        cur.set_xticks(x + width / 2.0)
        for tick in cur.xaxis.get_major_ticks() : 
            tick.tick1On = False 
            tick.tick2On = False 

        if col == 1 and row == 0 :
            cur.legend(prop = {'size' : 10}, loc = 'best')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            feature_labels = [pair[1] for pair in features]
            cur.set_xticklabels(feature_labels, rotation = 90, ha = 'center', va = 'top', multialignment = 'right')
        else :
            cur.set_xticklabels([])
        if col == 0 :
            cur.set_ylabel('$R^2$', fontsize = 13)
        
        yticks = cur.get_yticks()
        _ymin, _ymax = cur.get_ylim()
        for tick in yticks :
            cur.axhline(tick, linewidth = 1, color = 'w')
        cur.set_ylim(_ymin, _ymax)
        
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    if n_datasets <= 6 :
        f.set_size_inches(13, 7.5, forward = True)
    else :
        f.set_size_inches(13, 10.5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    plot.show()

def plot_dataset_subsample_feature_combination_performance(dataset_name = 'viral_native_dsRNA_all', dataset_labels = 'dsRNA viruses', results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = ('kmers_kmers-windows', 'Global & positional counts'), subsamples = [408, 374, 340, 307, 273, 240, 206, 173, 139, 106], n_reps = 5, min_kmer_length = 1, start_kmer_length = 1, max_kmer_length = 5, plot_gbrf = True) :
    '''
    '''
    import brewer2mpl
    bmap = brewer2mpl.get_map('Pastel2', 'qualitative', 5)
    colors = bmap.mpl_colors
    
    f, ax = plot.subplots(1, 1)
    folder = 'gbrf' if plot_gbrf else 'rf'
    subsamples = sorted(subsamples)
    n_subsamples = len(subsamples)
    feature_names, feature_labels = features
    configuration_name, experiment = results
    for kmer_length in range(start_kmer_length, max_kmer_length + 1) :
        performance_mean = np.zeros((n_subsamples, ), dtype = np.double)
        performance_std = np.zeros((n_subsamples, ), dtype = np.double)
        dataset_sizes = []
        for i, subsample in enumerate(subsamples) :
            subsample_results = []
            for rep in range(1, n_reps + 1) :
                file_path = '../results/regression/%s/%s_subsample_%d_rep%d/%s' % (configuration_name, dataset_name, subsample, rep, folder)
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
                if os.path.exists(filename) :
                    found = True
                elif i == len(subsamples) - 1 :
                    file_path = '../results/regression/%s/%s/%s' % (configuration_name, dataset_name, folder)
                    filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
                    if os.path.exists(filename) :
                        found = True

                if found :
                    result = lib.reader.read_pickle(filename)
                    subsample_results.append(result.overall_test_score['pearson'])
                    dataset_sizes.append(np.mean(result.dataset_size))
                else :
                    dataset_sizes.append(None)
                    print '[!] Missing: %s' % filename
            cur_mean, cur_std = np.mean(subsample_results), np.std(subsample_results)
            performance_mean[i], performance_std[i] = cur_mean, cur_std
        print performance_mean
        ax.fill_between(subsamples, performance_mean - performance_std, performance_mean + performance_std, alpha = 0.25, linewidth = 0, color = colors[kmer_length - min_kmer_length], zorder = 2)
        ax.plot(subsamples, performance_mean, linewidth = 2, color = colors[kmer_length - min_kmer_length], label = '$k=%d$' % kmer_length, zorder = 1)
    labels = ['%d\n(%.2f%%)' % (n_positive, n_positive * 100.0 / dataset_size) for n_positive, dataset_size in zip(subsamples, dataset_sizes)]
    ax.set_xticks(subsamples)
    ax.set_xticklabels(labels, rotation = 60)
    ax.set_ylabel('Pearson $r$', fontsize = 13)
    ax.set_xlabel('Positive sequences', fontsize = 13)
    ax.set_xlim(np.min(subsamples) - 10, np.max(subsamples) + 10)

    f.set_size_inches(7, 3.5, forward = True)
    f.tight_layout()
    #f.subplots_adjust(hspace = 0.2)
    plot.show()

def plot_island_activity_distributions(dataset_name = 'native', island_one = (100, 140), island_two = (180, 260), kmers = ['CT', 'TTC', 'CTT', 'TCT'], positive_only = True, log2 = True, atg_offset = 270, count_threshold = 1) :
    '''
    '''
    ds = lib.persistent.datasets[dataset_name]
    if positive_only :
        ds = [seq for seq in ds if seq.ires_activity > lib.tools.MINIMAL_IRES_ACTIVITY]
    site_one_values, site_two_values, none_values, both_values = [], [], [], []
    island_one_start, island_one_stop = island_one
    island_two_start, island_two_stop = island_two
    for seq in ds :
        ires_activity, sequence = seq.ires_activity, seq.sequence
        if log2 :
            ires_activity = np.log2(ires_activity)
        site_one = sequence[island_one_start : island_one_stop]
        site_two = sequence[island_two_start : island_two_stop]
        site_one_count, site_two_count = 0, 0
        for kmer in kmers :
            kmer_length = len(kmer)
            for i in range(len(site_one) - kmer_length) :
                if site_one[i : i + kmer_length] == kmer :
                    site_one_count += 1
            for i in range(len(site_two) - kmer_length) :
                if site_two[i : i + kmer_length] == kmer :
                    site_two_count += 1

        site_one_present, site_two_present = site_one_count >= count_threshold, site_two_count >= count_threshold

        if site_one_present and not site_two_present :
            site_one_values.append(ires_activity)
        elif not site_one_present and site_two_present :
            site_two_values.append(ires_activity)
        elif site_one_present and site_two_present :
            both_values.append(ires_activity)
        else :
            none_values.append(ires_activity)

    print '[i] None: %.2f (%d)' % (np.median(none_values), len(none_values))
    print '[i] Site one: %.2f (%d)' % (np.median(site_one_values), len(site_one_values))
    print '[i] Site two: %.2f (%d)' % (np.median(site_two_values), len(site_two_values))
    print '[i] Both: %.2f (%d)' % (np.median(both_values), len(both_values))

    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
    colors = bmap.mpl_colors
    
    f, ax = plot.subplots(1, 1)
    data = [none_values, site_one_values, site_two_values, both_values]
    labels = ['None', 'First', 'Second', 'Both']
    bp = ax.boxplot(data)
    ax.set_xticklabels(labels)
    title = 'Island activity: [%d, %d] > %d and [%d, %d] > %d' % (island_one_start - atg_offset, island_one_stop - atg_offset, count_threshold - 1, island_two_start - atg_offset, island_two_stop - atg_offset, count_threshold - 1)
    title += '\nFeatures: ' + ' '.join(kmers)
    ax.set_title(title, fontweight = 'bold', fontsize = 14)
    
    for i in range(0, len(bp['boxes'])):
       bp['boxes'][i].set_color(colors[i])
       # we have two whiskers!
       bp['whiskers'][i * 2].set_color(colors[i])
       bp['whiskers'][i * 2 + 1].set_color(colors[i])
       bp['whiskers'][i * 2].set_linewidth(2)
       bp['whiskers'][i * 2 + 1].set_linewidth(2)
       # top and bottom fliers
       # (set allows us to set many parameters at once)
       bp['fliers'][i].set(markerfacecolor = colors[i],
                       marker = 'o', alpha = 0.75, markersize = 6,
                       markeredgecolor = 'none')
       bp['medians'][i].set_color('black')
       bp['medians'][i].set_linewidth(3)
       # and 4 caps to remove
       for c in bp['caps']:
           c.set_linewidth(0)

       box = bp['boxes'][i]
       box.set_linewidth(0)
       boxX = []
       boxY = []
       for j in range(5):
           boxX.append(box.get_xdata()[j])
           boxY.append(box.get_ydata()[j])
           boxCoords = zip(boxX,boxY)
           boxPolygon = matplotlib.patches.Polygon(boxCoords, facecolor = colors[i], linewidth=0)
           ax.add_patch(boxPolygon)
    
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('none')
    f.set_size_inches(7, 4.7, forward = True)
    f.tight_layout()
    plot.show()

def predict_synthetic_experiments(result, types = ['TEV', 'Poliovirus_t_2'], backgrounds = ['HBB', 'Spacer1']) :
    '''
    '''
    n_backgrounds, n_types = len(backgrounds), len(types)
    seqs = lib.persistent.datasets['mult_all_comb']
    Xs, names = lib.regression.build_regression_matrix_list(result.dataset_name, result.features, use_aux = True, fold = None, compute_dataset_name = 'mult_all_comb')
    X, true_y = lib.regression.build_regression_matrix(Xs, 'mult_all_comb', log2 = True)
    X = X.toarray()
    predicted_y = result.regressor.predict(X)
    
    n_rows, n_cols = n_types, n_backgrounds
    f, ax = plot.subplots(n_rows, n_cols)
    for col, background in enumerate(backgrounds) :
        for row, type in enumerate(types) :
            if n_rows > 1 and n_cols > 1 :
                cur = ax[row, col]
            elif n_rows == 1 and n_cols == 1 :
                cur = ax
            elif n_rows == 1 :
                cur = ax[col]
            elif n_cols == 1 :
                cur = ax[row]
            selected_ids = []
            for id, seq in enumerate(seqs) :
                infos = lib.tools.get_sequence_combination_info(seq)
                selected = False
                for info in infos :
                    if info['type'] == type and info['background'] == background :
                        selected = True
                if selected :
                    selected_ids.append(id)
            selected_true_y, selected_predicted_y = np.array([true_y[id] for id in selected_ids]), np.array([predicted_y[id] for id in selected_ids])
            coef_r, pvalue = scipy.stats.pearsonr(selected_true_y, selected_predicted_y)
            print '[i] %s and %s: %.2f (p-value %.2f)' % (type, background, coef_r, pvalue)
            cur.scatter(selected_predicted_y, selected_true_y, s = 20)
            cur.set_xlabel('Predicted')
            cur.set_ylabel('Measured')
            cur.set_title('%s + %s' % (type, background), fontweight = 'bold')
    f.set_size_inches(9, 9, forward = True)
    f.tight_layout()
    plot.show()

def write_csv_file(dataset_name, filename, features = [{'correction_method': 'fdr_bh', 'cutoff': 1.0, 'feature_type': 'kmers', 'max_motif_length': 3, 'min_motif_length': 1, 'minimum_fraction': 0.0}]) :
    '''
    '''
    Xs, feature_names = lib.regression.build_regression_matrix_list(dataset_name, features, use_aux = True, fold = None)
    X, y = lib.regression.build_regression_matrix(Xs, dataset_name, log2 = True)
    names = []
    for names_list in feature_names :
        names.extend(names_list)
    fout = open(filename, 'wb')
    X = X.toarray()
    n_rows, n_cols = X.shape
    for row in range(n_rows) :
        for col in range(n_cols) :
            fout.write('%d,' % X[row, col])
        fout.write('%.10f\n' % y[row])
    fout.close()

def plot_dataset_feature_combination_performance_random(dataset_names = ['human_native_5utr', 'human_native_3utr', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive'], dataset_labels = ["Human $5'$ UTR", "Human $3'$ UTR", 'dsRNA viruses', 'Retroviruses', '(+) ssRNA viruses'], n_reps = 10, results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = [('kmers_kmers-windows', 'Global & positional')], min_kmer_length = 1, max_kmer_length = 5, plot_gbrf = True, save_filename=None) :
    '''
    '''
    n_datasets = len(dataset_names)
    if n_datasets <= 6 :
        n_rows, n_cols = 2, 3
    else :
        n_rows, n_cols = 3, 3
    f, ax = plot.subplots(n_rows, n_cols)
    colors = ['r', 'b', 'g']
    
    x = np.array(range(1, max_kmer_length + 1), dtype = np.int)
    row, col = 0, 0
    for dataset_name, dataset_label in zip(dataset_names, dataset_labels) :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        configuration_name, experiment = results
        k = 0
        for feature_names, feature_labels in features :
            performance = [[] for _ in xrange(max_kmer_length)]
            for kmer_length in range(max_kmer_length) :
                folder = 'gbrf' if plot_gbrf else 'rf'
                for rep in xrange(1, n_reps + 1) :
                    file_path = '../results/regression/%s/random_rep%d_%s/%s' % (configuration_name, rep, dataset_name, folder)
                    filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length + 1, experiment)
                    if os.path.exists(filename) :
                        result = lib.reader.read_pickle(filename)
                        performance[kmer_length].append(result.overall_test_score['r2'])
                    else :
                        print '[!] Missing: %s' % filename
            performance_mean = np.zeros((max_kmer_length, ), dtype = np.float)
            performance_std = np.zeros((max_kmer_length, ), dtype = np.float)
            for i in range(max_kmer_length) :
                performance_std[i] = np.std(performance[i])
                performance_mean[i] = np.mean(performance[i])
            cur.fill_between(x, performance_mean - performance_std, performance_mean + performance_std, alpha = 0.25, linewidth = 0, color = colors[k], zorder = 2)
            cur.plot(x, performance_mean, alpha = 0.45, marker = 'x', label = feature_labels + ' (random)', linewidth = 1.5, color = colors[k], linestyle = ':')

            # Plot the actual result
            performance = np.zeros((max_kmer_length, )) * float('NaN')
            for kmer_length in range(max_kmer_length) :
                folder = 'gbrf' if plot_gbrf else 'rf'
                file_path = '../results/regression/%s/%s/%s' % (configuration_name, dataset_name, folder)
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length + 1, experiment)
                if os.path.exists(filename) :
                    result = lib.reader.read_pickle(filename)
                    performance[kmer_length] = result.overall_test_score['r2']
                else :
                    print '[!] Missing: %s' % filename
            cur.plot(x, performance, alpha = 0.45, marker = 'x', label = feature_labels, linewidth = 1.5, color = colors[k])

            k += 1
        
        cur.locator_params(nbins = 6, axis = 'y')
        cur.set_xlim(0.75, max_kmer_length + 0.25)
        cur.set_title(dataset_label, fontweight = 'bold', fontsize = 12)
        
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('bottom')
        
        if col == 0 and row == 0 :
            cur.legend(prop = {'size' : 10}, loc = 'best')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('$k$-mer length', fontsize = 13)
        else :
            cur.set_xticklabels([])
        if col == 0 :
            cur.set_ylabel('$R^2$', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    if n_datasets <= 6 :
        f.set_size_inches(13, 5, forward = True)
    else :
        f.set_size_inches(13, 7.5, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    if save_filename is not None:
        f.savefig(save_filename)
    plot.show()

def plot_dataset_feature_combination_performance_permutation(dataset_names = ['human_native_5utr', 'human_native_3utr', 'human_native_cds', 'viral_native_dsRNA_all', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_negative'], n_reps = 10, results = ('without-nan-splicing-trimmed_activity0.995-long-multiple_testing-redo2', 'sqrt_eran_refined2-1k-minimum_fraction_0.1-cutoff_0.05-correction_method_fdr_bh'), features = [('kmers', 'Global'), ('kmers-windows', 'Positional'), ('kmers_kmers-windows', 'Global & positional')], min_kmer_length = 1, max_kmer_length = 5, plot_gbrf = True) :
    '''
    '''
    import scipy.stats
    import matplotlib.patches as mpatches
    import brewer2mpl
    bmap = brewer2mpl.get_map('Set2', 'qualitative', 7)
    colors = bmap.mpl_colors
    n_dataset_names = len(dataset_names)
    top = 0.01
    top = 0.04

    def format_pvalue(pvalue) :
        '''
        Format a pvalue for printing it in the figure.
        :param pvalue : pvalue to be formatted.
        '''
        if pvalue > 0.05 :
            return '$p=%.2f$' % pvalue
        elif pvalue > 0.01 :
            return '$p<0.05$'
        else :
            ZERO = 1e-200
            pow = np.log10(pvalue + ZERO)
            pow += 1
            pow = int(pow)
            return '$p<10^{%d}$' % pow

    def get_statistic(dataset_names, name_prefix, feature_names, configuration_name, min_kmer_length, kmer_length) :
        '''
        Return test statistic (variance in predictive power) for the set of datasets.
        '''
        folder = 'gbrf' if plot_gbrf else 'rf'
        performances = []
        for dataset_name in dataset_names :
            file_path = '../results/regression/%s/%s%s/%s' % (configuration_name, name_prefix, dataset_name, folder)
            filename = '%s/%s-min_kmer_%d-max_kmer_%d-%s-final.out' % (file_path, feature_names, min_kmer_length, kmer_length, experiment)
            if os.path.exists(filename) :
                try :
                    result = lib.reader.read_pickle(filename)
                except :
                    print '[-] Unable to read %s' % filename
                    return float('nan')
                #performances.append(result.overall_test_score['r2'])
                performances.append(result.overall_test_score['pearson'])
            else :
                print '[!] Missing: %s' % filename
                return float('nan')
        return np.var(performances)

    configuration_name, experiment = results
    n_features = len(features)
    n_rows, n_cols = 1, n_features
    f, ax = plot.subplots(n_rows, n_cols)
    
    row, col = 0, 0
    for features_name, features_label in features :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        data, defined_data = [], []
        kmer_lengths = range(1, max_kmer_length + 1)
        for kmer_length in kmer_lengths :
            statistics = []
            for rep in xrange(1, n_reps + 1) :
                name_prefix = 'permutation_rep%d_' % rep
                stat = get_statistic(dataset_names, name_prefix, features_name, configuration_name, min_kmer_length, kmer_length)
                if not math.isnan(stat) :
                    statistics.append(stat)

            data.append(statistics)
            statistic = get_statistic(dataset_names, '', features_name, configuration_name, min_kmer_length, kmer_length)
            defined_data.append(statistic)
            
            #loc, shape = scipy.stats.norm.fit(statistics)
            #pvalue = 1.0 - scipy.stats.norm.cdf(statistic, loc, shape)
            unknown_variance = np.mean(statistics)
            #df = len(statistics) - 1
            df = n_dataset_names - 1
            chi2 = scipy.stats.chi2(df)
            scaled_statistic = df * statistic / unknown_variance
            pvalue = 1.0 - chi2.cdf(scaled_statistic)
            cur.text(kmer_length * 1.05, statistic + 0.0005, format_pvalue(pvalue), horizontalalignment = 'center', size = '10',color = 'k')
            #print '[i] %s : k = %d, p = %g (mean = %g, var = %g, x = %g)' % (features_name, kmer_length, pvalue, loc, shape, statistic)
            print '[i] %s : k = %d, p = %g (df = %d, var = %g, x = %g)' % (features_name, kmer_length, pvalue, df, unknown_variance, scaled_statistic)

        bp = cur.boxplot(data)
        defined_scatter = cur.scatter(kmer_lengths, defined_data, label = 'Defined groups', color = colors[1], zorder = 200)
        
        cur.locator_params(nbins = 6, axis = 'y')
        cur.set_xlim(0.25, max_kmer_length + 0.75)
        cur.set_ylim(0, top)
        cur.set_title(features_label, fontweight = 'bold', fontsize = 12)
        
        cur.spines['right'].set_visible(False)
        cur.spines['top'].set_visible(False)
        cur.yaxis.set_ticks_position('left')
        cur.xaxis.set_ticks_position('none')
    
        for i in range(0, len(bp['boxes'])):
           bp['boxes'][i].set_color(colors[0])
           # we have two whiskers!
           bp['whiskers'][i * 2].set_color(colors[0])
           bp['whiskers'][i * 2 + 1].set_color(colors[0])
           bp['whiskers'][i * 2].set_linewidth(2)
           bp['whiskers'][i * 2 + 1].set_linewidth(2)
           # top and bottom fliers
           # (set allows us to set many parameters at once)
           bp['fliers'][i].set(markerfacecolor = colors[0],
                           marker = 'o', alpha = 0.75, markersize = 6,
                           markeredgecolor = 'none')
           bp['medians'][i].set_color('black')
           bp['medians'][i].set_linewidth(3)
           # and 4 caps to remove
           for c in bp['caps']:
               c.set_linewidth(0)

           box = bp['boxes'][i]
           box.set_linewidth(0)
           boxX, boxY = [], []
           for j in range(5) :
               boxX.append(box.get_xdata()[j])
               boxY.append(box.get_ydata()[j])
               boxCoords = zip(boxX,boxY)
               boxPolygon = matplotlib.patches.Polygon(boxCoords, facecolor = colors[0], linewidth = 0, zorder = 100)
               cur.add_patch(boxPolygon)
        
        if col == 0 and row == 0 :
            items, legends = [], []
            if not bp is None :
                #items.append(bp)
                red_patch = mpatches.Patch(color = colors[0])
                items.append(red_patch)
                legends.append('Permuted groups')
            if not defined_scatter is None :
                items.append(defined_scatter)
                legends.append('Defined groups')
            cur.legend(items, legends, prop = {'size' : 10}, loc = 'best')
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_datasets :
            cur.set_xlabel('$k$-mer length', fontsize = 13)
        else :
            cur.set_xticklabels([])
        if col == 0 :
            #cur.set_ylabel('Variance in\nprediction performance ($R^2$)', fontsize = 13)
            cur.set_ylabel('Variance in\nprediction performance (Pearson $r$)', fontsize = 13)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(13, 4, forward = True)
    f.tight_layout()
    f.subplots_adjust(hspace = 0.2)
    plot.show()

