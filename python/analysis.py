'''
A module implementing (some) analysis routines for Shira's paper.

10-05-2015
Alexey Gritsenko
'''

import numpy as np
import math
import copy

try :
    #import seaborn as sns
    pass
except :
    pass
from sklearn.cluster import KMeans
import sklearn.mixture
import pylab
import matplotlib.pyplot as plot
import matplotlib.colors
from matplotlib import gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib_venn import venn2
from matplotlib_venn import venn3
from pandas import DataFrame
from IPython.parallel import Client
import sklearn.neighbors

import lib.reader
import lib.tools
import lib.fmm
from lib.SimpleNamespace import SimpleNamespace

MAGIC_FOLD_CHANGE = 10 ** -2.5
BACKGROUND_IRES_ACTIVITY = 206.29
SIGNIFICANT_IRES_ACTIVITY = 600.0

_single_mutagenesis_group = set(['Scanning_Mut_CDS_IRESs', 'Scanning_Mut_High_Priority_Genes_5UTR', 'Scanning_Mut_High_Priority_Viruses_5UTR', 'Scanning_Mut_IRESite', 'Scanning_Mut_rRNA_matching_5UTR'])
_complementary_rRNA_group = set(['rRNAs_revcomp_blocks'])
_native_group = set(['CDS_screen', 'High_Priority_Genes_Blocks', 'High_Priority_Viruses_Blocks', 'Human_5UTR_Screen', 'Viral_5UTR_Screen', 'rRNA_Matching_5UTRs'])

def read_active_kmer_list(filename) :
    '''
    Read an active k-mer list from a given file.
    :param filename : name of the file to read the list from.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    motifs = {}
    parsing_active = False
    for line in lines :
        line = line.strip()
        if line == 'Active' :
            parsing_active = True
            continue
        elif line == 'Inactive' :
            parsing_active = False
            continue
        else :
            args = line.split('\t')
            n_args = len(args)
            if n_args == 3 :
                if args[0] == 'k-mer' :
                    continue
                if parsing_active :
                    kmer = args[0]
                    corrected_pvalue = float(args[2])
                    motifs[kmer] = corrected_pvalue
            elif n_args == 1 :
                continue
            else :
                print '[!] Incorrect number of argument'
    return motifs

def read_splicing_scores(filename) :
    '''
    Read splicing scores from a given filename.
    :param filename : name of the file to read splicing scores from.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    results = {}
    for line in lines :
        args = line.strip().split('\t')
        id = int(args[0])
        score = float(args[1])
        results[id] = score
    return results

def get_oligos(update_splicing_score = '../data/new_splicing_scores.tab') :
    '''
    Returns all oligos that are available.
    '''
    seqs = lib.reader.read_sequences()
    if update_splicing_score is not None :
        scores = read_splicing_scores(update_splicing_score)
        for seq in seqs :
            seq.splicing_score = scores[seq.index]
    return lib.tools.trim_sequences(seqs)

def read_shira_indices(filename = '../data/ires_lib_oligos_for_fmm_analysis.tab') :
    '''
    Read IDs for analysis provided by Shira.
    :param filename : name of the file, from which the sequences should be read.
    '''
    ids = []
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        args = line.strip().split('\t')
        id = int(args[0])
        ids.append(id)
    return ids

def read_shira_clusters(groups, local_filename = '../data/ires_ids_local_cluster.tab', global_filename = '../data/ires_ids_global_cluster.tab') :
    '''
    Read local and global mutagenesis clusters from Shira.
    :param group : mutagenesis groups to split into two clusters.
    :param local_filename : name of the local cluster IDs.
    :param global_filename : name of the global cluster IDs.
    '''
    local_cluster, global_cluster = {}, {}
    fin = open(local_filename, 'rb')
    lines = fin.readlines()
    fin.close()
    for line in lines :
        id = int(line.strip())
        found = False
        for group in groups :
            original = groups[group][0]
            if original.index == id :
                local_cluster[group] = groups[group]
                found = True
                break
        if not found :
            print '[i] Unable to find ID: %d (local cluster)' % id
            
    fin = open(global_filename, 'rb')
    lines = fin.readlines()
    fin.close()
    for line in lines :
        id = int(line.strip())
        found = False
        for group in groups :
            original = groups[group][0]
            if original.index == id :
                global_cluster[group] = groups[group]
                found = True
                break
        if not found :
            print '[i] Unable to find ID: %d (global cluster)' % id

    return local_cluster, global_cluster

def is_sequence_promoter(seq, threshold = 0.2) :
    '''
    Determines whether a sequence is a promoter.
    :param seq : sequence that should be checked.
    :param threshold : threshold for determining promoter activity (anything equal to or above has activity).
    '''
    return seq.promoter_activity > threshold and not math.isnan(seq.promoter_activity)

def remove_promoters(seqs, promoter_threshold = 0.2) :
    '''
    Remove sequences with promoter activity.
    :param seqs : a list of oligos to be filtered.
    :param promoter_threshold : threshold for promoter activity (anything equal to or above that is removed).
    '''
    results = []
    for seq in seqs :
        if is_sequence_promoter(seq, threshold = promoter_threshold) :
            continue
        results.append(seq)
    return results

def is_sequence_splice_site(seq, threshold = -2.5) :
    '''
    Checks whether a sequence is a splice site.
    :param seq : sequence that should be checked.
    :param threshold : threshold that is used to detect splice sites (everything below).
    '''
    return seq.splicing_score < threshold and not math.isnan(seq.splicing_score)

def remove_splice_sites(seqs, splicing_threshold = -2.5) :
    '''
    Remove sequences with cryptic splicing activity.
    :param seqs : a list of oligos to be filtered.
    :param splicing_threshold : threshold for splicing activity (everything below or equal is removed).
    '''
    results = []
    for seq in seqs :
        if is_sequence_splice_site(seq, threshold = splicing_threshold) :
            continue
        results.append(seq)
    return results

def get_mutagenesis_oligos(seqs, group = _single_mutagenesis_group) : 
    '''
    From a given list of oligos selects those that belong to the mutagenesis experiments.
    :param seqs : a list of sequences from which the selection should be performed.
    :param group : a set of libraries that should be selected.
    '''
    selected = []
    for seq in seqs :
        should_add = False
        sequence_libs = lib.tools.get_sequence_libs(seq)
        for library in sequence_libs :
            if library in group :
                should_add = True
                break
        if should_add :
            selected.append(seq)
    return selected

def get_native_oligos(seqs, group = _native_group) : 
    '''
    From a given list of oligos selects those that belong to the complementary rRNA libraries.
    :param seqs : a list of sequences from which the selection should be performed.
    :param group : a set of libraries that should be selected.
    '''
    selected = []
    for seq in seqs :
        should_add = False
        sequence_libs = lib.tools.get_sequence_libs(seq)
        for library in sequence_libs :
            if library in group :
                should_add = True
                break
        if should_add :
            selected.append(seq)
    return selected

def get_oligos_by_id(seqs, ids) :
    '''
    Select oligos by ID from a given list of oligos.
    :param seqs : a list of sequences to select from.
    :param ids : a list of ids to select.
    '''
    ids = set(ids)
    selected = []
    for seq in seqs :
        if seq.index in ids :
            selected.append(seq)
            ids.remove(seq.index)
    if len(ids) > 0 :
        print '[!] Could not find IDs: ' + ' '.join([str(id) for id in ids])
    return selected

def get_complementary_rRNA_oligos(seqs, group = _complementary_rRNA_group) : 
    '''
    From a given list of oligos selects those that belong to the native libraries.
    :param seqs : a list of sequences from which the selection should be performed.
    :param group : a set of libraries that should be selected.
    '''
    selected = []
    for seq in seqs :
        should_add = False
        sequence_libs = lib.tools.get_sequence_libs(seq)
        for library in sequence_libs :
            if library in group :
                should_add = True
                break
        if should_add :
            selected.append(seq)
    return selected

def is_sequence_structured(seq, threshold = -81.00) :
    '''
    Checks whether a sequence is structured.
    :param seq : sequence that should be checked.
    :param threshold : MFE threshold for structured sequences (everything <= is structured).
    '''
    return seq.mfe <= threshold

def is_sequence_unstructured(seq, threshold = -48.20) :
    '''
    Checks whether a sequence is unstructured.
    :param seq : sequence that should be checked.
    :param threshold : MFE threshold for unstructured sequences (everything >= is unstructured).
    '''
    return seq.mfe >= threshold

def get_structured_oligos(seqs, threshold = -81.00) :
    '''
    Selects structured oligos from a given list of oligos.
    :param seqs : sequences from which oligos should be selected.
    :param threshold : MFE threshold for structured sequences (everything <= is structured).
    '''
    selected = []
    for seq in seqs :
        if is_sequence_structured(seq, threshold) :
            selected.append(seq)
    return selected

def get_unstructured_oligos(seqs, threshold = -48.20) :
    '''
    Selects unstructured oligos from a given list of oligos.
    :param seqs : sequences from which oligos should be selected.
    :param threshold : MFE threshold for unstructured sequences (everything >= is unstructured).
    '''
    selected = []
    for seq in seqs :
        if is_sequence_unstructured(seq, threshold) :
            selected.append(seq)
    return selected

def _mutagenesis_oligo_position_key(seq) :
    '''
    Gets start position of a mutagenesis oligo.
    :param oligo : oligo for which the position should be established.
    '''
    args = seq.description.split(';')
    position = int(args[3])
    return position

def group_mutagenesis_by_original_oligo(seqs) :
    '''
    Groups mutagenesis oligos by the oligo they originate from (unmutated oligo).
    :param seqs : sequences that should be groupped.
    '''
    groups = {}
    originals = {}
    for seq in seqs :
        args = seq.description.split(';')
        barcode_type = args[0]
        library = args[1]
        oligo_description = args[2]
        start, length = int(args[3]), int(args[4])
        mutation_type = args[5]
        from_sequence = args[6]
        to_sequence = args[7]
        if '|' in to_sequence :
            to_sequence = to_sequence.split('|')[0]
        if from_sequence == to_sequence :
            if oligo_description in originals :
                print '[!] Duplicate original for: %s' % oligo_description
            originals[oligo_description] = seq
        else :
            if not oligo_description in groups :
                groups[oligo_description] = []
            groups[oligo_description].append(seq)
        #if args[1] == 'Scanning_Mut_rRNA_matching_5UTR' and from_sequence != to_sequence :
        #    print from_sequence, to_sequence
    group_keys = set(groups.keys())
    originals_keys = set(originals.keys())
    if group_keys != originals_keys :
        print '[!] Original and group sets of oligo descriptions do not match (for %d descriptions)' % (len(group_keys ^ originals_keys))
        #return groups, originals

    final_groups = {}
    for description in groups :
        if description in originals :
            final_groups[description] = copy.deepcopy(groups[description])
            final_groups[description].insert(0, originals[description])
            final_groups[description].sort(key = _mutagenesis_oligo_position_key)

    return final_groups

def contains_motif(sequence, motif, allowed_mismatches = 0) :
    '''
    Determines whether a sequence contains a given motif.
    :param sequence : sequence to be considered.
    :param motif : motif to be considered.
    :param allowed_mismatched : maximum number of mismatches allowed in the motif.
    '''
    sequence_length = len(sequence)
    motif_length = len(motif)
    for offset in range(sequence_length - motif_length + 1) :
        mismatches = 0
        for i in range(motif_length) :
            if motif[i] != sequence[offset + i] :
                mismatches += 1
                if mismatches > allowed_mismatches :
                    break
    #for offset in range(-motif_length, sequence_length) :
    #    mismatches = 0
    #    start, stop = 0, motif_length
    #    if offset < 0 :
    #        mismatches += -offset
    #        start = -offset
    #    if sequence_length - offset < motif_length :
    #        mismatches += motif_length - (sequence_length - offset)
    #        stop = sequence_length - offset
    #    for i in range(start, stop) :
    #        if motif[i] != sequence[offset + i] :
    #            mismatches += 1
    #            if mismatches > allowed_mismatches :
    #                break
        if mismatches <= allowed_mismatches :
            return True
    return False

def contains_motif_from_region(sequence, motif_sequence, motif_length = 7, allowed_mismatches = 2) :
    '''
    Determines whether a sequence contains any motif from a given motif sequence.
    :param sequence : sequence to be considered.
    :param motif_sequence : sequence that should used as motif source.
    :param motif_length : length of the motif that should be searched for.
    :param allowed_mismatches : number of allowed mismatched.
    '''
    motifs = set()
    sequence_length = len(motif_sequence)
    for offset in range(sequence_length - motif_length + 1) :
        motifs.add(motif_sequence[offset : offset + motif_length])
    for motif in motifs :
        if contains_motif(sequence, motif, allowed_mismatches = allowed_mismatches) :
            return True
    return False

def plot_mutagenesis_structured_unstructured_groups(groups, motif = 'TTTTT', allowed_mismatches = 1, should_plot = True) :
    '''
    Plot mutagenesis results for groups of structured and unstructured sequences.
    :param groups : a dictionary of groups that should be plotted.
    :param should_plot : a boolean flag determining whether the actual plot should be made. If not, then cluster descriptions are returned instead.
    '''
    descriptions = []
    n_skipped = 0
    ids = set()
    for oligo_description in groups :
        oligos = groups[oligo_description]
        original = oligos[0]
        if math.isnan(original.ires_activity) or is_sequence_promoter(original) or is_sequence_splice_site(original) or original.ires_activity <= SIGNIFICANT_IRES_ACTIVITY :
            n_skipped += 1
            continue
        descriptions.append(oligo_description)
        ids.add(original.index)
        
    n_descriptions = len(descriptions)
    print '[i] Total: %d, skipped: %d' % (len(groups), n_skipped)
    print '[i] Selected: %d' % n_descriptions
    n_oligos = len(groups[oligo_description]) - 1
    oligo_length = len(groups[oligo_description][0].sequence)
    oligo_segments = []
    for oligo in groups[oligo_description][1:] :
        args = oligo.description.split(';')
        start, length = int(args[3]), int(args[4])
        oligo_segments.append((start, start + length))

    array = np.zeros((n_descriptions, oligo_length), dtype = np.float) * float('nan')
    matched = []
    for i, description in enumerate(descriptions) :
        oligos = groups[description]
        original = oligos[0]
        original_ires_activity = original.ires_activity - BACKGROUND_IRES_ACTIVITY
        matched.append([])
        for j in range(n_oligos) :
            seq = oligos[j + 1]
            if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
                continue
            oligo_ires_activity = seq.ires_activity - BACKGROUND_IRES_ACTIVITY
            if oligo_ires_activity > 0 :
                fold_change = np.log2(oligo_ires_activity / original_ires_activity)
            else :
                fold_change = np.log2(MAGIC_FOLD_CHANGE)
                #print fold_change
                #fold_change = float('nan')
            start, stop = oligo_segments[j]
            array[i, start : stop] = fold_change
                
            description = seq.description
            description = description.split(';')
            original_sequence, mutated_sequence = description[6], description[7]
            if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
                mid = (stop + start) / 2
                matched[i].append(mid)
                array[i, mid] = 8
    
    nan_locations = np.isnan(array)
    for i in range(n_descriptions) :
        locations = np.isnan(array[i, :])
        array[i, locations] = np.nanmedian(array[i, :])

    est = KMeans(n_clusters = 2, random_state = 10)
    est.fit(array)
    dist = est.transform(array)
    array[nan_locations] = float('nan')
    labels = est.labels_
    class0, class1 = labels == 0, labels == 1
    n_class0, n_class1 = np.sum(class0, dtype = np.int), np.sum(class1, dtype = np.int)
    dist_class = np.zeros((n_descriptions, ))
    dist_class[class0] = dist[class0, 0]
    dist_class[class1] = dist[class1, 1]
    class0, class1 = np.where(class0)[0], np.where(class1)[0]
    class0, class1 = class0.tolist(), class1.tolist()
    class0.sort(key = lambda x : -dist_class[x])
    class1.sort(key = lambda x : -dist_class[x])
    print '[i] Cluster 0: %d' % n_class0
    print '[i] Cluster 1: %d' % n_class1
    if not should_plot :
        return ([descriptions[id] for id in class0], [descriptions[id] for id in class1])
    cmap = matplotlib.cm.coolwarm
    vmin, vmax = np.nanmin(array), np.nanmax(array)
  
    f = plot.figure(figsize = (8, 6))
    gs = gridspec.GridSpec(2, 1, height_ratios = [n_class0, n_class1])
    ax = np.array([plot.subplot(gs[0]), plot.subplot(gs[1])])
    vmin = -max(abs(vmin), abs(vmax))
    vmax = -vmin
    im = ax[0].imshow(array[class0, :], interpolation = 'nearest', cmap = cmap, vmin = vmin, vmax = vmax)
    im = ax[1].imshow(array[class1, :], interpolation = 'nearest', cmap = cmap, vmin = vmin, vmax = vmax)
    #ax[0].autoscale(False)
    #for i, id in enumerate(class0) :
    #    positions = matched[id]
    #    n_positions = len(positions)
    #    ax[0].scatter([i] * n_positions, positions, marker = '*')
    ax[0].set_title('Local sensitivity')
    ax[1].set_title('Global sensitivity')
    ax[0].set_xlim(0, 180)
    ax[1].set_xlim(0, 180)
    ax[0].yaxis.set_visible(False)
    ax[1].yaxis.set_visible(False)
    ax[0].set_xticklabels([])
    for tic in ax[0].xaxis.get_major_ticks():
        tic.tick1On = tic.tick2On = False
        tic.label1On = tic.label2On = False
    for tic in ax[1].xaxis.get_major_ticks():
        tic.tick1On = tic.tick2On = False

    plot.tight_layout()
    gs.update(left = 0.14, top = 0.99, bottom = 0.02, hspace = -0.2, wspace = 0)
    cbar_ax = f.add_axes([0.02, 0.05, 0.05, 0.90])
    f.colorbar(im, cax = cbar_ax)

    plot.show()
    return ids

def _generate_motifs(motif, same_at_only = True, pos = 0) :
    '''
    Generate background motifs for a given motif.
    :param motif : motif for which the background motifs should be generated.
    :param same_at_only : whether only background sequences with the same AT content should be generated.
    :param pos : position which is used for recursion.
    '''
    motif_length = len(motif)
    if pos == motif_length :
        return [motif]
    else :
        if same_at_only :
            if motif[pos] == 'T' or motif[pos] == 'A' :
                results = []
                pre, post = motif[:pos], motif[pos + 1 :]
                res1 = _generate_motifs(pre + 'T' + post, same_at_only, pos + 1)
                res2 = _generate_motifs(pre + 'A' + post, same_at_only, pos + 1)
                results.extend(res1)
                results.extend(res2)
                return results
            else :
                return _generate_motifs(motif, same_at_only, pos + 1)
        else :
            results = []
            pre, post = motif[:pos], motif[pos + 1 :]
            res1 = _generate_motifs(pre + 'T' + post, same_at_only, pos + 1)
            res2 = _generate_motifs(pre + 'A' + post, same_at_only, pos + 1)
            res3 = _generate_motifs(pre + 'C' + post, same_at_only, pos + 1)
            res4 = _generate_motifs(pre + 'G' + post, same_at_only, pos + 1)
            results.extend(res1)
            results.extend(res2)
            results.extend(res3)
            results.extend(res4)
            return results

def make_mutagenesis_UUUU_results(descriptions, groups, min_motif_length = 4, max_motif_length = 6, min_allowed_mismatches = 0, max_allowed_mismatches = 2, type = 'fold_change', select_negative_effect = True, same_at_only = True) :
    '''
    Compute mutagenesis poly-U results.
    :param descriptions : descriptions for which these results should be computed.
    :param groups : groups of mutagenesis windows.
    :param min_motif_length : minimum length of the poly-U motif to be considered.
    :param max_motif_length : maximum length of the poly-U motif to be considered.
    :param min_allowed_mismatches : minimum number of mismatches allowed.
    :param max_allowed_mismatches : maximum number of mismatches allowed.
    :param type : type of counting to perform : background of fold_change.
    :param select_negative_effect : whether negative effect windows (leading to reduced expression when mutagenized) should be selected.
    :param same_at_only : whether only the same AT content motifs should be used as background.
    '''
    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups
    #motif = 'TTTT'
    #allowed_mismatches = 0

    def _get_pvalue(motif, allowed_mismatches) :
        cnt = np.zeros((2, 2), dtype = np.int)
        for group in groups :
            original = group[0]
            original_ires_activity = original.ires_activity
            windows = group[1:]
            for window in windows :
                if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                    continue
                window_ires_activity = window.ires_activity
                description = window.description
                description = description.split(';')
                original_sequence, mutated_sequence = description[6], description[7]
                fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
                if math.isnan(fold_change) :
                    fold_change = np.log2(MAGIC_FOLD_CHANGE)
                is_selected = (window_ires_activity == BACKGROUND_IRES_ACTIVITY and type == 'background') or (fold_change < 0 and type == 'fold_change') 
                if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
                    if (is_selected and select_negative_effect) or (not select_negative_effect and not is_selected) :
                        cnt[0, 0] += 1
                    else :
                        cnt[0, 1] += 1
                else :
                    if (is_selected and select_negative_effect) or (not select_negative_effect and not is_selected) :
                        cnt[1, 0] += 1
                    else :
                        cnt[1, 1] += 1
        #fraction = cnt[0, 0] / float(cnt[0, 0] + cnt[0, 1])
        fraction = cnt[0, 0] / float(cnt[0, 0] + cnt[1, 0])
        import scipy.stats
        ratio, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
        return ratio, fraction, pvalue 

    results = []
    for motif_length in range(min_motif_length, max_motif_length + 1) : 
        motif = 'T' * motif_length
        polyA_motif = 'A' * motif_length
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            ratio, motif_fraction, motif_pvalue = _get_pvalue(motif, allowed_mismatches)
            polyA_ratio, polyA_motif_fraction, polyA_motif_pvalue = _get_pvalue(polyA_motif, allowed_mismatches)
            print motif, allowed_mismatches, ratio, motif_pvalue
            pvalues, fractions = [], []
            other_motifs = _generate_motifs(motif, same_at_only = same_at_only)
            actual_other_motifs = []
            min_pvalue = 1
            for other_motif in other_motifs :
                if other_motif == motif :
                    continue
                actual_other_motifs.append(other_motif)
                _, fraction, pvalue = _get_pvalue(other_motif, allowed_mismatches)
                if not math.isnan(pvalue) :
                    pvalues.append(pvalue)
                if not math.isnan(fraction) :
                    fractions.append(fraction)
                if min_pvalue > pvalue :
                    min_pvalue = pvalue
                    print 'New min: %s, %g' % (other_motif, pvalue)
            result = {'motif' : motif, 'pvalue' : motif_pvalue, 'fraction' : motif_fraction, 'background_pvalues' : pvalues, 'background_motifs' : actual_other_motifs, 'background_fractions' : fractions, 'allowed_mismatches' : allowed_mismatches, 'polyA_motif' : polyA_motif, 'polyA_pvalue' : polyA_motif_pvalue, 'polyA_fraction' : polyA_motif_fraction}
            results.append(result)
    return results

def plot_mutagenesis_UUUU_results(results, plot_fractions = False) :
    '''
    Plot mutagenesis results for poly-U counting/testing experiments.
    :param results : results that should be plotted.
    :param plot_fractions : a boolean flag that determines whether fractions (reduced activity windows / total windows) should be plotted.
    '''
    width = 0.7
    n_results = len(results)
    f, ax = plot.subplots(1, 1)
    pos = range(n_results)
    if not plot_fractions :
        data = [-np.log10(result['background_pvalues']) for result in results]
    else :
        data = [result['background_fractions'] for result in results]
    ax.violinplot(data, pos, points = 50, widths = width, showmeans = True, showextrema = True)
    print result
    if not plot_fractions :
        values = [-np.log10(result['pvalue']) for result in results]
    else :
        values = [result['fraction'] for result in results]
    ax.scatter(pos, values, marker = 'o', color = 'b')
    ax.set_xlim(-1, n_results)
    if not plot_fractions :
        _, ymax = ax.get_ylim()
        ax.set_ylim(0, ymax)
        ax.set_ylabel('$-\log_{10}p$', fontsize = 16)
    else :
        ax.set_ylabel('Fraction of reduced activity windows', fontsize = 16)
    ax.set_xticks(pos)
    ax.set_xticklabels(['$\mathrm{T}^{%d}$, %dmm' % (len(result['motif']), result['allowed_mismatches']) for result in results], rotation = 60, ha = 'right')

    for tick in ax.xaxis.get_major_ticks() :
        tick.label.set_fontsize(16)

    plot.tight_layout()
    plot.show()

def rewrite_motif(motif) :
    '''
    Rewrite motif into RNA alphabet (replace all Ts with Us).
    :param motif : motif to be re-written.
    '''
    chars = [nuc for nuc in motif]
    chars = ['U' if nuc == 'T' else nuc for nuc in chars]
    motif = ''.join(chars)
    return motif

def plot_pair_mutagenesis_UUUU_results(result_negative, result_positive, plot_fractions = False, highlight_motifs = None) :
    '''
    Plot mutagenesis results for poly-U counting/testing experiments.
    :param result_negative : positive results that should be plotted.
    :param result_positive : negative results that should be plotted.
    :param plot_fractions : a boolean flag that determines whether fractions (reduced activity windows / total windows) should be plotted.
    '''
    width = 0.7
    f, ax = plot.subplots(1, 1)
    colors = ['b', 'g', 'r', 'k', 'c', 'm']
    k = 0
    pos = range(2)
    if highlight_motifs is None :
        highlight_motifs = []
        highlight_motifs.append(result_negative['motif'])
        highlight_motifs.append(result_negative['polyA_motif'])

    if not plot_fractions :
        data = [-np.log10(result_negative['background_pvalues']), -np.log10(result_positive['background_pvalues'])]
    else :
        data = [result_negative['background_fractions'], result_positive['background_fractions']]
    ax.violinplot(data, pos, points = 150, widths = width, showmeans = True, showextrema = True, bw_method = 0.25)
    if not plot_fractions :
        values = [-np.log10(result_negative['pvalue']), -np.log10(result_positive['pvalue'])]
        polyA_values = [-np.log10(result_negative['polyA_pvalue']), -np.log10(result_positive['polyA_pvalue'])]
    else :
        values = [result_negative['fraction'], result_positive['fraction']]
        polyA_values = [result_negative['polyA_fraction'], result_positive['polyA_fraction']]
    
    if result_negative['motif'] in highlight_motifs :
        ax.scatter(pos, values, marker = 'o', color = colors[k], s = 80, label = '%s' % rewrite_motif(result_negative['motif']))
        k += 1
    if result_negative['polyA_motif'] in highlight_motifs :
        ax.scatter(pos, polyA_values, marker = 'o', color = colors[k], s = 80, label = '%s' % rewrite_motif(result_negative['polyA_motif']))
        k += 1
    
    for motif in highlight_motifs :
        if motif == result_negative['motif'] or motif == result_negative['polyA_motif'] :
            continue
        pos, values = [], []
        index = np.where(np.array(result_negative['background_motifs']) == motif)[0]
        print '+', motif, index
        if len(index) > 0 :
            index = index[0]
            pos.append(0)
            if not plot_fractions :
                values.append(-np.log10(result_negative['background_pvalues'][index]))
            else :
                values.append(result_negative['background_fractions'][index])
        index = np.where(np.array(result_positive['background_motifs']) == motif)[0]
        if len(index) > 0 :
            index = index[0]
            pos.append(1)
            if not plot_fractions :
                values.append(-np.log10(result_positive['background_pvalues'][index]))
            else :
                values.append(result_positive['background_fractions'][index])
        ax.scatter(pos, values, marker = 'o', color = colors[k], s = 80, label = '%s' % rewrite_motif(motif))
        k += 1
    
    ax.set_xlim(-1, 2)
    ax.legend(prop = {'size' : 14, 'family' : 'monospace'})
    if not plot_fractions :
        _, ymax = ax.get_ylim()
        ax.set_ylim(0, ymax)
        ax.set_ylabel('$-\log_{10}p$', fontsize = 16)
    else :
        ax.set_ylabel('Fraction of windows', fontsize = 16)
    ax.set_xticks([0, 1])
    ax.set_xticklabels(['Reduced\nwindows', 'No change\nwindows'], rotation = 0, ha = 'center')

    for tick in ax.xaxis.get_major_ticks() :
        tick.label.set_fontsize(16)

    plot.tight_layout()
    plot.show()

def plot_selected_mutagenesis_UUUU_results(result, plot_fractions = False) :
    '''
    Plot mutagenesis results for poly-U counting/testing experiments.
    :param result : results that should be plotted.
    :param plot_fractions : a boolean flag that determines whether fractions (reduced activity windows / total windows) should be plotted.
    '''
    width = 0.7
    f, ax = plot.subplots(1, 1)
    pos = range(1)
    if not plot_fractions :
        data = [-np.log10(result['background_pvalues'])]
    else :
        data = [result['background_fractions']] 
    ax.violinplot(data, pos, points = 50, widths = width, showmeans = True, showextrema = True)
    if not plot_fractions :
        values = [-np.log10(result['pvalue'])]
        polyA_values = [-np.log10(result['polyA_pvalue'])]
    else :
        values = [result['fraction']]
        polyA_values = [result['polyA_fraction']]
    ax.scatter(pos, values, marker = 'o', color = 'b', s = 80, label = '%s' % rewrite_motif(result['motif']))
    ax.scatter(pos, polyA_values, marker = 'o', color = 'g', s = 80, label = '%s' % rewrite_motif(result['polyA_motif']))
    ax.set_xlim(-1, 2)
    ax.legend(prop = {'size' : 14, 'family' : 'monospace'})
    if not plot_fractions :
        _, ymax = ax.get_ylim()
        ax.set_ylim(0, ymax)
        ax.set_ylabel('$-\log_{10}p$', fontsize = 16)
    else :
        ax.set_ylabel('Fraction of windows', fontsize = 16)
    ax.set_xticks(pos)
    ax.set_xticklabels(['Reduced\nwindows'], rotation = 0, ha = 'center')

    for tick in ax.xaxis.get_major_ticks() :
        tick.label.set_fontsize(16)

    plot.tight_layout()
    plot.show()

def run_ffm_mutagenesis_analysis(descriptions, groups, background_type = 'mutagenized', select_negative_effect = True, full_length = False, select_on_fold_change = True) :
    '''
    Runs FMM motif search for mutagenesis windows.
    :param descriptions : a list of descriptions that should be analysed.
    :param groups : groups of mutagenesis windows.
    :param background_type : type of background sequences to use for mutagenesis analysis (can be mutagenized or no_effect).
    :param select_negative_effect : whether negative effect windows (leading to reduced expression when mutagenized) should be selected.
    :param full_length : whether full length oligos should be used, if False - only the original and mutagenized windows are used.
    :param select_on_fold_change : if True, oligos are selected if, when mutagenized, they lead to reduced expression. If False, oligo are selected if, when mutagenized, they lead to background IRES activity.
    '''
    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups
   
    sequences, background_sequences = [], []
    for group in groups :
        should_add_full_length = False
        original = group[0]
        original_ires_activity = original.ires_activity
        windows = group[1:]
        for window in windows :
            if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                continue
            window_ires_activity = window.ires_activity
            description = window.description
            description = description.split(';')
            original_sequence, mutated_sequence = description[6], description[7]
            fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
            if math.isnan(fold_change) :
                fold_change = np.log2(MAGIC_FOLD_CHANGE)
            selected = (select_on_fold_change and fold_change < 0) or (not select_on_fold_change and window_ires_activity == BACKGROUND_IRES_ACTIVITY)
            if (selected and select_negative_effect) or (not select_negative_effect and not selected) :
                if not full_length :
                    sequences.append(('Seq%d_original' % window.index, original_sequence))
                    if background_type == 'mutagenized' :
                        background_sequences.append(('Seq%d_mutagenized' % window.index, mutated_sequence))
                else :
                    should_add_full_length = True
                    if background_type == 'mutagenized' :
                        background_sequences.append(('Seq%d_mutagenized_full' % window.index, window.sequence))
            else :
                if not full_length :
                    if background_type == 'no_effect' :
                        background_sequences.append(('Seq%d_original' % window.index, original_sequence))
                else :
                    if background_type == 'no_effect' :
                        raise Exception('Full length option is incompatible with %s background type' % background_type)
        if should_add_full_length :
            sequences.append(('Seq%d_original_full' % original.index, original.sequence))
    motifs = lib.fmm.find_pssm_motifs(sequences, background_sequences)
    print '[i] Sequences:  %d' % len(sequences)
    print '[i] Background: %d' % len(background_sequences)
    return motifs

def plot_fmm_motifs(motifs) :
    '''
    Plot FMM motifs.
    :param motifs : motifs that should be plotted.
    '''
    n_motifs = len(motifs)
    f, ax = plot.subplots(1, n_motifs)
    if n_motifs == 1 :
        ax = [ax]
    for cur, motif in zip(ax, motifs) :
        cur.imshow(motif.logo, interpolation = 'nearest', aspect = 'auto')
        cur.set_title('$p=%g$' % motif.pvalue)
        cur.axes.get_xaxis().set_visible(False)
        cur.axes.get_yaxis().set_visible(False)
    
    figure_width = 18.0 / 5
    f.set_size_inches(figure_width * n_motifs, 3, forward = True)
    plot.tight_layout()
    plot.show()

def perform_global_enrichment_analysis(seqs, min_motif_length = 4, max_motif_length = 6, min_allowed_mismatches = 0, max_allowed_mismatches = 2, same_at_only = True) :
    
    def _get_pvalue(motif, positive_seqs, negative_seqs, allowed_mismatches) :
        cnt = np.zeros((2, 2), dtype = np.int)
        for seq in positive_seqs :
            if contains_motif(seq.sequence, motif, allowed_mismatches) :
                cnt[0, 0] += 1
            else :
                cnt[0, 1] += 1
        for seq in negative_seqs :
            if contains_motif(seq.sequence, motif, allowed_mismatches) :
                cnt[1, 0] += 1
            else :
                cnt[1, 1] += 1
        import scipy.stats
        _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
        return pvalue 
    
    positive_seqs, negative_seqs = [], []
    n_skipped = 0
    for seq in seqs :
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            n_skipped += 1
            continue
        if seq.ires_activity == BACKGROUND_IRES_ACTIVITY :
            negative_seqs.append(seq)
        else :
            positive_seqs.append(seq)

    print '[i] Total: %d' % len(seqs)
    print '[i] Skipped: %d' % n_skipped
    print '[i] Positive: %d' % len(positive_seqs)
    print '[i] Negative: %d' % len(negative_seqs)
    results = []
    for motif_length in range(min_motif_length, max_motif_length + 1) : 
        motif = 'T' * motif_length
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            print '[i] Processing: %s (%d mismatches)' % (motif, allowed_mismatches)
            motif_pvalue = _get_pvalue(motif, positive_seqs, negative_seqs, allowed_mismatches)
            pvalues = []
            other_motifs = _generate_motifs(motif, same_at_only = same_at_only)
            for other_motif in other_motifs :
                if other_motif == motif :
                    continue
                pvalue = _get_pvalue(other_motif, positive_seqs, negative_seqs, allowed_mismatches)
                if not math.isnan(pvalue) :
                    pvalues.append(pvalue)
            result = {'motif' : motif, 'pvalue' : motif_pvalue, 'background_pvalues' : pvalues, 'allowed_mismatches' : allowed_mismatches}
            results.append(result)
    return results

def plot_global_enrichment_results(results) :
    '''
    Plot mutagenesis results for poly-U counting/testing experiments.
    :param results : results that should be plotted.
    '''
    width = 0.7
    n_results = len(results)
    f, ax = plot.subplots(1, 1)
    pos = range(n_results)
    data = [-np.log10(result['background_pvalues']) for result in results]
    ax.violinplot(data, pos, points = 50, widths = width, showmeans = True, showextrema = True)
    values = [-np.log10(result['pvalue']) for result in results]
    ax.scatter(pos, values, marker = 'o', color = 'b')
    ax.set_xlim(-1, n_results)
    _, ymax = ax.get_ylim()
    ax.set_ylim(0, ymax)
    ax.set_ylabel('$-\log_{10}p$', fontsize = 16)
    ax.set_xticks(pos)
    ax.set_xticklabels(['$\mathrm{T}^{%d}$, %dmm' % (len(result['motif']), result['allowed_mismatches']) for result in results], rotation = 60, ha = 'right')

    for tick in ax.xaxis.get_major_ticks() :
        tick.label.set_fontsize(16)

    plot.tight_layout()
    plot.show()

def fit_splicing_score_mixture_model(seqs, n_components = 2, n_bins = 80) :
    '''
    Fit a Gaussian mixture model to the splicing score measurements.
    :param n_components : number of mixture components to fit.
    :param n_bins : number of bins to use for histogram visualization.
    '''
    scores = np.array([seq.splicing_score for seq in seqs if not math.isnan(seq.splicing_score)])
    scores_array = np.atleast_2d(scores)
    mixture = sklearn.mixture.GMM(n_components = n_components)
    mixture.fit(scores_array.transpose())
    f, ax = plot.subplots(1, 1)
    n, bins, patches = ax.hist(scores, bins = n_bins, normed = True, histtype = 'stepfilled', facecolor = 'b', alpha = 0.45)
    colors = ['r', 'g', 'k', 'y', 'm', 'b']
    for i in range(n_components) :
        y = mixture.weights_[i] * pylab.normpdf(bins, mixture.means_[i][0], math.sqrt(mixture.covars_[i][0]))
        line = ax.plot(bins, y, colors[i], linewidth = 1.5)
    ax.set_xlabel('Splicing score')
    ax.yaxis.set_visible(False)
    print '[i] BIC: %f' % mixture.bic(scores_array.transpose())
    print '[i] AIC: %f' % mixture.aic(scores_array.transpose())
    plot.tight_layout()
    plot.show()

def make_local_global_sensitivity_plot() :
    '''
    Make local global sensitivity cluster plot.
    '''
    seqs = get_oligos()
    mut = get_mutagenesis_oligos(seqs)
    mut_groups = group_mutagenesis_by_original_oligo(mut)
    ids = plot_mutagenesis_structured_unstructured_groups(mut_groups)

def make_mutagenesis_UUUU_plot(type = 'fold_change', plot_fractions = False, same_at_only = True, select_negative_effect = True) :
    '''
    Make a plot of UUUU mutagenesis results for the group of local sensitivity oligos.
    :param type : type of counting to perform : background of fold_change.
    :param plot_fractions : a boolean flag that determines whether fractions (reduced activity windows / total windows) should be plotted.
    :param same_at_only : a boolean flag determining whether only same AT-content motifs should be used as background motifs.
    :param select_negative_effect : whether negative effect windows (leading to reduced expression when mutagenized) should be selected.
    '''
    seqs = get_oligos()
    mut = get_mutagenesis_oligos(seqs)
    mut_groups = group_mutagenesis_by_original_oligo(mut)
    local_desc, global_desc = plot_mutagenesis_structured_unstructured_groups(mut_groups, should_plot = False)
    results = make_mutagenesis_UUUU_results(local_desc, mut_groups, type = type, same_at_only = same_at_only, select_negative_effect = select_negative_effect)
    plot_mutagenesis_UUUU_results(results, plot_fractions = plot_fractions)

def plot_venn_diagrams(seqs, motifs = ['TTTT', 'ATAT', 'AAAA'], allowed_mismatches = 0) :
    '''
    Plot Venn diagrams for several motifs.
    '''
    n_motifs = len(motifs)
    f, ax = plot.subplots(1, n_motifs)
    for i, motif in enumerate(motifs) :
        n_positive, n_contains_motif, n_positive_and_contains_motif = 0, 0, 0
        n_negative, n_negative_and_contains_motif = 0, 0
        n_skipped = 0
        cnt = np.zeros((2, 2), dtype = np.int)
        for seq in seqs :
            if is_sequence_promoter(seq) or is_sequence_splice_site(seq) :
                n_skipped += 1
                continue
            has_motif = contains_motif(seq.sequence, motif, allowed_mismatches)
            if has_motif :
                n_contains_motif += 1
            if seq.ires_activity > SIGNIFICANT_IRES_ACTIVITY :
                n_positive += 1
                if has_motif :
                    n_positive_and_contains_motif += 1
                    cnt[0, 0] += 1
                else :
                    cnt[0, 1] += 1 
            else :
                n_negative += 1
                if has_motif :
                    n_negative_and_contains_motif += 1
                    cnt[1, 0] += 1 
                else :
                    cnt[1, 1] += 1
        #cnt = np.rot90(cnt, 3)
        #venn2([n_contains_motif - n_positive, n_positive - n_positive_and_contains_motif, n_positive_and_contains_motif], ('With motif, %dmm' % allowed_mismatches, 'Positive'), ax = ax[i])
        #venn3([n_contains_motif, n_positive - n_positive_and_contains_motif, n_positive_and_contains_motif, n_negative - n_negative_and_contains_motif, n_negative_and_contains_motif, 0, 0], ('With motif, %dmm' % allowed_mismatches, 'Positive', 'Negative'), ax = ax[i])
        venn3([0, n_positive - n_positive_and_contains_motif, n_positive_and_contains_motif, n_negative - n_negative_and_contains_motif, n_negative_and_contains_motif, 0, 0], ('With motif,\n%dmm' % allowed_mismatches, 'Positive', 'Negative'), ax = ax[i])
        import scipy.stats
        odds, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater') 
        #_, pvalue, _, _ = scipy.stats.chi2_contingency(cnt) 
        print '[i] Motif: %s (odds %g, %g)' % (motif, odds, pvalue)
        print '[i] Total: %d, skipped: %d' % (len(seqs), n_skipped)
        print '[i] With motif: %d, Positive: %d, Both: %d' % (n_contains_motif, n_positive, n_positive_and_contains_motif)
        print '[i] With motif: %d, Negative: %d, Both: %d' % (n_contains_motif, n_negative, n_negative_and_contains_motif)
        print ''
        print cnt
        original_pvalue = pvalue
        pvalue = math.log(pvalue, 10)
        pvalue = int(pvalue + 1)
        #ax[i].set_title('%s, $\\log_{10}p=%.2f$' % (motif, pvalue))
        if pvalue < 0 :
            ax[i].set_title('%s, $p<10^{%d}$' % (rewrite_motif(motif), pvalue))
        elif original_pvalue > 0.5 :
            ax[i].set_title('%s, $p>0.5$' % rewrite_motif(motif))
        elif original_pvalue > 0.05 :
            ax[i].set_title('%s, $p>0.05$' % rewrite_motif(motif))
    f.set_size_inches(12, 4.5, forward = True)
    plot.tight_layout()
    plot.show()

def plot_complementary_rRNA(seqs, type = '18S', window_size = 50, should_plot = True) :
    '''
    Plot complementary rRNA IRES activity.
    :param seqs : sequences that should be processed.
    :param type : type of rRNA sequence that should be plotted (18S or 28S).
    :param window_size : size of the moving average windows that should be used for smoothing.
    :param should_plot : a boolean flag determining whether the results should be plotted (otherwise only the active segments are returned).
    '''
    selected_seqs = []
    rRNA_length = -1
    for seq in seqs :
        args = seq.description.split(';')
        which = args[2]
        start = int(args[4]) - 1
        if which != type :
            continue
        rRNA_length = max(rRNA_length, start + len(seq.sequence))
        selected_seqs.append(seq)
    seqs = selected_seqs
    print '[i] %s has length %d' % (type, rRNA_length)
    
    activity = np.zeros((rRNA_length, ), dtype = np.float)
    covered = np.zeros((rRNA_length, ), dtype = np.int)
    sequence = np.zeros((rRNA_length, ), dtype = 'S')
    for seq in seqs :
        args = seq.description.split(';')
        which = args[2]
        start = int(args[4]) - 1
        oligo_length = len(seq.sequence)
        sequence[start : start + oligo_length] = [c for c in seq.sequence]
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            continue
        ires_activity = seq.ires_activity
        activity[start : start + oligo_length] += ires_activity
        covered[start : start + oligo_length] += 1

    regions = []
    activity /= covered
    activity = np.log2(activity / BACKGROUND_IRES_ACTIVITY)
    activity = np.convolve(activity, np.ones((window_size,)) / window_size)[window_size - 1 :]
    background_level = np.log2(SIGNIFICANT_IRES_ACTIVITY / BACKGROUND_IRES_ACTIVITY)
    background_levels = np.array([background_level] * rRNA_length)
    pos = 0
    while pos < rRNA_length :
        if activity[pos] >= background_level :
            start = pos
            pos += 1
            while pos < rRNA_length and activity[pos] >= background_level :
                pos += 1
            regions.append(('active', start, pos, ''.join(sequence[start : pos])))
        pos += 1
    pos = 0
    while pos < rRNA_length :
        if activity[pos] < background_level :
            start = pos
            pos += 1
            while pos < rRNA_length and activity[pos] < background_level :
                pos += 1
            regions.append(('inactive', start, pos, ''.join(sequence[start : pos])))
        pos += 1

    if should_plot :
        x = np.array(range(1, rRNA_length + 1))
        f, ax = plot.subplots(1, 1)
        ax.plot(x, activity)
        ax.plot(x, background_levels)
        ax.set_xlim(1, rRNA_length)
        ax.set_title('%s rRNA' % type)
        ax.set_xlabel('Position', fontsize = 16)
        ax.set_ylabel('Average IRES activity, $\\log_2$', fontsize = 16)
        f.set_size_inches(13, 5, forward = True)
        plot.tight_layout()
        plot.show()

    return regions

def _get_pvalue(motif, positive_seqs, negative_seqs, allowed_mismatches) :
    '''
    Perform Fisher's exact test and return its p-value.
    :param motif : motif for which the test should be performed.
    :param posititve_seqs : positive class sequences.
    :param negative_seqs : negative class sequences.
    :param allowed_mismatches : number of allowed mismatches.
    '''
    cnt = np.zeros((2, 2), dtype = np.int)
    for seq in positive_seqs :
        if contains_motif(seq.sequence, motif, allowed_mismatches) :
            cnt[0, 0] += 1
        else :
            cnt[0, 1] += 1
    for seq in negative_seqs :
        if contains_motif(seq.sequence, motif, allowed_mismatches) :
            cnt[1, 0] += 1
        else :
            cnt[1, 1] += 1
    import scipy.stats
    _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
    return pvalue

def perform_kmer_region_mutagenesis_enrichment_analysis(descriptions, groups, regions, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2) :
    '''
    Calculate k-mer enrichment in mutagenesis windows for k-mers from active and inactive rRNA regions.
    :param descriptions : list of descriptions of mutagenesis sequences.
    :param groups : groups of mutagenesis sequences (grouped by original oligo).
    :param regions : a list of rRNA regions.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _get_pvalue(motif, groups, allowed_mismatches) :
        cnt = np.zeros((2, 2), dtype = np.int)
        for group in groups :
            original = group[0]
            original_ires_activity = original.ires_activity
            windows = group[1:]
            for window in windows :
                if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                    continue
                window_ires_activity = window.ires_activity
                description = window.description
                description = description.split(';')
                original_sequence, mutated_sequence = description[6], description[7]
                fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
                if math.isnan(fold_change) :
                    fold_change = np.log2(MAGIC_FOLD_CHANGE)
                is_selected = fold_change < 0 
                if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
                    if is_selected :
                        cnt[0, 0] += 1
                    else :
                        cnt[0, 1] += 1
                else :
                    if is_selected :
                        cnt[1, 0] += 1
                    else :
                        cnt[1, 1] += 1
        import scipy.stats
        _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
        return pvalue 

    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups

    motifs = {}
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for i, region in enumerate(regions) :
            region_type, region_start, region_stop, region_sequence = region
            region_length = len(region_sequence)
            for offset in range(region_length - motif_length + 1) :
                motif = region_sequence[offset : offset + motif_length]
                if not motif in motifs :
                    motifs[motif] = set()
                region_description = (i, region_type, region_start, region_stop)
                motifs[motif].add(region_description)

    pvalues = {}
    for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
        print '[i] Processing: %d mismatches' % allowed_mismatches
        for motif in motifs :
            pvalue = _get_pvalue(motif, groups, allowed_mismatches)
            pvalues[(motif, allowed_mismatches)] = pvalue
            print '%g (%s)' % (pvalue, motif),
        print ''

    return motifs, pvalues

def perform_kmer_region_enrichment_analysis(seqs, regions, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2, client_url = None) :
    '''
    Calculate k-mer enrichment in positive vs. negative sequences for k-mers from active and inactive rRNA regions.
    :param seqs : a list of sequences that should be used for calculating enrichment.
    :param regions : a list of rRNA regions.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _package_rRNA_tests(positive_seqs, negative_seqs, tasks, batch_size = 20) :
        '''
        Package rRNA tests into a parallel task.
        :param positive_seqs : positive sequences.
        :param negative_seqs : negative sequences.
        :param tasks : a list of tasks that should be packaged.
        :param batch_size : size of the batch for packaging.
        '''
        n_tasks = len(tasks)
        packages = []
        start = 0
        while start < n_tasks :
            package = (positive_seqs, negative_seqs, tasks[start : start + batch_size])
            packages.append(package)
            start += batch_size
        return packages

    def _parallel_get_pvalue(param) :
        '''
        Parallel version of get_pvalue.
        :param param : a list of parameters (positive sequences, negative sequences and tasks).
        '''
        def contains_motif(sequence, motif, allowed_mismatches = 0) :
            '''
            Determines whether a sequence contains a given motif.
            :param sequence : sequence to be considered.
            :param motif : motif to be considered.
            :param allowed_mismatched : maximum number of mismatches allowed in the motif.
            '''
            sequence_length = len(sequence)
            motif_length = len(motif)
            for offset in range(sequence_length - motif_length + 1) :
                mismatches = 0
                for i in range(motif_length) :
                    if motif[i] != sequence[offset + i] :
                        mismatches += 1
                        if mismatches > allowed_mismatches :
                            break
                if mismatches <= allowed_mismatches :
                    return True
            return False
        import scipy.stats
        positive_seqs, negative_seqs, tasks = param
        results = {}
        for motif, allowed_mismatches in tasks :
            cnt = np.zeros((2, 2), dtype = np.int)
            for seq in positive_seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    cnt[0, 0] += 1
                else :
                    cnt[0, 1] += 1
            for seq in negative_seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    cnt[1, 0] += 1
                else :
                    cnt[1, 1] += 1
            _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
            results[(motif, allowed_mismatches)] = pvalue
        return results
    
    positive_seqs, negative_seqs = [], []
    n_skipped = 0
    for seq in seqs :
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            n_skipped += 1
            continue
        if seq.ires_activity == BACKGROUND_IRES_ACTIVITY :
            negative_seqs.append(seq)
        else :
            positive_seqs.append(seq)

    print '[i] Total: %d' % len(seqs)
    print '[i] Skipped: %d' % n_skipped
    print '[i] Positive: %d' % len(positive_seqs)
    print '[i] Negative: %d' % len(negative_seqs)
    motifs = {}
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for i, region in enumerate(regions) :
            region_type, region_start, region_stop, region_sequence = region
            region_length = len(region_sequence)
            for offset in range(region_length - motif_length + 1) :
                motif = region_sequence[offset : offset + motif_length]
                if not motif in motifs :
                    motifs[motif] = set()
                region_description = (i, region_type, region_start, region_stop)
                motifs[motif].add(region_description)
    pvalues = {}
    if client_url is None :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            print '[i] Processing: %d mismatches' % allowed_mismatches
            for motif in motifs :
                pvalue = _get_pvalue(motif, positive_seqs, negative_seqs, allowed_mismatches)
                pvalues[(motif, allowed_mismatches)] = pvalue
    else :
        tasks = []
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            for motif in motifs :
                tasks.append((motif, allowed_mismatches))
        packages = _package_rRNA_tests(positive_seqs, negative_seqs, tasks, batch_size = 100)
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
        results = view.map(_parallel_get_pvalue, packages, block = False, ordered = True)
        for result in results :
            for key, pvalue in result.iteritems() :
                pvalues[key] = pvalue
        del view
        client.close()
        del client

    return motifs, pvalues

def perform_kmer_active_vs_inactive_region_enrichment_analysis(regions, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2) :
    '''
    Calculate k-mer enrichment of k-mers in active regions vs. k-mers in inactive regions.
    :param regions : a list of rRNA regions.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    '''

    def _get_pvalue(motif, regions, allowed_mismatches) :
        motif_length = len(motif)
        motif_array = np.array([c for c in motif])
        inactive_cnt = np.zeros((2, ), dtype = np.int)
        active_cnt = np.zeros((2, ), dtype = np.int)
        counts = []
        for i, region in enumerate(regions) :
            region_type, region_start, region_stop, region_sequence = region
            cnt = np.zeros((2, ), dtype = np.int)
            region_length = len(region_sequence)
            for offset in range(region_length - motif_length + 1) :
                current_motif = region_sequence[offset : offset + motif_length]
                current_motif = np.array([c for c in current_motif])
                mismatches = np.sum(motif_array != current_motif, dtype = np.int)
                if mismatches <= allowed_mismatches :
                    cnt[0] += 1
                else :
                    cnt[1] += 1
            if region_type == 'inactive' :
                inactive_cnt += cnt
            else :
                active_cnt += cnt
            counts.append(cnt)

        import scipy.stats
        results = []
        for i, region in enumerate(regions) :
            region_type, region_start, region_stop, region_sequence = region
            if region_type == 'active' :
                _, pvalue = scipy.stats.fisher_exact([counts[i], inactive_cnt], alternative = 'greater')
                results.append((i, pvalue))

        _, pvalue = scipy.stats.fisher_exact([active_cnt, inactive_cnt], alternative = 'greater')
        results.append(('all', pvalue))
        return results

    motifs = set()
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for i, region in enumerate(regions) :
            region_type, region_start, region_stop, region_sequence = region
            region_length = len(region_sequence)
            for offset in range(region_length - motif_length + 1) :
                motif = region_sequence[offset : offset + motif_length]
                motifs.add(motif)

    pvalues = {}
    for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
        print '[i] Processing: %d mismatches' % allowed_mismatches
        for motif in motifs :
            res = _get_pvalue(motif, regions, allowed_mismatches)
            for region, pvalue in res :
                pvalues[(motif, region, allowed_mismatches)] = pvalue
                if pvalue < 0.05 :
                    print '%s, region %s, %d mismatches: pvalue %g' % (motif, str(region), allowed_mismatches, pvalue)

    return motifs, pvalues

def plot_kmer_region_enrichment_results(motifs, pvalues, regions, chosen_region = 0, remove_intersection = False) :
    '''
    Plot k-mer enrichment results for rRNA regions.
    :param motifs : a dictionary of motifs with a list of regions in which they occur.
    :param pvalues : a dictionary of pvalues for the corresponding tests.
    :param regions : a list of rRNA regions.
    :param chosen_region : region for which the results should be plotted (integer or 'all').
    :param remove_intersection : whether k-mers present in both active and inactive regions should be removed from the plot.
    '''
    width = 0.7
    n_regions = len(regions)
    active_regions, inactive_regions = [], []
    for i, region in enumerate(regions) :
       region_type, region_start, region_stop, region_sequence = region
       if region_type == 'active' :
           active_regions.append(i)
       else :
           inactive_regions.append(i)
    n_active_regions = len(active_regions)
    min_allowed_mismatches, max_allowed_mismatches = float('inf'), -float('inf')
    min_motif_length, max_motif_length = float('inf'), -float('inf')
    for key in pvalues :
        motif, allowed_mismatches = key
        motif_length = len(motif)
        min_motif_length = min(min_motif_length, motif_length)
        max_motif_length = max(max_motif_length, motif_length)
        min_allowed_mismatches = min(min_allowed_mismatches, allowed_mismatches)
        max_allowed_mismatches = max(max_allowed_mismatches, allowed_mismatches)
    
    n_results = (max_motif_length - min_motif_length + 1) * (max_allowed_mismatches - min_allowed_mismatches + 1)
    
    inactive_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(set(inactive_regions) & set([description[0] for description in motif_regions])) > 0]
    inactive_motifs = set(inactive_motifs)

    f, ax = plot.subplots(2, 1, sharex = True)
    positions = range(n_results)
    active_data, inactive_data, labels = [], [], []
    if chosen_region == 'active' :
        chosen_set = set(active_regions)
    else :
        chosen_set = set([chosen_region])
    data_frame = []
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            active_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(chosen_set & set([description[0] for description in motif_regions])) > 0]
            
            if remove_intersection :
                active_motifs = set(active_motifs)
                intersection = active_motifs & inactive_motifs
                active_motifs -= intersection
                current_inactive_motifs = inactive_motifs - intersection
            else :
                current_inactive_motifs = inactive_motifs
            
            active_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in active_motifs if len(motif) == motif_length]
            inactive_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in current_inactive_motifs if len(motif) == motif_length]
            if len(set(active_pvalues)) == 1 :
                active_pvalues = [float('Nan'), 1]
            if len(set(inactive_pvalues)) == 1 :
                inactive_pvalues = [float('Nan'), 1]
            import scipy.stats
            active_data.append(-np.log10(active_pvalues))
            inactive_data.append(-np.log10(inactive_pvalues))
            labels.append('$k=%d$, %dmm' % (motif_length, allowed_mismatches))
            print '$k=%d$, %dmm' % (motif_length, allowed_mismatches), scipy.stats.ranksums(active_pvalues, inactive_pvalues)
            for motif in active_motifs :
                if len(motif) == motif_length : 
                    entry = (motif, True, -np.log10(pvalues[(motif, allowed_mismatches)]), '$k=%d$, %dmm' % (motif_length, allowed_mismatches))
                    data_frame.append(entry)
            for motif in inactive_motifs :
                if len(motif) == motif_length :
                    entry = (motif, False, -np.log10(pvalues[(motif, allowed_mismatches)]), '$k=%d$, %dmm' % (motif_length, allowed_mismatches))
                    data_frame.append(entry)
    dt = np.dtype([('motif', np.str_, 16), ('active', np.bool, 1), ('pvalue', np.float, 1), ('setting', np.str_, 30)])
    data_frame = np.array(data_frame, dtype = dt)
    data_frame = DataFrame(data_frame, columns = ['motif', 'active', 'pvalue', 'setting'])
    ax[0].violinplot(active_data, positions, points = 50, widths = width, showmeans = True, showextrema = True)
    #sns.violinplot(x = 'setting', y = 'pvalue', hue = 'active', data = data_frame, width = width, scale = 'count', ax = ax[0])
    ax[0].set_ylabel('$-\log_{10}p$', fontsize = 16)
    ax[1].set_ylabel('$-\log_{10}p$', fontsize = 16)
    ax[1].set_xlim(-1, n_results)
    ax[1].set_xticks(positions)
    ax[1].set_xticklabels(labels, rotation = 60, ha = 'right')
    ax[1].violinplot(inactive_data, positions, points = 50, widths = width, showmeans = True, showextrema = True)
    for tick in ax[1].xaxis.get_major_ticks() :
        tick.label.set_fontsize(16)
    if chosen_region != 'active' :
        region_type, region_start, region_stop, region_sequence = regions[chosen_region]
        ax[0].set_title('Region $[%d, %d]$' % (region_start, region_stop - 1))
    else :
        ax[0].set_title('All active regions')
    ax[1].set_title('All inactive regions')
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def plot_histogram_kmer_region_enrichment_results(motifs, pvalues, regions, chosen_region = 0, remove_intersection = False) :
    '''
    Plot k-mer enrichment results for rRNA regions as histograms.
    :param motifs : a dictionary of motifs with a list of regions in which they occur.
    :param pvalues : a dictionary of pvalues for the corresponding tests.
    :param regions : a list of rRNA regions.
    :param chosen_region : region for which the results should be plotted (integer or 'all').
    :param remove_intersection : whether k-mers present in both active and inactive regions should be removed from the plot.
    '''
    trim_quantile = 0.95
    n_steps = 1000
    bandwidth = 0.3
    n_regions = len(regions)
    active_regions, inactive_regions = [], []
    for i, region in enumerate(regions) :
       region_type, region_start, region_stop, region_sequence = region
       if region_type == 'active' :
           active_regions.append(i)
       else :
           inactive_regions.append(i)
    n_active_regions = len(active_regions)
    min_allowed_mismatches, max_allowed_mismatches = float('inf'), -float('inf')
    min_motif_length, max_motif_length = float('inf'), -float('inf')
    for key in pvalues :
        motif, allowed_mismatches = key
        motif_length = len(motif)
        min_motif_length = min(min_motif_length, motif_length)
        max_motif_length = max(max_motif_length, motif_length)
        min_allowed_mismatches = min(min_allowed_mismatches, allowed_mismatches)
        max_allowed_mismatches = max(max_allowed_mismatches, allowed_mismatches)
    
    n_results = (max_motif_length - min_motif_length + 1) * (max_allowed_mismatches - min_allowed_mismatches + 1)

    if n_results <= 4 :
        n_rows, n_cols = 1, n_results
    elif n_results <= 12 :
        n_rows, n_cols = 3, 4
    elif n_results <= 20 :
        n_rows, n_cols = 5, 4
    elif n_results <= 24 :
        n_rows, n_cols = 4, 6
    elif n_results <= 36 :
        n_rows, n_cols = 6, 6
    
    inactive_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(set(inactive_regions) & set([description[0] for description in motif_regions])) > 0]
    inactive_motifs = set(inactive_motifs)

    f, ax = plot.subplots(n_rows, n_cols, sharey = True)
    positions = range(n_results)
    if chosen_region == 'active' :
        chosen_set = set(active_regions)
    else :
        chosen_set = set([chosen_region])
    row, col = 0, 0
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            active_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(chosen_set & set([description[0] for description in motif_regions])) > 0]
            
            if remove_intersection :
                active_motifs = set(active_motifs)
                intersection = active_motifs & inactive_motifs
                active_motifs -= intersection
                current_inactive_motifs = inactive_motifs - intersection
            else :
                current_inactive_motifs = inactive_motifs
            
            active_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in active_motifs if len(motif) == motif_length]
            inactive_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in current_inactive_motifs if len(motif) == motif_length]
            import scipy.stats
            z_score, pvalue = scipy.stats.ranksums(active_pvalues, inactive_pvalues)
            if n_rows == 1 :
                cur = ax[col]
            else :
                cur = ax[row, col]
            col += 1
            if col >= n_cols :
                row += 1
                col = 0
            n_active, n_inactive = len(active_pvalues), len(inactive_pvalues)
            quant = max(scipy.stats.mstats.mquantiles(-np.log10(active_pvalues), trim_quantile)[0], scipy.stats.mstats.mquantiles(-np.log10(inactive_pvalues), trim_quantile)[0])
            x_plot = np.linspace(0, quant, n_steps)
            if n_active > 0 :
                kernel_active = sklearn.neighbors.KernelDensity(bandwidth = bandwidth)
                kernel_active.fit(-np.log10(active_pvalues)[:, np.newaxis])
                active_score = kernel_active.score_samples(x_plot[:, np.newaxis])
                cur.plot(x_plot, np.exp(active_score), color = 'b', alpha = 0.45)
            if n_inactive > 0 :
                kernel_inactive = sklearn.neighbors.KernelDensity(bandwidth = bandwidth)
                kernel_inactive.fit(-np.log10(inactive_pvalues)[:, np.newaxis])
                inactive_score = kernel_inactive.score_samples(x_plot[:, np.newaxis])
                cur.plot(x_plot, np.exp(inactive_score), color = 'r', alpha = 0.45)
            cur.set_xlim(0, quant)
            print '[i] k = %d, mm = %d, z-score = %g, pvalue = %g' % (motif_length, allowed_mismatches, z_score, pvalue)
            if pvalue >= 0.5 or z_score > 0 :
                cur.set_title('$k=%d$, %dmm' % (motif_length, allowed_mismatches))
            else :
                if pvalue < 0.05 :
                    n_stars = 1
                    if pvalue < 1e-3 :
                        n_stars = 2
                    if pvalue < 1e-5 :
                        n_stars = 3
                    cur.set_title('$k=%d$, %dmm, $^{%s}$' % (motif_length, allowed_mismatches, '*' * n_stars))
                else :
                    cur.set_title('$k=%d$, %dmm' % (motif_length, allowed_mismatches))

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def plot_ranking_kmer_region_enrichment_results(motifs, pvalues, regions, chosen_region = 0, do_fdr = True, remove_intersection = False) :
    '''
    Plot k-mer enrichment results for rRNA regions as ranking.
    :param motifs : a dictionary of motifs with a list of regions in which they occur.
    :param pvalues : a dictionary of pvalues for the corresponding tests.
    :param regions : a list of rRNA regions.
    :param chosen_region : region for which the results should be plotted (integer or 'all').
    :param remove_intersection : whether k-mers present in both active and inactive regions should be removed from the plot.
    '''
    n_regions = len(regions)
    active_regions, inactive_regions = [], []
    for i, region in enumerate(regions) :
       region_type, region_start, region_stop, region_sequence = region
       if region_type == 'active' :
           active_regions.append(i)
       else :
           inactive_regions.append(i)
    n_active_regions = len(active_regions)
    min_allowed_mismatches, max_allowed_mismatches = float('inf'), -float('inf')
    min_motif_length, max_motif_length = float('inf'), -float('inf')
    for key in pvalues :
        motif, allowed_mismatches = key
        motif_length = len(motif)
        min_motif_length = min(min_motif_length, motif_length)
        max_motif_length = max(max_motif_length, motif_length)
        min_allowed_mismatches = min(min_allowed_mismatches, allowed_mismatches)
        max_allowed_mismatches = max(max_allowed_mismatches, allowed_mismatches)
    
    n_results = (max_motif_length - min_motif_length + 1) * (max_allowed_mismatches - min_allowed_mismatches + 1)

    if n_results <= 4 :
        n_rows, n_cols = 1, n_results
    elif n_results <= 12 :
        n_rows, n_cols = 3, 4
    elif n_results <= 20 :
        n_rows, n_cols = 5, 4
    elif n_results <= 24 :
        n_rows, n_cols = 4, 6
    elif n_results <= 36 :
        n_rows, n_cols = 6, 6
    
    inactive_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(set(inactive_regions) & set([description[0] for description in motif_regions])) > 0]

    f, ax = plot.subplots(n_rows, n_cols, sharey = False)
    positions = range(n_results)
    if chosen_region == 'active' :
        chosen_set = set(active_regions)
    else :
        chosen_set = set([chosen_region])
    row, col = 0, 0
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            active_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(chosen_set & set([description[0] for description in motif_regions])) > 0]
            active_motifs = [motif for motif in active_motifs if len(motif) == motif_length]
            current_inactive_motifs = copy.deepcopy(inactive_motifs)
            current_inactive_motifs = [motif for motif in current_inactive_motifs if len(motif) == motif_length]
            
            active_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in active_motifs]
            inactive_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in current_inactive_motifs]
            from statsmodels.sandbox.stats.multicomp import multipletests
            if do_fdr :
                _, active_pvalues, _, _ = multipletests(active_pvalues, method = 'fdr_bh')
                _, inactive_pvalues, _, _ = multipletests(inactive_pvalues, method = 'fdr_bh')
            active_pvalues, inactive_pvalues = np.array(active_pvalues), np.array(inactive_pvalues)
            
            if remove_intersection :
                intersection = set(active_motifs) & set(current_inactive_motifs)
                active_motifs_set = set(active_motifs) - intersection
                current_inactive_motifs_set = set(current_inactive_motifs) - intersection
                active_indices = np.array([motif in active_motifs_set for motif in active_motifs])
                inactive_indices = np.array([motif in current_inactive_motifs_set for motif in current_inactive_motifs])
                active_pvalues = active_pvalues[active_indices]
                inactive_pvalues = inactive_pvalues[inactive_indices]
            
            #active_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in active_motifs]
            #inactive_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in current_inactive_motifs]

            import scipy.stats
            if n_rows == 1 :
                cur = ax[col]
            else :
                cur = ax[row, col]
            cur_pvalues = [('active', pvalue) for pvalue in active_pvalues]
            cur_pvalues.extend([('inactive', pvalue) for pvalue in inactive_pvalues])
            np.random.shuffle(cur_pvalues)
            cur_pvalues.sort(key = lambda x : np.log10(x[1]))
            cur_pvalues = [(cur_pvalues[i][0], i + 1, cur_pvalues[i][1]) for i in range(len(cur_pvalues))]
            plot_x, plot_y, where_active, where_inactive = [], [], [], []
            for type, position, pvalue in cur_pvalues :
                plot_x.extend([position, position + 1])
                plot_y.extend([pvalue, pvalue])
                is_active = type == 'active'
                where_active.extend([is_active, is_active])
                where_inactive.extend([not is_active, not is_active])
            plot_x, plot_y, where_active, where_inactive = np.array(plot_x), np.array(plot_y), np.array(where_active), np.array(where_inactive)
            plot_y = -np.log10(plot_y)
            cur.fill_between(plot_x, plot_y, where = where_active, interpolate = False, color = 'r', alpha = 0.45, label = 'Active')
            cur.fill_between(plot_x, plot_y, where = where_inactive, interpolate = False, color = 'b', alpha = 0.45, label = 'Inactive')
            cur.set_xlim(1, len(cur_pvalues) + 1)
            mi, ma = cur.get_ylim()
            cur.set_ylim(0, ma)

            import rpy2.robjects
            active_pvalues, inactive_pvalues = rpy2.robjects.FloatVector(active_pvalues), rpy2.robjects.FloatVector(inactive_pvalues)
            wilcoxon = rpy2.robjects.r.get('wilcox.test')
            test_results = wilcoxon(active_pvalues, inactive_pvalues, alternative = 'less')
            statistic, pvalue = test_results[0][0], test_results[2][0]
            print '[i] k = %d, mm = %d, statistic = %g, pvalue = %g' % (motif_length, allowed_mismatches, statistic, pvalue)
            cur.set_title('$k=%d$, %dmm: $-\\log_{10}p=%.2f$' % (motif_length, allowed_mismatches, -math.log(pvalue, 10)))
            '''
            if pvalue >= 0.05 :
                cur.set_title('$k=%d$, %dmm' % (motif_length, allowed_mismatches))
            else :
                n_stars = 1
                if pvalue < 1e-3 :
                    n_stars = 2
                if pvalue < 1e-5 :
                    n_stars = 3
                cur.set_title('$k=%d$, %dmm, $^{%s}$' % (motif_length, allowed_mismatches, '*' * n_stars))
            '''

            if col == 0 and row == 0 :
                p1 = plot.Rectangle((0, 0), 1, 1, fc = 'r', alpha = 0.45)
                p2 = plot.Rectangle((0, 0), 1, 1, fc = 'b', alpha = 0.45)
                cur.legend([p1, p2], ['Active', 'Inactive'])
            if col == 0 :
                cur.set_ylabel('$-\\log_{10}p$', fontsize = 16)
            if row == n_rows - 1 :
                cur.set_xlabel('Rank', fontsize = 16)
            for tick in cur.xaxis.get_major_ticks() :
                tick.label.set_rotation(45)

            col += 1
            if col >= n_cols :
                row += 1
                col = 0

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def perform_kmer_region_permutation_enrichment_analysis(seqs, regions, n_permutations = 200, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2, client_url = None) :
    '''
    Calculate k-mer enrichment in positive vs. negative sequences for k-mers from active and permuted active rRNA regions.
    :param seqs : a list of sequences that should be used for calculating enrichment.
    :param regions : a list of rRNA regions.
    :param n_permutations : number of permutations to consider.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _package_rRNA_tests(positive_seqs, negative_seqs, tasks, batch_size = 20) :
        '''
        Package rRNA tests into a parallel task.
        :param positive_seqs : positive sequences.
        :param negative_seqs : negative sequences.
        :param tasks : a list of tasks that should be packaged.
        :param batch_size : size of the batch for packaging.
        '''
        n_tasks = len(tasks)
        packages = []
        start = 0
        while start < n_tasks :
            package = (positive_seqs, negative_seqs, tasks[start : start + batch_size])
            packages.append(package)
            start += batch_size
        return packages

    def _parallel_get_pvalue(param) :
        '''
        Parallel version of get_pvalue.
        :param param : a list of parameters (positive sequences, negative sequences and tasks).
        '''
        def contains_motif(sequence, motif, allowed_mismatches = 0) :
            '''
            Determines whether a sequence contains a given motif.
            :param sequence : sequence to be considered.
            :param motif : motif to be considered.
            :param allowed_mismatched : maximum number of mismatches allowed in the motif.
            '''
            sequence_length = len(sequence)
            motif_length = len(motif)
            for offset in range(sequence_length - motif_length + 1) :
                mismatches = 0
                for i in range(motif_length) :
                    if motif[i] != sequence[offset + i] :
                        mismatches += 1
                        if mismatches > allowed_mismatches :
                            break
                if mismatches <= allowed_mismatches :
                    return True
            return False
        import scipy.stats
        positive_seqs, negative_seqs, tasks = param
        results = {}
        for motif, allowed_mismatches in tasks :
            cnt = np.zeros((2, 2), dtype = np.int)
            for seq in positive_seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    cnt[0, 0] += 1
                else :
                    cnt[0, 1] += 1
            for seq in negative_seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    cnt[1, 0] += 1
                else :
                    cnt[1, 1] += 1
            _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
            results[(motif, allowed_mismatches)] = pvalue
        return results
    
    positive_seqs, negative_seqs = [], []
    n_skipped = 0
    for seq in seqs :
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            n_skipped += 1
            continue
        if seq.ires_activity == BACKGROUND_IRES_ACTIVITY :
            negative_seqs.append(seq)
        else :
            positive_seqs.append(seq)

    print '[i] Total: %d' % len(seqs)
    print '[i] Skipped: %d' % n_skipped
    print '[i] Positive: %d' % len(positive_seqs)
    print '[i] Negative: %d' % len(negative_seqs)
    motifs = {}
    for i, region in enumerate(regions) :
        region_type, region_start, region_stop, region_sequence = region
        region_length = len(region_sequence)
        for permutation_id in range(n_permutations + 1) :
            for motif_length in range(min_motif_length, max_motif_length + 1) :
                for offset in range(region_length - motif_length + 1) :
                    motif = region_sequence[offset : offset + motif_length]
                    if not motif in motifs :
                        motifs[motif] = set()
                    region_description = (i, permutation_id, region_type, region_start, region_stop)
                    motifs[motif].add(region_description)
            region_sequence = [c for c in region_sequence]
            np.random.shuffle(region_sequence)
            region_sequence = ''.join(region_sequence)
    print '[i] Motifs to test: %d' % len(motifs)
    pvalues = {}
    if client_url is None :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            print '[i] Processing: %d mismatches' % allowed_mismatches
            for motif in motifs :
                pvalue = _get_pvalue(motif, positive_seqs, negative_seqs, allowed_mismatches)
                print motif, pvalue
                pvalues[(motif, allowed_mismatches)] = pvalue
    else :
        tasks = []
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            for motif in motifs :
                tasks.append((motif, allowed_mismatches))
        packages = _package_rRNA_tests(positive_seqs, negative_seqs, tasks, batch_size = 100)
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
        results = view.map(_parallel_get_pvalue, packages, block = False, ordered = True)
        for result in results :
            for key, pvalue in result.iteritems() :
                pvalues[key] = pvalue
        del view
        client.close()
        del client

    return motifs, pvalues

def perform_kmer_region_permutation_mutagenesis_enrichment_analysis(descriptions, groups, regions, n_permutations = 100, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2) :
    '''
    Calculate k-mer enrichment in mutagenesis windows for k-mers from active and permuted active rRNA regions.
    :param descriptions : list of descriptions of mutagenesis sequences.
    :param groups : groups of mutagenesis sequences (grouped by original oligo).
    :param regions : a list of rRNA regions.
    :param n_permutations : number of permutations to consider.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _get_pvalue(motif, groups, allowed_mismatches) :
        cnt = np.zeros((2, 2), dtype = np.int)
        for group in groups :
            original = group[0]
            original_ires_activity = original.ires_activity
            windows = group[1:]
            for window in windows :
                if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                    continue
                window_ires_activity = window.ires_activity
                description = window.description
                description = description.split(';')
                original_sequence, mutated_sequence = description[6], description[7]
                fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
                if math.isnan(fold_change) :
                    fold_change = np.log2(MAGIC_FOLD_CHANGE)
                is_selected = fold_change < 0 
                if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
                    if is_selected :
                        cnt[0, 0] += 1
                    else :
                        cnt[0, 1] += 1
                else :
                    if is_selected :
                        cnt[1, 0] += 1
                    else :
                        cnt[1, 1] += 1
        import scipy.stats
        _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
        return pvalue 

    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups

    motifs = {}
    for i, region in enumerate(regions) :
        region_type, region_start, region_stop, region_sequence = region
        region_length = len(region_sequence)
        for permutation_id in range(n_permutations + 1) :
            for motif_length in range(min_motif_length, max_motif_length + 1) :
                for offset in range(region_length - motif_length + 1) :
                    motif = region_sequence[offset : offset + motif_length]
                    if not motif in motifs :
                        motifs[motif] = set()
                    region_description = (i, permutation_id, region_type, region_start, region_stop)
                    motifs[motif].add(region_description)
            region_sequence = [c for c in region_sequence]
            np.random.shuffle(region_sequence)
            region_sequence = ''.join(region_sequence)
    print '[i] Motifs to test: %d' % len(motifs)

    pvalues = {}
    for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
        print '[i] Processing: %d mismatches' % allowed_mismatches
        for motif in motifs :
            pvalue = _get_pvalue(motif, groups, allowed_mismatches)
            pvalues[(motif, allowed_mismatches)] = pvalue
            print '%g (%s)' % (pvalue, motif),
        print ''

    return motifs, pvalues

def plot_ranking_kmer_region_permutation_enrichment_results(motifs, pvalues, chosen_region = 0, do_fdr = False, remove_intersection = False) :
    '''
    Plot k-mer permutation enrichment results for rRNA regions as ranking.
    :param motifs : a dictionary of motifs with a list of regions in which they occur.
    :param pvalues : a dictionary of pvalues for the corresponding tests.
    :param chosen_region : region for which the results should be plotted (integer or 'all').
    :param remove_intersection : whether k-mers present in both original and permuted regions should be removed from the plot.
    '''
    min_allowed_mismatches, max_allowed_mismatches = float('inf'), -float('inf')
    min_motif_length, max_motif_length = float('inf'), -float('inf')
    for key in pvalues :
        motif, allowed_mismatches = key
        motif_length = len(motif)
        min_motif_length = min(min_motif_length, motif_length)
        max_motif_length = max(max_motif_length, motif_length)
        min_allowed_mismatches = min(min_allowed_mismatches, allowed_mismatches)
        max_allowed_mismatches = max(max_allowed_mismatches, allowed_mismatches)
    
    n_results = (max_motif_length - min_motif_length + 1) * (max_allowed_mismatches - min_allowed_mismatches + 1)

    if n_results <= 4 :
        n_rows, n_cols = 1, n_results
    elif n_results <= 12 :
        n_rows, n_cols = 3, 4
    elif n_results <= 20 :
        n_rows, n_cols = 5, 4
    elif n_results <= 24 :
        n_rows, n_cols = 4, 6
    elif n_results <= 36 :
        n_rows, n_cols = 6, 6
    
    f, ax = plot.subplots(n_rows, n_cols, sharey = False)
    positions = range(n_results)
    row, col = 0, 0
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            original_motifs = [motif for motif, motif_regions in motifs.iteritems() if 0 in set([r[1] for r in motif_regions if (chosen_region == 'all' or chosen_region == r[0])])]
            background_motifs = [motif for motif, motif_regions in motifs.iteritems() if len([r[1] for r in motif_regions if (chosen_region == 'all' or chosen_region == r[0]) and r[1] > 0]) > 0]

            original_motifs = [motif for motif in original_motifs if len(motif) == motif_length]
            background_motifs = [motif for motif in background_motifs if len(motif) == motif_length]
            
            original_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in original_motifs]
            background_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in background_motifs]
            from statsmodels.sandbox.stats.multicomp import multipletests
            if do_fdr :
                _, original_pvalues, _, _ = multipletests(original_pvalues, method = 'fdr_bh')
                _, background_pvalues, _, _ = multipletests(background_pvalues, method = 'fdr_bh')
            original_pvalues, background_pvalues = np.array(original_pvalues), np.array(background_pvalues)

            if remove_intersection :
                original_motifs_set = set(original_motifs)
                background_motifs_set = set(background_motifs)
                intersection = original_motifs_set & background_motifs_set
                original_motifs_set -= intersection
                background_motifs_set -= intersection
                original_indices = np.array([motif in original_motifs_set for motif in original_motifs])
                background_indices = np.array([motif in background_motifs_set for motif in background_motifs])
                original_pvalues = original_pvalues[original_indices]
                background_pvalues = background_pvalues[background_indices]
            
            import scipy.stats
            if n_rows == 1 :
                cur = ax[col]
            else :
                cur = ax[row, col]
            cur_pvalues = [('original', pvalue) for pvalue in original_pvalues]
            cur_pvalues.extend([('background', pvalue) for pvalue in background_pvalues])
            np.random.shuffle(cur_pvalues)
            cur_pvalues.sort(key = lambda x : np.log10(x[1]))
            cur_pvalues = [(cur_pvalues[i][0], i + 1, cur_pvalues[i][1]) for i in range(len(cur_pvalues))]
            plot_x, plot_y, where_original, where_background = [], [], [], []
            for type, position, pvalue in cur_pvalues :
                plot_x.extend([position, position + 1])
                plot_y.extend([pvalue, pvalue])
                is_original = type == 'original'
                where_original.extend([is_original, is_original])
                where_background.extend([not is_original, not is_original])
            plot_x, plot_y, where_original, where_background = np.array(plot_x), np.array(plot_y), np.array(where_original), np.array(where_background)
            plot_y = -np.log10(plot_y)
            cur.fill_between(plot_x, plot_y, where = where_original, interpolate = False, color = 'r', alpha = 0.45, label = 'Original')
            cur.fill_between(plot_x, plot_y, where = where_background, interpolate = False, color = 'b', alpha = 0.45, label = 'Permuted')
            cur.set_xlim(1, len(cur_pvalues) + 1)
            mi, ma = cur.get_ylim()
            cur.set_ylim(0, ma)

            import rpy2.robjects
            original_pvalues, background_pvalues = rpy2.robjects.FloatVector(original_pvalues), rpy2.robjects.FloatVector(background_pvalues)
            wilcoxon = rpy2.robjects.r.get('wilcox.test')
            try :
                test_results = wilcoxon(original_pvalues, background_pvalues, alternative = 'less')
                statistic, pvalue = test_results[0][0], test_results[2][0]
            except :
                statistic, pvalue = float('nan'), 1.0
            print '[i] k = %d, mm = %d, statistic = %g, pvalue = %g' % (motif_length, allowed_mismatches, statistic, pvalue)
            #if pvalue >= 0.05 :
            #    cur.set_title('$k=%d$, %dmm' % (motif_length, allowed_mismatches))
            #else :
            #    n_stars = 1
            #    if pvalue < 1e-3 :
            #        n_stars = 2
            #    if pvalue < 1e-5 :
            #        n_stars = 3
            #    cur.set_title('$k=%d$, %dmm, $^{%s}$' % (motif_length, allowed_mismatches, '*' * n_stars))
            cur.set_title('$k=%d$, %dmm: $-\\log_{10}p=%.2f$' % (motif_length, allowed_mismatches, -math.log(pvalue, 10)))

            if col == 0 and row == 0 :
                p1 = plot.Rectangle((0, 0), 1, 1, fc = 'r', alpha = 0.45)
                p2 = plot.Rectangle((0, 0), 1, 1, fc = 'b', alpha = 0.45)
                cur.legend([p1, p2], ['Original', 'Permuted'])
            if col == 0 :
                cur.set_ylabel('$-\\log_{10}p$', fontsize = 16)
            if row == n_rows - 1 :
                cur.set_xlabel('Rank', fontsize = 16)
            for tick in cur.xaxis.get_major_ticks() :
                tick.label.set_rotation(45)

            col += 1
            if col >= n_cols :
                row += 1
                col = 0

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def get_known_ires_elements(filename = '../data/ires_elements_position_on_18S_rRNA_revcomp.tab') :
    '''
    Get the list of known IRES elements and their locations on the complementary rRNA.
    :param filename : name of the file, from which the elements should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    seqs = []
    for line in lines :
        args = line.strip().split('\t')
        type, name, start, stop, sequence = args[0], args[1], int(args[2]) - 1, int(args[3]) - 1, args[4]
        entry = (name, start, stop, sequence)
        seqs.append(entry)
    return seqs

def get_accessible_rRNA_regions(filename = '../data/accessible_regions_on_18s_rrna_for_matlab_analysis.tab') :
    '''
    Read accessible rRNA regions.
    :param filename : name of the file from which the accessible regions should be read.
    '''
    import Bio.Seq
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    rRNA = lib.reader.read_ribosomal_rna_seq()
    rRNA = rRNA['18S']
    rRNA = str(Bio.Seq.Seq(rRNA).reverse_complement())
    rRNA_length = len(rRNA)
    segments = []
    for line in lines :
        args = line.strip().split('\t')
        start, stop = int(args[1]), int(args[2])
        start, stop = rRNA_length - stop, rRNA_length - start
        segments.append((start, stop + 1))
    segments.sort(key = lambda x : x[0])
    regions = []
    for start, stop in segments :
        regions.append(('active', start, stop, rRNA[start : stop]))
    pos = 0
    for start, stop in segments :
        if pos < start :
            regions.append(('inactive', pos, start, rRNA[pos : start]))
        pos = stop
    if pos < rRNA_length :
        regions.append(('inactive', pos, rRNA_length, rRNA[pos : rRNA_length]))
    return regions

def get_active_inactive_rRNA_regions() :
    '''
    Return active and inactive complementary rRNA regions (as determined by the assay).
    '''
    seqs = get_oligos()
    rRNA = get_complementary_rRNA_oligos(seqs)
    regions = plot_complementary_rRNA(rRNA, type = '18S', should_plot = False)
    return regions

def get_shira_active_inactive_rRNA_regions() :
    given_regions = [(215 - 1, 293), (812 - 1, 1233), (1520 - 1, 1638)]
    rRNA = lib.reader.read_ribosomal_rna_seq()
    rRNA = rRNA['18S']
    import Bio.Seq
    rRNA = str(Bio.Seq.Seq(rRNA).reverse_complement())
    rRNA_length = len(rRNA)
    given_regions = [(rRNA_length - stop, rRNA_length - start) for start, stop in given_regions]
    given_regions.sort(key = lambda x : x[0])
    regions = []
    for start, stop in given_regions :
        region = ('active', start, stop, rRNA[start:stop])
        regions.append(region)
    start, stop = 0, 0
    for segment_start, segment_stop in given_regions + [(rRNA_length, rRNA_length)] :
        stop = segment_start
        region = ('inative', start, stop, rRNA[start:stop])
        regions.append(region)
        start = segment_stop
    return regions

def plot_count_rRNA_oligos_within_cluster(result, desc, regions) :
    '''
    Plot counted presence of rRNA motifs in mutagenised regions.
    :param results : counts to be plotted.
    :param regions : rRNA regions to be used 
    '''
    n_rows, n_cols = 4, 6
    row, col = 0, 0
    f, ax = plot.subplots(n_rows, n_cols)
    items = result.keys()
    items.sort()
    n_items = len(items)
    n_regions = len(regions)
    for motif_length, allowed_mismatches in items :
        item = (motif_length, allowed_mismatches)
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        width = 0.8
        x = np.array(range(n_regions)) * width + width / 2.0
        cur.bar(x, result[item] / float(len(desc)), width)
        labels = ['%s %d' % (region[0], i) for i, region in enumerate(regions)]
        cur.set_xlim(0, np.max(x) + width + width / 2.0)
        cur.set_title('Length %d, %d mm' % (motif_length, allowed_mismatches))
        if col == 0 :
            cur.set_ylabel('Fraction of oligos', fontsize = 13)
        if row == 0 and col == 0 :
            cur.legend(ncol = 3, prop = {'size' : 8}, loc = 'lower center')
            cur.set_xticks(x + width / 2.0)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_items :
            cur.set_xticks(x + width / 2.0)
            cur.set_xticklabels(labels, rotation = 'vertical')
        else :
            cur.set_xticklabels([])

        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def count_rRNA_oligos_within_cluster(descriptions, groups, regions, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2) :
    '''
    Compute number of mutagenesis oligos for which at least one window has a motif from the define rRNA region.
    :param descriptions : a list of cluster descriptions.
    :param groups : groups of mutagenesis oligos.
    :param regions : rRNA regions to be used for counting.
    :param min_motif_length : minimum length of the motif to be considered.
    :param max_motif_length : maximum length of the motif to be conisdered.
    :param min_allowed_mismatches : minimum number of allowed mismatches to be considered.
    :param max_allowed_mismatches : maximum number of allowed mismatches to be considered.
    '''
    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups
   
    results = {}
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            print '[i] Processing: length %d, mismatches %d' % (motif_length, allowed_mismatches)
            n_regions = len(regions)
            region_counts = np.zeros((n_regions, ), dtype = np.int)
            for i, region in enumerate(regions) :
                region_type, region_start, region_stop, region_sequence = region
                region_length = len(region_sequence)
                n_groups = len(groups)
                for group in groups :
                    oligo_has_motif = False
                    original = group[0]
                    original_ires_activity = original.ires_activity
                    windows = group[1:]
                    for window in windows :
                        if oligo_has_motif :
                            break
                        if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                            continue
                        window_ires_activity = window.ires_activity
                        description = window.description
                        description = description.split(';')
                        original_sequence, mutated_sequence = description[6], description[7]
                        fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
                        if math.isnan(fold_change) :
                            fold_change = np.log2(MAGIC_FOLD_CHANGE)
                        is_selected = fold_change < 0
                        for offset in range(region_length - motif_length + 1) :
                            motif = region_sequence[offset : offset + motif_length]
                            if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
                                oligo_has_motif = True
                                break
                    if oligo_has_motif :
                        region_counts[i] += 1
            results[(motif_length, allowed_mismatches)] = region_counts
    return results

def check_mutagenesis_cluster_for_motif(descriptions, groups, motif, allowed_mismatches = 2) :
    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups
    n_groups = len(groups)
    n_rows, n_cols = 6, 8
    row, col = 0, 0

    motif_length = len(motif)
    f, ax = plot.subplots(n_rows, n_cols)
    for group in groups :
        oligo_has_motif = False
        original = group[0]
        original_ires_activity = original.ires_activity
        original_ires_activity = np.log2(original_ires_activity / BACKGROUND_IRES_ACTIVITY)
        windows = group[1:]
        oligo_length = len(original.sequence)
        original_index = original.index
        fold_changes = []
        locations = []
        marked_regions = []
        for offset in range(oligo_length - motif_length + 1) :
            sub_seq = original.sequence[offset : offset + motif_length]
            if contains_motif(sub_seq, motif, allowed_mismatches) :
                marked_regions.append((offset, offset + motif_length))
                
                description = original.description.split(';')
                name = description[2].split(':')[1]
                if name == 'NM_001005240' or name == 'NM_001256932' :
                    print name, sub_seq
        for index, window in enumerate(windows) :
            window_description = window.description.split(';')
            window_start, window_stop = int(window_description[3]) - 1, int(window_description[4])
            name = window_description[2].split(':')[1]
            window_stop += window_start
            window_center = (window_stop + window_start) / 2
            locations.append(window_center)
            if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                fold_changes.append(float('NaN'))
                continue
            window_ires_activity = window.ires_activity
            description = window.description
            description = description.split(';')
            original_sequence, mutated_sequence = description[6], description[7]
            #fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
            #if math.isnan(fold_change) :
            #    fold_change = np.log2(MAGIC_FOLD_CHANGE)
            fold_change = np.log2(window_ires_activity / BACKGROUND_IRES_ACTIVITY)
            fold_changes.append(fold_change)
            #if name == 'NM_001118886' :
            #    print window_start, window_stop
            #    print contains_motif(original_sequence, motif, allowed_mismatches), contains_motif(mutated_sequence, motif, allowed_mismatches)
            #    print original_sequence, motif
            #    print ''
            #if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
            #    marked_regions.append((window_start, window_stop))
        
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        cur.plot(locations, fold_changes, marker = 'x', markersize = 10, linewidth = 1.5)
        cur.plot([0, oligo_length - 1], [original_ires_activity] * 2, linewidth = 1.5, linestyle = '--', color = 'r')
        cur.set_title('%s (%d)' % (name, original_index))
        print name
        mi, ma = cur.get_ylim()
        cur.set_yticks([mi, ma])
        for start, stop in marked_regions :
            cur.axvspan(start, stop, alpha = 0.2, color = 'green')
        cur.set_xlim(0, oligo_length - 1)
        if row == n_rows - 1 or (row + 1) * n_cols + col + 1 > n_groups :
            labels = range(0, oligo_length - 1, 40)
            cur.set_xticks(labels)
            cur.set_xticklabels(labels, rotation = 'vertical')
        else :
            cur.set_xticklabels([])
        
        col += 1
        if col >= n_cols :
            row += 1
            col = 0
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1

    f.suptitle('%s (%d mismatches)' % (motif, allowed_mismatches), fontweight = 'bold')
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    f.subplots_adjust(top = 0.92)
    plot.show()

def print_enriched_kmers_per_region(motifs, pvalues, pvalues2 = None, pvalues3 = None, regions_of_interest = [0, 1, 2], top_n = 10) :
    for region in regions_of_interest :
        combinations = pvalues.keys()
        combinations = [combo for combo in combinations if combo[1] == region]
        combinations.sort(key = lambda x : pvalues[x])
        for motif, motif_region, mismatches in combinations[:top_n] :
            print '%s\t%d\t%d\t%g' % (motif, motif_region, mismatches, pvalues[(motif, motif_region, mismatches)]),
            if pvalues2 is not None :
                print '\t%g' % pvalues2[(motif, mismatches)],
            if pvalues3 is not None :
                print '\t%g' % pvalues3[(motif, mismatches)],
            print ''
        print ''

def perform_group_kmer_region_enrichment_analysis(seqs, regions, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2) :
    '''
    Calculate k-mer enrichment in positive vs. negative sequences for k-mers from active and inactive rRNA regions.
    :param seqs : a list of sequences that should be used for calculating enrichment.
    :param regions : a list of rRNA regions.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    '''

    def get_pvalue(seqs, region_kmers, allowed_mismatches) :
        '''
        Get test p-value.
        :param param : a list of parameters (positive sequences, negative sequences and tasks).
        '''
        def contains_motif(sequence, motif, allowed_mismatches = 0) :
            '''
            Determines whether a sequence contains a given motif.
            :param sequence : sequence to be considered.
            :param motif : motif to be considered.
            :param allowed_mismatched : maximum number of mismatches allowed in the motif.
            '''
            sequence_length = len(sequence)
            motif_length = len(motif)
            for offset in range(sequence_length - motif_length + 1) :
                mismatches = 0
                for i in range(motif_length) :
                    if motif[i] != sequence[offset + i] :
                        mismatches += 1
                        if mismatches > allowed_mismatches :
                            break
                if mismatches <= allowed_mismatches :
                    return True
            return False
        import scipy.stats
        seqs_with, seqs_without = [], []
        for seq in seqs :
            seq_has_kmer = False
            for kmer in region_kmers :
                if contains_motif(seq.sequence, kmer, allowed_mismatches) :
                    seq_has_kmer = True
                    break
            if seq_has_kmer :
                seqs_with.append(seq.ires_activity)
            else :
                seqs_without.append(seq.ires_activity)
        import rpy2.robjects
        print 'Total %d, with %d, without %d' % (len(seqs), len(seqs_with), len(seqs_without))
        if len(seqs_with) <= 10 or len(seqs_without) <= 10 :
            return float('nan')
        seqs_with, seqs_without = rpy2.robjects.FloatVector(seqs_with), rpy2.robjects.FloatVector(seqs_without)
        wilcoxon = rpy2.robjects.r.get('wilcox.test')
        test_results = wilcoxon(seqs_without, seqs_with, alternative = 'less')
        statistic, pvalue = test_results[0][0], test_results[2][0]
        return pvalue
   
    print '[i] Total: %d' % len(seqs)
    seqs = [seq for seq in seqs if not (is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity))]
    print '[i] Left: %d' % len(seqs)

    for i, region in enumerate(regions) :
        region_type, region_start, region_stop, region_sequence = region
        region_length = len(region_sequence)
        for motif_length in range(min_motif_length, max_motif_length + 1) :
            region_kmers = list(set([region_sequence[k : k + motif_length] for k in range(region_length - motif_length + 1)]))
            for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
                print '[i] Processing region %d: %d motif length, %d mismatches' % (i, motif_length, allowed_mismatches)
                pvalue = get_pvalue(seqs, region_kmers, allowed_mismatches)
                print pvalue

def perform_kmer_region_wilcoxon_analysis(seqs, regions, min_motif_length = 4, max_motif_length = 11, min_allowed_mismatches = 0, max_allowed_mismatches = 2, client_url = None) :
    '''
    Calculate k-mer enrichment in positive vs. negative sequences for k-mers from active and inactive rRNA regions.
    :param seqs : a list of sequences that should be used for calculating enrichment.
    :param regions : a list of rRNA regions.
    :param min_motif_length : minimum length of the motifs/k-mers that should be considered.
    :param max_motif_length : maximum length of the motifs/k-mers that should be considered.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _package_rRNA_tests(seqs, tasks, batch_size = 20) :
        '''
        Package rRNA tests into a parallel task.
        :param seqs : sequences.
        :param tasks : a list of tasks that should be packaged.
        :param batch_size : size of the batch for packaging.
        '''
        n_tasks = len(tasks)
        packages = []
        start = 0
        while start < n_tasks :
            package = (seqs, tasks[start : start + batch_size])
            packages.append(package)
            start += batch_size
        return packages

    def _parallel_get_pvalue(param) :
        '''
        Parallel version of get_pvalue.
        :param param : a list of parameters (positive sequences, negative sequences and tasks).
        '''
        def contains_motif(sequence, motif, allowed_mismatches = 0) :
            '''
            Determines whether a sequence contains a given motif.
            :param sequence : sequence to be considered.
            :param motif : motif to be considered.
            :param allowed_mismatched : maximum number of mismatches allowed in the motif.
            '''
            sequence_length = len(sequence)
            motif_length = len(motif)
            for offset in range(sequence_length - motif_length + 1) :
                mismatches = 0
                for i in range(motif_length) :
                    if motif[i] != sequence[offset + i] :
                        mismatches += 1
                        if mismatches > allowed_mismatches :
                            break
                if mismatches <= allowed_mismatches :
                    return True
            return False
        seqs, tasks = param
        results = {}
        for motif, allowed_mismatches in tasks :
            seqs_with, seqs_without = [], []
            for seq in seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    seqs_with.append(seq.ires_activity)
                else :
                    seqs_without.append(seq.ires_activity)
            import rpy2.robjects
            try :
                seqs_with, seqs_without = rpy2.robjects.FloatVector(seqs_with), rpy2.robjects.FloatVector(seqs_without)
                wilcoxon = rpy2.robjects.r.get('wilcox.test')
                test_results = wilcoxon(seqs_without, seqs_with, alternative = 'less')
                statistic, pvalue = test_results[0][0], test_results[2][0]
                results[(motif, allowed_mismatches)] = pvalue
                print 'Total %d, with %d, without %d: %g' % (len(seqs), len(seqs_with), len(seqs_without), pvalue)
            except :
                results[(motif, allowed_mismatches)] = float('nan')
        return results
   
    def _get_pvalue(motif, seqs, allowed_mismatches) :
        seqs_with, seqs_without = [], []
        for seq in seqs :
            if contains_motif(seq.sequence, motif, allowed_mismatches) :
                seqs_with.append(seq.ires_activity)
            else :
                seqs_without.append(seq.ires_activity)
        import rpy2.robjects
        if len(seqs_with) <= 10 or len(seqs_without) <= 10 :
            return float('nan')
        seqs_with, seqs_without = rpy2.robjects.FloatVector(seqs_with), rpy2.robjects.FloatVector(seqs_without)
        wilcoxon = rpy2.robjects.r.get('wilcox.test')
        test_results = wilcoxon(seqs_without, seqs_with, alternative = 'less')
        statistic, pvalue = test_results[0][0], test_results[2][0]
        print 'Total %d, with %d, without %d: %g' % (len(seqs), len(seqs_with), len(seqs_without), pvalue)
        return pvalue

    print '[i] Total: %d' % len(seqs)
    motifs = {}
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for i, region in enumerate(regions) :
            region_type, region_start, region_stop, region_sequence = region
            region_length = len(region_sequence)
            for offset in range(region_length - motif_length + 1) :
                motif = region_sequence[offset : offset + motif_length]
                if not motif in motifs :
                    motifs[motif] = set()
                region_description = (i, region_type, region_start, region_stop)
                motifs[motif].add(region_description)
    pvalues = {}
    if client_url is None :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            print '[i] Processing: %d mismatches' % allowed_mismatches
            for motif in motifs :
                pvalue = _get_pvalue(motif, seqs, allowed_mismatches)
                pvalues[(motif, allowed_mismatches)] = pvalue
    else :
        tasks = []
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            for motif in motifs :
                tasks.append((motif, allowed_mismatches))
        packages = _package_rRNA_tests(seqs, tasks, batch_size = 100)
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
        view.retries = 0
        results = view.map(_parallel_get_pvalue, packages, block = False, ordered = True)
        for result in results :
            for key, pvalue in result.iteritems() :
                pvalues[key] = pvalue
        del view
        client.close()
        del client

    return motifs, pvalues

def generate_random_kmer(length) :
    '''
    Generate a k-mer of a given length.
    :parma length : length of a k-mer to be generated.
    '''
    nucleotides = ['A', 'C', 'T', 'G']
    kmer = np.random.choice(nucleotides, length, replace = True)
    kmer = ''.join(kmer)
    return kmer

def perform_random_kmer_enrichment_analysis(seqs, motif_length = 7, allowed_mismatches = 2, n_motifs = 5000, client_url = None) :
    '''
    Calculate k-mer enrichment in positive vs. negative sequences for k-mers from active and inactive rRNA regions.
    :param seqs : a list of sequences that should be used for calculating enrichment.
    :param motif_length : length of the motifs to be considered.
    :param allowed_mismatches : number of allowed mismatches.
    :param n_motifs : number of motifs to sample.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _package_rRNA_tests(positive_seqs, negative_seqs, tasks, batch_size = 20) :
        '''
        Package rRNA tests into a parallel task.
        :param positive_seqs : positive sequences.
        :param negative_seqs : negative sequences.
        :param tasks : a list of tasks that should be packaged.
        :param batch_size : size of the batch for packaging.
        '''
        n_tasks = len(tasks)
        packages = []
        start = 0
        while start < n_tasks :
            package = (positive_seqs, negative_seqs, tasks[start : start + batch_size])
            packages.append(package)
            start += batch_size
        return packages

    def _parallel_get_pvalue(param) :
        '''
        Parallel version of get_pvalue.
        :param param : a list of parameters (positive sequences, negative sequences and tasks).
        '''
        def contains_motif(sequence, motif, allowed_mismatches = 0) :
            '''
            Determines whether a sequence contains a given motif.
            :param sequence : sequence to be considered.
            :param motif : motif to be considered.
            :param allowed_mismatched : maximum number of mismatches allowed in the motif.
            '''
            sequence_length = len(sequence)
            motif_length = len(motif)
            for offset in range(sequence_length - motif_length + 1) :
                mismatches = 0
                for i in range(motif_length) :
                    if motif[i] != sequence[offset + i] :
                        mismatches += 1
                        if mismatches > allowed_mismatches :
                            break
                if mismatches <= allowed_mismatches :
                    return True
            return False
        import scipy.stats
        positive_seqs, negative_seqs, tasks = param
        results = {}
        for motif, allowed_mismatches in tasks :
            cnt = np.zeros((2, 2), dtype = np.int)
            for seq in positive_seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    cnt[0, 0] += 1
                else :
                    cnt[0, 1] += 1
            for seq in negative_seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    cnt[1, 0] += 1
                else :
                    cnt[1, 1] += 1
            _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
            results[(motif, allowed_mismatches)] = pvalue
        return results
    
    positive_seqs, negative_seqs = [], []
    n_skipped = 0
    for seq in seqs :
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            n_skipped += 1
            continue
        if seq.ires_activity == BACKGROUND_IRES_ACTIVITY :
            negative_seqs.append(seq)
        else :
            positive_seqs.append(seq)

    print '[i] Total: %d' % len(seqs)
    print '[i] Skipped: %d' % n_skipped
    print '[i] Positive: %d' % len(positive_seqs)
    print '[i] Negative: %d' % len(negative_seqs)
    
    motifs = set()
    while len(motifs) < n_motifs :
        random_motif = generate_random_kmer(motif_length)
        motifs.add(random_motif)
    motifs = list(motifs)
    pvalues = {}
    if client_url is None :
        print '[i] Processing: %d mismatches' % allowed_mismatches
        for motif in motifs :
            pvalue = _get_pvalue(motif, positive_seqs, negative_seqs, allowed_mismatches)
            pvalues[(motif, allowed_mismatches)] = pvalue
    else :
        tasks = []
        for motif in motifs :
            tasks.append((motif, allowed_mismatches))
        packages = _package_rRNA_tests(positive_seqs, negative_seqs, tasks, batch_size = 100)
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
        view.retries = 0
        results = view.map(_parallel_get_pvalue, packages, block = False, ordered = True)
        for result in results :
            for key, pvalue in result.iteritems() :
                pvalues[key] = pvalue
        del view
        client.close()
        del client

    return motifs, pvalues

def perform_random_kmer_wilcoxon_analysis(seqs, motif_length = 7, allowed_mismatches = 2, n_motifs = 2000, client_url = None) :
    '''
    Calculate k-mer enrichment in positive vs. negative sequences for k-mers from active and inactive rRNA regions.
    :param seqs : a list of sequences that should be used for calculating enrichment.
    :param motif_length : length of the motif to be analysed.
    :param allowed_mismatches : maximum number of allowed mismatches.
    :param client_url : URL of the IPython parallel client that should be used for parallel computations.
    '''

    def _package_rRNA_tests(seqs, tasks, batch_size = 20) :
        '''
        Package rRNA tests into a parallel task.
        :param seqs : sequences.
        :param tasks : a list of tasks that should be packaged.
        :param batch_size : size of the batch for packaging.
        '''
        n_tasks = len(tasks)
        packages = []
        start = 0
        while start < n_tasks :
            package = (seqs, tasks[start : start + batch_size])
            packages.append(package)
            start += batch_size
        return packages

    def _parallel_get_pvalue(param) :
        '''
        Parallel version of get_pvalue.
        :param param : a list of parameters (positive sequences, negative sequences and tasks).
        '''
        def contains_motif(sequence, motif, allowed_mismatches = 0) :
            '''
            Determines whether a sequence contains a given motif.
            :param sequence : sequence to be considered.
            :param motif : motif to be considered.
            :param allowed_mismatched : maximum number of mismatches allowed in the motif.
            '''
            sequence_length = len(sequence)
            motif_length = len(motif)
            for offset in range(sequence_length - motif_length + 1) :
                mismatches = 0
                for i in range(motif_length) :
                    if motif[i] != sequence[offset + i] :
                        mismatches += 1
                        if mismatches > allowed_mismatches :
                            break
                if mismatches <= allowed_mismatches :
                    return True
            return False
        seqs, tasks = param
        results = {}
        for motif, allowed_mismatches in tasks :
            seqs_with, seqs_without = [], []
            for seq in seqs :
                if contains_motif(seq.sequence, motif, allowed_mismatches) :
                    seqs_with.append(seq.ires_activity)
                else :
                    seqs_without.append(seq.ires_activity)
            import rpy2.robjects
            try :
                seqs_with, seqs_without = rpy2.robjects.FloatVector(seqs_with), rpy2.robjects.FloatVector(seqs_without)
                wilcoxon = rpy2.robjects.r.get('wilcox.test')
                test_results = wilcoxon(seqs_without, seqs_with, alternative = 'less')
                statistic, pvalue = test_results[0][0], test_results[2][0]
                results[(motif, allowed_mismatches)] = pvalue
                print 'Total %d, with %d, without %d: %g' % (len(seqs), len(seqs_with), len(seqs_without), pvalue)
            except :
                results[(motif, allowed_mismatches)] = float('nan')
        return results
   
    def _get_pvalue(motif, seqs, allowed_mismatches) :
        seqs_with, seqs_without = [], []
        for seq in seqs :
            if contains_motif(seq.sequence, motif, allowed_mismatches) :
                seqs_with.append(seq.ires_activity)
            else :
                seqs_without.append(seq.ires_activity)
        import rpy2.robjects
        if len(seqs_with) <= 10 or len(seqs_without) <= 10 :
            return float('nan')
        seqs_with, seqs_without = rpy2.robjects.FloatVector(seqs_with), rpy2.robjects.FloatVector(seqs_without)
        wilcoxon = rpy2.robjects.r.get('wilcox.test')
        test_results = wilcoxon(seqs_without, seqs_with, alternative = 'less')
        statistic, pvalue = test_results[0][0], test_results[2][0]
        print 'Total %d, with %d, without %d: %g' % (len(seqs), len(seqs_with), len(seqs_without), pvalue)
        return pvalue

    print '[i] Total: %d' % len(seqs)
    motifs = set()
    while len(motifs) < n_motifs :
        random_motif = generate_random_kmer(motif_length)
        motifs.add(random_motif)
    motifs = list(motifs)

    pvalues = {}
    if client_url is None :
        for motif in motifs :
            pvalue = _get_pvalue(motif, seqs, allowed_mismatches)
            pvalues[(motif, allowed_mismatches)] = pvalue
    else :
        tasks = []
        for motif in motifs :
            tasks.append((motif, allowed_mismatches))
        packages = _package_rRNA_tests(seqs, tasks, batch_size = 100)
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
        view.retries = 0
        results = view.map(_parallel_get_pvalue, packages, block = False, ordered = True)
        for result in results :
            for key, pvalue in result.iteritems() :
                pvalues[key] = pvalue
        del view
        client.close()
        del client

    return motifs, pvalues

def plot_sample_ranking_kmer_region_enrichment_results(motifs, pvalues, regions, chosen_region = 0, n_samples = 5000, remove_intersection = False, cutoff = 0.01) :
    '''
    Plot k-mer enrichment results for rRNA regions as ranking.
    :param motifs : a dictionary of motifs with a list of regions in which they occur.
    :param pvalues : a dictionary of pvalues for the corresponding tests.
    :param regions : a list of rRNA regions.
    :param chosen_region : region for which the results should be plotted (integer or 'all').
    :param remove_intersection : whether k-mers present in both active and inactive regions should be removed from the plot.
    '''
    n_regions = len(regions)
    active_regions, inactive_regions = [], []
    for i, region in enumerate(regions) :
       region_type, region_start, region_stop, region_sequence = region
       if region_type == 'active' :
           active_regions.append(i)
       else :
           inactive_regions.append(i)
    n_active_regions = len(active_regions)
    min_allowed_mismatches, max_allowed_mismatches = float('inf'), -float('inf')
    min_motif_length, max_motif_length = float('inf'), -float('inf')
    for key in pvalues :
        motif, allowed_mismatches = key
        motif_length = len(motif)
        min_motif_length = min(min_motif_length, motif_length)
        max_motif_length = max(max_motif_length, motif_length)
        min_allowed_mismatches = min(min_allowed_mismatches, allowed_mismatches)
        max_allowed_mismatches = max(max_allowed_mismatches, allowed_mismatches)
    
    n_results = (max_motif_length - min_motif_length + 1) * (max_allowed_mismatches - min_allowed_mismatches + 1)

    if n_results <= 4 :
        n_rows, n_cols = 1, n_results
    elif n_results <= 12 :
        n_rows, n_cols = 3, 4
    elif n_results <= 20 :
        n_rows, n_cols = 5, 4
    elif n_results <= 24 :
        n_rows, n_cols = 4, 6
    elif n_results <= 36 :
        n_rows, n_cols = 6, 6
    
    inactive_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(set(inactive_regions) & set([description[0] for description in motif_regions])) > 0]

    f, ax = plot.subplots(n_rows, n_cols, sharey = False)
    positions = range(n_results)
    if chosen_region == 'active' :
        chosen_set = set(active_regions)
    else :
        chosen_set = set([chosen_region])
    row, col = 0, 0
    for motif_length in range(min_motif_length, max_motif_length + 1) :
        for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
            active_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(chosen_set & set([description[0] for description in motif_regions])) > 0]
            active_motifs = [motif for motif in active_motifs if len(motif) == motif_length]
            current_inactive_motifs = copy.deepcopy(inactive_motifs)
            current_inactive_motifs = [motif for motif in current_inactive_motifs if len(motif) == motif_length]
            
            active_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in active_motifs]
            inactive_pvalues = [pvalues[(motif, allowed_mismatches)] for motif in current_inactive_motifs]
            
            if n_rows == 1 :
                cur = ax[col]
            else :
                cur = ax[row, col]
            n_active = len(active_motifs)
            sample_pvalues = []
            for _ in range(n_samples) :
                sample_active_motifs = copy.deepcopy(active_motifs)
                sample_active_pvalues = copy.deepcopy(active_pvalues)
                items = zip(current_inactive_motifs, inactive_pvalues)
                n_items = len(items)
                items_array = np.zeros((n_items, ), dtype = np.object)
                items_array[:] = items
                sample_inactive_motifs = np.random.choice(range(n_items), n_active, replace = True)
                sample_inactive_motifs = items_array[sample_inactive_motifs]
                sample_inactive_motifs, sample_inactive_pvalues = zip(*sample_inactive_motifs)
                sample_inactive_motifs, sample_inactive_pvalues = list(sample_inactive_motifs), list(sample_inactive_pvalues)
                sample_active_pvalues, sample_inactive_pvalues = np.array(sample_active_pvalues), np.array(sample_inactive_pvalues)
                if remove_intersection :
                    intersection = set(sample_active_motifs) & set(sample_inactive_motifs)
                    sample_active_motifs_set = set(sample_active_motifs) - intersection
                    sample_inactive_motifs_set = set(sample_inactive_motifs) - intersection
                    sample_active_indices = np.array([motif in sample_active_motifs_set for motif in sample_active_motifs])
                    sample_inactive_indices = np.array([motif in sample_inactive_motifs_set for motif in sample_inactive_motifs])
                    sample_active_pvalues = sample_active_pvalues[sample_active_indices]
                    sample_inactive_pvalues = sample_inactive_pvalues[sample_inactive_indices]
                import rpy2.robjects
                sample_active_pvalues, sample_inactive_pvalues = rpy2.robjects.FloatVector(sample_active_pvalues), rpy2.robjects.FloatVector(sample_inactive_pvalues)
                wilcoxon = rpy2.robjects.r.get('wilcox.test')
                test_results = wilcoxon(sample_active_pvalues, sample_inactive_pvalues, alternative = 'less')
                statistic, pvalue = test_results[0][0], test_results[2][0]
                sample_pvalues.append(pvalue)
            sample_pvalues = -np.log10(sample_pvalues)
            import scipy.stats
            density = scipy.stats.gaussian_kde(sample_pvalues)
            x_min, x_max = np.min(sample_pvalues), np.max(sample_pvalues)
            x = np.linspace(0, x_max, 1000)

            # plot things
            cur.grid(b = False)
            cur.plot(x, density(x), linewidth = 1.5, color = 'r', alpha = 0.45)
            mi, ma = cur.get_ylim()
            cur.set_ylim(0, ma)
            cur.plot([-np.log10(cutoff)] * 2, [0, ma], color = 'k', linewidth = 1.5, linestyle = '--')

            print '[i] Processed: k = %d, mm = %d' % (motif_length, allowed_mismatches)
            cur.set_title('$k=%d$, %dmm: $-\\log_{10}p=%.2f$' % (motif_length, allowed_mismatches, -math.log(pvalue, 10)))
            cur.set_yticks([])

            if col == 0 :
                cur.set_ylabel('Density', fontsize = 16)
            if row == n_rows - 1 :
                cur.set_xlabel('$-\\log_{10}p$', fontsize = 16)
            for tick in cur.xaxis.get_major_ticks() :
                tick.label.set_rotation(45)

            col += 1
            if col >= n_cols :
                row += 1
                col = 0

    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def print_kmer_region_enrichment_results(motifs, pvalues, random_pvalues, regions, chosen_region = 1, chosen_motif_length = 7, chosen_allowed_mismatches = 2, do_fdr = True, cutoff = 0.01, write_to_file = None) :
    '''
    Plot k-mer enrichment results for rRNA regions as ranking.
    :param motifs : a dictionary of motifs with a list of regions in which they occur.
    :param pvalues : a dictionary of pvalues for the corresponding tests.
    :param random_pvalues : a dictionary of pvalues for randomly generated k-mers.
    :param regions : a list of rRNA regions.
    :param chosen_region : region for which the results should be plotted (integer or 'all').
    :parma chosen_motif_length : length of the motifs which should be printed.
    :param chosen_allowed_mismatches : number of allowed mismatches for which the p-values should be plotted.
    :param remove_intersection : whether k-mers present in both active and inactive regions should be removed from the plot.
    '''
    n_regions = len(regions)
    active_regions, inactive_regions = [], []
    for i, region in enumerate(regions) :
       region_type, region_start, region_stop, region_sequence = region
       if region_type == 'active' :
           active_regions.append(i)
       else :
           inactive_regions.append(i)
    n_active_regions = len(active_regions)
    
    inactive_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(set(inactive_regions) & set([description[0] for description in motif_regions])) > 0]

    if chosen_region == 'active' :
        chosen_set = set(active_regions)
    else :
        chosen_set = set([chosen_region])
        
    active_motifs = [motif for motif, motif_regions in motifs.iteritems() if len(chosen_set & set([description[0] for description in motif_regions])) > 0]
    active_motifs = [motif for motif in active_motifs if len(motif) == chosen_motif_length]
    inactive_motifs = [motif for motif in inactive_motifs if len(motif) == chosen_motif_length]
    n_active = len(active_motifs)
    if random_pvalues is not None :
        random_motifs = [key[0] for key in random_pvalues.keys()]
        random_motifs = np.random.choice(random_motifs, n_active, replace = False)
    
    active_pvalues = [pvalues[(motif, chosen_allowed_mismatches)] for motif in active_motifs]
    inactive_pvalues = [pvalues[(motif, chosen_allowed_mismatches)] for motif in inactive_motifs]

    for item in zip(active_motifs, active_pvalues) :
        if item[0] == 'ATCGTCT' :
            print item

    if random_pvalues is not None :
        random_pvalues = [random_pvalues[(motif, chosen_allowed_mismatches)] for motif in random_motifs]
    from statsmodels.sandbox.stats.multicomp import multipletests
    
    
    uncorrected_active_pvalues, uncorrected_inactive_pvalues = active_pvalues[:], inactive_pvalues[:]
    _, active_pvalues, _, _ = multipletests(active_pvalues, alpha = cutoff, method = 'fdr_bh')
    _, inactive_pvalues, _, _ = multipletests(inactive_pvalues, alpha = cutoff, method = 'fdr_bh')
    if random_pvalues is not None :
        uncorrected_random_pvalues = random_pvalues
        _, random_pvalues, _, _ = multipletests(random_pvalues, alpha = cutoff, method = 'fdr_bh')

    active = zip(active_motifs, uncorrected_active_pvalues, active_pvalues)
    inactive = zip(inactive_motifs, uncorrected_inactive_pvalues, inactive_pvalues)
    if random_pvalues is not None :
        random = zip(random_motifs, uncorrected_random_pvalues, random_pvalues)

    active.sort(key = lambda x : x[1])
    inactive.sort(key = lambda x : x[1])
    if random_pvalues is not None :
        random.sort(key = lambda x : x[1])

    if random_pvalues is not None :
        print '[i] Number random: %d (out of %d)' % (len(random), len(random_motifs))
    print ''

    if write_to_file is not None :
        fout = open(write_to_file, 'wb')
        fout.write('Active\n\n');
        fout.write('k-mer\tp-value\tCorrected p-value\n')

    print '[i] Active: ',
    for motif, uncorrected_pvalue, pvalue in active :
        print '(%s, %g) ' % (motif, pvalue),
        if write_to_file is not None :
            fout.write('%s\t%g\t%g\n' % (motif, uncorrected_pvalue, pvalue))
    print ''
    print ''

    if write_to_file is not None :
        fout.write('\nInactive\n\n');
        fout.write('k-mer\tp-value\tCorrected p-value\n')
    print '[i] Inactive: ',
    for motif, uncorrected_pvalue, pvalue in inactive :
        print '(%s, %g) ' % (motif, pvalue),
        if write_to_file is not None :
            fout.write('%s\t%g\t%g\n' % (motif, uncorrected_pvalue, pvalue))
    print ''
    print ''

    if random_pvalues is not None :
        if write_to_file is not None :
            fout.write('\nRandom\n\n');
            fout.write('k-mer\tp-value\tCorrected p-value\n')
        print '[i] Random: ',
        for motif, uncorrected_pvalue, pvalue in random :
            print '(%s, %g) ' % (motif, pvalue),
            if write_to_file is not None :
                fout.write('%s\t%g\t%g\n' % (motif, uncorrected_pvalue, pvalue))
        print ''

    if write_to_file is not None :
        fout.close()
    
    active = [item for item in active if item[2] < cutoff]
    inactive = [item for item in inactive if item[2] < cutoff]
    if random_pvalues is not None :
        random = [item for item in random if item[2] < cutoff]
    
    print '[i] Number active: %d (out of %d)' % (len(active), len(active_motifs))
    print '[i] Number inactive: %d (out of %d)' % (len(inactive), len(inactive_motifs))
    
    fraction_active = float(len(active)) / len(active_motifs)
    fraction_inactive = float(len(inactive)) / len(inactive_motifs)
    if random_pvalues is not None :
        fraction_random = float(len(random)) / len(random_motifs)

    f, ax = plot.subplots(1, 1)
    width = 0.8
    if random_pvalues is not None :
        data = np.array([fraction_random, fraction_active, fraction_inactive])
        pos = np.array([0, 1, 2]) * width * 1.2 + width / 2.0
    else :
        data = np.array([fraction_active, fraction_inactive])
        pos = np.array([0, 1]) * width * 1.2 + width / 2.0
    ax.bar(pos, data, color = 'k')
    ax.set_xticks(pos + width / 2.0)
    if random_pvalues is not None :
        ax.set_xticklabels(['Random', 'Active', 'Inactive'])
    else :
        ax.set_xticklabels(['Active', 'Inactive'])
    ax.set_ylabel('Fraction of tested $k$-mers', fontsize = 16)
    ax.set_xlim(0, np.max(pos) + width + width / 2.0)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(14)
    ax.set_ylim(0, np.max(data) * 1.35)

    def label_diff(i, j, text, X, Y) :
        x = (X[i] + X[j]) / 2
        y = 1.03 * max(Y[i], Y[j])
        props = {'connectionstyle' : 'bar','arrowstyle' : '-', 'shrinkA': 20,'shrinkB' : 20,'lw' : 2}
        ax.annotate('', xy = (X[i], y), xytext = (X[j], y), arrowprops = props)
        ax.annotate(text, xy = ((X[i] + X[j]) / 2.0, y + 0.08), zorder = 10, fontsize = 15, ha = 'center')

    def two_proportion_test(x1, x2, n1, n2) :
        num = (x1 / float(n1)) - (x2 / float(n2))
        common = (x1 + x2) / float(n1 + n2)
        den = math.sqrt(common * (1 - common) * (1.0 / n1 + 1.0 / n2))
        z = num / den
        import scipy.stats
        return scipy.stats.norm.sf(abs(z)) * 2

    def format_pvalue(pvalue) :
        log_pvalue = int(np.log10(pvalue)) + 1
        if log_pvalue < 0 :
            return '$p<10^{%d}$' % log_pvalue
        elif pvalue > 0.5 :
            return '$p>0.5$'
        elif pvalue > 0.05 :
            return '$p>0.05$'

    if random_pvalues is not None :
        diff_random = two_proportion_test(len(random), len(active), len(random_motifs), len(active_motifs))
        label_diff(0, 1, format_pvalue(diff_random), pos + width / 2.0 * 0.9, data)
        diff_inactive = two_proportion_test(len(inactive), len(active), len(inactive_motifs), len(active_motifs))
        label_diff(1, 2, format_pvalue(diff_inactive), pos + width / 2.0 * 1.1, data)
    else :
        diff_inactive = two_proportion_test(len(inactive), len(active), len(inactive_motifs), len(active_motifs))
        label_diff(0, 1, format_pvalue(diff_inactive), pos + width / 2.0, data)

    plot.tight_layout()
    plot.show()

def perform_kmer_list_mutagenesis_enrichment_analysis(descriptions, groups, kmer_list, min_allowed_mismatches = 0, max_allowed_mismatches = 1) :
    '''
    Calculate k-mer enrichment in mutagenesis windows for k-mers from a given list.
    :param descriptions : list of descriptions of mutagenesis sequences.
    :param groups : groups of mutagenesis sequences (grouped by original oligo).
    :param kmer_list : list of k-mers that should be analysed.
    :param min_allowed_mismatches : minimum number of allowed mismatches.
    :param max_allowed_mismatches : maximum number of allowed mismatches.
    '''

    def _get_pvalue(motif, groups, allowed_mismatches) :
        cnt = np.zeros((2, 2), dtype = np.int)
        for group in groups :
            original = group[0]
            original_ires_activity = original.ires_activity
            windows = group[1:]
            for window in windows :
                if is_sequence_promoter(window) or is_sequence_splice_site(window) or math.isnan(window.ires_activity) :
                    continue
                window_ires_activity = window.ires_activity
                description = window.description
                description = description.split(';')
                original_sequence, mutated_sequence = description[6], description[7]
                fold_change = np.log2((window_ires_activity - BACKGROUND_IRES_ACTIVITY) / (original_ires_activity - BACKGROUND_IRES_ACTIVITY))
                if math.isnan(fold_change) :
                    fold_change = np.log2(MAGIC_FOLD_CHANGE)
                is_selected = fold_change < 0 
                if contains_motif(original_sequence, motif, allowed_mismatches) and not contains_motif(mutated_sequence, motif, allowed_mismatches) :
                    if is_selected :
                        cnt[0, 0] += 1
                    else :
                        cnt[0, 1] += 1
                else :
                    if is_selected :
                        cnt[1, 0] += 1
                    else :
                        cnt[1, 1] += 1
        import scipy.stats
        _, pvalue = scipy.stats.fisher_exact(cnt, alternative = 'greater')
        return pvalue 

    selected_groups = []
    for description in descriptions :
        selected_groups.append(groups[description])
    groups = selected_groups

    pvalues = {}
    motifs = copy.deepcopy(kmer_list)
    for allowed_mismatches in range(min_allowed_mismatches, max_allowed_mismatches + 1) :
        print '[i] Processing: %d mismatches' % allowed_mismatches
        for motif in motifs :
            pvalue = _get_pvalue(motif, groups, allowed_mismatches)
            pvalues[(motif, allowed_mismatches)] = pvalue
            print '%g (%s)' % (pvalue, motif),
        print ''

    return motifs, pvalues

def found_motif_locations_in_region(motifs, region) :
    '''
    Calculate k-mer locations in a given region.
    :param motifs : a dictionary of motifs to be checked.
    :param region : region in which k-mers should be located.
    '''
    locations = {}
    str = region[-1] 
    for motif in motifs :
        locations[motif] = []
        loc = str.find(motif)
        while loc >= 0 :
            locations[motif].append(loc)
            loc = str.find(motif, loc + 1)
    return locations

def write_pvalues_to_file(motifs, pvalues, global_pvalues, filename, allowed_mismatches = 1) :
    fout = open(filename, 'wb')
    items = []
    for motif in motifs :
        items.append((motif, pvalues[(motif, allowed_mismatches)]))
    items.sort(key = lambda x : x[1])
    motifs = [motif for motif, pvalue in items]
    corrected_pvalues = [pvalue for motif, pvalue in items]
    from statsmodels.sandbox.stats.multicomp import multipletests
    _, corrected_pvalues, _, _ = multipletests(corrected_pvalues, method = 'fdr_bh')
    fout.write('k-mer\tGlobal analysis p-value (FDR)\tMutagenesis p-value\tMutagenesis p-value (FDR)\n')
    for motif, corrected_pvalue in zip(motifs, corrected_pvalues) :
        line = '%s\t%g\t%g\t%g\n' % (motif, global_pvalues[motif], pvalues[(motif, allowed_mismatches)], corrected_pvalue)
        fout.write(line)
    fout.close()

def read_activity_along_ssRNA_single_transcript_viruses(seqs, filename = '../data/input_file_for_matlab_ires_activity_along_ssRNA_single_cds_viral_genomes_new.tab') :
    '''
    Read IRES activity along positions on the ssRNA viruses.
    :param filename : name of the file to be read.
    '''
    indexed_seqs = {}
    for seq in seqs :
        indexed_seqs[seq.index] = seq
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    oligos = []
    for line in lines :
        args = line.strip().split('\t')
        index = int(args[0])
        library = args[1]
        gene = args[2]
        virus_name = args[3]
        virus_type = args[4]
        start, stop = int(args[5]) - 1, int(args[6])
        oligo_position = int(args[7]) - 1
        ires_activity = float(args[8])
        promoter_activity = float(args[9])
        splicing_activity = float(args[10])

        entry = SimpleNamespace()
        entry.index = index
        entry.gene = gene
        entry.library = library
        entry.virus_name = virus_name
        entry.virus_type = virus_type
        entry.cds_start = start
        entry.cds_stop = stop
        entry.oligo_position = oligo_position
        entry.ires_activity = ires_activity
        entry.promoter_activity = promoter_activity
        entry.splicing_score = splicing_activity
        entry.sequence = indexed_seqs[entry.index].sequence
        oligos.append(entry)
    return oligos

def read_mature_protein_annotations(filename = '../data/mature_peptide_position.tab') :
    '''
    Read mature protein annotations (for the single transcript ssRNA viruses) from a filename.
    :param filename : name of the file from which the annotations should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    annotations = {}
    for line in lines :
        args = line.strip().split('\t')
        gene = args[0]
        start = int(args[1]) - 1
        stop = int(args[2])
        protein_name = args[3]
        
        protein = SimpleNamespace()
        protein.gene = gene
        protein.start = start
        protein.stop = stop
        protein.name = protein_name
        if not gene in annotations :
            annotations[gene] = []
        annotations[gene].append(protein)
    return annotations

def filter_proteins_without_start_codon(seqs, annotations, max_distance_codons = None, oligo_length = 174) :
    '''
    Filter out protein annotations for proteins that do not have a start codon close to their start.
    :param seqs : a dictionary of oligos separated by 
    '''
    annotations = copy.deepcopy(annotations)
    if max_distance_codons is None :
        return annotations
    for gene in seqs.keys() :
        if not gene in annotations :
            continue
        #print 'Processing: %s' % gene
        seqs[gene].sort(key = lambda x : x.oligo_position)
        sequence = ''.join(seq.sequence for seq in seqs[gene])
        cds_start, cds_stop = seqs[gene][0].cds_start, seqs[gene][0].cds_stop
        frame_start = cds_start % 3
        #print 'Frame start: ', frame_start
        pos = frame_start
        codons = []
        while pos < cds_stop :
            codons.append(sequence[pos : pos + 3])
            pos += 3
        codons = np.array(codons)
        is_start_codon = codons == 'ATG'
        kept_annotations = []
        #protein_starts = set([(protein.start - frame_start) / 3 for protein in annotations[gene]])
        #for i, flag in enumerate(is_start_codon) :
        #    if i in protein_starts :
        #        print '_',
        #    print 'Y' if flag else 'N',
        #    if i in protein_starts :
        #        print '_',
        #print ''
        should_remove_gene = False
        for protein in annotations[gene] :
            protein_start = protein.start
            protein_start -= frame_start
            if protein_start % 3 != 0 :
                print 'ERROR: protein %s, gene %s' % (protein.name, gene)
                should_remove_gene = True
            protein_start /= 3
            left = max(0, protein_start - max_distance_codons)
            right = min(protein_start + max_distance_codons, len(is_start_codon) - 1)
            if np.any(is_start_codon[left : right]) :
                kept_annotations.append(protein)
         #       print 'Keeping'
         #   else :
         #       print 'Removing'
        annotations[gene] = kept_annotations
        if len(kept_annotations) == 0 :
            should_remove_gene = True
        if should_remove_gene :
            del annotations[gene]
            print '[i] Removing gene %s' % gene
        #return None
    return annotations

def perform_colocalization_analysis(oligos, annotations, allowed_classes = ['Picornaviridae', 'Hepacivirus'], oligo_length = 174, n_reps = 1000, summary_type = 'sum') :
    annotations = copy.deepcopy(annotations)
    kept_oligos = []
    for seq in oligos :
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            continue
        allowed_class = False
        for cur_class in allowed_classes :
            if cur_class in seq.virus_type :
                allowed_class = True
                break
        if not allowed_class :
            continue
        if seq.ires_activity > SIGNIFICANT_IRES_ACTIVITY :
            kept_oligos.append(seq)
    oligos = kept_oligos
    print '[i] Kept oligos: %d' % len(oligos)

    seqs = {}
    for seq in oligos :
        if not seq.gene in seqs :
            seqs[seq.gene] = []
        seqs[seq.gene].append(seq)
    print '[i] From %d genes' % len(seqs)

    for gene in annotations.keys() :
        if not gene in seqs :
            del annotations[gene]

    for gene in annotations.keys() :
        seq = seqs[gene][0]
        total_length = seq.cds_stop - seq.cds_start
        total_occupied_length = 0
        for protein in annotations[gene] :
            total_occupied_length += protein.stop - protein.start
        if total_occupied_length > total_length :
            print '[!] Gene: %s, total length: %d, total occupied length: %d -> removing' % (gene, total_length, total_occupied_length)
            del annotations[gene]
            del seqs[gene]

    def compute_distance_per_protein(seqs, annotations) :
        distances = {}
        for gene in annotations :
            distances[gene] = {}
            for protein in annotations[gene] :
                cur_min = float('inf')
                for seq in seqs[gene] :
                    cur_dist = abs(protein.start - (seq.oligo_position + oligo_length))
                    if cur_dist < cur_min :
                        cur_min = cur_dist
                distances[gene][protein.name] = cur_min
        return distances

    def summarize_distance_per_gene(distances, summary_type = 'sum') :
        summary = {}
        for gene in distances :
            if summary_type == 'sum' :
                summary[gene] = np.sum(distances[gene].values())
            elif isinstance(summary_type, int) :
                values = copy.deepcopy(distances[gene].values())
                values.sort()
                summary[gene] = np.sum(values[:summary_type])
        return summary

    def permute_annotations(annotations) :
        for gene in annotations :
            seq = seqs[gene][0]
            np.random.shuffle(annotations[gene])
            pos = seq.cds_start
            total_length = seq.cds_stop - seq.cds_start - 3
            total_occupied_length = 0
            for protein in annotations[gene] :
                length = protein.stop - protein.start
                protein.start = pos
                protein.stop = pos + length
                pos += length
                total_occupied_length += length
            space_left = total_length - total_occupied_length
            space_left_codons = space_left / 3
            n_proteins = len(annotations[gene])
            shift = np.zeros((n_proteins, ), dtype = np.int)
            for i in range(space_left_codons) :
                index = np.random.randint(0, n_proteins + 1)
                if index == n_proteins :
                    continue
                else :
                    shift[index] += 3
            cum = 0
            for i in range(n_proteins) :
                cum += shift[i]
                protein = annotations[gene][i]
                protein.start += cum
                protein.stop += cum

    original_stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
    stats = []
    for _ in range(n_reps) :
        permute_annotations(annotations)
        stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
        stats.append(stat)

    pvalues = {}
    gene_stats_dict = {}
    for gene in original_stat :
        gene_stats = np.array([s[gene] for s in stats])
        gene_stats_dict[gene] = gene_stats
        pvalue = np.sum(gene_stats <= original_stat[gene], dtype = np.int) / float(n_reps)
        pvalues[gene] = pvalue

    from statsmodels.sandbox.stats.multicomp import multipletests
    corrected_pvalues = {}
    corrected_pvalues_list = [pvalues[gene] for gene in original_stat]
    _, corrected_pvalues_list, _, _ = multipletests(corrected_pvalues_list, method = 'fdr_bh')
    i = 0
    for gene in original_stat :
        corrected_pvalues[gene] = corrected_pvalues_list[i]
        i += 1
    for gene in original_stat :
        print '[i] Gene: %s, pvalue: %g, corrected pvalue: %g' % (gene, pvalues[gene], corrected_pvalues[gene])

    return seqs, annotations, pvalues, corrected_pvalues, original_stat, gene_stats_dict

def perform_colocalization_analysis_permute_ires_locations(oligos, annotations, allowed_classes = ['Picornaviridae', 'Hepacivirus'], remove_isolated_ireses = False, circular_permutation = False, filter_proteins = None, oligo_length = 174, n_reps = 1000, summary_type = 'sum') :
    annotations = annotations.copy()
    oligos = copy.deepcopy(oligos)

    kept_oligos = []
    seqs_locations = {}
    seqs_all = {}
    for seq in oligos :
        allowed_class = False
        for cur_class in allowed_classes :
            if cur_class in seq.virus_type :
                allowed_class = True
                break
        if not allowed_class :
            continue
        if not seq.gene in seqs_locations :
            seqs_locations[seq.gene] = []
        if not seq.gene in seqs_all :
            seqs_all[seq.gene] = []
        seqs_locations[seq.gene].append(seq.oligo_position)
        seqs_all[seq.gene].append(seq)
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            continue
        if seq.ires_activity > SIGNIFICANT_IRES_ACTIVITY :
            kept_oligos.append(seq)
    oligos = kept_oligos
    print '[i] Kept oligos: %d' % len(oligos)

    seqs = {}
    for seq in oligos :
        if not seq.gene in annotations :
            continue
        if not seq.gene in seqs :
            seqs[seq.gene] = []
        seqs[seq.gene].append(seq)
    print '[i] From %d genes' % len(seqs)
    
    if remove_isolated_ireses :
        for gene in seqs.keys() :
            seqs[gene].sort(key = lambda x : x.oligo_position)
            n_ireses = len(seqs[gene])
            isolated_indices = set()
            for i in range(n_ireses) :
                if (i > 0 and seqs[gene][i - 1].oligo_position + oligo_length == seqs[gene][i].oligo_position) or (i < n_ireses - 1 and seqs[gene][i].oligo_position + oligo_length == seqs[gene][i + 1].oligo_position) :
                    continue
                isolated_indices.add(i)
            print 'Gene: %s, isolated %d / %d' % (gene, len(isolated_indices), n_ireses)
            seqs[gene] = [seqs[gene][i] for i in range(n_ireses) if not i in isolated_indices]
            if len(isolated_indices) == n_ireses :
                print '    => Removing'
                del seqs[gene]
    
    for gene in annotations.keys() :
        if not gene in seqs :
            del annotations[gene]

    for gene in annotations.keys() :
        seq = seqs[gene][0]
        total_length = seq.cds_stop - seq.cds_start
        total_occupied_length = 0
        for protein in annotations[gene] :
            total_occupied_length += protein.stop - protein.start
        if total_occupied_length > total_length :
            print '[!] Gene: %s, total length: %d, total occupied length: %d -> removing' % (gene, total_length, total_occupied_length)
            del annotations[gene]
            del seqs[gene]
    
    if filter_proteins is not None :
        annotations = filter_proteins_without_start_codon(seqs_all, annotations, max_distance_codons = filter_proteins, oligo_length = oligo_length)

    for gene in annotations.keys() :
        if not gene in seqs :
            del annotations[gene]

    print '[i] Left with: %d genes' % len(annotations)

    def compute_distance_per_protein(seqs, annotations) :
        distances = {}
        for gene in annotations :
            distances[gene] = {}
            for protein in annotations[gene] :
                cur_min = float('inf')
                for seq in seqs[gene] :
                    cur_dist = abs(protein.start - (seq.oligo_position + oligo_length))
                    if cur_dist < cur_min :
                        cur_min = cur_dist
                distances[gene][protein.name] = cur_min
        return distances

    def summarize_distance_per_gene(distances, summary_type = 'sum') :
        summary = {}
        for gene in distances :
            if summary_type == 'sum' :
                summary[gene] = np.sum(distances[gene].values())
            elif isinstance(summary_type, int) :
                values = copy.deepcopy(distances[gene].values())
                values.sort()
                summary[gene] = np.sum(values[:summary_type])
        return summary

    def permute_ires_locations(seqs, seqs_locations, circular_permutation = False) :
        for gene in seqs :
            n_oligos = len(seqs[gene])
            if not circular_permutation :
                locations = np.random.choice(seqs_locations[gene], n_oligos, replace = False)
            else :
                max_position = np.max(seqs_locations[gene]) + 1
                locations = np.array([seq.oligo_position for seq in seqs[gene]])
                shift = np.random.randint(0, high = len(seqs_locations[gene]))
                locations = (locations + oligo_length * shift) % max_position
            for seq, location in zip(seqs[gene], locations) :
                seq.oligo_position = location
            seqs[gene].sort(key = lambda x : x.oligo_position)

    original_stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
    stats = []
    for _ in range(n_reps) :
        permute_ires_locations(seqs, seqs_locations, circular_permutation = circular_permutation)
        stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
        stats.append(stat)

    pvalues = {}
    gene_stats_dict = {}
    for gene in original_stat :
        gene_stats = np.array([s[gene] for s in stats])
        gene_stats_dict[gene] = gene_stats
        pvalue = np.sum(gene_stats <= original_stat[gene], dtype = np.int) / float(n_reps)
        pvalues[gene] = pvalue

    from statsmodels.sandbox.stats.multicomp import multipletests
    corrected_pvalues = {}
    corrected_pvalues_list = [pvalues[gene] for gene in original_stat]
    print corrected_pvalues_list
    _, corrected_pvalues_list, _, _ = multipletests(corrected_pvalues_list, method = 'fdr_bh')
    i = 0
    print corrected_pvalues_list
    for gene in original_stat :
        corrected_pvalues[gene] = corrected_pvalues_list[i]
        i += 1
    cnt_positive, cnt_all = 0, 0
    cnt_all_dict = {}
    for gene in original_stat :
        print '[i] Gene: %s, pvalue: %g, corrected pvalue: %g (%d / %d, %.3f)' % (gene, pvalues[gene], corrected_pvalues[gene], len(seqs[gene]), len(seqs_locations[gene]), len(seqs[gene]) / float(len(seqs_locations[gene])))
        cnt_positive += len(seqs[gene])
        cnt_all += len(seqs_locations[gene])
        cnt = len(seqs_locations[gene])
        cnt_all_dict[cnt] = cnt_all_dict.get(cnt, 0) + 1
    print '[i]  %d / %d, %.3f' % (cnt_positive, cnt_all, cnt_positive / float(cnt_all))
    print cnt_all_dict

    return seqs, annotations, pvalues, corrected_pvalues, original_stat, gene_stats_dict

def plot_colocalization_analysis(pvalues, pvalues_corrected, orig_stats, gene_stats) :
    '''
    Plot colocalization analysis statistics for a number of genes.
    :param pvalues : a dictionary of p-values to be plotted.
    :param pvalues_corrected : a dictionary of corrected p-values.
    :param orig_stats : original statistics per gene to be plotted.
    :param gene_stats : background model statistics per gene.
    '''
    n_bins = 50
    print '[i] Number of genes: %d' % len(pvalues)
    n_rows, n_cols = 7, 8
    f, ax = plot.subplots(n_rows, n_cols)
    row, col = 0, 0
    genes = pvalues.keys()
    genes.sort(key = lambda x : pvalues[x])
    for gene in genes :
        if n_rows == 1 :
            cur = ax[col]
        else :
            cur = ax[row, col]
        cur.hist(gene_stats[gene], n_bins, facecolor = 'blue', alpha = 0.45, histtype = 'stepfilled')
        ymin, ymax = cur.get_ylim()
        xmin, xmax = cur.get_xlim()
        xmin = min(xmin, orig_stats[gene] - 10)
        xmax = max(xmax, orig_stats[gene] + 10)
        cur.set_xlim(xmin, xmax)
        cur.set_xticks([xmin, xmax])
        cur.set_yticks([ymin, ymax])
        cur.plot([orig_stats[gene], orig_stats[gene]], [ymin, ymax], color = 'r', linewidth = 2.5)
        cur.text(0.3, 0.3, '$p=%g$\n$p_\\mathrm{corr}=%g$' % (pvalues[gene], pvalues_corrected[gene]), ha = 'left', va = 'bottom', transform = cur.transAxes, fontsize = 12, fontweight = 'bold')
        cur.set_title(gene)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    while col < n_cols and row < n_rows :
        cur = ax[row][col]
        f.delaxes(cur)
        col += 1
        if col >= n_cols :
            col = 0
            row += 1
    
    f.set_size_inches(19, 10, forward = True)
    plot.tight_layout()
    plot.show()

def plot_colocalization_analysis_summary_statistic(pvalue, orig_stat, stats) :
    '''
    Plot colocalization analysis statistics for a number of genes.
    :param pvalue : pvalue of the test.
    :param orig_stat : original statistic to be plotted.
    :param stats : background model statistics per gene.
    '''
    n_bins = 50
    f, ax = plot.subplots(1, 1)
    ax.hist(stats, n_bins, facecolor = 'blue', alpha = 0.45, histtype = 'stepfilled')
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()
    xmin = min(xmin, orig_stat - 10)
    xmax = max(xmax, orig_stat + 10)
    ax.set_xlim(xmin, xmax)
    ax.set_xticks([xmin, xmax])
    ax.set_yticks([ymin, ymax])
    ax.plot([orig_stat, orig_stat], [ymin, ymax], color = 'r', linewidth = 2.5)
    ax.text(0.3, 0.3, '$p=%g$' % pvalue, ha = 'left', va = 'bottom', transform = ax.transAxes, fontsize = 12, fontweight = 'bold')
    ax.set_title('Summary statistic')
    
    f.set_size_inches(8, 6, forward = True)
    plot.tight_layout()
    plot.show()

def perform_joint_colocalization_analysis_permute_ires_locations(oligos, annotations, allowed_classes = ['Picornaviridae', 'Hepacivirus'], oligo_length = 174, n_reps = 1000, summary_type = 'sum') :
    annotations = copy.deepcopy(annotations)
    oligos = copy.deepcopy(oligos)
    kept_oligos = []
    seqs_locations = {}
    seqs_all = {}
    for seq in oligos :
        allowed_class = False
        for cur_class in allowed_classes :
            if cur_class in seq.virus_type :
                allowed_class = True
                break
        if not allowed_class :
            continue
        if not seq.gene in seqs_locations :
            seqs_locations[seq.gene] = []
        if not seq.gene in seqs_all :
            seqs_all[seq.gene] = []
        seqs_locations[seq.gene].append(seq.oligo_position)
        seqs_all[seq.gene].append(seq)
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            continue
        if seq.ires_activity > SIGNIFICANT_IRES_ACTIVITY :
            kept_oligos.append(seq)
    
    oligos = kept_oligos
    print '[i] Kept oligos: %d' % len(oligos)

    seqs = {}
    for seq in oligos :
        if not seq.gene in seqs :
            seqs[seq.gene] = []
        seqs[seq.gene].append(seq)
    print '[i] From %d genes' % len(seqs)

    for gene in annotations.keys() :
        if not gene in seqs :
            del annotations[gene]

    for gene in annotations.keys() :
        seq = seqs[gene][0]
        total_length = seq.cds_stop - seq.cds_start
        total_occupied_length = 0
        for protein in annotations[gene] :
            total_occupied_length += protein.stop - protein.start
        if total_occupied_length > total_length :
            print '[!] Gene: %s, total length: %d, total occupied length: %d -> removing' % (gene, total_length, total_occupied_length)
            del annotations[gene]
            del seqs[gene]
    
    print '[i] Left with: %d genes' % len(annotations)

    n_positive_oligos, n_total_oligos = 0, 0
    status = []
    for gene in seqs :
        n_positive_oligos += len(seqs[gene])
        n_total_oligos += len(seqs_locations[gene])
        status.extend([1] * n_positive_oligos + [0] * (n_total_oligos - n_positive_oligos)) 

    def compute_distance_per_protein(seqs, annotations) :
        distances = {}
        for gene in annotations :
            distances[gene] = {}
            for protein in annotations[gene] :
                cur_min = float('inf')
                for seq in seqs[gene] :
                    cur_dist = abs(protein.start - (seq.oligo_position + oligo_length))
                    if cur_dist < cur_min :
                        cur_min = cur_dist
                distances[gene][protein.name] = cur_min
        return distances

    def summarize_distance_per_gene(distances, summary_type = 'sum') :
        summary = {}
        for gene in distances :
            if summary_type == 'sum' :
                summary[gene] = np.sum(distances[gene].values())
            elif isinstance(summary_type, int) :
                values = copy.deepcopy(distances[gene].values())
                values.sort()
                summary[gene] = np.sum(values[:summary_type])
        return summary

    def permute_ires_locations(seqs, seqs_locations) :
        np.random.shuffle(status)
        pos = 0
        for gene in seqs :
            n_total_gene_oligos = len(seqs_locations[gene])
            n_positive_gene_oligos = np.sum(status[pos:pos + n_total_gene_oligos], dtype = np.int)
            seqs[gene] = np.random.choice(seqs_all[gene], n_positive_gene_oligos, replace = False)
            pos += n_total_gene_oligos

    original_stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
    stats = []
    for _ in range(n_reps) :
        permute_ires_locations(seqs, seqs_locations)
        stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
        stats.append(stat)

    pvalues = {}
    gene_stats_dict = {}
    for gene in original_stat :
        gene_stats = np.array([s[gene] for s in stats])
        gene_stats_dict[gene] = gene_stats
        pvalue = np.sum(gene_stats <= original_stat[gene], dtype = np.int) / float(n_reps)
        pvalues[gene] = pvalue

    from statsmodels.sandbox.stats.multicomp import multipletests
    corrected_pvalues = {}
    corrected_pvalues_list = [pvalues[gene] for gene in original_stat]
    _, corrected_pvalues_list, _, _ = multipletests(corrected_pvalues_list, method = 'fdr_bh')
    i = 0
    for gene in original_stat :
        corrected_pvalues[gene] = corrected_pvalues_list[i]
        i += 1
    cnt_positive, cnt_all = 0, 0
    cnt_all_dict = {}
    for gene in original_stat :
        print '[i] Gene: %s, pvalue: %g, corrected pvalue: %g (%d / %d, %.3f)' % (gene, pvalues[gene], corrected_pvalues[gene], len(seqs[gene]), len(seqs_locations[gene]), len(seqs[gene]) / float(len(seqs_locations[gene])))
        cnt_positive += len(seqs[gene])
        cnt_all += len(seqs_locations[gene])
        cnt = len(seqs_locations[gene])
        cnt_all_dict[cnt] = cnt_all_dict.get(cnt, 0) + 1
    print '[i]  %d / %d, %.3f' % (cnt_positive, cnt_all, cnt_positive / float(cnt_all))
    print cnt_all_dict

    return seqs, annotations, pvalues, corrected_pvalues, original_stat, gene_stats_dict

def perform_colocalization_analysis_permute_ires_locations_summary_statistic(oligos, annotations, allowed_classes = ['Picornaviridae', 'Hepacivirus'], remove_isolated_ireses = False, circular_permutation = False, oligo_length = 174, n_reps = 1000, summary_type = 'sum') :
    annotations = copy.deepcopy(annotations)
    oligos = copy.deepcopy(oligos)
    kept_oligos = []
    seqs_locations = {}
    for seq in oligos :
        allowed_class = False
        for cur_class in allowed_classes :
            if cur_class in seq.virus_type :
                allowed_class = True
                break
        if not allowed_class :
            continue
        if not seq.gene in seqs_locations :
            seqs_locations[seq.gene] = []
        seqs_locations[seq.gene].append(seq.oligo_position)
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            continue
        if seq.ires_activity > SIGNIFICANT_IRES_ACTIVITY :
            kept_oligos.append(seq)
    oligos = kept_oligos
    print '[i] Kept oligos: %d' % len(oligos)

    seqs = {}
    for seq in oligos :
        if not seq.gene in seqs :
            seqs[seq.gene] = []
        seqs[seq.gene].append(seq)
    print '[i] From %d genes' % len(seqs)

    if remove_isolated_ireses :
        for gene in seqs.keys() :
            seqs[gene].sort(key = lambda x : x.oligo_position)
            n_ireses = len(seqs[gene])
            isolated_indices = set()
            for i in range(n_ireses) :
                if (i > 0 and seqs[gene][i - 1].oligo_position + oligo_length == seqs[gene][i].oligo_position) or (i < n_ireses - 1 and seqs[gene][i].oligo_position + oligo_length == seqs[gene][i + 1].oligo_position) :
                    continue
                isolated_indices.add(i)
            print 'Gene: %s, isolated %d / %d' % (gene, len(isolated_indices), n_ireses)
            seqs[gene] = [seqs[gene][i] for i in range(n_ireses) if not i in isolated_indices]
            if len(isolated_indices) == n_ireses :
                print '    => Removing'
                del seqs[gene]

    for gene in annotations.keys() :
        if not gene in seqs :
            del annotations[gene]

    for gene in annotations.keys() :
        seq = seqs[gene][0]
        total_length = seq.cds_stop - seq.cds_start
        total_occupied_length = 0
        for protein in annotations[gene] :
            total_occupied_length += protein.stop - protein.start
        if total_occupied_length > total_length :
            print '[!] Gene: %s, total length: %d, total occupied length: %d -> removing' % (gene, total_length, total_occupied_length)
            del annotations[gene]
            del seqs[gene]
    
    print '[i] Left with: %d genes' % len(annotations)

    def compute_distance_per_protein(seqs, annotations) :
        distances = {}
        for gene in annotations :
            distances[gene] = {}
            for protein in annotations[gene] :
                cur_min = float('inf')
                for seq in seqs[gene] :
                    cur_dist = abs(protein.start - (seq.oligo_position + oligo_length))
                    if cur_dist < cur_min :
                        cur_min = cur_dist
                distances[gene][protein.name] = cur_min
        return distances

    def summarize_distance_per_gene(distances, summary_type = 'sum') :
        summary = {}
        for gene in distances :
            if summary_type == 'sum' :
                summary[gene] = np.sum(distances[gene].values())
            elif isinstance(summary_type, int) :
                values = copy.deepcopy(distances[gene].values())
                values.sort()
                summary[gene] = np.sum(values[:summary_type])
        return summary

    def permute_ires_locations(seqs, seqs_locations, circular_permutation = False) :
        for gene in seqs :
            n_oligos = len(seqs[gene])
            if not circular_permutation :
                locations = np.random.choice(seqs_locations[gene], n_oligos, replace = False)
            else :
                max_position = np.max(seqs_locations[gene]) + 1
                locations = np.array([seq.oligo_position for seq in seqs[gene]])
                shift = np.random.randint(0, high = len(seqs_locations[gene]))
                locations = (locations + oligo_length * shift) % max_position
            for seq, location in zip(seqs[gene], locations) :
                seq.oligo_position = location
            seqs[gene].sort(key = lambda x : x.oligo_position)

    def summarize_statistic(stat) :
        summary_stat = np.sum(stat.values(), dtype = np.int)
        return summary_stat

    original_stat = summarize_statistic(summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type))
    stats = []
    for _ in range(n_reps) :
        permute_ires_locations(seqs, seqs_locations, circular_permutation = circular_permutation)
        stat = summarize_statistic(summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type))
        stats.append(stat)

    stats = np.array(stats)
    pvalue = np.sum(stats <= original_stat, dtype = np.int) / float(n_reps)

    print '[i] pvalue: %g, stat: %g' % (pvalue, stat) 

    return seqs, annotations, pvalue, original_stat, stats

def perform_colocalization_analysis_permute_genes(oligos, annotations, selected_oligo_count = 43, allowed_classes = ['Picornaviridae', 'Hepacivirus'], oligo_length = 174, n_reps = 1000, summary_type = 'sum') :
    annotations = copy.deepcopy(annotations)
    oligos = copy.deepcopy(oligos)
    kept_oligos = []
    seqs_locations = {}
    for seq in oligos :
        allowed_class = False
        for cur_class in allowed_classes :
            if cur_class in seq.virus_type :
                allowed_class = True
                break
        if not allowed_class :
            continue
        if not seq.gene in seqs_locations :
            seqs_locations[seq.gene] = []
        seqs_locations[seq.gene].append(seq.oligo_position)
        if is_sequence_promoter(seq) or is_sequence_splice_site(seq) or math.isnan(seq.ires_activity) :
            continue
        if seq.ires_activity > SIGNIFICANT_IRES_ACTIVITY :
            kept_oligos.append(seq)
    oligos = kept_oligos
    print '[i] Kept oligos: %d' % len(oligos)

    seqs = {}
    for seq in oligos :
        if not seq.gene in seqs :
            seqs[seq.gene] = []
        seqs[seq.gene].append(seq)
    print '[i] From %d genes' % len(seqs)

    for gene in annotations.keys() :
        if not gene in seqs :
            del annotations[gene]
    for gene in seqs.keys() :
        if not gene in annotations :
            del seqs[gene]

    for gene in annotations.keys() :
        seq = seqs[gene][0]
        total_length = seq.cds_stop - seq.cds_start
        total_occupied_length = 0
        for protein in annotations[gene] :
            total_occupied_length += protein.stop - protein.start
        if total_occupied_length > total_length :
            print '[!] Gene: %s, total length: %d, total occupied length: %d -> removing' % (gene, total_length, total_occupied_length)
            if gene in annotations :
                del annotations[gene]
            if gene in seqs :
                del seqs[gene]
        if len(seqs_locations[gene]) != selected_oligo_count :
            print '[i] Removing gene: %s (oligo count %d)' % (gene, len(seqs_locations[gene]))
            if gene in annotations :
                del annotations[gene]
            if gene in seqs :
                del seqs[gene]
    
    print '[i] Left with: %d genes' % len(annotations)

    def compute_distance_per_protein(seqs, annotations) :
        distances = {}
        for gene in annotations :
            distances[gene] = {}
            for protein in annotations[gene] :
                cur_min = float('inf')
                for seq in seqs[gene] :
                    cur_dist = abs(protein.start - (seq.oligo_position + oligo_length))
                    if cur_dist < cur_min :
                        cur_min = cur_dist
                distances[gene][protein.name] = cur_min
        return distances

    def summarize_distance_per_gene(distances, summary_type = 'sum') :
        summary = {}
        for gene in distances :
            if summary_type == 'sum' :
                summary[gene] = np.sum(distances[gene].values())
            elif isinstance(summary_type, int) :
                values = copy.deepcopy(distances[gene].values())
                values.sort()
                summary[gene] = np.sum(values[:summary_type])
        return summary

    def permute_ires_locations(seqs, seqs_orig) :
        genes = seqs.keys()
        genes_shuffle = copy.deepcopy(genes)
        np.random.shuffle(genes_shuffle)
        for gene_a, gene_b in zip(genes, genes_shuffle) :
            seqs[gene_a] = seqs_orig[gene_b]

    original_stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
    stats = []
    seqs_orig = copy.deepcopy(seqs)
    for _ in range(n_reps) :
        permute_ires_locations(seqs, seqs_orig)
        stat = summarize_distance_per_gene(compute_distance_per_protein(seqs, annotations), summary_type = summary_type)
        stats.append(stat)

    pvalues = {}
    gene_stats_dict = {}
    for gene in original_stat :
        gene_stats = np.array([s[gene] for s in stats])
        gene_stats_dict[gene] = gene_stats
        pvalue = np.sum(gene_stats <= original_stat[gene], dtype = np.int) / float(n_reps)
        pvalues[gene] = pvalue

    from statsmodels.sandbox.stats.multicomp import multipletests
    corrected_pvalues = {}
    corrected_pvalues_list = [pvalues[gene] for gene in original_stat]
    _, corrected_pvalues_list, _, _ = multipletests(corrected_pvalues_list, method = 'fdr_bh')
    i = 0
    for gene in original_stat :
        corrected_pvalues[gene] = corrected_pvalues_list[i]
        i += 1

    cnt_positive, cnt_all = 0, 0
    cnt_all_dict = {}
    for gene in original_stat :
        print '[i] Gene: %s, pvalue: %g, corrected pvalue: %g (%d / %d, %.3f)' % (gene, pvalues[gene], corrected_pvalues[gene], len(seqs[gene]), len(seqs_locations[gene]), len(seqs[gene]) / float(len(seqs_locations[gene])))
        cnt_positive += len(seqs[gene])
        cnt_all += len(seqs_locations[gene])
        cnt = len(seqs_locations[gene])
        cnt_all_dict[cnt] = cnt_all_dict.get(cnt, 0) + 1
    print '[i]  %d / %d, %.3f' % (cnt_positive, cnt_all, cnt_positive / float(cnt_all))
    print cnt_all_dict

    return seqs, annotations, pvalues, corrected_pvalues, original_stat, gene_stats_dict

