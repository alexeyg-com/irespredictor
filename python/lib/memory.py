import threading
import atexit

_PER_MB = 1024.0
_INTERVAL_SEC = 0.5

class MemoryMonitor(object) :
    '''
    Memory monitor class.
    '''

    def __init__(self) :
        self._peak_usage = float('nan')
        self._is_running = True
        self._lock = threading.Lock()
        self._thread = threading.Thread(name = 'monitor', target = self._monitor_method, args = (self._lock, self))
        self._thread.daemon = True
        self._thread.start()

    def finalize(self) :
        self._is_running = False
        self._thread.join()

    @property
    def peak_usage(self) :
        self._lock.acquire(True)
        value = self._peak_usage
        self._lock.release()
        return value
    
    @staticmethod
    def _monitor_method(lock, obj):
        import resource
        import time
        lock.acquire(True)
        obj._peak_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / _PER_MB
        lock.release()
        while obj._is_running :
            usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / _PER_MB
            lock.acquire(True)
            if usage > obj._peak_usage :
                obj._peak_usage = usage
            lock.release()
            time.sleep(_INTERVAL_SEC)

