'''
Implements a record type class used for data representation throughout the project.

Alexey Gritsenko
31-10-2013
'''

class SimpleNamespace :
    ''' 
    SimpleNamespace is a class the implements a mutable record type. It uses the internal __dict__ to add and update fields.
    ''' 

    def __init__(self, **kwargs) :
        ''' 
        Record class constructor. Passes arguments to the update of the internal dictionary.
        ''' 
        self.__dict__.update(kwargs)
    
    def has_key(self, key) :
        ''' 
        Returns true if the record has a corresponding field.
        :param key: the name of the field to check.
        ''' 
        return self.__dict__.has_key(key)

    def __repr__(self):
        ''' 
        A method defining string representation of the record class.
        ''' 
        keys = sorted(self.__dict__)
        items = ("{}={!r}".format(k, self.__dict__[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))
