'''
A quick module for making TSL (Two-Sample Logos) using the TwoSampleLogo.Org website.

Alexey Gritsenko
22-10-2015
'''

import cookielib
import mechanize
from matplotlib import image

_TSL_URL = 'http://www.twosamplelogo.org/cgi-bin/tsl/tsl.cgi'
_TSL_BASE_URL = 'http://www.twosamplelogo.org'

def fasta_format_sequences(sequences) :   
    '''
    Format sequences in FASTA format to be used in the request.
    :param sequences : a list of sequences to be formatted.
    '''
    lines = []
    for name, seq in sequences :
        lines.append('>%s' % name)
        lines.append(seq)
    text = '\n'.join(lines)
    return text

def make_logo(positive_seqs, negative_seqs) :
    '''
    Make a logo using the positive and negative sequences.
    :param positive_seqs : a list of positive sequences.
    :param negative_seqs : a list of negative sequences.
    '''
    positive_sample = fasta_format_sequences(positive_seqs)
    negative_sample = fasta_format_sequences(negative_seqs)
    
    values = {'antialias'       : True,
              'color0'          : 'orange',
              'color1'          : 'red',
              'color10'         : '',
              'color11'         : '', 
              'color12'         : '', 
              'color13'         : '', 
              'color14'         : '', 
              'color2'          : 'blue',
              'color3'          : 'green',
              'color4'          : '', 
              'color5'          : '', 
              'color6'          : '', 
              'color7'          : '',
              'color8'          : '', 
              'color9'          : '',
              'colors'          : 'nucleo_weblogo',
              'conserved'       : True,
              'format'          : 'PNG',
              'input_kind'      : 'N',
              'logo_end'        : '',
              'logo_height'     : 5,
              'logo_start'      : '', 
              'logo_units'      : 'cm',
              'logo_width'      : 18,
              'negative_sample' : negative_sample,
              'p_value'         : 0.05,
              'positive_sample' : positive_sample,
              'res'             : 96,
              'res_units'       : 'ppi',
              'shrink'          : 0.5,
              'start_num'       : 1,
              'symbols0'        : 'G',
              'symbols1'        : 'TU',
              'symbols10'       : '',
              'symbols11'       : '',
              'symbols12'       : '',
              'symbols13'       : '',
              'symbols14'       : '',
              'symbols2'        : 'C',
              'symbols3'        : 'A',
              'symbols4'        : '',
              'symbols5'        : '',
              'symbols6'        : '',
              'symbols7'        : '',
              'symbols8'        : '',
              'symbols9'        : '',
              'test'            : 'binomial',
              'title'           : '',
              'xaxis'           : True,
              'yaxis'           : True,
        }

    cookiejar = cookielib.CookieJar()
    
    import mechanize
    br = mechanize.Browser()
    br.set_cookiejar(cookiejar)

    br.set_handle_equiv(True)
    br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)

    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time = 1)

    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    visit_request = br.open(_TSL_URL)
    visit_html = visit_request.read()

    br.select_form(nr = 0)
    form = br.form
    for key, value in values.iteritems() :
        if isinstance(value, bool) :
            value = 'on' if value else 'off'
        value = str(value)

        control = br.form.find_control(key)
        if isinstance(control, mechanize.ListControl) or isinstance(control, mechanize.RadioControl) :
            value = [value]
        form[key] = value
    
    br.click(type = 'submit', nr = 0)
    html = br.submit().read()
    
    link = html[27:-16]
    link = _TSL_BASE_URL + link
    response = br.retrieve(link)
    response_filename = response[0]

    im = image.imread(response_filename)

    return im
    
