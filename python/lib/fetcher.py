'''
A library that implements some code required for fetching files form the Grid.
Used for storing precomputed k-mer correlations on the Grid and fetching them on demand.

06-03-2014
Alexey Gritsenko
'''

import numpy as np
import os
import shlex
import subprocess
import time
import cluster_storage

grid_path = 'grid:/IRES_Predictor/datasets/%s/k5'
n_retries = 7
sleep_mean = 120
sleep_std = 40

def fetch_file(source, destination) :
    '''
    Fetches a file from the Grid.
    :param source: name/path of the file to fetch from the grid.
    :param destination: name/path where the file should be stored locally.
    '''
    print 'Want: %s' % source
    lfc_home = os.environ.get('LFC_HOME')
    if lfc_home is None :
        return False
    cs = cluster_storage.ClusterStorageEngine()

    source = source[6:]
    if not source.startswith('/'):
    	source = os.path.join(lfc_home, source)
    attempt = 0
    while attempt < n_retries :
        cs.retrieve_file(source, destination)
        if not os.path.exists(destination) :
            attempt += 1
            time_to_sleep = np.random.normal(sleep_mean, sleep_std)
            time.sleep(time_to_sleep)
        else :
            return True
    return False

def unzip_file(filename) :
    '''
    Unzip a given file (CV dataset).
    '''
    current_dir = os.path.abspath(os.getcwd())
    path = os.path.join(current_dir, 'datasets/current')
    args = shlex.split('unzip %s -d %s' % (filename, path))
    code = subprocess.call(args, shell = False)
    if code > 0 :
        return False
    return True

def get_dataset_type() :
    '''
    Return the type of dataset currently used (i.e. the set symlink)
    '''
    return os.readlink('./datasets/current')

def fetch_dataset_crossvalidation(dataset_name) :
    '''
    Fetch precomputed CV correlations from the Grid
    :param dataset_name : name of the dataset for which the correlations should be fetched.
    '''
    cv_path = './datasets/current/%s/cv' % dataset_name

    if os.path.exists(cv_path) :
        return True
    filename = '%s-cv.zip' % dataset_name
    destination_filename = './datasets/current/%s' % filename
    current_dataset_type = get_dataset_type()
    real_grid_path = grid_path % current_dataset_type
    source_filename = '%s/%s' % (real_grid_path, filename)
    destination_filename = os.path.abspath(destination_filename)
    if not fetch_file(source_filename, destination_filename) :
        return False
    return unzip_file(destination_filename)

