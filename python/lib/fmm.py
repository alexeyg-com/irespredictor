'''
A wrapper around FMM for identifying motifs in sequences and scoring sequences with motifs.

29-03-2015
Alexey Gritsenko
'''

import numpy as np
import math
import tempfile
import uuid
import os
import shlex
import shutil
import subprocess
from xml.etree import ElementTree
from ipyparallel import Client

import persistent
from SimpleNamespace import SimpleNamespace

FMM_EXECUTABLE = '../tools/FMM/fmm08_motif_finder.pl'

def create_temp_dir() :
    '''
    Create a new temporary directory.
    '''
    tempdir = tempfile.gettempdir()
    while True :
        random_string = uuid.uuid4().hex
        dir_path = os.path.join(tempdir, random_string)
        if os.path.exists(dir_path) :
            continue
        os.makedirs(dir_path)
        break
    return dir_path

def write_fasta_file(sequences, filename) :
    '''
    Write FASTA file with sequences (given as a list of (name, sequence) tuples) into a file
    :param sequences : list of sequences to be written.
    :param filename : name of the file to which the sequences should be written.
    '''
    fout = open(filename, 'wb')
    if not isinstance(sequences[0], tuple) :
        sequences = [(i, seq) for i, seq in enumerate(sequences)]
    for name, seq in sequences :
        fout.write('>%s\n' % name)
        fout.write('%s\n' % seq)
    fout.close()

def read_fmm_motif(filename) :
    '''
    Read a motif from its GXW file.
    :param filename : name of the file to be read.
    '''
    result = SimpleNamespace()
    tree = ElementTree.parse(filename)
    root = tree.getroot()
    children = root.getchildren()
    root = children[0]
    alphabet = root.attrib['Alphabet']
    double_strand = root.attrib['DoubleStrandBinding'] == 'true'
    partition_function_Z = float(root.attrib['LogPartitionFuncZ'])
    prior = float(root.attrib['WeightMatrixPriorProbability'])

    children = root.getchildren()
    features_node, background_node = children[0], children[1]
    features = []
    for feature_node in features_node.getchildren() :
        feature, positions = {}, []
        feature['weight'] = float(feature_node.attrib['Weight'])
        #feature['n_positions'] = int(feature_node.attrib['FeaturesNum'])
        feature['positions'] = positions
        positions_node = feature_node.getchildren()[0]
        feature_type = positions_node.attrib['Type']
        if feature_type != 'LettersAtPositions' :
            raise Exception('Cannot parse feature type: %s' % feature_type)
        #print 'Feature: %f' % feature['weight']
        for position_node in positions_node.getchildren() :
            letters_str = position_node.attrib['Letters']
            if len(letters_str) > 1 :
                raise Exception('Cannot parse letters: %s' % letters_str)
            letters = int(letters_str)
            position = int(position_node.attrib['Position'])
            letter = alphabet[letters]
            positions.append((position, letter))
            #print '%s at %d' % (letter, position)
        features.append(feature)

    result.alphabet = alphabet
    result.double_strand = double_strand
    result.partition_function_Z = partition_function_Z
    result.prior = prior
    result.features = features
    return result

def read_pssm_motif(filename) :
    '''
    Read a PSSM motif from its GXW file.
    :param filename : name of the file to be read.
    '''
    result = SimpleNamespace()
    tree = ElementTree.parse(filename)
    root = tree.getroot()
    children = root.getchildren()
    root = children[0]
    alphabet = root.attrib['Alphabet']
    double_strand = root.attrib['DoubleStrandBinding'] == 'true'
    prior = float(root.attrib['WeightMatrixPriorProbability'])
    zero_weight = float(root.attrib['ZeroWeight'])

    children = root.getchildren()
    positions = []
    for position_node in root.getchildren() :
        weights = position_node.attrib['Weights']
        weights = [float(weight) for weight in weights.split(';')]
        positions.append(weights)

    result.alphabet = alphabet
    result.double_strand = double_strand
    result.prior = prior
    result.positions = positions
    result.zero_weight = zero_weight
    return result

def read_pssm_logo(filename) :
    '''
    Read a PSSM logo from a given filename.
    :param filename : name of the file from which the logo should be read.
    '''
    import matplotlib.image as mpimg
    return mpimg.imread(filename)

def read_fmm_results(results_path, n_motifs = 5) :
    '''
    Read FMM motif results from a given directory.
    :param results_path : path containing results.
    :param n_motifs : number of motifs that should be read.
    '''
    pvalues = np.zeros((n_motifs, ))

    filename = os.path.join(results_path, 'Out_pvalues.tab')
    if os.path.exists(filename) :
        fin = open(filename, 'rb')
        lines = fin.readlines()
        fin.close()
        n_lines = len(lines)
        if n_lines < n_motifs :
            n_motifs = n_lines
        for line in lines :
            args = line.strip().split('\t')
            id = int(args[0])
            pvalue = float(args[1])
            pvalues[id - 1] = pvalue
    
    else :
        n_motifs = 0

    results = []
    for i in range(n_motifs) :
        filename = os.path.join(results_path, 'Out_FMM%d.gxw' % (i + 1))
        result = read_fmm_motif(filename)
        result.pvalue = pvalues[i]
        results.append(result)
    return results

def read_pssm_results(results_path, n_motifs = 5) :
    '''
    Read PSSM motif results from a given directory.
    :param results_path : path containing results.
    :param n_motifs : number of motifs that should be read.
    '''
    pvalues = np.zeros((n_motifs, ))

    filename = os.path.join(results_path, 'Out_pvalues.tab')
    if os.path.exists(filename) :
        fin = open(filename, 'rb')
        lines = fin.readlines()
        fin.close()
        n_lines = len(lines)
        if n_lines < n_motifs :
            n_motifs = n_lines
        for line in lines :
            args = line.strip().split('\t')
            id = int(args[0])
            pvalue = float(args[1])
            pvalues[id - 1] = pvalue
    
    else :
        n_motifs = 0

    results = []
    for i in range(n_motifs) :
        filename_motif = os.path.join(results_path, 'Out_PSSM%d.gxw' % (i + 1))
        filename_logo = os.path.join(results_path, 'Out_PSSM%d.png' % (i + 1))
        result = read_pssm_motif(filename_motif)
        result.logo = read_pssm_logo(filename_logo)
        result.pvalue = pvalues[i]
        results.append(result)
    return results

def score_sequence_fmm(sequence, motif) :
    '''
    Score a given sequence with a motif.
    :param sequence : sequence that should be scored.
    :param motif : motif that should be scored.
    '''
    sequence_length = len(sequence)
    Z = motif.partition_function_Z
    scores = np.zeros((sequence_length, ))
    for offset in range(sequence_length) :
        sum = 0.0
        for feature in motif.features :
            weight = feature['weight']
            all_conditions = True
            for position, letter in feature['positions'] :
                if offset + position >= sequence_length or sequence[offset + position] != letter:
                    all_conditions = False
                    break
            if all_conditions :
                sum += weight
        prob = math.exp(sum) / Z
        #prob = math.exp(sum - Z)
        #prob = math.exp(sum / Z) * motif.prior
        #print 'Position: %d, score %.5g' % (offset, prob)
        scores[offset] = prob
    #scores = -1 / np.log2(scores)
    #scores /= np.max(scores)
    #for offset in range(sequence_length) :
    #    prob = scores[offset]
    #    print 'Position: %d, score %.5f' % (offset, prob)
    return scores

def read_hits_file(filename, index, motif_index) :
    '''
    Reads the FMM hits from a hits file. Only hits for a particular sequence and motif are read.
    Used for debugging purposes.
    :param filename : name of the hits file to be read.
    :param index : index of the sequence for which the data should be read.
    :param motif_index : index of the motif (1 - based) for which the data should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    indices, scores = [], []
    lines = lines[1:]
    for line in lines :
        args = line.strip().split('\t')
        name = args[0]
        position = int(args[1])
        motif_name = args[3]
        score = float(args[4])
        name = name[3:]
        args = motif_name.split()
        motif_name = args[-1]
        if int(name) == index and int(motif_name) == motif_index :
            indices.append(position)
            scores.append(score)
    return np.array(indices), np.array(scores)

def find_fmm_motifs(sequences, background_sequences = None, min_seed = 3, max_seed = 10, num_motifs = 5, seed = 31337) :
    '''
    Find FMM motifs in a list of sequences.
    :param sequences : sequences given as a list of (index, sequence) tuples.
    :param background_sequences : sequences given as a list (index, sequences) tuples. If none, random background sequences are generated by FMM.
    :param min_seed : minimum seed length.
    :param max_seed : maximum seed length.
    :param num_motifs : number of motifs to be considered.
    :param seed : seed to be used.
    '''
    current_dir = os.getcwd()
    try :
        fmm_executable = os.path.abspath(FMM_EXECUTABLE)
        output_dir = create_temp_dir()
        os.chdir(output_dir)
        sequences_filename = os.path.join(output_dir, 'input.fa')
        write_fasta_file(sequences, sequences_filename)
        if background_sequences is None :
            command = '%s -learn_fmm true -learn_pssm false -use_rev_comp false -min_seed %d -max_seed %d -rand_n -rand_seed %d -num_motifs %d -output_motif_hits_files true -gen_motif_logos false -output_dir %s -p %s' % (fmm_executable, min_seed, max_seed, seed, num_motifs, output_dir, sequences_filename)
        else :
            negative_sequences_filename = os.path.join(output_dir, 'negative_input.fa')
            write_fasta_file(background_sequences, negative_sequences_filename)
            command = '%s -learn_fmm true -learn_pssm false -use_rev_comp false -min_seed %d -max_seed %d -rand_seed %d -num_motifs %d -output_motif_hits_files true -gen_motif_logos false -output_dir %s -p %s -n %s' % (fmm_executable, min_seed, max_seed, seed, num_motifs, output_dir, sequences_filename, negative_sequences_filename)
        #print command
        args = shlex.split(command)
        code = subprocess.call(args, shell = False)
        #code = subprocess.call(command, shell = True)
        results = read_fmm_results(output_dir)
    finally :
        os.chdir(current_dir)
    shutil.rmtree(output_dir)
    return results

def find_pssm_motifs(sequences, background_sequences = None, min_seed = 5, max_seed = 10, num_motifs = 5, seed = 31337) :
    '''
    Find PSSM motifs in a list of sequences.
    :param sequences : sequences given as a list of (index, sequence) tuples.
    :param background_sequences : sequences given as a list (index, sequences) tuples. If none, random background sequences are generated by FMM.
    :param min_seed : minimum seed length.
    :param max_seed : maximum seed length.
    :param num_motifs : number of motifs to be considered.
    :param seed : seed to be used.
    '''
    current_dir = os.getcwd()
    try :
        fmm_executable = os.path.abspath(FMM_EXECUTABLE)
        output_dir = create_temp_dir()
        print 'Directory: %s' % output_dir
        os.chdir(output_dir)
        sequences_filename = os.path.join(output_dir, 'input.fa')
        write_fasta_file(sequences, sequences_filename)
        if background_sequences is None :
            command = '%s -learn_fmm false -learn_pssm true -use_rev_comp false -min_seed %d -max_seed %d -rand_n -rand_seed %d -num_motifs %d -output_motif_hits_files true -gen_motif_logos true -output_dir %s -p %s' % (fmm_executable, min_seed, max_seed, seed, num_motifs, output_dir, sequences_filename)
        else :
            negative_sequences_filename = os.path.join(output_dir, 'negative_input.fa')
            write_fasta_file(background_sequences, negative_sequences_filename)
            command = '%s -learn_fmm false -learn_pssm true -use_rev_comp false -min_seed %d -max_seed %d -rand_seed %d -num_motifs %d -output_motif_hits_files true -gen_motif_logos true -output_dir %s -p %s -n %s' % (fmm_executable, min_seed, max_seed, seed, num_motifs, output_dir, sequences_filename, negative_sequences_filename)
        args = shlex.split(command)
        code = subprocess.call(args, shell = False)
        results = read_pssm_results(output_dir)
    finally :
        os.chdir(current_dir)
    shutil.rmtree(output_dir)
    return results

def look_for_fmm_motifs(dataset_name, indices = None, min_motif_length = 5, max_motif_length = 10, n_motifs = 5) :
    '''
    Look for FMM motifs in sequences belonging to a particular dataset.
    :param dataset_name : name of the dataset to be analysed.
    :param indices : indices of items that should participate in the analysis.
    :param min_motif_length : minimum length of the motif that should be searched for.
    :param max_motif_length : maximum length of the motif that should be searched for.
    :param n_motifs : number of motifs to look for.
    '''
    dataset = persistent.datasets[dataset_name]
    if indices is not None :
        indices = list(indices)
        sequences_tuples = [(dataset[k].index, dataset[k].sequence) for k in indices]
    else :
        sequences_tuples = [(seq.index, seq.sequence) for seq in dataset]
    result = find_fmm_motifs(sequences_tuples, min_seed = min_motif_length, max_seed = max_motif_length, num_motifs = n_motifs)
    return result

def _parallel_fmm_motifs_in_windows(param) :
    '''
    A routine for running FMM motif discovery. Used in parallel computations.
    :param param : routine parameters (dataset_name, indices, window, min_motif_length, max_motif_length)
    '''
    import lib.persistent
    dataset_name, indices, window, min_motif_length, max_motif_length, n_motifs = param
    window_offset, window_stop = window
    dataset = lib.persistent.datasets[dataset_name]
    if indices is not None :
        indices = list(indices)
        sequences_tuples = [(dataset[k].index, dataset[k].sequence[window_offset:window_stop]) for k in indices]
    else :
        sequences_tuples = [(seq.index, seq.sequence[window_offset:window_stop]) for seq in dataset]
    result = find_fmm_motifs(sequences_tuples, min_seed = min_motif_length, max_seed = max_motif_length, num_motifs = n_motifs)
    result = [(window, res) for res in result]
    return result

def look_for_fmm_motifs_in_windows(dataset_name, indices = None, min_motif_length = 5, max_motif_length = 10, n_motifs = 5, window_size = 20, window_step = 10, client_url = None) :
    '''
    Look for FMM motifs in windows of sequences belonging to a particular dataset.
    :param dataset_name : name of the dataset to be analysed.
    :param indices : indices of items that should participate in the analysis.
    :param min_motif_length : minimum length of the motif that should be searched for.
    :param max_motif_length : maximum length of the motif that should be searched for.
    :param n_motifs : number of motifs to look for.
    :param window_size : size of the moving window that should be analysed.
    :param window_step : step with which the moving window should be shifted.
    :param client_url : URL of the IPython parallel client that should be used for parallel computing.
    '''
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
    
    dataset = persistent.datasets[dataset_name]
    sequence_length = len(dataset[0].sequence)
    results = []
    if view is None :
        window_offset = 0
        while window_offset < sequence_length :
            window_stop = min(window_offset + window_size, sequence_length)
            window = (window_offset, window_stop)
            print window
            if indices is not None :
                indices = list(indices)
                sequences_tuples = [(dataset[k].index, dataset[k].sequence[window_offset:window_stop]) for k in indices]
            else :
                sequences_tuples = [(seq.index, seq.sequence[window_offset:window_stop]) for seq in dataset]
            result = find_fmm_motifs(sequences_tuples, min_seed = min_motif_length, max_seed = max_motif_length, num_motifs = n_motifs)
            for motif in result :
                results.append((window, motif))
            window_offset += window_step
    else :
        tasks = []
        window_offset = 0
        while window_offset < sequence_length :
            window_stop = min(window_offset + window_size, sequence_length)
            window = (window_offset, window_stop)
            tasks.append((dataset_name, indices, window, min_motif_length, max_motif_length, n_motifs))
            window_offset += window_step
        map_results = view.map(_parallel_fmm_motifs_in_windows, tasks, block = True, ordered = True)
        map_results = [res for res_group in map_results for res in res_group]
        for window, motif in map_results :
            results.append((window, motif))
        del view
        client.close()
        del client
    return results

