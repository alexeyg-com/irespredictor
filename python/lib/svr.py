'''
A module implementing some basic random forest regression routines.

09-04-2017
Alexey Gritsenko
'''

import sklearn.metrics as metrics
from sklearn.svm import LinearSVR
import gbrf
import numpy as np
import scipy.stats
import time
import math
import sys

import tools
import crossvalidation

def get_scaling_factors(X, y):
    '''
    Obtain scaling factors c1 and c2 for rescaling features and the response variable to range [-1; +1].
    :param np.ndarray X: regression feature matrix.
    :param np.ndarray y: regression response variable.
    :returns: X and y coefficients
    :rtype: (np.ndarray, np.ndarray, float, float)
    '''
    X_min, X_max = X.min(axis=0).todense(), X.max(axis=0).todense()
    #print 'min', X_min
    #print 'max', X_max
    y_min, y_max = np.min(y), np.max(y)
    # Scaling for X
    denom = (X_max - X_min).astype(np.float)
    denom[denom == 0] = 1.0
    X_c2, X_c1 = -(X_max + X_min) / denom, 2.0 / denom
    # Scaling for y
    denom = float(y_min - y_max)
    if denom == 0:
        denom = 1.0
    y_c2, y_c1 = -(y_max + y_min) / denom, 2.0 / denom
    #print 'c1', X_c1
    #print 'c2', X_c2
    
    test_X = rescale_X(X, X_c1, X_c2)
    #print 'test min', np.min(test_X, axis=0)
    #print 'test max', np.max(test_X, axis=0)
    
    return X_c1, X_c2, y_c1, y_c2

def rescale_X(X, c1, c2):
    '''
    Rescale feature matrix.
    :param np.ndarray X: regression feature matrix.
    :param np.ndarray c1: rescaling coefficient C1.
    :param np.ndarray c2: rescaling coefficient C2.
    :returns: rescaled feature matrix.
    :rtype: np.ndarray
    '''
    n_samples, n_features = X.shape
    #print X.shape
    #print np.atleast_2d(c1).shape
    #print np.repeat(np.atleast_2d(c1), n_samples, axis=0).shape
    #print X.multiply(np.repeat(np.atleast_2d(c1), n_samples, axis=0)).shape
    return X.multiply(np.repeat(np.atleast_2d(c1), n_samples, axis=0)) + np.repeat(np.atleast_2d(c2), n_samples, axis=0)

def rescale_y(y, c1, c2):
    '''
    Rescale response variable.
    :param np.ndarray y: regression response varaiable.
    :param np.ndarray c1: rescaling coefficient C1.
    :param np.ndarray c2: rescaling coefficient C2.
    :returns: rescaled response variable.
    :rtype: np.ndarray
    '''
    return c1 * y + c2

def scale_up(y, c1, c2):
    '''
    Scale up the predicted response.
    :param np.ndarray y: predicted response.
    :param np.ndarray c1: rescaling coefficient C1.
    :param np.ndarray c2: rescaling coefficient C2.
    :returns: rescaled predicted response.
    :rtype: np.ndarray
    '''
    return 1.0 / c1 * y - c2 / c1

def train_svr_cv(X, y, C, epsilon, tol=1e-3, max_iter=1000, normalize=True, scoring = gbrf.REGRESSION_METRICS, cv_folds = 10) :
    '''
    Train a linear SVR.
    :param X : feature matrix.
    :param y : response variable.
    :param float C : penalty term of the SVR.
    :param float epsilon : epsilon for epsilon SVR.
    :param float tol : optimisation tolerance of the regressor.
    :param int max_iter : maximum number of iterations (-1 = inifinity).
    :param bool normalize: should the features and the response variable be normalised?
    :param scoring : scoring function to be used.
    :param cv_folds : an iterator (or list) of CV index pairs.
    '''
    if isinstance(cv_folds, int) :
        n_folds = cv_folds
        cv_folds = None
    else :
        n_folds = len(cv_folds)
    
    n_features, n_samples = X.shape[1], len(y)
    train_score, test_score = {}, {}
    overall_test_score = {}
    for score in scoring :
        test_score[score] = np.zeros((n_folds, ), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, ), dtype = np.float16)
    times = np.zeros((n_folds, ), dtype = np.float16)
    X_c1, X_c2 = np.zeros((n_folds, n_features), dtype=np.float), np.zeros((n_folds, n_features), dtype=np.float)
    y_c1, y_c2 = np.zeros((n_folds, ), dtype=np.float), np.zeros((n_folds, ), dtype=np.float)
    if cv_folds is None :
        folds = crossvalidation.stratified_KFold(y, n_folds = n_folds)
    else :
        folds = cv_folds
    fold = 0 
    all_true_y, all_predicted_y = [], []
    for train_index, test_index in folds :
        train_X, test_X = X[train_index, :], X[test_index, :]
        train_y, test_y = y[train_index], y[test_index]
        if normalize:
            X_c1[fold, :], X_c2[fold, :], y_c1[fold], y_c2[fold] = get_scaling_factors(train_X, train_y)
        all_true_y.extend(test_y)
        regressor = LinearSVR(loss='epsilon_insensitive', epsilon=epsilon, C=C, tol=tol, max_iter=max_iter, fit_intercept=True)
        start_time = time.time()
        actual_train_y = train_y
        n_train_samples = len(train_y)        
        if normalize:
            regressor.fit(rescale_X(train_X, X_c1[fold, :], X_c2[fold, :]),
                          rescale_y(actual_train_y, y_c1[fold], y_c2[fold]))
        else:
            regressor.fit(train_X, train_y)
        elapsed_time = time.time() - start_time
        times[fold] = elapsed_time
        if normalize:
            test_y_predicted = scale_up(regressor.predict(rescale_X(test_X, X_c1[fold, :], X_c2[fold, :])),
                                        y_c1[fold],
                                        y_c2[fold])
        else:
            test_y_predicted = regressor.predict(test_X)
        all_predicted_y.extend(test_y_predicted)
        for score in scoring:
            test_score[score][fold] = gbrf.calculate_regression_score(test_y, test_y_predicted, score)
        if normalize:
            train_y_predicted = scale_up(regressor.predict(rescale_X(train_X, X_c1[fold, :], X_c2[fold, :])),
                                         y_c1[fold],
                                         y_c2[fold])
        else:
            train_y_predicted = regressor.predict(train_X)
        for score in scoring:
            train_score[score][fold] = gbrf.calculate_regression_score(train_y, train_y_predicted, score)
        fold += 1
        del regressor
        del test_X
        del test_y
        del train_X

    for score in scoring:
        overall_test_score[score] = gbrf.calculate_regression_score(all_true_y, all_predicted_y, score)
    del all_predicted_y
    del all_true_y
    return train_score, test_score, overall_test_score, times

def train_svr_cv_fold(X, y, train_index, test_index, C, epsilon, tol=1e-3, max_iter=1000, normalize=True, scoring = gbrf.REGRESSION_METRICS, return_regressor = False) :
    '''
    Train a linear SVR.
    :param X : feature matrix.
    :param y : response variable.
    :param train_index : index of items inside the training set.
    :param test_index : index of items inside the esting set.
    :param float C : penalty term of the SVR.
    :param float epsilon : epsilon for epsilon SVR.
    :param float tol : optimisation tolerance of the regressor.
    :param int max_iter : maximum number of iterations (-1 = inifinity).
    :param bool normalize: should the features and the response variable be normalised?
    :param scoring : scoring function to be used.
    :param return_regressor : whether the constructed regressor should be returned too.
    '''
    n_features, n_samples = X.shape[1], len(y)
    train_score, test_score = {}, {}
    for score in scoring :
        test_score[score] = np.nan
        train_score[score] = np.nan
    train_X, test_X = X[train_index, :], X[test_index, :]
    train_y, test_y = y[train_index], y[test_index]
    if normalize:
        X_c1, X_c2, y_c1, y_c2 = get_scaling_factors(train_X, train_y)
    regressor = LinearSVR(loss='epsilon_insensitive', epsilon=epsilon, C=C, tol=tol, max_iter=max_iter, fit_intercept=True)
    start_time = time.time()
    actual_train_y = train_y
    n_train_samples = len(train_y)
    if normalize:
        regressor.fit(rescale_X(train_X, X_c1, X_c2),
                      rescale_y(actual_train_y, y_c1, y_c2))
    else:
        regressor.fit(train_X, actual_train_y)
    elapsed_time = time.time() - start_time
    true_y = test_y
    if normalize:
        predicted_y = scale_up(regressor.predict(rescale_X(test_X, X_c1, X_c2)), y_c1, y_c2)
    else:
        predicted_y = regressor.predict(test_X)
    for score in scoring :
        test_score[score] = gbrf.calculate_regression_score(test_y, predicted_y, score)
      
    if normalize:
        train_y_predicted = scale_up(regressor.predict(rescale_X(train_X, X_c1, X_c2)), y_c1, y_c2)
    else:
        train_y_predicted = regressor.predict(train_X)
    for score in scoring :
        train_score[score] = gbrf.calculate_regression_score(train_y, train_y_predicted, score)
    del test_X
    del test_y
    del train_X
    del train_y
    if not return_regressor :
        del regressor
        if normalize:
            del X_c1, X_c2, y_c1, y_c2
        return train_score, test_score, true_y, predicted_y, elapsed_time
    else :
        return regressor, (X_c1, X_c2, y_c1, y_c2), train_score, test_score, true_y, predicted_y, elapsed_time

def train_svr(X, y, C, epsilon, tol=1e-3, max_iter=1000, normalize=True, scoring = gbrf.REGRESSION_METRICS) :
    '''
    Train a linear SVR. 
    :param X : feature matrix.
    :param y : response variable.
    :param float C : penalty term of the SVR.
    :param float epsilon : epsilon for epsilon SVR.
    :param float tol : optimisation tolerance of the regressor.
    :param int max_iter : maximum number of iterations (-1 = inifinity).
    :param bool normalize: should the features and the response variable be normalised?
    :param scoring : scoring function to be used.
    '''
    n_features, n_samples = X.shape[1], len(y)
    train_score = {}
    overall_train_score = {}
    for score in scoring :
        train_score[score] = np.nan
    if normalize:
        X_c1, X_c2, y_c1, y_c2 = get_scaling_factors(X, y)
    regressor = LinearSVR(loss='epsilon_insensitive', epsilon=epsilon, C=C, tol=tol, max_iter=max_iter, fit_intercept=True)
    start_time = time.time()
    actual_y = y
    if normalize:
        regressor.fit(rescale_X(X, X_c1, X_c2),
                      rescale_y(actual_y, y_c1, y_c2))
    else:
        regressor.fit(X, actual_y)
    elapsed_time = time.time() - start_time
    predicted_y = []
    true_y = y
    
    #print 'Real y'
    #print np.min(actual_y)
    #print np.max(actual_y)
    
    #print 'Rescaled y'
    #print np.min(rescale_y(actual_y, y_c1, y_c2))
    #print np.max(rescale_y(actual_y, y_c1, y_c2))

    #print 'Upscaled y'
    #print np.min(scale_up(rescale_y(actual_y, y_c1, y_c2),
    #                      y_c1,
    #                      y_c2))
    #print np.max(scale_up(rescale_y(actual_y, y_c1, y_c2),
    #                      y_c1,
    #                      y_c2))
    
    if normalize:
        train_y_predicted = scale_up(regressor.predict(rescale_X(X, X_c1, X_c2)), y_c1, y_c2)
    else:
        train_y_predicted = regressor.predict(X)
    predicted_y.extend(train_y_predicted)
    for score in scoring :
        train_score[score] = gbrf.calculate_regression_score(true_y, train_y_predicted, score)
    del X
    del y
    for score in scoring :
        overall_train_score[score] = gbrf.calculate_regression_score(true_y, predicted_y, score)
    del true_y
    del predicted_y
    return train_score, overall_train_score, elapsed_time, regressor
