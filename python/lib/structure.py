'''
A module implementing some routines to work with RNA structure.

10-02-2015
Alexey Gritsenko
'''

import RNA
import copy
import numpy as np
import scipy.stats
import sys
from ipyparallel import Client

import writer
import reader
import memory
import gc

def pf_fold(sequence) :
    '''
    Partition function fold of the sequence.
    :param sequence : RNA sequence to be folded using partition function.
    '''
    import RNA
    import numpy as np
    length = len(sequence)
    struct, energy = RNA.pf_fold(sequence)
    bppm = RNA.export_bppm()
    index = RNA.get_iindx(length)
    index_np = np.zeros((length + 1, ), dtype = np.int)
    for i in range(1, length + 1) :
        index_np[i] = RNA.intP_getitem(index, i)
    bppm_np = np.zeros((length, length), dtype = np.float)
    for i in range(1, length + 1) :
        for j in range(i + 1, length + 1) :
            bppm_np[i - 1, j - 1] = RNA.doubleP_getitem(bppm, index_np[i] - j)
            bppm_np[j - 1, i - 1] = bppm_np[i - 1, j - 1]
    return struct, energy, bppm_np

def _calculate_bppm_for_kmer_length(param) :
    '''
    Calculate bppm for all offsets and a give k-mer length.
    :param param : a tuple of index, sequence and kmer length.
    '''
    import numpy as np
    import lib.structure as structure
    index, sequence, kmer_length = param
    _, _, bppm = structure.pf_fold(sequence)
    length = len(sequence)
    accessibility = -np.ones((length, ), dtype = np.float)
    for offset in range(length - kmer_length + 1) :
        prob =  np.sum(bppm[offset : offset + kmer_length, :])
        prob =  (kmer_length - prob) / float(kmer_length)
        accessibility[offset] = prob
    return accessibility

def calculate_bppm_accessibility_for_all_possible_kmers(dataset, max_kmer_length = 5, client_url = None) :
    '''
    Calculated bppm-based accessibility for all possible k-mers up to a certain length.
    :param dataset : dataset, i.e. a list of sequences, for which accessibility should be calculated.
    :param max_kmer_length : maximum length of kmers that should be considered.
    :param client_url : URL of the IPython parallel client to be used.
    '''
    import structure
    result = {}
    if client_url is None :
        n_total = len(dataset)
        cnt = 0
        for seq in dataset :
            cnt += 1
            print '[i] Processing %d/%d' % (cnt, n_total)
            sequence = seq.sequence
            length = len(sequence)
            accessibility = -np.ones((length, max_kmer_length), dtype = np.float)
            _, _, bppm = pf_fold(sequence)
            for kmer_length in range(1, max_kmer_length + 1) :
                for offset in range(length - kmer_length + 1) :
                    prob = np.sum(bppm[offset : offset + kmer_length, :])
                    prob = (kmer_length - prob) / float(kmer_length)
                    accessibility[offset, kmer_length - 1] = prob
            result[seq.index] = accessibility
    else :
        tasks = []
        for seq in dataset :
            index, sequence = seq.index, seq.sequence
            for kmer_length in range(1, max_kmer_length + 1) :
                task = (index, sequence, kmer_length)
                tasks.append(task)
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100
        async_results = view.map(_calculate_bppm_for_kmer_length, tasks, ordred = True, block = False)
        for task, res in zip(tasks, async_results) :
            index, sequence, kmer_length = task
            length = len(sequence)
            if index not in result :
                accessibility = -np.ones((length, max_kmer_length), dtype = np.float)
                result[index] = accessibility
            result[index][:, kmer_length - 1] = res
        del view
        client.close()
        del client
    return result

def create_bppm_dataset(source, window_size = 20, window_step = 10) :
    '''
    Create base pairing datasets.
    :param source : a dictionary with computer base pairing.
    :param window_size : size of the window to be used.
    :param window_step : step of the window to be used.
    '''
    import time
    result = {}
    working_with_seq = False
    if isinstance(source, list) :
        working_with_seq = True
    n_items = len(source)
    cnt, start_time = 0, time.time()
    memory_monitor = memory.MemoryMonitor()
    for item in source :
        gc.collect()
        cnt += 1
        if working_with_seq :
            seq = item
            index, sequence = seq.index, seq.sequence
            mem = memory_monitor.peak_usage / 1024.0
            print '[i] Folding: %d (%d / %d) [usage: %.2fGb]' % (index, cnt, n_items, mem)
            sys.stdout.flush()
            structure, energy, bppm = pf_fold(sequence)
            if cnt % 100 == 0 :
                duration = time.time() - start_time
                speed = cnt / duration * 60
                count_left = n_items - cnt
                estimated_time_left = count_left / speed
                print '   [!] Avg. speed: %.3f seq/min => ETA: %.2f mins' % (speed, estimated_time_left)
                sys.stdout.flush()
        else :
            index = item
            structure, energy, bppm = pairing[index]
        length = len(structure)
        window_probs = {}
        start = 0
        while start < length :
            stop = min(start + window_size, length)
            window_prob = np.sum(bppm[start : stop, :])
            max_value = float(stop - start)
            window_prob = (max_value - window_prob) / max_value
            window_probs[(start, stop)] = window_prob
            start += window_step
        window_pair_probs = {}
        offset = 0
        while offset < length :
            offset_stop = min(offset + window_size, length)
            offset2 = offset
            while offset2 < length :
                offset2_stop = min(offset2 + window_size, length)
                sub_matrix = bppm[offset : offset_stop, offset2 : offset2_stop]
                window_pair_prob = np.sum(sub_matrix)
                window_pair_prob = window_pair_prob / float(np.prod(sub_matrix.shape))
                window_pair_probs[(offset, offset_stop, offset2, offset2_stop)] = window_pair_prob
                offset2 += window_step
            offset += window_step
        result[index] = (energy, window_probs, window_pair_probs)
    memory_monitor.finalize()
    return result

def look_for_bppm_features(dataset_name, structures = None, indices = None, window_step = 10, window_size = 20, cutoff = 0.05) :
    '''
    Looks for base pairing probability matrix features.
    :param dataset_name : name of the dataset for which correlations should be computed.
    :param structures : dictionary of structures to be used for k-mer generation.
    :param indices : indices of the sequences to be used for k-mer computation.
    :param window_step : step of the moving window.
    :param window_size : size of the moving window.
    :param cutoff : the p-value cutoff to be used for kmer selection.
    '''
    import persistent
    if structures is None :
        structures = persistent.get_bppm()
    seqs = persistent.datasets[dataset_name]
    ires_activity = np.array([seq.ires_activity for seq in seqs])
    if indices is None :
        n_seqs = len(seqs)
        indices = np.array(range(n_seqs))
    ires_activity = ires_activity[indices]
    seq_indices = [seqs[i].index for i in indices]
    length = len(seqs[0].sequence)
    results = []
    start = 0
    while start < length :
        stop = min(start + window_size, length)
        description = (start, stop)
        feature = [structures[index][1][description] for index in seq_indices]
        coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
        if pvalue < cutoff :
            results.append(((start, stop), coef_r, pvalue, 1.0))
        start += window_step
    return results

def look_for_bppm_pair_features(dataset_name, structures = None, indices = None, window_step = 10, window_size = 20, cutoff = 0.05) :
    '''
    Looks for base pairing probability matrix pair features.
    :param dataset_name : name of the dataset for which correlations should be computed.
    :param structures : dictionary of structures to be used for k-mer generation.
    :param indices : indices of the sequences to be used for k-mer computation.
    :param window_step : step of the moving window.
    :param window_size : size of the moving window.
    :param cutoff : the p-value cutoff to be used for kmer selection.
    '''
    import persistent
    if structures is None :
        structures = persistent.get_bppm()
    seqs = persistent.datasets[dataset_name]
    ires_activity = np.array([seq.ires_activity for seq in seqs])
    if indices is None :
        n_seqs = len(seqs)
        indices = np.array(range(n_seqs))
    ires_activity = ires_activity[indices]
    seq_indices = [seqs[i].index for i in indices]
    length = len(seqs[0].sequence)
    results = []
    start = 0
    while start < length :
        stop = min(start + window_size, length)
        start2 = start
        while start2 < length :
            stop2 = min(start2 + window_size, length)
            description = (start, stop, start2, stop2)
            index = seq_indices[0]
            if isinstance(structures[index][2][description], tuple) :
                feature1 = np.array([structures[index][2][description][0] for index in seq_indices])
                feature2 = np.array([structures[index][2][description][0] for index in seq_indices])
                feature = feature1 + feature2
            else :
                feature = np.array([structures[index][2][description] for index in seq_indices])
            coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
            if pvalue < cutoff :
                results.append(((start, stop, start2, stop2), coef_r, pvalue, 1.0))
            start2 += window_step
        start += window_step
    return results

