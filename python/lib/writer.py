'''
Implements routines for data output.

Alexey Gritsenko
31-10-2013
'''

import pickle
import marshal

def write_sequences(sequences, filename) :
    ''' 
    Output sequences in a file (one per line).
    :param filename : name of the file to output sequences into.
    ''' 
    fout = open(filename, 'wb')
    fout.writelines([seq.sequence + '\n' for seq in sequences])
    fout.close()

def write_sequences_fasta(sequences, filename) :
    ''' 
    Output sequences in a FASTA file (one per line).
    :param filename : name of the file to output sequences into.
    ''' 
    fout = open(filename, 'wb')
    for seq in sequences :
        fout.write('>Seq%d\n' % seq.index)
        fout.write('%s\n' % seq.sequence)
    fout.close()

def write_fasta(sequences, filename) :
    ''' 
    Output sequences in a FASTA file (one per line).
    :param filename : name of the file to output sequences into.
    ''' 
    fout = open(filename, 'wb')
    for name, seq in sequences :
        fout.write('>%s\n' % name)
        fout.write('%s\n' % seq)
    fout.close()

def write_marshal(data, filename) :
    ''' 
    Marshals a variable into file. Should be faster than pickling.
    :param data: the variable to be marshaled.
    :param filename: name of the file to save the marshal in. 
    ''' 
    fout = open(filename, 'wb')
    marshal.dump(data, fout)
    fout.close()

def write_pickle(data, filename) :
    ''' 
    Pickles a variable into file.
    :param data: the variable to be pickled.
    :param filename: name of the file to save the pickle in. 
    ''' 
    fout = open(filename, 'wb')
    pickle.dump(data, fout, protocol = pickle.HIGHEST_PROTOCOL)
    fout.close()
