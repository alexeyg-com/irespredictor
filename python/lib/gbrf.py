'''
A module implementing some basic random forest regression routines.

09-02-2015
Alexey Gritsenko
'''

import sklearn.metrics as metrics
from sklearn.ensemble import GradientBoostingRegressor
import numpy as np
import scipy.stats
import time
import math

import tools
import crossvalidation

ESTIMATOR_RECORDING_FREQUENCY = 100
REGRESSION_METRICS = ['r2', 'spearman', 'pearson', 'r2-positive', 'spearman-positive', 'pearson-positive', 'auc-roc', 'auc-pr']

def calculate_regression_score(y_true, y_pred, score) :
    '''
    Calculate regression score.
    :param y_true : try class labels.
    :param y_pred : predicted class.
    :param score : score which should be calculated.
    '''
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    if score == 'r2' :
        return metrics.r2_score(y_true, y_pred)
    elif score == 'spearman' :
        return scipy.stats.spearmanr(y_true, y_pred)[0]
    elif score == 'pearson' :
        return scipy.stats.pearsonr(y_true, y_pred)[0]
    elif score == 'auc-roc' :
        try :
            n_positive = np.sum(y_true > tools.MINIMAL_IRES_ACTIVITY, dtype = np.int)
            if n_positive == 0 :
                minimal_activity = np.log2(tools.MINIMAL_IRES_ACTIVITY)
            else :
                minimal_activity = tools.MINIMAL_IRES_ACTIVITY
            y_label = np.ones(y_true.shape)
            y_label[y_true == minimal_activity] = 0
            return metrics.roc_auc_score(y_label, y_pred)
        except :
            return float('NaN')
    elif score == 'auc-pr' :
        try :
            n_positive = np.sum(y_true > tools.MINIMAL_IRES_ACTIVITY, dtype = np.int)
            if n_positive == 0 :
                minimal_activity = np.log2(tools.MINIMAL_IRES_ACTIVITY)
            else :
                minimal_activity = tools.MINIMAL_IRES_ACTIVITY
            y_label = np.ones(y_true.shape)
            y_label[y_true == minimal_activity] = 0
            return metrics.average_precision_score(y_label, y_pred)
        except :
            return float('NaN')
    elif score == 'r2-positive' :
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        indices = y_true > tools.MINIMAL_IRES_ACTIVITY
        if np.sum(indices, dtype = np.int) == 0 :
            indices = y_true > math.log(tools.MINIMAL_IRES_ACTIVITY, 2)
        try :
            return metrics.r2_score(y_true[indices], y_pred[indices])
        except :
            return float('nan')
    elif score == 'spearman-positive' :
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        indices = y_true > tools.MINIMAL_IRES_ACTIVITY
        if np.sum(indices, dtype = np.int) == 0 :
            indices = y_true > math.log(tools.MINIMAL_IRES_ACTIVITY, 2)
        return scipy.stats.spearmanr(y_true[indices], y_pred[indices])[0]
    elif score == 'pearson-positive' :
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        indices = y_true > tools.MINIMAL_IRES_ACTIVITY
        if np.sum(indices, dtype = np.int) == 0 :
            indices = y_true > math.log(tools.MINIMAL_IRES_ACTIVITY, 2)
        return scipy.stats.pearsonr(y_true[indices], y_pred[indices])[0]
    else :
        raise Exception('Unknown scoring function requested (%s).' % score)

def train_gbrf_cv(X, y, max_depth = 5, learn_intercept = False, n_estimators = 500, min_samples_leaf = 1, learning_rate = 0.1, subsample = 0.7, max_features = 'log2', huber_alpha = None, scoring = REGRESSION_METRICS, cv_folds = 10) :
    '''
    Train Gradient Boosted Random Forest Regression.
    :param X : feature matrix.
    :param y : response variable.
    :param max_deph : maximum depth of a regression tree.
    :param learn_intercept : whether we should also learn the intercept.
    :param n_estimators : number of trees to train.
    :param min_samples_leaf : minimum number of leaf samples.
    :param learning_rate : parameter defining the learning rate of the GBRT.
    :param subsample : fraction of data to sample for each tree.
    :param max_features : setting defining how many features should be considered while looking for the best split. Check sklearn documentation.
    :param scoring : scoring function to be used.
    :param cv_folds : an iterator (or list) of CV index pairs.
    '''
    if isinstance(cv_folds, int) :
        n_folds = cv_folds
        cv_folds = None
    else :
        n_folds = len(cv_folds)
    
    n_features, n_samples = X.shape[1], len(y)
    train_score, test_score = {}, {}
    overall_test_score = {}
    n_recorded_estimators = int(n_estimators / ESTIMATOR_RECORDING_FREQUENCY)
    if n_estimators % ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    for score in scoring :
        dtype = get_classification_score_array_dtype(score)
        test_score[score] = np.zeros((n_folds, n_recorded_estimators), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, n_recorded_estimators), dtype = np.float16)
    times = np.zeros((n_folds, ), dtype = np.float16)
    if cv_folds is None :
        folds = crossvalidation.stratified_KFold(y, n_folds = n_folds)
    else :
        folds = cv_folds
    fold = 0 
    all_true_y, all_predicted_y = [], []
    loss_type = 'ls' if huber_alpha is None else 'huber'
    if huber_alpha is None :
        huber_alpha = 0.9
    for train_index, test_index in folds :
        train_X, test_X = X[train_index, :], X[test_index, :]
        train_y, test_y = y[train_index], y[test_index]
        all_true_y.extend(test_y)
        regressor = GradientBoostingRegressor(n_estimators = n_estimators, loss = loss_type, alpha = huber_alpha, learning_rate = learning_rate, min_samples_leaf = min_samples_leaf, max_depth = max_depth, subsample = subsample, max_features = max_features)
        start_time = time.time()
        if learn_intercept :
            intercept = np.mean(train_y)
            actual_train_y = train_y - intercept
        else :
            intercept = 0
            actual_train_y = train_y
        n_train_samples = len(train_y)
        regressor.fit(train_X, actual_train_y)
        elapsed_time = time.time() - start_time
        times[fold] = elapsed_time
        #train_y_predicted = regressor.predict(train_X)
        #test_y_predicted = regressor.predict(test_X)
        pos = 0
        for i, test_y_predicted in enumerate(regressor.staged_decision_function(test_X)) :
            if (i + 1) % ESTIMATOR_RECORDING_FREQUENCY == 0 or i == n_estimators - 1 :
                pos += 1
            else :
                continue
            test_y_predicted = test_y_predicted[:, 0] + intercept
            if i == n_estimators - 1 :
                all_predicted_y.extend(test_y_predicted)
            for score in scoring :
                test_score[score][fold][pos] = calculate_regression_score(test_y, test_y_predicted, score)
        pos = 0
        for i, train_y_predicted in enumerate(regressor.staged_decision_function(train_X)) :
            if (i + 1) % ESTIMATOR_RECORDING_FREQUENCY == 0 or i == n_estimators - 1 :
                pos += 1
            else :
                continue
            train_y_predicted = train_y_predicted[:, 0] + intercept
            for score in scoring :
                train_score[score][fold][pos] = calculate_regression_score(train_y, train_y_predicted, score)
        fold += 1
        del regressor
        del test_X
        del test_y
        del train_X

    for score in scoring :
        overall_test_score[score] = calculate_regression_score(all_true_y, all_predicted_y, score)
    del all_predicted_y
    del all_true_y
    return train_score, test_score, overall_test_score, times 

def train_gbrf_cv_fold(X, y, train_index, test_index, max_depth = 5, learn_intercept = False, n_estimators = 500, min_samples_leaf = 1, learning_rate = 0.1, subsample = 0.7, max_features = 'log2', huber_alpha = None, scoring = REGRESSION_METRICS, return_regressor = False) :
    '''
    Train Gradient Boosted Random Forest Regression.
    :param X : feature matrix.
    :param y : response variable.
    :param train_index : index of items inside the training set.
    :param test_index : index of items inside the esting set.
    :param max_deph : maximum depth of a regression tree.
    :param learn_intercept : whether we should explicitly learn the intercept.
    :param n_estimators : number of trees to train.
    :param min_samples_leaf : minimum number of leaf samples.
    :param learning_rate : parameter defining the learning rate of the GBRT.
    :param subsample : fraction of data to sample for each tree.
    :param max_features : setting defining how many features should be considered while looking for the best split. Check sklearn documentation.
    :param scoring : scoring function to be used.
    :param return_regressor : whether the constructed regressor should be returned too.
    '''
    n_features, n_samples = X.shape[1], len(y)
    train_score, test_score = {}, {}
    n_recorded_estimators = int(n_estimators / ESTIMATOR_RECORDING_FREQUENCY)
    if n_estimators % ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    for score in scoring :
        test_score[score] = np.zeros((n_recorded_estimators, ), dtype = np.float16)
        train_score[score] = np.zeros((n_recorded_estimators, ), dtype = np.float16)
    train_X, test_X = X[train_index, :], X[test_index, :]
    train_y, test_y = y[train_index], y[test_index]
    loss_type = 'ls' if huber_alpha is None else 'huber'
    if huber_alpha is None :
        huber_alpha = 0.9
    regressor = GradientBoostingRegressor(n_estimators = n_estimators, loss = loss_type, alpha = huber_alpha, learning_rate = learning_rate, min_samples_leaf = min_samples_leaf, max_depth = max_depth, subsample = subsample, max_features = max_features)
    start_time = time.time()
    train_X = np.asfortranarray(train_X.toarray(), dtype = np.float32)
    train_y = np.asfortranarray(train_y, dtype = np.float32)
    test_X = np.asfortranarray(test_X.toarray(), dtype = np.float32)
    test_y = np.asfortranarray(test_y, dtype = np.float32)
    if learn_intercept :
        intercept = np.mean(train_y)
        actual_train_y = train_y - intercept
    else :
        intercept = 0
        actual_train_y = train_y
    n_train_samples = len(train_y)
    regressor.fit(train_X, actual_train_y)
    elapsed_time = time.time() - start_time
    predicted_y = []
    true_y = test_y
    pos = 0
    for i, test_y_predicted in enumerate(regressor.staged_decision_function(test_X)) :
        if (i + 1) % ESTIMATOR_RECORDING_FREQUENCY != 0 and i != n_estimators - 1 :
            continue
        test_y_predicted = test_y_predicted[:, 0] + intercept
        if i == n_estimators - 1 :
            predicted_y.extend(test_y_predicted)
        for score in scoring :
            test_score[score][pos] = calculate_regression_score(test_y, test_y_predicted, score)
        pos += 1
    pos = 0
    for i, train_y_predicted in enumerate(regressor.staged_decision_function(train_X)) :
        if (i + 1) % ESTIMATOR_RECORDING_FREQUENCY != 0 and i != n_estimators - 1 :
            continue
        train_y_predicted = train_y_predicted[:, 0] + intercept
        for score in scoring :
            train_score[score][pos] = calculate_regression_score(train_y, train_y_predicted, score)
        pos += 1
    del test_X
    del test_y
    del train_X
    del train_y
    if not return_regressor :
        del regressor
        return train_score, test_score, true_y, predicted_y, elapsed_time
    else :
        return regressor, train_score, test_score, true_y, predicted_y, elapsed_time

def train_gbrf(X, y, max_depth = 5, learn_intercept = False, n_estimators = 500, min_samples_leaf = 1, learning_rate = 0.1, subsample = 0.7, max_features = 'log2', huber_alpha = None, scoring = REGRESSION_METRICS) :
    '''
    Train Gradient Boosted Random Forest Regression.
    :param X : feature matrix.
    :param y : response variable.
    :param max_deph : maximum depth of a regression tree.
    :param learn_intercept : whether we should explicitly learn the intercept.
    :param n_estimators : number of trees to train.
    :param min_samples_leaf : minimum number of leaf samples.
    :param learning_rate : parameter defining the learning rate of the GBRT.
    :param subsample : fraction of data to sample for each tree.
    :param max_features : setting defining how many features should be considered while looking for the best split. Check sklearn documentation.
    :param scoring : scoring function to be used.
    '''
    n_features, n_samples = X.shape[1], len(y)
    train_score = {}
    overall_train_score = {}
    n_recorded_estimators = int(n_estimators / ESTIMATOR_RECORDING_FREQUENCY)
    if n_estimators % ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    for score in scoring :
        train_score[score] = np.zeros((n_recorded_estimators, ), dtype = np.float16)
    loss_type = 'ls' if huber_alpha is None else 'huber'
    if huber_alpha is None :
        huber_alpha = 0.9
    regressor = GradientBoostingRegressor(n_estimators = n_estimators, loss = loss_type, alpha = huber_alpha, learning_rate = learning_rate, min_samples_leaf = min_samples_leaf, max_depth = max_depth, subsample = subsample, max_features = max_features)
    start_time = time.time()
    if not isinstance(X, np.ndarray) :
        X = np.asfortranarray(X.toarray(), dtype = np.float32)
    if not isinstance(y, np.ndarray) :
        y = np.asfortranarray(y, dtype = np.float32)
    if learn_intercept :
        intercept = np.mean(y)
        actual_y = y - intercept
    else :
        intercept = 0
        actual_y = y
    n_samples = len(y)
    regressor.fit(X, actual_y)
    elapsed_time = time.time() - start_time
    predicted_y = []
    true_y = y
    pos = 0
    for i, train_y_predicted in enumerate(regressor.staged_decision_function(X)) :
        if (i + 1) % ESTIMATOR_RECORDING_FREQUENCY != 0 and i != n_estimators - 1 :
            continue
        train_y_predicted = train_y_predicted[:, 0] + intercept
        if i == n_estimators - 1 :
            predicted_y.extend(train_y_predicted)
        for score in scoring :
            train_score[score][pos] = calculate_regression_score(true_y, train_y_predicted, score)
        pos += 1
    del X
    del y
    for score in scoring :
        overall_train_score[score] = calculate_regression_score(true_y, predicted_y, score)
    del true_y
    del predicted_y
    return train_score, overall_train_score, elapsed_time, regressor, intercept

def get_classification_score_array_dtype(score) :
    '''
    Get data type for classification array scores for the corresponding score.
    :param score : the type of score for which the data type should be determined.
    '''
    if score in ['auc-roc', 'zero-one-loss', 'recall', 'precision', 'auc-pr', 'f1', 'brier', 'mcc'] :
        return np.float16
    elif score in ['prc', 'roc', 'confusion'] :
        return np.object
    else :
        return None
