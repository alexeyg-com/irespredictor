'''
Some data maintenance routines.

12-02-2015
Alexey Gritsenko
'''

import cPickle
import redis
import numpy as np

def expand_redis_pickles(host, db_from = 0, db_struct = 1, db_mfe = 2) :
    '''
    Expand pickles inside the redis database.
    :param host : hostname of the Redis server.
    :param db_from : id of the database containing pickled entires.
    :param db_struct : id of the database to which the structure entries should be expanded.
    :param db_mfe : id of the database to which the mfe entires should be expanded.
    '''
    conn_from = redis.Redis(host, db = db_from)
    conn_struct = redis.Redis(host, db = db_struct)
    conn_mfe = redis.Redis(host, db = db_mfe)
    keys = conn_from.keys()
    for index in keys :
        str = conn_from.get(index)
        struct_dict = cPickle.loads(str)
        for interval, data in struct_dict.iteritems() :
            struct, mfe = data
            conn_struct.hset(index, interval, struct)
            conn_mfe.hset(index, interval, mfe)
        conn_from.delete(index)
        del str
        del struct_dict

def construct_mfe_matrix_from_redis(host, db = 0) :
    '''
    Construct MFE matrices (one per oligo) from RNA secondary structures stored in a Redis database.
    :param host : hostname of the Redis server.
    :param db : ID of the database where structures are stored.
    '''
    conn = redis.Redis(host, db = db)
    indices = conn.keys()
    n_indices = len(indices)
    results = {}
    for i, index in enumerate(indices) :
        str = conn.get(index)
        struct_dict = cPickle.loads(str)
        del str
        range_min, range_max = +float('inf'), -float('inf')
        for left, right in struct_dict :
            range_min, range_max = min(range_min, left), max(range_max, right)
        array = np.zeros((range_max + 1, range_max + 1))
        for segment, structure in struct_dict.iteritems() :
            left, right = segment
            struct, mfe = structure
            array[left, right] = mfe
        results[index] = array
        if i % 100 == 0 :
            print '[i] Progress: %d / %d' % (i, n_indices)
    return results

def construct_structure_dict(segment, host, db = 0) :
    '''
    Construct a dictionary of predicted MFE structures from RNA secondary structures stored in a Redis database.
    :param segment : segment for which structure is requested.
    :param host : hostname of the Redis server.
    :param db : ID of the database where structures are stored.
    '''
    conn = redis.Redis(host, db = db)
    indices = conn.keys()
    n_indices = len(indices)
    results = {}
    for i, index in enumerate(indices) :
        str = conn.get(index)
        struct_dict = cPickle.loads(str)
        del str
        struct, mfe = structure = struct_dict[segment]
        results[index] = struct
        if i % 100 == 0 :
            print '[i] Progress: %d / %d' % (i, n_indices)
    return results

