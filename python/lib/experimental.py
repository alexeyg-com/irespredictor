'''
A module with experimental code that did not make it to the main methods (yet).

Alexey Gritsenko
13-08-2015
'''

import sys
import time
import numpy as np
import scipy.stats

import persistent
import kmers

import pysais
import sgraph

def compute_structure_ensemble(seqs, n_structures = 5000) :
    '''
    Sample a number of structures from the ensemble for a given list of sequences.
    :param seqs : sequences for which the ensemble should be sampled.
    :param n_structures : number of structures to be sampled per sequence.
    '''
    results = {}
    n_seqs = len(seqs)
    for i, seq in enumerate(seqs) :
        structures = sgraph.sample_structures(seq.sequence, n_structures)
        results[seq.index] = structures
        print '[i] Done %d / %d' % (i + 1, n_seqs)
    return results

def compute_average_distance_matrix(sequence, n_structures = 1000) :
    '''
    Compute average pairwise distance matrix for a number of structures sampled from the ensemble of structures for the given sequence.
    :param sequence : sequence for which the structures should be sampled.
    :param n_structures : number of structures to be sampled.
    '''
    structures = sgraph.sample_structures(sequence, n_structures)
    length = len(sequence)
    average_dist = np.zeros((length, length), dtype = np.float)
    for structure in structures :
        dist = sgraph.get_structure_pairwise_distances(structure)
        average_dist += dist
    average_dist /= n_structures
    return average_dist

def compute_average_distance_matrix_for_sequences(seqs, n_structures = 1000) :
    result = {}
    n_seqs = len(seqs)
    for i, seq in enumerate(seqs) :
        start_time = time.time()
        dist = compute_average_distance_matrix(seq.sequence, n_structures = n_structures)
        result[seq.index] = dist
        duration = time.time() - start_time
        print '[i] Done for %d / %d (%.2f sec)' % (i + 1, n_seqs, duration)
    return result

def look_for_distance_correlations(seqs, dist, window_size = 1, window_step = 1, cutoff = 0.05) :
    '''
    Look for distance correlations between distances (between windows) and IRES activity).
    :param seqs : a list of sequences for which the correlation should be checked.
    :param dist : a dictionary of id -> distance matrix for sequences in seqs.
    :param window_size : size of the window to be considered.
    :param window_step : step for the window.
    :param cutoff : threshold cutoff for the Spearman correlation test.
    '''
    ires_activity = [seq.ires_activity for seq in seqs]
    length = len(seqs[0].sequence)
    start_window_a = 0
    while start_window_a + window_size < length :
        start_window_b = start_window_a + window_step
        while start_window_b + window_size < length :
            features = [np.mean(dist[seq.index][start_window_a : start_window_a + window_size, start_window_b : start_window_b + window_step]) for seq in seqs]
            r, p = scipy.stats.spearmanr(features, ires_activity)
            if p < cutoff :
                print '[i] [%d %d] x [%d %d] : r = %5.2f (p = %8g)' % (start_window_a, start_window_a + window_size - 1, start_window_b, start_window_b + window_size - 1, r, p)
            start_window_b += window_step
        start_window_a += window_step

def _parallel_structures(params) :
    '''
    A routine for computing structure count Spearman correlations in the data. Used in parallel computations.
    :param params : routine parameters - (dataset_name, indices, other_params, cutoff, [motif]) tuple.
    '''
    import scipy.stats
    import lib.persistent as persistent
    import numpy as np
    result = []
    if isinstance(params, tuple) :
        params = [params]
    for dataset_name, indices, other_params, cutoff, tasks in params :
        dataset = persistent.datasets[dataset_name]
        structures = persistent.structures
        n_samples = len(dataset)
        example_structures = structures[dataset[0].index]
        str_length = len(example_structures[0])
        n_structures_per_sequence = float(len(example_structures))
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed_structure[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
        for motif in tasks :
            counts = pysais.count_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            counts = counts / n_structures_per_sequence
            if indices is not None :
                counts = counts[indices]
            coef_r, pvalue = scipy.stats.spearmanr(counts, ires_activity)
            usage = np.sum(counts > 0, dtype = np.int) / float(n_index_samples)
            if pvalue <= cutoff :
                result.append((motif, 'all', coef_r, pvalue, usage))
    return result

def _parallel_structures_in_windows(params) :
    '''
    A routine for computing kmer count Spearman correlations in the data in windows. Used in parallel computations.
    :param params : routine parameters - (dataset_name, indices, (window_step, window_size), [motif]) tuple.
    '''
    import scipy.stats
    import lib.persistent as persistent
    import numpy as np
    result = []
    if isinstance(params, tuple) :
        params = [params]
    for dataset_name, indices, other_params, cutoff, tasks in params :
        window_step, window_size = other_params
        dataset = persistent.datasets[dataset_name]
        structures = persistent.structures
        n_samples = len(dataset)
        example_structures = structures[dataset[0].index]
        str_length = len(example_structures[0])
        n_structures_per_sequence = float(len(example_structures))
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed_structure[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
        for motif in tasks :
            counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            if indices is not None :
                counts = counts[indices, :]
            rows, cols = counts.shape
            window_offset = 0
            while window_offset < cols :
                window_indices = np.array(range(window_offset, min(window_offset + window_size, cols)))
                window = (window_offset, min(window_offset + window_size, cols))
                feature = np.sum(counts[:, window_indices], axis = 1)
                feature = feature / n_structures_per_sequence
                coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
                usage = np.sum(feature > 0, dtype = np.int) / float(n_index_samples)
                if pvalue <= cutoff :
                    result.append((motif, window, 'all', coef_r, pvalue, usage))
                window_offset += window_step
    return result

def look_for_ddG_open_in_windows(dataset_name, indices = None, window_size = 20, window_step = 10, cutoff = 0.05) :
    '''
    Compute correlations between RNA accessibility and IRES activity in moving windows.
    :param dataset_name : name of the dataset to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param window_size : size of the window that should be considered.
    :param window_step : the number of bases by which a window should be moved.
    :param cutoff : pvalue cutoff used for selecting well-correlating windows.
    '''
    ddG = persistent.ddG[(window_size, window_step)]
    dataset = persistent.datasets[dataset_name]
    ddG = np.array([ddG[seq.index] for seq in dataset])
    ires_activity = np.array([seq.ires_activity for seq in dataset])
    if indices is not None :
        ddG, ires_activity = ddG[indices, :], ires_activity[indices]
    correlations = []
    start, length = 0, len(dataset[0].sequence)
    _, n_windows = ddG.shape
    for i in range(n_windows) :
        stop = min(start + window_size, length)
        coef_r, pvalue = scipy.stats.spearmanr(ddG[:, i], ires_activity)
        if pvalue < cutoff :
            item = ((start, stop), coef_r, pvalue, 1.0)
            correlations.append(item)
        start += window_step
    return correlations

def look_for_structures_in_windows(dataset_name, indices = None, alphabet = ['(', '.', ')'], min_motif_length = 1, max_motif_length = 6, window_size = 20, window_step = 10, batch_size = 10000000, client_url = None, chunk_size = 100, cutoff = 0.05) :
    '''
    Compute correlations between structure counts and IRES activity in moving windows.
    :param dataset_name : name of the dataset to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param alphabet : alphabet to use for constructing motifs.
    :param min_motif_length : minimum length of motifs to consider.
    :param max_motif_length : maximum length of motifs to consider.
    :param window_size : size of the window that should be considered.
    :param window_step : the number of bases by which a window should be moved.
    :param batch_size : size of the batch for parallel computing. Also determines how often intermediate results are written to a file.
    :param client_url : if not None, define URL of the IPython controller.
    :param chunk_size : number of tasks to send per engine.
    :param cutoff : pvalue cutoff used for selecting well-correlating motifs.
    '''
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100 

    alphabet_size = len(alphabet)
    start = 0 
    for i in range(1, min_motif_length) :
        start += alphabet_size ** i
    stop = 0 
    for i in range(1, max_motif_length + 1) :
        stop += alphabet_size ** i
    
    if view is None :
        dataset = persistent.datasets[dataset_name]
        structures = persistent.structures
        example_structures = structures[dataset[0].index]
        str_length = len(example_structures[0])
        n_structures_per_sequence = float(len(example_structures))
        n_samples = len(dataset)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed_structure[dataset_name]
        ires_activity = np.atleast_1d([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)

    result = []
    motif_num = start
    while motif_num < stop :
        batch_ind = 0
        if view is None :
            while batch_ind < batch_size and motif_num < stop :
                motif = kmers._construct_motif(alphabet, motif_num)
                counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                if indices is not None :
                    counts = counts[indices, :]
                rows, cols = counts.shape
                window_offset = 0
                while window_offset < cols :
                    window_indices = np.array(range(window_offset, min(window_offset + window_size, cols)))
                    window = (window_offset, min(window_offset + window_size, cols))
                    feature = np.sum(counts[:, window_indices], axis = 1)
                    feature = feature / n_structures_per_sequence
                    #coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
                    coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
                    usage = np.sum(feature > 0, dtype = np.int) / float(n_index_samples)
                    if pvalue <= cutoff :
                        #if usage >= 0.1 :
                        #print '[i] %s in [%d, %d] : r = %5.2f (p = %8g), usage = %5.2f (%d / %d)' % (motif, window[0], window[1], coef_r, pvalue, usage, np.sum(feature > 0, dtype = np.int), n_index_samples)
                        result.append((motif, window, 'all', coef_r, pvalue, usage))
                    window_offset += window_step
                batch_ind += 1
                motif_num += 1
        else :
            tasks = []
            while batch_ind < batch_size and motif_num < stop :
                motif = kmers._construct_motif(alphabet, motif_num)
                tasks.append(motif)
                batch_ind += 1
                motif_num += 1
            tasks = kmers._package_parallel_tasks(dataset_name, indices, (window_step, window_size), cutoff, tasks, chunk_size)
            map_results = view.map(_parallel_structures_in_windows, tasks, block = True, ordered = True)
            client.purge_local_results('all')
            map_results = [res for res_group in map_results for res in res_group]
            for motif, window, coef_r, pvalue, usage in map_results :
                if pvalue <= cutoff :
                    result.append((motif, window, coef_r, pvalue, usage))
    if client is not None :
        del view
        client.close()
        del client
    return result

def look_for_structures(dataset_name, indices = None, alphabet = ['(', '.', ')'], min_motif_length = 1, max_motif_length = 6, batch_size = 10000000, client_url = None, chunk_size = 100, cutoff = 0.05) : 
    ''' 
    Compute correlations between structure counts and IRES activity.
    :param dataset_name : name of the dataset to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param alphabet : alphabet to use for constructing motifs.
    :param min_motif_length : minimum length of motifs to consider.
    :param max_motif_length : maximum length of motifs to consider.
    :param batch_size : size of the batch for parallel computing. Also determines how often intermediate results are written to a file.
    :param client_url : if not None, define URL of the IPython controller.
    :param chunk_size : number of tasks to send per engine.
    :param cutoff : pvalue cutoff used for selecting well-correlating motifs.
    ''' 
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100 

    alphabet_size = len(alphabet)
    start = 0 
    for i in range(1, min_motif_length) :
        start += alphabet_size ** i
    stop = 0 
    for i in range(1, max_motif_length + 1) :
        stop += alphabet_size ** i

    if view is None :
        dataset = persistent.datasets[dataset_name]
        structures = persistent.structures
        n_samples = len(dataset)
        example_structures = structures[dataset[0].index]
        str_length = len(example_structures[0])
        n_structures_per_sequence = float(len(example_structures))
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed_structure[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
    
    result = []
    motif_num = start
    while motif_num < stop :
        batch_ind = 0 
        if view is None :
            while batch_ind < batch_size and motif_num < stop :
                motif = kmers._construct_motif(alphabet, motif_num)
                counts = pysais.count_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                if not indices is None :
                    counts = counts[indices]
                counts = counts / n_structures_per_sequence
                coef_r, pvalue = scipy.stats.spearmanr(counts, ires_activity)
                usage = np.sum(counts > 0, dtype = np.int) / float(n_index_samples)
                if pvalue <= cutoff :
                    #print '[i] %s : r = %5.2f (p = %8g)' % (motif, coef_r, pvalue)
                    result.append((motif, 'all', coef_r, pvalue, usage))
                batch_ind += 1
                motif_num += 1
        else :
            tasks = []
            while batch_ind < batch_size and motif_num < stop :
                motif = kmers._construct_motif(alphabet, motif_num)
                tasks.append(motif)
                batch_ind += 1
                motif_num += 1
            tasks = kmers._package_parallel_tasks(dataset_name, indices, None, cutoff, tasks, chunk_size)
            map_results = view.map(_parallel_structures, tasks, block = True, ordered = True)
            client.purge_local_results('all')
            map_results = [res for res_group in map_results for res in res_group]
            for motif, frame, coef_r, pvalue, usage in map_results :
                if pvalue <= cutoff :
                    result.append((motif, frame, coef_r, pvalue, usage))
            del map_results

    if client is not None :
        del view
        client.close()
        del client
    return result

