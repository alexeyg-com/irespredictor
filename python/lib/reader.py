'''
Implements some file manipulation routines.

Alexey Gritsenko
01-01-2015
'''

import cPickle
import marshal
from lib.SimpleNamespace import SimpleNamespace

def read_marshal(filename) :
    ''' 
    Read marshaled data from a file and return it. 
    :param filename : name of file to read data from.
    ''' 
    fin = open(filename, 'rb')
    loaded = marshal.load(fin)
    fin.close()

    return loaded

def read_pickle(filename) :
    ''' 
    Read pickled data from a file and return it. 
    :param filename : name of file to read data from.
    ''' 
    fin = open(filename, 'rb')
    loaded = cPickle.load(fin)
    fin.close()

    return loaded

def read_splicing_scores(filename) :
    '''
    Read splicing scores from a given filename.
    :param filename : name of the file to read splicing scores from.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    results = {}
    for line in lines :
        args = line.strip().split('\t')
        id = int(args[0])
        score = float(args[1])
        results[id] = score
    return results

def read_replicate_ires_activity(filename) :
    '''
    Read IRES activity scores for two replicates from a given file.
    :param filename : name of the filename from which the replicated IRES activities should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    result = {}
    for line in lines :
        line = line.strip()
        args = line.split('\t')
        id = int(args[0])
        activity1 = float(args[1])
        activity2 = float(args[2])
        result[id] = (activity1, activity2)
    return result

def read_sequences(filename = '../data/55k_oligos_sequence_and_expression_measurements.tab', read_replicate_activity = '../data/ires_activity_two_biological_replicates.tab', update_splicing_score = '../data/new_splicing_scores.tab') :
    '''
    Read the IRES library from a tab file.
    :param filename : name of the file to read the library from.
    :param read_replicate_activity : None of name of the file from which replicates of IRES activity should be read.
    :param update_splicing_score : None or name of the file from which updated splicing scores should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    result = []
    for line in lines :
        args = line.strip().split('\t')
        index, description, sequence, mfe, ires_activity, promoter_activity, splicing_score = args[0], args[1], args[2], args[3], args[4], args[5], args[6]
        instance = SimpleNamespace()
        instance.index = int(index)
        instance.description = description
        instance.sequence = sequence
        instance.mfe = float(mfe)
        instance.ires_activity = float(ires_activity)
        instance.promoter_activity = float(promoter_activity)
        instance.splicing_score = float(splicing_score)
        result.append(instance)
    
    if update_splicing_score is not None :
        scores = read_splicing_scores(update_splicing_score)
        for seq in result :
            seq.splicing_score = scores[seq.index]
   
    if  read_replicate_activity is not None :
        replicate_activity = read_replicate_ires_activity(read_replicate_activity)
        for seq in result :
            del seq.__dict__['ires_activity']
            seq.ires_activity_replicate1, seq.ires_activity_replicate2 = replicate_activity[seq.index]

    return result

def read_ribosomal_rna_seq(filename = '../data/rRNAs_Sequences.tab') :
    '''
    Read ribosomal RNA sequences from a given file.
    :param filename : file to read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    result = {}
    for line in lines :
        args = line.split('\t')
        name, seq = args[0].strip(), args[2].strip()
        result[name] = seq
    return result

def read_sequence_ids(filename = '../data/ires_lib_oligos_for_fmm_analysis.tab') :
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    results = []
    for line in lines :
        args = line.strip().split('\t')
        index = int(args[0])
        results.append(index)
    return results

def _get_dazzler_segment_indices(str) :
    '''
    Return a pair of indices parsed from Dazzler output.
    :param str : a string that should be parsed.
    '''
    import re
    segment_regex = '.*(\d+)\.\.(\d+).*'
    match = re.match(segment_regex, str)
    if not match :
        return None
    else :
        a = int(match.group(1))
        b = int(match.group(2))
        return (a, b)

def read_dazzler_alignments(seqs, filename, min_length = 174) :
    '''
    Read output of the dazzler LAshow command.
    :param seqs : a list of sequences that were aligned against each other.
    :param filename : name of the file to read alignments from.
    :param min_length : minimum alignment length required for the alignment to be kept.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[2:]
    alignments = []
    for line in lines :
        args = line.strip().split('x')
        args_left = args[0].split('n')
        args_right = args[1].split('<')
        args = args_left[0].strip().split()
        i, j = args[0], args[1]
        i = int(''.join([c for c in i if c != ','])) - 1
        j = int(''.join([c for c in j if c != ','])) - 1
        start_i, stop_i = _get_dazzler_segment_indices(args_left[1])
        start_j, stop_j = _get_dazzler_segment_indices(args_right[0])
        index_i, index_j = seqs[i].index, seqs[j].index
        args = args_right[1].split()
        diffs = int(args[0]) 
        length_i, length_j = stop_i - start_i, stop_j - start_j
        if length_i < min_length and length_j < min_length :
            continue
        identity = 1.0 - float(diffs) / max(length_i, length_j)
        alignment = {'index_i' : index_i, 'index_j' : index_j, 'segment_i' : (start_i, stop_i), 'segment_j' : (start_j, stop_j), 'diffs' : diffs, 'identity' : identity}
        alignments.append(alignment)
    return alignments

