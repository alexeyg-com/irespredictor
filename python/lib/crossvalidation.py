'''
A module used for constructing cross-validation folds for model/fit evaluation.
Borrowed from the elongation speed project.

Alexey Gritsenko
05-02-2015
'''

from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from numpy.random import RandomState
import numpy as np

import persistent
from SimpleNamespace import SimpleNamespace

def stratified_KFold(values, n_folds = 10, shuffle = True, random_state = None, n_items_per_bin = 50) :
    '''
    Crate stratified CV folds by binning sequences based on their IRES activity into several bins.
    :param values : an array of IRES activity values.
    :param n_folds : number of desired CV folds.
    :param shuffle : whether sequences should be shuffled prior to binning.
    :param random_state : a random state object used to randomize the shufling.
    :param n_items_per_bin : the number of items per bin. If the total number of items is too small, then 10 bins are used. If the number of items N is below 10, then N bins are used.
    '''
    n = len(values)
    indices_and_values = np.array([values, range(n)]).transpose().tolist()
    indices_and_values.sort(key = lambda x : x[0])
    indices_and_values = np.array(indices_and_values).transpose()
    groups = np.zeros((n, ), dtype = np.int)
    values, indices = indices_and_values[0, :], indices_and_values[1, :].astype(np.int)
    n_bins = int(n / float(n_items_per_bin))
    if n_bins == 0 :
        n_bins = min(10, n)
    cur_bin, left_bins = 0, n_bins
    left_values, cur_bin_size = n, 0
    i = 0
    while cur_bin < n_bins and i < n :
        cur_value = float('nan')
        wanted_bin_size = left_values / float(left_bins)
        cur_bin_size = 0
        #print '%d : want size %d' % (cur_bin, wanted_bin_size)
        #print 'at position %d' % i
        while i < n and (cur_value == values[i] or cur_bin_size <= wanted_bin_size or cur_bin == n_bins - 1) :
            groups[indices[i]] = cur_bin
            left_values -= 1
            cur_bin_size += 1
            cur_value = values[i]
            i += 1
        #print 'Group %d : size %d' % (cur_bin, cur_bin_size)
        cur_bin += 1
        left_bins -= 1
    return StratifiedKFold(groups, n_folds = n_folds, shuffle = shuffle, random_state = random_state)

def create_folds(dataset_name, n_folds_outer = 10, n_folds_inner = 10, random_seed = 1337357) :
    '''
    Create stratified double loop cross-validation folds for training the final predictor.
    :param dataset_name : name of the dataset that should be used.
    :param n_outer_folds : number of outer-loop CV folds.
    :param n_inner_folds : number of inner-loop CV folds.
    :param random_seed : random seed for constructing the CV folds.
    '''
    result = SimpleNamespace()
    result.outer_folds = []
    result.inner_folds = []
    state = RandomState(random_seed)
    dataset = persistent.datasets[dataset_name]
    values = np.array([seq.ires_activity for seq in dataset])
    outer_folds = stratified_KFold(values, n_folds = n_folds_outer, shuffle = True, random_state = state)
    for train_index_outer, test_index_outer in outer_folds :
        train_outer_values = values[train_index_outer]
        result.outer_folds.append((train_index_outer, test_index_outer))
        
        saved_inner_folds = []
        inner_folds = stratified_KFold(train_outer_values, n_folds = n_folds_inner, random_state = state)
        for train_index_inner, test_index_inner in inner_folds :
            actual_train_index_inner, actual_test_index_inner = train_index_outer[train_index_inner], train_index_outer[test_index_inner]
            saved_inner_folds.append((actual_train_index_inner, actual_test_index_inner))
        result.inner_folds.append(saved_inner_folds)
    return result

