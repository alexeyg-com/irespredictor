'''
Implements persistent dataset loading.
Usually useful for multiplexing when performing large parallel computations.
Copied from the elongation speed project.

Alexey Gritsenko
04-02-2015
'''

import tools
import reader
import writer
import sa 
import sys
import os
import numpy as np

_dataset_mappings = { 
    'viral_native_3utr'                        : 'datasets/current/viral_native_3utr/sequences.out',

    'human_native_5utr'                        : 'datasets/current/human_native_5utr/sequences.out',

    'viral_native_dsRNA_noRNA'                 : 'datasets/current/viral_native_dsRNA_noRNA/sequences.out',
    
    'native_3utr'                              : 'datasets/current/native_3utr/sequences.out',

    'human_native_cds'                         : 'datasets/current/human_native_cds/sequences.out',

    'human_native'                             : 'datasets/current/human_native/sequences.out',

    'native'                                   : 'datasets/current/native/sequences.out',

    'viral_native_ssRNA_negative'              : 'datasets/current/viral_native_ssRNA_negative/sequences.out',

    'viral_native'                             : 'datasets/current/viral_native/sequences.out',

    'viral_native_dsRNA_all'                   : 'datasets/current/viral_native_dsRNA_all/sequences.out',

    'viral_native_5utr'                        : 'datasets/current/viral_native_5utr/sequences.out',

    'native_5utr'                              : 'datasets/current/native_5utr/sequences.out',

    'human_native_3utr'                        : 'datasets/current/human_native_3utr/sequences.out',

    'viral_native_ssRNA_all'                   : 'datasets/current/viral_native_ssRNA_all/sequences.out',

    'native_cds'                               : 'datasets/current/native_cds/sequences.out',

    'viral_native_RTV'                         : 'datasets/current/viral_native_RTV/sequences.out',

    'viral_native_ssRNA_positive'              : 'datasets/current/viral_native_ssRNA_positive/sequences.out',
    
    'viral_native_ssRNA_positive_uncapped'     : 'datasets/current/viral_native_ssRNA_positive_uncapped/sequences.out',
    
    'viral_native_ssRNA_positive_capped'       : 'datasets/current/viral_native_ssRNA_positive_capped/sequences.out',

    'viral_native_dsRNA'                       : 'datasets/current/viral_native_dsRNA/sequences.out',

    'viral_native_ssRNA'                       : 'datasets/current/viral_native_ssRNA/sequences.out',

    'viral_native_cds'                         : 'datasets/current/viral_native_cds/sequences.out',
    
    'mult_all_comb'                            : 'datasets/current/mult_all_comb/sequences.out',
    'distance'                                 : 'datasets/current/distance/sequences.out',
    'combinations'                             : 'datasets/current/combinations/sequences.out',
    'backgrounds'                              : 'datasets/current/backgrounds/sequences.out',

    'viral_native_dsRNA_all_subsample_374_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_374_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_340_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_340_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_307_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_307_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_273_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_273_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_240_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_240_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_206_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_206_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_173_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_173_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_139_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_139_rep1/sequences.out',
    'viral_native_dsRNA_all_subsample_106_rep1': 'datasets/current/viral_native_dsRNA_all_subsample_106_rep1/sequences.out',
    
    'viral_native_dsRNA_all_subsample_374_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_374_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_340_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_340_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_307_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_307_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_273_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_273_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_240_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_240_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_206_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_206_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_173_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_173_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_139_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_139_rep2/sequences.out',
    'viral_native_dsRNA_all_subsample_106_rep2': 'datasets/current/viral_native_dsRNA_all_subsample_106_rep2/sequences.out',

    'viral_native_dsRNA_all_subsample_374_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_374_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_340_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_340_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_307_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_307_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_273_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_273_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_240_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_240_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_206_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_206_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_173_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_173_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_139_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_139_rep3/sequences.out',
    'viral_native_dsRNA_all_subsample_106_rep3': 'datasets/current/viral_native_dsRNA_all_subsample_106_rep3/sequences.out',

    'viral_native_dsRNA_all_subsample_374_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_374_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_340_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_340_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_307_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_307_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_273_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_273_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_240_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_240_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_206_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_206_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_173_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_173_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_139_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_139_rep4/sequences.out',
    'viral_native_dsRNA_all_subsample_106_rep4': 'datasets/current/viral_native_dsRNA_all_subsample_106_rep4/sequences.out',

    'viral_native_dsRNA_all_subsample_374_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_374_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_340_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_340_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_307_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_307_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_273_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_273_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_240_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_240_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_206_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_206_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_173_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_173_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_139_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_139_rep5/sequences.out',
    'viral_native_dsRNA_all_subsample_106_rep5': 'datasets/current/viral_native_dsRNA_all_subsample_106_rep5/sequences.out'
    }

_structures_filename = './datasets/mfe-structure.out'
_ddG_filename = './datasets/ddG.out'
_dG0_filename = './datasets/dG0.out'
_accessibility_filename = './datasets/accessibility.out'
_virus_type_filename = '../data/Comb_Unique_Viral_Class_new.tab'
_virus_cds_coordinates_filename = '../data/input_file_for_matlab_ires_activity_along_ssRNA_single_cds_viral_genomes.tab'
_bppm_filename = './datasets/pf-bppm.out'

class DatasetDict :
    '''
    A class implementing automatic loading of datasets.
    '''

    def __init__(self) :
        '''
        Record class constructor. Creates an internal dictionary for storage.
        '''
        self._datasets = {}

    def __getitem__(self, index) :
        '''
        A routine for accessing datasets and fetching them as necessary.
        :param index : dataset name.
        '''
        global _dataset_mappings

        if self.has_key(index) :
            return self._datasets[index]
        elif index in _dataset_mappings :
            ds = reader.read_pickle(_dataset_mappings[index])
            self._datasets[index] = ds
            return ds
        elif index[:10] == 'random_rep' or index[:15] == 'permutation_rep' :
            filename = 'datasets/current/%s/sequences.out' % index
            ds = reader.read_pickle(filename)
            self._datasets[index] = ds
            return ds
        else :
            raise KeyError()

    def has_key(self, key) :
        '''
        Returns true if the record has a corresponding field.
        :param key: the name of the field to check.
        '''
        return self._datasets.has_key(key)

    def __repr__(self):
        '''
        A method defining string representation of the record class.
        '''
        keys = sorted(self._datasets)
        items = ("{}={!r}".format(k, self._datasets[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))

class PrecomputedDict :
    '''
    A class implementing automatic pre-computing of motif counts.
    '''

    def __init__(self) :
        '''
        Record class constructor. Creates an internal dictionary for storage.
        '''
        self._datasets = {}

    def __getitem__(self, index) :
        '''
        A routine for accessing datasets and fetching them as necessary.
        :param index : dataset name.
        '''

        if self.has_key(index) :
            return self._datasets[index]
        elif index in _dataset_mappings or index[:10] == 'random_rep' or index[:15] == 'permutation_rep' :
            dataset = datasets[index]
            precomputed = sa.construct_SA(dataset)
            self._datasets[index] = precomputed
            return precomputed
        else :
            raise KeyError()

    def has_key(self, key) :
        '''
        Returns true if the record has a corresponding field.
        :param key: the name of the field to check.
        '''
        return self._datasets.has_key(key)

    def __repr__(self):
        '''
        A method defining string representation of the record class.
        '''
        keys = sorted(self._datasets)
        items = ("{}={!r}".format(k, self._datasets[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))

class PrecomputedStructureDict :
    '''
    A class implementing automatic pre-computing of structure motif counts (in dot-bracket notation).
    '''

    def __init__(self) :
        '''
        Record class constructor. Creates an internal dictionary for storage.
        '''
        self._datasets = {}

    def __getitem__(self, index) :
        '''
        A routine for accessing datasets and fetching them as necessary.
        :param index : dataset name.
        '''

        if self.has_key(index) :
            return self._datasets[index]
        elif index in _dataset_mappings :
            dataset = datasets[index]
            precomputed = sa.construct_structure_SA(dataset)
            self._datasets[index] = precomputed
            return precomputed
        else :
            raise KeyError()

    def has_key(self, key) :
        '''
        Returns true if the record has a corresponding field.
        :param key: the name of the field to check.
        '''
        return self._datasets.has_key(key)

    def __repr__(self):
        '''
        A method defining string representation of the record class.
        '''
        keys = sorted(self._datasets)
        items = ("{}={!r}".format(k, self._datasets[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))

def make_random_datasets(dataset_name_prefix = 'random', target_dataset_names = ['viral_native_dsRNA_all', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_negative', 'human_native_5utr', 'human_native_3utr', 'human_native_cds', 'viral_native_RTV'], source_dataset_name = 'native', n_random_samples = 10) :
    '''
    Create a several random sample sequence groups to confirm the hypothesis of beneficial separation of sequences into groups. Keep the number of positives and negatives in the groups the same.
    :param dataset_name_prefix : prefix used for saving the subsampled datasets.
    :param target_dataset_names : names of the dataset that should be mimicked.
    :param source_dataset_name : name of the dataset to sample from. 
    :param n_random_samples : number of random dataset samples to take.
    '''
    global datasets
    source_ds = datasets[source_dataset_name] 
    n_source = len(source_ds) 
    source_activity = np.array([seq.ires_activity for seq in source_ds]) 
    source_positive_indices = np.where(source_activity != tools.MINIMAL_IRES_ACTIVITY)
    source_positive_indices = source_positive_indices[0]
    source_negative_indices = np.where(source_activity == tools.MINIMAL_IRES_ACTIVITY)
    source_negative_indices = source_negative_indices[0]
    n_source_positive = len(source_positive_indices)
    n_source_negative = n_source - n_source_positive
    source_percent = n_source_positive / float(n_source)
   
    print '[i] Source: %s' % source_dataset_name
    print '    [i] Positive: %d' % n_source_positive
    print '    [i] Negative: %d' % n_source_negative
    print '    [i] Relative: %.2f%%' % (source_percent * 100)
    print ''

    for dataset_name in target_dataset_names :
        destination_ds = datasets[dataset_name]
        n_destination = len(destination_ds)
        destination_activity = np.array([seq.ires_activity for seq in destination_ds])
        destination_positive_indices = np.where(destination_activity != tools.MINIMAL_IRES_ACTIVITY)
        destination_positive_indices = destination_positive_indices[0]
        n_destination_positive = len(destination_positive_indices)
        n_destination_negative = n_destination - n_destination_positive
        destination_percent = n_destination_positive / float(n_destination)

        print '[i] Destination: %s' % dataset_name
        print '    [i] Positive: %d' % n_destination_positive
        print '    [i] Negative: %d' % n_destination_negative
        print '    [i] Relative: %.2f%%' % (destination_percent * 100)
        print ''
        
        for rep in xrange(1, n_random_samples + 1) :
            current_positive_indices = np.random.choice(source_positive_indices, n_destination_positive, replace = False)
            current_negative_indices = np.random.choice(source_negative_indices, n_destination_negative, replace = False)
            current_ds = [source_ds[ind] for ind in current_positive_indices] + [source_ds[ind] for ind in current_negative_indices]
            current_ds.sort(key = lambda x : x.ires_activity)
            n_total_current = len(current_ds)
            new_dataset_name = '%s_rep%d_%s' % (dataset_name_prefix, rep, dataset_name)
            current_ds.sort(key = lambda x : x.index)
            print '    [+] Creating random sample: %d / %d (%s)' % (rep, n_random_samples, new_dataset_name)
            if not os.path.exists('./datasets/current/%s' % new_dataset_name) :
                os.makedirs('./datasets/current/%s' % new_dataset_name)
            writer.write_pickle(current_ds, './datasets/current/%s/sequences.out' % new_dataset_name)
        print ''

def make_permuted_datasets(dataset_name_prefix = 'permutation', target_dataset_names = ['viral_native_dsRNA_all', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_negative', 'human_native_5utr', 'human_native_3utr', 'human_native_cds', 'viral_native_RTV'], source_dataset_name = 'native', n_permutations = 10) :
    '''
    Create a several random permuted sequence groups to confirm the hypothesis of beneficial separation of sequences into groups. Keep the number of positives and negatives in the groups the same.
    :param dataset_name_prefix : prefix used for saving the subsampled datasets.
    :param target_dataset_names : names of the dataset that should be mimicked.
    :param source_dataset_name : name of the dataset to sample from. 
    :param n_random_samples : number of random dataset samples to take.
    '''
    global datasets
    source_ds = datasets[source_dataset_name]
    source_ds_positive = [seq for seq in source_ds if seq.ires_activity != tools.MINIMAL_IRES_ACTIVITY]
    source_ds_negative = [seq for seq in source_ds if seq.ires_activity == tools.MINIMAL_IRES_ACTIVITY]
    n_source_positive, n_source_negative = len(source_ds_positive), len(source_ds_negative)
    source_percent = n_source_positive / float(n_source_negative + n_source_positive)

    print '[i] Source: %s' % source_dataset_name
    print '    [i] Positive: %d' % n_source_positive
    print '    [i] Negative: %d' % n_source_negative
    print '    [i] Relative: %.2f%%' % (source_percent * 100)
    print ''

        
    for rep in xrange(1, n_permutations + 1) :
        print '[i] Repeat %d / %d' % (rep, n_permutations) 
        np.random.shuffle(source_ds_positive)
        np.random.shuffle(source_ds_negative)
        offset_positive, offset_negative = 0, 0
        for dataset_name in target_dataset_names :
            destination_ds = datasets[dataset_name]
            n_destination = len(destination_ds)
            destination_activity = np.array([seq.ires_activity for seq in destination_ds])
            n_destination_positive = np.sum(destination_activity != tools.MINIMAL_IRES_ACTIVITY, dtype = np.int)
            n_destination_negative = np.sum(destination_activity == tools.MINIMAL_IRES_ACTIVITY, dtype = np.int)
            destination_percent = n_destination_positive / float(n_destination_positive + n_destination_negative)

            print '    [i] Destination: %s' % dataset_name
            print '        [i] Positive: %d' % n_destination_positive
            print '        [i] Negative: %d' % n_destination_negative
            print '        [i] Relative: %.2f%%' % (destination_percent * 100)

            chosen_positive_seqs, chosen_negative_seqs = source_ds_positive[offset_positive : offset_positive + n_destination_positive], source_ds_negative[offset_negative : offset_negative + n_destination_negative]
            offset_positive += n_destination_positive
            offset_negative += n_destination_negative

            current_ds = chosen_positive_seqs + chosen_negative_seqs
            current_ds.sort(key = lambda x : x.index)
            new_dataset_name = '%s_rep%d_%s' % (dataset_name_prefix, rep, dataset_name)
            if not os.path.exists('./datasets/current/%s' % new_dataset_name) :
                os.makedirs('./datasets/current/%s' % new_dataset_name)
            writer.write_pickle(current_ds, './datasets/current/%s/sequences.out' % new_dataset_name)
            print '        [+] Created random sample: %s' % new_dataset_name

def make_subsample_datasets(dataset_name_suffix, source_dataset_name = 'viral_native_dsRNA_all', destination_dataset_name = 'viral_native_ssRNA_negative', n_steps = 10) :
    '''
    Create a series of subsample datasets by repeatedly sub-sampling the source dataset until its number of positive and negative samples matches that of the destination dataset.
    :param dataset_name_prefix : prefix used for saving the subsampled datasets.
    :param source_dataset_name : name of the source dataset.
    :param destination_dataset_name : name of the destination dataset.
    :param n_steps : number of subsampling steps.
    '''
    global datasets
    source_ds, destination_ds = datasets[source_dataset_name], datasets[destination_dataset_name]
    n_source, n_destination = len(source_ds), len(destination_ds)
    source_activity, destination_activity = np.array([seq.ires_activity for seq in source_ds]), np.array([seq.ires_activity for seq in destination_ds])
    source_positive_indices, destination_positive_indices = np.where(source_activity != tools.MINIMAL_IRES_ACTIVITY), np.where(destination_activity != tools.MINIMAL_IRES_ACTIVITY)
    source_positive_indices, destination_positive_indices = source_positive_indices[0], destination_positive_indices[0]
    source_negative_indices = np.where(source_activity == tools.MINIMAL_IRES_ACTIVITY)
    source_negative_indices = source_negative_indices[0]
    n_source_positive, n_destination_positive = len(source_positive_indices), len(destination_positive_indices)
    n_source_negative, n_destination_negative = n_source - n_source_positive, n_destination - n_destination_positive
    source_percent, destination_percent = n_source_positive / float(n_source), n_destination_positive / float(n_destination)
   
    print '[i] Destination: %s' % destination_dataset_name
    print '    [i] Positive: %d' % n_destination_positive
    print '    [i] Negative: %d' % n_destination_negative
    print '    [i] Relative: %.2f%%' % (destination_percent * 100)
    print '[i] Source: %s' % source_dataset_name
    print '    [i] Positive: %d' % n_source_positive
    print '    [i] Negative: %d' % n_source_negative
    print '    [i] Relative: %.2f%%' % (source_percent * 100)
    if n_destination_positive > n_source_positive or destination_percent > source_percent :
        print ''
        print '[-] Error: destination dataset has more positives than the source.'
        return
    #if n_destination_negative > n_source_negative :
    #    print '[-] Error: destination dataset has more negatives than the source.'
    #    return
    wanted_n_source_positive = int(destination_percent * n_source_negative / (1.0 - destination_percent))
    print '    [i] Want positive: %d' % wanted_n_source_positive
    print ''
    subsample_sizes = np.linspace(n_source_positive, wanted_n_source_positive, n_steps).astype(np.int)
    print '[i] Sub-sample sizes: %s' % ', '.join([str(item) for item in subsample_sizes])
    for sample_size in subsample_sizes[1:] :
        current_positive_indices = np.random.choice(source_positive_indices, sample_size, replace = False)
        current_ds = [source_ds[ind] for ind in current_positive_indices] + [source_ds[ind] for ind in source_negative_indices]
        n_total_current = len(current_ds)
        new_dataset_name = '%s_subsample_%d%s' % (source_dataset_name, sample_size, dataset_name_suffix)
        current_ds.sort(key = lambda x : x.index)
        print '    [+] Creating subsample: %d / %d (%s)' % (sample_size, n_total_current, new_dataset_name)
        if not os.path.exists('./datasets/current/%s' % new_dataset_name) :
            os.makedirs('./datasets/current/%s' % new_dataset_name)
        writer.write_pickle(current_ds, './datasets/current/%s/sequences.out' % new_dataset_name)

def make_datasets_from_groups(source_filename = '../data/55k_oligos_sequence_and_expression_measurements.tab', selection = ['native', 'human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_positive_uncapped', 'viral_native_ssRNA_positive_capped'], without_nan_splicing = False, without_nan_promoter = False, trim_ires_activity = False, splicing_threshold = -2.5, promoter_threshold = 0.2) :
    '''
    Construct sequence datasets and write them to disk.
    :param source_filename : path to the sequences file.
    :param selection : a list of groups that should be saved as datasets.
    :param without_nan_splicing : whether or not oligos with NaN splicing scores should be removed.
    :param trim_ires_activity : whether or not IRES activity should be trimmed. True/False or float to define the trimming quantile.
    :param splicing_threshold : threshold for splicing activity.
    :param promoter_threshold : threshold for promoter activity.
    '''
    all_seqs = reader.read_sequences(source_filename)
    all_seqs = tools.grow_sequences(all_seqs)
    all_seqs = tools.merge_ires_activity_replicates(all_seqs)
    all_seqs = tools.filter_sequences(all_seqs, remove_nan_splicing = without_nan_splicing, remove_nan_promoter = without_nan_promoter, splicing_threshold = splicing_threshold, promoter_threshold = promoter_threshold)
    all_seqs = tools.filter_replicates(all_seqs)
    if trim_ires_activity == True :
        all_seqs = tools.trim_ires_activity(all_seqs)
    elif isinstance(trim_ires_activity, float) :
        all_seqs = tools.trim_ires_activity(all_seqs, trim_ires_activity)
    for group in selection :
        print '[i] Creating group: %s' % group
        seqs = tools.get_group_sequences(all_seqs, group)
        if not os.path.exists('./datasets/current/%s' % group) :
            os.makedirs('./datasets/current/%s' % group)
        writer.write_pickle(seqs, './datasets/current/%s/sequences.out' % group)

def _initialize_virus_types() :
    '''
    Read the virus types definition file and initialize the corresponding dictionary.
    '''
    global virus_types
    virus_types = {}
    if not os.path.exists(_virus_type_filename) :
        return
    fin = open(_virus_type_filename, 'rb')
    lines = fin.readlines()
    fin.close()
    lines = lines[1:]
    for line in lines :
        if line[-1] == '.' :
            line = line[:-1]
        args = line.strip().split('\t')
        id = args[0]
        args = args[1].split(';')
        args = [arg.strip() for arg in args]
        virus_types[id] = args

def _initialize_virus_cds_coordinates() :
    '''
    Read the virus CDS coordinates file and initialize the corresponding dictionary.
    '''
    global virus_cds_coordinates
    virus_cds_coordinates = {}
    if not os.path.exists(_virus_cds_coordinates_filename) :
        return
    fin = open(_virus_cds_coordinates_filename, 'rb')
    lines = fin.readlines()
    fin.close()
    for line in lines :
        args = line.strip().split('\t')
        index = int(args[0])
        cds_start, cds_stop = int(args[5]) - 1, int(args[6])
        virus_cds_coordinates[index] = (cds_start, cds_stop)

def _initialize_structures() :
    '''
    Read predicted RNA structures from a file and initialize the dictionary.
    '''
    global structures
    if not os.path.exists(_structures_filename) :
        structures = {}
        return
    structures = reader.read_pickle(_structures_filename)

def _initialize_ddG() :
    '''
    Read ddG open from a file and initialize the dictionary.
    '''
    global ddG
    if not os.path.exists(_ddG_filename) :
        ddG = {}
        return
    ddG = reader.read_pickle(_ddG_filename)

def _initialize_dG0() :
    '''
    Read dG0 from a file and initialize the dictionary.
    '''
    global dG0
    if not os.path.exists(_dG0_filename) :
        dG0 = {}
        return
    dG0 = reader.read_pickle(_dG0_filename)

def _initialize_accessibility() :
    '''
    Read accessibility from a file and initialize the dictionary.
    '''
    global accessibility
    if not os.path.exists(_accessibility_filename) :
        accessibility = {}
        return
    accessibility = reader.read_pickle(_accessibility_filename)

def get_bppm() :
    '''
    Return summarise accessibility and interaction measures based on BPPM data.
    Read the dataset first, if required.
    '''
    global bppm
    if bppm is None :
        try :
            bppm = reader.read_pickle(_bppm_filename)
        except :
            pass
    return bppm

def _initialize() :
    '''
    Initialize the module
    '''
    _initialize_virus_types()
    _initialize_virus_cds_coordinates()
#    _initialize_structures()
    _initialize_ddG()
    _initialize_dG0()
    _initialize_accessibility()

datasets = DatasetDict()
precomputed = PrecomputedDict()
precomputed_structure = PrecomputedStructureDict()
#structures = None
ddG = None
accessibility = None
virus_types = None
virus_cds_coordinates = None
bppm = None

_initialize()
