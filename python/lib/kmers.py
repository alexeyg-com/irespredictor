'''
A module implementing some k-mer checking/searching routines.
Was copied from the Elongation-speed project.

Alexey Gritsenko
04-02-2015
'''

from ipyparallel import Client
import scipy.stats
import statsmodels.sandbox.stats.multicomp
import scipy.cluster
import numpy as np
import Bio.Seq

import structure
import persistent

import pysais

def _construct_motif(alphabet, motif_num) :
    '''
    Constructs a sequence motif from its numerical representation.
    :param alphabet : alphabet to use for reconstruction.
    :param motif_num : numerical representation of the motif.
    '''
    alphabet_size = len(alphabet)
    seq = []
    length = 0
    while motif_num >= alphabet_size ** (length + 1) :
        length += 1
        motif_num -= alphabet_size ** length
    length += 1
    for i in range(length) :
        num = motif_num % alphabet_size
        motif_num /= alphabet_size
        seq.append(alphabet[num])
    seq = ''.join(seq)
    return seq

def _package_parallel_tasks(dataset_name, indices, other_params, test_type, cutoff, tasks, chunk_size) :
    '''
    Package kmer tasks for parallel execution.
    :param dataset_name : name of the dataset that we should package tasks for.
    :param indices : indices of items in the dataset used to execute the tasks (used when pre-computing CV items).
    :param other_params : any object containing additional parameters passed with the task.
    :param test_type : type of statistical test to be performed for k-mer feature pre-selection.
    :param cutoff : p-value cutoff used for discarding results.
    :param tasks : a list of tasks that should be packaged.
    :param chunk_size : size of the packaged chunk.
    '''
    package = []
    i, n_tasks = 0, len(tasks)
    while i <= n_tasks :
        package.append((dataset_name, indices, other_params, test_type, cutoff, tasks[i: i + chunk_size]))
        i += chunk_size
    return package

def kmer_pvalue(counts, ires_activity, type = 'both') :
    '''
    Perform a series of tests to see if one of them is significant.
    :param counts : an array of counts (feature values).
    :param ires_activity : IRES activity corresponding to the features.
    '''
    if type == 'both' or type == 'spearman' :
        coef_r, spearman_pvalue = scipy.stats.spearmanr(counts, ires_activity)
    else :
        coef_r, spearman_pvalue = 0, 1
    
    ind_a = counts == 0
    ind_b = np.logical_not(ind_a)
    if np.sum(ind_a, dtype = np.int) == 0 or np.sum(ind_b, dtype = np.int) == 0 or type not in ['both', 'utest'] :
        u_stat, mannwhitney_pvalue = 0, 1
    else :
        u_stat, mannwhitney_pvalue = scipy.stats.mannwhitneyu(ires_activity[ind_a], ires_activity[ind_b])

    if spearman_pvalue < mannwhitney_pvalue :
        return coef_r, spearman_pvalue
    else :
        return u_stat, mannwhitney_pvalue

def _parallel_kmers(params) :
    '''
    A routine for computing kmer count Spearman correlations in the data. Used in parallel computations.
    :param params : routine parameters - (dataset_name, indices, other_params, cutoff, [motif]) tuple.
    '''
    import scipy.stats
    import lib.persistent as persistent
    import numpy as np
    from lib.kmers import kmer_pvalue
    result = []
    if isinstance(params, tuple) :
        params = [params]
    for dataset_name, indices, other_params, test_type, cutoff, tasks in params :
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
        for motif in tasks :
            #counts = pysais.count_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            if indices is not None :
                counts = counts[indices, :]
            counts = np.sum(counts, axis = 1, dtype = np.float)
            u_stat, pvalue = kmer_pvalue(counts, ires_activity, test_type)
            #coef_r, pvalue = scipy.stats.spearmanr(counts, ires_activity)
            usage = np.sum(counts > 0, dtype = np.int) / float(n_index_samples)
            if pvalue <= cutoff :
                result.append((motif, 'all', u_stat, pvalue, usage))
    return result

def _parallel_kmers_in_windows(params) :
    '''
    A routine for computing kmer count Spearman correlations in the data in windows. Used in parallel computations.
    :param params : routine parameters - (dataset_name, indices, (window_step, window_size), [motif]) tuple.
    '''
    import scipy.stats
    import lib.persistent as persistent
    import numpy as np
    from lib.kmers import kmer_pvalue
    result = []
    if isinstance(params, tuple) :
        params = [params]
    for dataset_name, indices, other_params, cutoff, test_type, tasks in params :
        window_step, window_size = other_params
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
        for motif in tasks :
            counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            if indices is not None :
                counts = counts[indices, :]
            rows, cols = counts.shape
            window_offset = 0
            while window_offset < cols :
                window_indices = np.array(range(window_offset, min(window_offset + window_size, cols)))
                window = (window_offset, min(window_offset + window_size, cols))
                feature = np.sum(counts[:, window_indices], axis = 1)
                #coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
                u_stat, pvalue = kmer_pvalue(feature, ires_activity, test_type)
                usage = np.sum(feature > 0, dtype = np.int) / float(n_index_samples)
                if pvalue <= cutoff :
                    result.append((motif, window, 'all', u_stat, pvalue, usage))
                window_offset += window_step
    return result

def look_for_kmers_in_windows(dataset_name, indices = None, alphabet = ['A', 'C', 'T', 'G'], min_motif_length = 1, max_motif_length = 6, window_size = 20, window_step = 10, batch_size = 10000000, client_url = None, chunk_size = 100, test_type = 'both', cutoff = 0.05) :
    '''
    Compute correlations between kmer counts and IRES activity in moving windows.
    :param dataset_name : name of the dataset (mean, std, seq) to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param alphabet : alphabet to use for constructing motifs.
    :param min_motif_length : minimum length of motifs to consider.
    :param max_motif_length : maximum length of motifs to consider.
    :param window_size : size of the window that should be considered.
    :param window_step : the number of bases by which a window should be moved.
    :param batch_size : size of the batch for parallel computing. Also determines how often intermediate results are written to a file.
    :param client_url : if not None, define URL of the IPython controller.
    :param chnuk_size : number of tasks to send per engine.
    :param test_type : type of statistical test to perform (spearman, utest or both).
    :param cutoff : pvalue cutoff used for selecting well-correlating motifs.
    '''
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100 

    alphabet_size = len(alphabet)
    start = 0 
    for i in range(1, min_motif_length) :
        start += alphabet_size ** i
    stop = 0 
    for i in range(1, max_motif_length + 1) :
        stop += alphabet_size ** i
    
    if view is None :
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.atleast_1d([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)

    result = []
    motif_num = start
    while motif_num < stop :
        batch_ind = 0
        if view is None :
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                    if indices is not None :
                        counts = counts[indices, :]
                    rows, cols = counts.shape
                    window_offset = 0
                    while window_offset < cols :
                        window_indices = np.array(range(window_offset, min(window_offset + window_size, cols)))
                        window = (window_offset, min(window_offset + window_size, cols))
                        feature = np.sum(counts[:, window_indices], axis = 1)
                        u_stat, pvalue = kmer_pvalue(feature, ires_activity, test_type)
                        #coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
                        usage = np.sum(feature > 0, dtype = np.int) / float(n_index_samples)
                        if pvalue <= cutoff :
                            result.append((motif, window, 'all', u_stat, pvalue, usage))
                        window_offset += window_step
                    batch_ind += 1
                motif_num += 1
        else :
            tasks = []
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    tasks.append(motif)
                    batch_ind += 1
                motif_num += 1
            tasks = _package_parallel_tasks(dataset_name, indices, (window_step, window_size), test_type, cutoff, tasks, chunk_size)
            map_results = view.map(_parallel_kmers_in_windows, tasks, block = True, ordered = True)
            client.purge_local_results('all')
            map_results = [res for res_group in map_results for res in res_group]
            for motif, window, frame, coef_r, pvalue, usage in map_results :
                if pvalue <= cutoff :
                    result.append((motif, window, frame, coef_r, pvalue, usage))
    if client is not None :
        del view
        client.close()
        del client
    return result

def look_for_kmers(dataset_name, indices = None, alphabet = ['A', 'C', 'T', 'G'], min_motif_length = 1, max_motif_length = 6, batch_size = 10000000, client_url = None, chunk_size = 100, test_type = 'both', cutoff = 0.05) : 
    ''' 
    Compute correlations between kmer counts and IRES activity.
    :param dataset_name : name of the dataset (mean, std, seq) to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param alphabet : alphabet to use for constructing motifs.
    :param min_motif_length : minimum length of motifs to consider.
    :param max_motif_length : maximum length of motifs to consider.
    :param batch_size : size of the batch for parallel computing. Also determines how often intermediate results are written to a file.
    :param client_url : if not None, define URL of the IPython controller.
    :param chnuk_size : number of tasks to send per engine.
    :param test_type : type of statistical test to be used for pre-selection (spearman, utest or both).
    :param cutoff : pvalue cutoff used for selecting well-correlating motifs.
    ''' 
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100 

    alphabet_size = len(alphabet)
    start = 0 
    for i in range(1, min_motif_length) :
        start += alphabet_size ** i
    stop = 0 
    for i in range(1, max_motif_length + 1) :
        stop += alphabet_size ** i

    if view is None :
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
    
    result = []
    motif_num = start
    while motif_num < stop :
        batch_ind = 0 
        if view is None :
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    #counts = pysais.count_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                    counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                    if not indices is None :
                        counts = counts[indices, :]
                    counts = np.sum(counts, axis = 1, dtype = np.float)
                    #coef_r, pvalue = scipy.stats.spearmanr(counts, ires_activity)
                    u_stat, pvalue = kmer_pvalue(counts, ires_activity, test_type)
                    usage = np.sum(counts > 0, dtype = np.int) / float(n_index_samples)
                    if pvalue <= cutoff :
                        result.append((motif, 'all', u_stat, pvalue, usage))
                    batch_ind += 1
                motif_num += 1
        else :
            tasks = []
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    tasks.append(motif)
                    batch_ind += 1
                motif_num += 1
            tasks = _package_parallel_tasks(dataset_name, indices, None, test_type, cutoff, tasks, chunk_size)
            map_results = view.map(_parallel_kmers, tasks, block = True, ordered = True)
            client.purge_local_results('all')
            map_results = [res for res_group in map_results for res in res_group]
            for motif, frame, coef_r, pvalue, usage in map_results :
                if pvalue <= cutoff :
                    result.append((motif, frame, coef_r, pvalue, usage))
            del map_results

    if client is not None :
        del view
        client.close()
        del client
    return result

def calculate_accessibility_weights(dataset, counts, motif) :
    '''
    Calculate k-mer accessibilities for a given count array.
    :param dataset : dataset for which accessibilities should be assessed.
    :param counts : an array of k-mer counts (presences).
    :param motif : the motif that should be assessed (we need its length only).
    '''
    accessibility = persistent.accessibility
    motif_length = len(motif)
    n_samples, n_positions = counts.shape
    weights = np.zeros((n_samples, n_positions), dtype = np.float)
    for i in range(n_samples) :
        index = dataset[i].index
        weights[i, :] = accessibility[index][:n_positions, motif_length - 1]
    return weights

def _parallel_accessible_kmers(params) :
    '''
    A routine for computing accessible kmer count Spearman correlations in the data. Used in parallel computations.
    :param params : routine parameters - (dataset_name, indices, other_params, cutoff, [motif]) tuple.
    '''
    import scipy.stats
    import lib.persistent as persistent
    import numpy as np
    from lib.kmers import kmer_pvalue, calculate_accessibility_weights
    result = []
    if isinstance(params, tuple) :
        params = [params]
    for dataset_name, indices, other_params, test_type, cutoff, tasks in params :
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
        for motif in tasks :
            counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            weights = calculate_accessibility_weights(dataset, counts, motif)
            if indices is not None :
                counts = counts[indices, :]
                weights = weights[indices, :]
            counts = counts * weights
            counts = np.sum(counts, axis = 1, dtype = np.float)
            u_stat, pvalue = kmer_pvalue(counts, ires_activity, test_type)
            #coef_r, pvalue = scipy.stats.spearmanr(counts, ires_activity)
            usage = np.sum(counts > 0, dtype = np.int) / float(n_index_samples)
            if pvalue <= cutoff :
                result.append((motif, 'all', u_stat, pvalue, usage))
    return result

def _parallel_accessible_kmers_in_windows(params) :
    '''
    A routine for computing kmer count Spearman correlations in the data in windows. Used in parallel computations.
    :param params : routine parameters - (dataset_name, indices, (window_step, window_size), [motif]) tuple.
    '''
    import scipy.stats
    import lib.persistent as persistent
    import numpy as np
    from lib.kmers import kmer_pvalue, calculate_accessibility_weights
    result = []
    if isinstance(params, tuple) :
        params = [params]
    for dataset_name, indices, other_params, test_type, cutoff, tasks in params :
        window_step, window_size = other_params
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
        for motif in tasks :
            counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
            weights = calculate_accessibility_weights(dataset, counts, motif)
            if indices is not None :
                counts = counts[indices, :]
                weights = weights[indices, :]
            counts = counts * weights
            rows, cols = counts.shape
            window_offset = 0
            while window_offset < cols :
                window_indices = np.array(range(window_offset, min(window_offset + window_size, cols)))
                window = (window_offset, min(window_offset + window_size, cols))
                feature = np.sum(counts[:, window_indices], axis = 1)
                #coef_r, pvalue = scipy.stats.spearmanr(feature, ires_activity)
                u_stat, pvalue = kmer_pvalue(feature, ires_activity, test_type)
                usage = np.sum(feature > 0, dtype = np.int) / float(n_index_samples)
                if pvalue <= cutoff :
                    result.append((motif, window, 'all', u_stat, pvalue, usage))
                window_offset += window_step
    return result

def look_for_accessible_kmers_in_windows(dataset_name, indices = None, alphabet = ['A', 'C', 'T', 'G'], min_motif_length = 1, max_motif_length = 6, window_size = 20, window_step = 10, batch_size = 10000000, client_url = None, chunk_size = 100, test_type = 'both', cutoff = 0.05) :
    '''
    Compute correlations between accessible kmer counts and IRES activity in moving windows.
    :param dataset_name : name of the dataset (mean, std, seq) to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param alphabet : alphabet to use for constructing motifs.
    :param min_motif_length : minimum length of motifs to consider.
    :param max_motif_length : maximum length of motifs to consider.
    :param window_size : size of the window that should be considered.
    :param window_step : the number of bases by which a window should be moved.
    :param batch_size : size of the batch for parallel computing. Also determines how often intermediate results are written to a file.
    :param client_url : if not None, define URL of the IPython controller.
    :param chnuk_size : number of tasks to send per engine.
    :param test_type : type of statistical test to perform for k-mer pre-selection (spearman, utest or both).
    :param cutoff : pvalue cutoff used for selecting well-correlating motifs.
    '''
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100 

    alphabet_size = len(alphabet)
    start = 0 
    for i in range(1, min_motif_length) :
        start += alphabet_size ** i
    stop = 0 
    for i in range(1, max_motif_length + 1) :
        stop += alphabet_size ** i
    
    if view is None :
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.atleast_1d([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)

    result = []
    motif_num = start
    while motif_num < stop :
        batch_ind = 0
        if view is None :
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                    weights = calculate_accessibility_weights(dataset, counts, motif)
                    if indices is not None :
                        counts = counts[indices, :]
                        weights = weights[indices, :]
                    counts = counts * weights
                    rows, cols = counts.shape
                    window_offset = 0
                    while window_offset < cols :
                        window_indices = np.array(range(window_offset, min(window_offset + window_size, cols)))
                        window = (window_offset, min(window_offset + window_size, cols))
                        feature = np.sum(counts[:, window_indices], axis = 1)
                        u_stat, pvalue = kmer_pvalue(feature, ires_activity, test_type)
                        usage = np.sum(feature > 0, dtype = np.int) / float(n_index_samples)
                        if pvalue <= cutoff :
                            result.append((motif, window, 'all', u_stat, pvalue, usage))
                        window_offset += window_step
                    batch_ind += 1
                motif_num += 1
        else :
            tasks = []
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    tasks.append(motif)
                    batch_ind += 1
                motif_num += 1
            tasks = _package_parallel_tasks(dataset_name, indices, (window_step, window_size), test_type, cutoff, tasks, chunk_size)
            map_results = view.map(_parallel_accessible_kmers_in_windows, tasks, block = True, ordered = True)
            client.purge_local_results('all')
            map_results = [res for res_group in map_results for res in res_group]
            for motif, window, frame, coef_r, pvalue, usage in map_results :
                if pvalue <= cutoff :
                    result.append((motif, window, frame, coef_r, pvalue, usage))
    if client is not None :
        del view
        client.close()
        del client
    return result

def look_for_accessible_kmers(dataset_name, indices = None, alphabet = ['A', 'C', 'T', 'G'], min_motif_length = 1, max_motif_length = 6, batch_size = 10000000, client_url = None, chunk_size = 100, test_type = 'both', cutoff = 0.05) : 
    ''' 
    Compute correlations between kmer counts and IRES activity.
    :param dataset_name : name of the dataset (mean, std, seq) to be analysed.
    :param indices : indices of items that should participate in the correlation.
    :param alphabet : alphabet to use for constructing motifs.
    :param min_motif_length : minimum length of motifs to consider.
    :param max_motif_length : maximum length of motifs to consider.
    :param batch_size : size of the batch for parallel computing. Also determines how often intermediate results are written to a file.
    :param client_url : if not None, define URL of the IPython controller.
    :param chnuk_size : number of tasks to send per engine.
    :param test_type : type of statistical test to perform for k-mer pre-selection (spearman, utest or both).
    :param cutoff : pvalue cutoff used for selecting well-correlating motifs.
    ''' 
    if client_url is None :
        client, view = None, None
    else :
        client = Client(client_url)
        view = client.load_balanced_view()
        view.retries = 100 

    alphabet_size = len(alphabet)
    start = 0 
    for i in range(1, min_motif_length) :
        start += alphabet_size ** i
    stop = 0 
    for i in range(1, max_motif_length + 1) :
        stop += alphabet_size ** i

    if view is None :
        dataset = persistent.datasets[dataset_name]
        n_samples = len(dataset)
        str_length = len(dataset[0].sequence)
        sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
        ires_activity = np.array([seq.ires_activity for seq in dataset])
        n_index_samples = n_samples
        if indices is not None :
            ires_activity = ires_activity[indices]
            n_index_samples = len(indices)
    
    result = []
    motif_num = start
    while motif_num < stop :
        batch_ind = 0 
        if view is None :
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    #counts = pysais.count_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                    counts = pysais.count_position_occurrences(sequence, motif, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
                    weights = calculate_accessibility_weights(dataset, counts, motif)
                    if indices is not None :
                        counts = counts[indices, :]
                        weights = weights[indices, :]
                    counts = counts * weights
                    counts = np.sum(counts, axis = 1, dtype = np.float)
                    #coef_r, pvalue = scipy.stats.spearmanr(counts, ires_activity)
                    u_stat, pvalue = kmer_pvalue(counts, ires_activity, test_type)
                    usage = np.sum(counts > 0, dtype = np.int) / float(n_index_samples)
                    if pvalue <= cutoff :
                        result.append((motif, 'all', u_stat, pvalue, usage))
                    batch_ind += 1
                motif_num += 1
        else :
            tasks = []
            while batch_ind < batch_size and motif_num < stop :
                motif = _construct_motif(alphabet, motif_num)
                can_continue = True
                if can_continue :
                    tasks.append(motif)
                    batch_ind += 1
                motif_num += 1
            tasks = _package_parallel_tasks(dataset_name, indices, None, test_type, cutoff, tasks, chunk_size)
            map_results = view.map(_parallel_accessible_kmers, tasks, block = True, ordered = True)
            client.purge_local_results('all')
            map_results = [res for res_group in map_results for res in res_group]
            for motif, frame, coef_r, pvalue, usage in map_results :
                if pvalue <= cutoff :
                    result.append((motif, frame, coef_r, pvalue, usage))
            del map_results

    if client is not None :
        del view
        client.close()
        del client
    return result

