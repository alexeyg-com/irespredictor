'''
A module containing routines useful for regression.

05-02-2015
Alexey Gritsenko
'''

import numpy as np
import scipy.sparse
import scipy.stats
import copy
from ipyparallel import Client
import sklearn.metrics as metrics

import tools
import kmers
import experimental
import structure
import reader
import persistent
import gbrf
import svr
import fetcher
from SimpleNamespace import SimpleNamespace
from kmers import calculate_accessibility_weights

import pysais

## 2 400
_grid_search_settings_eran_refined2 = {
                         'learning_rate' : [0.001, 0.002, 0.004, 0.008],                       # 4
                         'max_depth' : [None],
                         'min_samples_leaf' : [5, 25, 125],                                    # 3
                         'max_features' : ['sqrt'],
                         'subsample' : [0.9, 0.7],                                             # 1
                         'huber_alpha' : [None]}                                             

## 2 400
_grid_search_settings_eran_refined2_stumps = {
                         'learning_rate' : [0.001, 0.002, 0.004, 0.008],                       # 4
                         'max_depth' : [1],
                         'min_samples_leaf' : [5, 25, 125],                                    # 3
                         'max_features' : ['sqrt'],
                         'subsample' : [0.9, 0.7],                                             # 2
                         'huber_alpha' : [None]}

## 14 400
_grid_search_settings_eran_refined2_huber = {
                         'learning_rate' : [0.001, 0.002, 0.004, 0.008],                       # 4
                         'max_depth' : [None],
                         'min_samples_leaf' : [5, 25, 125],                                    # 3
                         'max_features' : ['sqrt'],
                         'subsample' : [0.9, 0.7],                                             # 2
                         'huber_alpha' : [0.70, 0.75, 0.80, 0.85, 0.90, 0.95]}                 # 6

## SVR
## 2 400
_svr_grid_search_settings_pcb = {
                         'C' : [0.001, 0.003, 0.009, 0.027, 0.081, 0.243],                     # 6
                         'epsilon' : [0.027, 0.081, 0.243, 0.729]}                             # 4

## SVR
## 5 600
_svr_grid_search_settings_pcb_extended = {
                         'C' : [0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064, 0.128],               # 8
                         'epsilon' : [0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064, 0.128, 0.256]}  # 9

_grid_search_settings = _grid_search_settings_eran_refined2
_svr_grid_search_settings = _svr_grid_search_settings_pcb

def build_kmer_regression_matrix(dataset_name, kmers) :
    ''' 
    Build a kmer regression matrix X for a given dataset. The matrix is stored as list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param kmers : a list of kmers that should be used for regression.
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    str_length = len(dataset[0].sequence)
    sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
    n_features = len(kmers)
    X = []
    for i in range(n_features) :
        kmer = kmers[i][0]
        counts = pysais.count_position_occurrences(sequence, kmer, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
        counts = np.sum(counts, axis = 1, dtype = np.float)
        feature_dict = {}
        indices = np.where(counts != 0)
        indices = indices[0]
        for index in indices :
            feature_dict[index] = counts[index]
        X.append(feature_dict)
    return X

def build_kmer_presence_regression_matrix(dataset_name, kmers) :
    ''' 
    Build a kmer regression matrix X for a given dataset. The matrix is stored as list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param kmers : a list of kmers that should be used for regression.
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    str_length = len(dataset[0].sequence)
    sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
    n_features = len(kmers)
    X = []
    for i in range(n_features) :
        kmer = kmers[i][0]
        counts = pysais.count_position_occurrences(sequence, kmer, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
        counts = np.sum(counts, axis = 1, dtype = np.float)
        feature_dict = {}
        indices = np.where(counts != 0)
        indices = indices[0]
        for index in indices :
            feature_dict[index] = 1 if counts[index] > 0 else 0
        X.append(feature_dict)
    return X

def build_accessible_kmer_regression_matrix(dataset_name, kmers) :
    ''' 
    Build a kmer regression matrix X for a given dataset. The matrix is stored as list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param kmers : a list of kmers that should be used for regression.
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    str_length = len(dataset[0].sequence)
    sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
    n_features = len(kmers)
    X = []
    for i in range(n_features) :
        kmer = kmers[i][0]
        counts = pysais.count_position_occurrences(sequence, kmer, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
        weights = calculate_accessibility_weights(dataset, counts, kmer)
        counts = counts * weights
        counts = np.sum(counts, axis = 1, dtype = np.float)
        feature_dict = {}
        indices = np.where(counts != 0)
        indices = indices[0]
        for index in indices :
            feature_dict[index] = counts[index]
        X.append(feature_dict)
    return X

def build_kmer_in_windows_regression_matrix(dataset_name, kmers) :
    ''' 
    Build a windowed kmer regression matrix X for a given dataset. The matrix is stored as list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param kmers : a list of kmers that should be used for regression.
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    str_length = len(dataset[0].sequence)
    sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
    n_features = len(kmers)
    X = []
    for i in range(n_features) :
        kmer = kmers[i][0]
        window = kmers[i][1]
        counts = pysais.count_position_occurrences(sequence, kmer, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
        window_start, window_stop = window
        rows, cols = counts.shape
        window_stop = min(window_stop, cols)
        window_indices = np.array(range(window_start, window_stop))
        feature = np.sum(counts[:, window_indices], axis = 1, dtype = np.float)
        feature_dict = {}
        indices = np.where(feature != 0)
        indices = indices[0]
        for index in indices :
            feature_dict[index] = feature[index]
        X.append(feature_dict)
    return X

def build_kmer_presence_in_windows_regression_matrix(dataset_name, kmers) :
    ''' 
    Build a windowed kmer regression matrix X for a given dataset. The matrix is stored as list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param kmers : a list of kmers that should be used for regression.
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    str_length = len(dataset[0].sequence)
    sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
    n_features = len(kmers)
    X = []
    for i in range(n_features) :
        kmer = kmers[i][0]
        window = kmers[i][1]
        counts = pysais.count_position_occurrences(sequence, kmer, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
        window_start, window_stop = window
        rows, cols = counts.shape
        window_stop = min(window_stop, cols)
        window_indices = np.array(range(window_start, window_stop))
        feature = np.sum(counts[:, window_indices], axis = 1, dtype = np.float)
        feature_dict = {}
        indices = np.where(feature != 0)
        indices = indices[0]
        for index in indices :
            feature_dict[index] = 1 if feature[index] > 0 else 0
        X.append(feature_dict)
    return X

def build_accessible_kmer_in_windows_regression_matrix(dataset_name, kmers) :
    ''' 
    Build a windowed kmer regression matrix X for a given dataset. The matrix is stored as list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param kmers : a list of kmers that should be used for regression.
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    str_length = len(dataset[0].sequence)
    sequence, assignment, sa, lcp, lcp_left, lcp_right = persistent.precomputed[dataset_name]
    n_features = len(kmers)
    X = []
    for i in range(n_features) :
        kmer = kmers[i][0]
        window = kmers[i][1]
        counts = pysais.count_position_occurrences(sequence, kmer, assignment, sa, lcp, lcp_left, lcp_right, n_samples, str_length)
        weights = calculate_accessibility_weights(dataset, counts, kmer)
        counts = counts * weights
        window_start, window_stop = window
        rows, cols = counts.shape
        window_stop = min(window_stop, cols)
        window_indices = np.array(range(window_start, window_stop))
        feature = np.sum(counts[:, window_indices], axis = 1, dtype = np.float)
        feature_dict = {}
        indices = np.where(feature != 0)
        indices = indices[0]
        for index in indices :
            feature_dict[index] = feature[index]
        X.append(feature_dict)
    return X

def build_bppm_regression_matrix(dataset_name, features, structures = None, window_step = 5, window_size = 10) :
    '''
    Build BPPM regression matrix.
    :param dataset_name : name of the dataset for which regression matrix should be generated.
    :param features : a list of features that should be used for regression matrix generation.
    :param structures : a dictionary of structures to used for creating 
    :param window_step : step of the moving window.
    :param window_size : size of the moving window.
    '''
    if structures is None :
        structures = persistent.get_bppm()
    seqs = persistent.datasets[dataset_name]
    X = []
    for feature in features :
        feature_dict = {}
        window = feature[0]
        for i, seq in enumerate(seqs) :
            feature_dict[i] = structures[seq.index][1][window]
        X.append(feature_dict)
    return X

def build_bppm_pair_regression_matrix(dataset_name, features, structures = None, window_step = 5, window_size = 10) :
    '''
    Build BPPM pair regression matrix.
    :param dataset_name : name of the dataset for which regression matrix should be generated.
    :param features : a list of features that should be used for regression matrix generation.
    :param structures : a dictionary of structures to used for creating 
    :param window_step : step of the moving window.
    :param window_size : size of the moving window.
    '''
    if structures is None :
        structures = persistent.get_bppm()
    seqs = persistent.datasets[dataset_name]
    X = []
    for feature in features :
        feature_dict = {}
        window = feature[0]
        for i, seq in enumerate(seqs) :
            feature_dict[i] = structures[seq.index][2][window]
        X.append(feature_dict)
    return X

def correct_for_multiple_testing(features, min_kmer_length, max_kmer_length, minimum_fraction, cutoff, method = 'fdr_bh') :
    '''
    Correct feature tests for multiple testing.
    :param features : list of features to be corrected (and selected from).
    :param min_kmer_length : minimum allowed length of k-mer.
    :param max_kmer_length : maximum allowed length of k-mer.
    :param minimum_fraction : minimum fraction of presence in the data.
    :param cutoff : p-value cutoff to be used.
    :param method : method for multiple testing correction to be used.
    '''
    from statsmodels.sandbox.stats.multicomp import multipletests
    if min_kmer_length is not None and max_kmer_length is not None :
        features = [feature for feature in features if feature[-1] >= minimum_fraction and len(feature[0]) >= min_kmer_length and len(feature[0]) <= max_kmer_length]
    else :
        features = [feature for feature in features if feature[-1] >= minimum_fraction]

    new_features = []
    if method == 'none' or method is None :
        for feature in features :
            if feature[-2] < cutoff :
                new_features.append(feature)
    else :
        pvalues = [feature[-2] for feature in features]
        _, corrected_pvalues, _, _ = multipletests(pvalues, alpha = cutoff, method = method)
        for feature, pvalue in zip(features, corrected_pvalues) :
            if pvalue < cutoff :
                feature = list(feature)
                feature[-2] = pvalue
                feature = tuple(feature)
                new_features.append(feature)
    return new_features

def build_regression_matrix_list(select_dataset_name, features, fold = None, use_aux = True, compute_dataset_name = None, client_url = None) :
    '''
    Builds a regression matrix from feature descriptions.
    :param dataset_name : name of the dataset that should be regressed.
    :param features : a list of feature descriptions.
    :param fold : number of the outer CV folds (used only together with use_aux = True) or a numpy array of indices.
    :param use_aux : a boolean flag determining whether it is allowed to use precomputed and stored values.
    :param client_url : IPython parallel client URL to be used in regression.
    '''
    # Pull the datasets if we don't have them
    if use_aux :
        fetcher.fetch_dataset_crossvalidation(select_dataset_name)

    if compute_dataset_name is None :
        compute_dataset_name = select_dataset_name

    Xs = []
    feature_names = []
    for feature in features :
        feature_type = feature.get('feature_type', None)
        cutoff = feature.get('cutoff', 1.0)
        min_motif_length = feature.get('min_motif_length', 1)
        max_motif_length = feature.get('max_motif_length', None)
        minimum_fraction = feature.get('minimum_fraction', 0.0)
        correction_method = feature.get('correction_method', None)
        if feature_type == 'kmers' :
            alphabet = feature.get('alphabet', ['A', 'C', 'T', 'G'])
            if not use_aux :
                selected = kmers.look_for_kmers(select_dataset_name, indices = fold, alphabet = alphabet, min_motif_length = min_motif_length, max_motif_length = max_motif_length, cutoff = cutoff, client_url = None)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-kmers.out' % (select_dataset_name, fold[0], fold[1])
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-kmers.out' % (select_dataset_name, fold)
                else :
                    filename = './datasets/current/%s/kmers.out' % select_dataset_name
                selected = reader.read_pickle(filename)
            selected = correct_for_multiple_testing(selected, min_kmer_length = min_motif_length, max_kmer_length = max_motif_length, minimum_fraction = minimum_fraction, cutoff = cutoff, method = correction_method)
            selected_names = ['count-%s-RF_%s' % (kmer[0], str(kmer[1])) for kmer in selected]
            X = build_kmer_regression_matrix(compute_dataset_name, selected)
        elif feature_type == 'kmers-windows' :
            alphabet = feature.get('alphabet', ['A', 'C', 'T', 'G'])
            window_size = feature.get('window_size', 20)
            window_step = feature.get('window_step', 10)
            if not use_aux :
                selected = kmers.look_for_kmers_in_windows(select_dataset_name, indices = fold, alphabet = alphabet, min_motif_length = min_motif_length, max_motif_length = max_motif_length, window_step = window_step, window_size = window_size, cutoff = 1.0, client_url = None)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-kmers-windows%d_step%d.out' % (select_dataset_name, fold[0], fold[1], window_size, window_step)
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-kmers-windows%s_step%d.out' % (select_dataset_name, fold, window_size, window_step)
                else :
                    filename = './datasets/current/%s/kmers-windows%d_step%d.out' % (select_dataset_name, window_size, window_step)
                selected = reader.read_pickle(filename)
            selected = correct_for_multiple_testing(selected, min_kmer_length = min_motif_length, max_kmer_length = max_motif_length, minimum_fraction = minimum_fraction, cutoff = cutoff, method = correction_method)
            selected_names = ['count-%s-RF_%s-window_%d_%d' % (kmer[0], str(kmer[2]), kmer[1][0], kmer[1][1]) for kmer in selected]
            X = build_kmer_in_windows_regression_matrix(compute_dataset_name, selected)
        elif feature_type == 'kmers_presence' :
            alphabet = feature.get('alphabet', ['A', 'C', 'T', 'G'])
            if not use_aux :
                selected = kmers.look_for_kmers(select_dataset_name, indices = fold, alphabet = alphabet, min_motif_length = min_motif_length, max_motif_length = max_motif_length, test_type = 'utest', cutoff = cutoff, client_url = None)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-kmers_presence.out' % (select_dataset_name, fold[0], fold[1])
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-kmers_presence.out' % (select_dataset_name, fold)
                else :
                    filename = './datasets/current/%s/kmers.out' % select_dataset_name
                selected = reader.read_pickle(filename)
            selected = correct_for_multiple_testing(selected, min_kmer_length = min_motif_length, max_kmer_length = max_motif_length, minimum_fraction = minimum_fraction, cutoff = cutoff, method = correction_method)
            selected_names = ['presence-%s-RF_%s' % (kmer[0], str(kmer[1])) for kmer in selected]
            X = build_kmer_presence_regression_matrix(compute_dataset_name, selected)
        elif feature_type == 'kmers_presence-windows' :
            alphabet = feature.get('alphabet', ['A', 'C', 'T', 'G'])
            window_size = feature.get('window_size', 20)
            window_step = feature.get('window_step', 10)
            if not use_aux :
                selected = kmers.look_for_kmers_in_windows(select_dataset_name, indices = fold, alphabet = alphabet, min_motif_length = min_motif_length, max_motif_length = max_motif_length, window_step = window_step, window_size = window_size, test_type = 'utest', cutoff = 1.0, client_url = None)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-kmers_presence-windows%d_step%d.out' % (select_dataset_name, fold[0], fold[1], window_size, window_step)
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-kmers_presence-windows%s_step%d.out' % (select_dataset_name, fold, window_size, window_step)
                else :
                    filename = './datasets/current/%s/kmers_presence-windows%d_step%d.out' % (select_dataset_name, window_size, window_step)
                selected = reader.read_pickle(filename)
            selected = correct_for_multiple_testing(selected, min_kmer_length = min_motif_length, max_kmer_length = max_motif_length, minimum_fraction = minimum_fraction, cutoff = cutoff, method = correction_method)
            selected_names = ['presence-%s-RF_%s-window_%d_%d' % (kmer[0], str(kmer[2]), kmer[1][0], kmer[1][1]) for kmer in selected]
            X = build_kmer_presence_in_windows_regression_matrix(compute_dataset_name, selected)
        elif feature_type == 'accessible_kmers' :
            alphabet = feature.get('alphabet', ['A', 'C', 'T', 'G'])
            if not use_aux :
                selected = kmers.look_for_accessible_kmers(select_dataset_name, indices = fold, alphabet = alphabet, min_motif_length = min_motif_length, max_motif_length = max_motif_length, cutoff = cutoff, client_url = None)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-accessible_kmers.out' % (select_dataset_name, fold[0], fold[1])
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-accessible_kmers.out' % (select_dataset_name, fold)
                else :
                    filename = './datasets/current/%s/accessible_kmers.out' % select_dataset_name
                selected = reader.read_pickle(filename)
            selected = correct_for_multiple_testing(selected, min_kmer_length = min_motif_length, max_kmer_length = max_motif_length, minimum_fraction = minimum_fraction, cutoff = cutoff, method = correction_method)
            selected_names = ['accessible_count-%s-RF_%s' % (kmer[0], str(kmer[1])) for kmer in selected]
            X = build_accessible_kmer_regression_matrix(compute_dataset_name, selected)
        elif feature_type == 'accessible_kmers-windows' :
            alphabet = feature.get('alphabet', ['A', 'C', 'T', 'G'])
            window_size = feature.get('window_size', 20)
            window_step = feature.get('window_step', 10)
            if not use_aux :
                selected = kmers.look_for_accessible_kmers_in_windows(select_dataset_name, indices = fold, alphabet = alphabet, min_motif_length = min_motif_length, max_motif_length = max_motif_length, window_step = window_step, window_size = window_size, cutoff = 1.0, client_url = None)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-accessible_kmers-windows%d_step%d.out' % (select_dataset_name, fold[0], fold[1], window_size, window_step)
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-accessible_kmers-windows%s_step%d.out' % (select_dataset_name, fold, window_size, window_step)
                else :
                    filename = './datasets/current/%s/accessible_kmers-windows%d_step%d.out' % (select_dataset_name, window_size, window_step)
                selected = reader.read_pickle(filename)
            selected = correct_for_multiple_testing(selected, min_kmer_length = min_motif_length, max_kmer_length = max_motif_length, minimum_fraction = minimum_fraction, cutoff = cutoff, method = correction_method)
            selected_names = ['accessible_count-%s-RF_%s-window_%d_%d' % (kmer[0], str(kmer[2]), kmer[1][0], kmer[1][1]) for kmer in selected]
            X = build_accessible_kmer_in_windows_regression_matrix(compute_dataset_name, selected)
        elif feature_type == 'bppm' :
            window_size = feature.get('window_size', 10)
            window_step = feature.get('window_step', 5)
            if not use_aux :
                selected = structure.look_for_bppm_features(select_dataset_name, indices = fold, window_step = window_step, window_size = window_size, cutoff = cutoff)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-bppm-windows%d_step%d.out' % (select_dataset_name, fold[0], fold[1], window_size, window_step)
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-bppm-windows%d_step%d.out' % (select_dataset_name, fold, window_size, window_step)
                else :
                    filename = './datasets/current/%s/bppm-windows%d_step%d.out' % (select_dataset_name, window_size, window_step)
                selected = reader.read_pickle(filename)
            selected = [feature for feature in selected if feature[-2] <= cutoff]
            selected_names = ['bppm-window_%d_%d' % feature[0] for feature in selected]
            X = build_bppm_regression_matrix(compute_dataset_name, selected, window_step = window_step, window_size = window_size)
        elif feature_type == 'bppm_pair' :
            window_size = feature.get('window_size', 10)
            window_step = feature.get('window_step', 5)
            if not use_aux :
                selected = structure.look_for_bppm_pair_features(select_dataset_name, indices = fold, window_step = window_step, window_size = window_size, cutoff = cutoff)
            else :
                if isinstance(fold, tuple) :
                    filename = './datasets/current/%s/cv/fold%d-%d-bppm-pair-windows%d_step%d.out' % (select_dataset_name, fold[0], fold[1], window_size, window_step)
                elif isinstance(fold, int) :
                    filename = './datasets/current/%s/cv/fold%d-bppm-pair-windows%d_step%d.out' % (select_dataset_name, fold, window_size, window_step)
                else :
                    filename = './datasets/current/%s/bppm-pair-windows%d_step%d.out' % (select_dataset_name, window_size, window_step)
                selected = reader.read_pickle(filename)
            selected = [feature for feature in selected if feature[-2] <= cutoff]
            selected_names = ['bppm-pair-window_%d_%d_%d_%d' % feature[0] for feature in selected]
            X = build_bppm_pair_regression_matrix(compute_dataset_name, selected, window_step = window_step, window_size = window_size)
        del selected
        Xs.append(X)
        feature_names.append(selected_names)
    return Xs, feature_names

def build_regression_matrix(Xs, dataset_name, log2 = True, output = 'csc', allow_empty = False) :
    '''
    Builds a combined regression matrix X and a response vector y for a given list of regression matrices, a dataset.
    :param Xs : a list of regression matrices given as a list of column dictionaries.
    :param dataset_name : name of the dataset to be used.
    :param log2 : whether a log2 transformation should be applied to the response variable.
    :param output : type of the output to use.
    :param allow_empty : a boolean flag determining whether empty regression matrices are allowed. If not - empty matrices are appended with an 1 +/- EPS column (EPS is added or subtracted to introduce variance).
    '''
    dataset = persistent.datasets[dataset_name]
    n_samples = len(dataset)
    total_features = 0
    for sub_X in Xs :
        total_features += len(sub_X)
    if output == 'csc' :
        data, i, j = [], [], []
        feature_offset = 0
        for sub_X in Xs :
            for column_index, column in enumerate(sub_X) :
                for row_index, value in column.iteritems() :
                    data.append(value)
                    i.append(row_index)
                    j.append(feature_offset + column_index)
            feature_offset += len(sub_X)
        if total_features == 0 and not allow_empty :
            for row in range(n_samples) :
                data.append(1.0 + tools.EPS * (-1) ** row)
            i.extend(range(n_samples))
            j.extend([0] * n_samples)
            total_features += 1
        data = np.array(data)
        ij = np.array([i, j])
        del i, j
        X = scipy.sparse.csc_matrix((data, ij), (n_samples, total_features), dtype = np.float)
    elif output == 'row-dict' :
        X = []
        for i in xrange(n_samples) :
            X.append(dict())
        feature_offset = 0
        for sub_X in Xs :
            for column_index, column in enumerate(sub_X) :
                for row_index, value in column.iteritems() :
                    X[row_index][feature_offset + column_index] = value
            feature_offset += len(sub_X)
        if total_features == 0 and not allow_empty :
            for i in range(n_samples) :
                X[i][0] = 1.0 + (-1) ** i * tools.EPS
        X = np.array(X)
    else :
        raise Exception('Unknown output format requested: %s' % output)
    y = np.array([seq.ires_activity for seq in dataset])
    if log2 :
        y = np.log2(y)
    return X, y

def _parallel_train_gbrf(param) :
    '''
    Runs GBRF regression in parallel for a single set of data/fold.
    '''
    import lib.gbrf as gbrf
    import lib.regression as regression
    dataset_name, features, log2, train_index, test_index, use_aux, learn_intercept, n_estimators, min_samples_leaf, learning_rate, subsample, max_features, max_depth, huber_alpha, scoring, fold, return_regressor = param
    if use_aux :
        Xs, feature_names = regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = fold)
    else :
        Xs, feature_names  = regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
    X, y = regression.build_regression_matrix(Xs, dataset_name, log2 = log2)
    del Xs
    n_features = X.shape[1]
    result = gbrf.train_gbrf_cv_fold(X, y, train_index, test_index, learn_intercept = learn_intercept, n_estimators = n_estimators, min_samples_leaf = min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, max_depth = max_depth, huber_alpha = huber_alpha, scoring = scoring, return_regressor = return_regressor)
    if not return_regressor :
        train_score, test_score, true_y, predicted_y, time = result
    else :
        regressor, train_score, test_score, true_y, predicted_y, time = result
    del X
    del y
    if not return_regressor :
        return train_score, test_score, true_y, predicted_y, n_features, feature_names, time
    else :
        feature_names = [name for name_group in feature_names for name in name_group]
        return regressor, train_score, test_score, true_y, predicted_y, n_features, feature_names, time

def submit_gbrf_tasks(client, tasks) :
    '''
    Submit async parallel GBRF tasks to the client.
    :param client : client or client_url to be used.
    :param tasks : a list of tasks.
    '''
    if isinstance(client, basestring) :
        client = Client(client)
    view = client.load_balanced_view()
    view.retries = 100
    results = view.map(_parallel_train_gbrf, tasks, block = False, ordered = True)
    return results

def _parallel_train_svr(param) :
    '''
    Runs SVR regression in parallel for a single set of data/fold.
    '''
    import lib.svr as svr
    import lib.regression as regression
    
    dataset_name, features, log2, train_index, test_index, use_aux, C, epsilon, normalize, scoring, fold, return_regressor = param
    if use_aux :
        Xs, feature_names = regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = fold)
    else :
        Xs, feature_names  = regression.build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
    X, y = regression.build_regression_matrix(Xs, dataset_name, log2 = log2)
    del Xs
    n_features = X.shape[1]
    result = svr.train_svr_cv_fold(X, y, train_index, test_index, C=C, epsilon=epsilon, normalize=normalize, scoring = scoring, return_regressor = return_regressor)
    if not return_regressor :
        train_score, test_score, true_y, predicted_y, time = result
    else :
        regressor, scaling, train_score, test_score, true_y, predicted_y, time = result
    del X
    del y
    if not return_regressor :
        return train_score, test_score, true_y, predicted_y, n_features, feature_names, time
    else :
        feature_names = [name for name_group in feature_names for name in name_group]
        return regressor, scaling, train_score, test_score, true_y, predicted_y, n_features, feature_names, time

def submit_svr_tasks(client, tasks) :
    '''
    Submit async parallel SVR tasks to the client.
    :param client : client or client_url to be used.
    :param tasks : a list of tasks.
    '''
    if isinstance(client, basestring) :
        client = Client(client)
    view = client.load_balanced_view()
    view.retries = 100
    #view.retries = 0
    results = view.map(_parallel_train_svr, tasks, block = False, ordered = True)
    return results

def train_gbrf_inner_cv(dataset_name, features, log2 = True, learn_intercept = False, use_aux = False, n_estimators = 500, grid_search = _grid_search_settings, scoring = gbrf.REGRESSION_METRICS, cv_folds = None, outer_fold = None, client_url = None) :
    '''
    Compute GBRT regression on a given dataset - full dataset or outer fold of CV (variable pre-selection is done on the inner fold dataset, then training is performed).
    :param features : features to be used when constructing the regression matrix.
    :param log2 : whether a log2 transformation should be applied to the response variable.
    :param learn_intercept : whether we should explicitly learn the intercept.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param n_estimators : number of estimators to learn.
    :param grid_search : a dictionary of settings to be probed in grid search.
    :param scoring : scoring function to be used.
    :param cv_folds : an iterator (or list) of CV index pairs or a number of folds to be used.
    :param client_url : URL of an IPython client to be used for parallel computing.
    '''
    if not isinstance(scoring, list) :
        scoring = [scoring]

    result = SimpleNamespace()
    if isinstance(cv_folds, int) :
        n_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        values = np.array([seq.ires_activity for seq in dataset])
        cv_folds = crossvalidation.stratified_KFold(values, n_folds)
    else :
        n_folds = len(cv_folds)
   
    setting_learning_rate = grid_search.get('learning_rate', [0.1])
    setting_max_depth = grid_search.get('max_depth', [5])
    setting_min_samples_leaf = grid_search.get('min_samples_leaf', [1])
    setting_max_features = grid_search.get('max_features', [0.5])
    setting_subsample = grid_search.get('subsample', [0.7])
    setting_huber_alpha = grid_search.get('huber_alpha', [0.9])
    n_learning_rate = len(setting_learning_rate)
    n_max_depth = len(setting_max_depth)
    n_min_samples_leaf = len(setting_min_samples_leaf)
    n_max_features = len(setting_max_features)
    n_subsample = len(setting_subsample)
    n_huber_alpha = len(huber_alpha)

    train_score, test_score = {}, {}
    overall_test_score = {}
    n_recorded_estimators = int(n_estimators / gbrf.ESTIMATOR_RECORDING_FREQUENCY)
    if n_estimators % gbrf.ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    for score in scoring :
        test_score[score] = np.zeros((n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
        overall_test_score[score] = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
    n_features, dataset_size = np.zeros((n_folds, ), dtype = np.int), np.zeros((n_folds, ), dtype = np.int)
    feature_names = []
    times = np.zeros((n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
    all_true_y, all_predicted_y = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object), np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object)
    for i, learning_rate in enumerate(setting_learning_rate) :
        for j, max_depth in enumerate(setting_max_depth) :
            for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                for l, max_features in enumerate(setting_max_features) :
                    for m, subsample in enumerate(setting_subsample) :
                        for n, huber_alpha in enumerate(setting_huber_alpha) :
                            all_true_y[i, j, k, l, m, n] = []
                            all_predicted_y[i, j, k, l, m, n] = []
    if client_url is None :
        inner_fold = 0
        for train_index, test_index in cv_folds :
            if use_aux :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = (outer_fold, inner_fold))
            else :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
            fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
            feature_names.append(fold_feature_names)
            X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)
            del Xs
            n_features[inner_fold] = X.shape[1]
            dataset_size[inner_fold] = len(train_index) + len(test_index)
            for i, learning_rate in enumerate(setting_learning_rate) :
                for j, max_depth in enumerate(setting_max_depth) :
                    for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                        for l, max_features in enumerate(setting_max_features) :
                            for m, subsample in enumerate(setting_subsample) :
                                for n, huber_alpha in enumerate(setting_huber_alpha) :
                                    fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = gbrf.train_gbrf_cv_fold(X, y, train_index, test_index, learn_intercept = learn_intercept, n_estimators = n_estimators, min_samples_leaf = min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, max_depth = max_depth, huber_alpha = huber_alpha, scoring = scoring, return_regressor = False)
                                    all_true_y[i, j, k, l, m, n].extend(fold_true_y)
                                    all_predicted_y[i, j, k, l, m, n].extend(fold_predicted_y)
                                    for score in scoring :
                                        train_score[score][inner_fold, i, j, k, l, m, n, :] = fold_train_score[score]
                                        test_score[score][inner_fold, i, j, k, l, m, n, :] = fold_test_score[score]
                                    times[inner_fold, i, j, k, l, m, n] = fold_time
            del X
            del y
            inner_fold += 1
    else :
        tasks, indices = [], []
        inner_fold = 0
        for train_index, test_index in cv_folds :
            for i, learning_rate in enumerate(setting_learning_rate) :
                for j, max_depth in enumerate(setting_max_depth) :
                    for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                        for l, max_features in enumerate(setting_max_features) :
                            for m, subsample in enumerate(setting_subsample) :
                                for n, huber_alpha in enumerate(setting_huber_alpha) :
                                    tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, learn_intercept, n_estimators, min_samples_leaf, learning_rate, subsample, max_features, max_depth, huber_alpha, scoring, (outer_fold, inner_fold), False))
                                    indices.append((inner_fold, i, j, k, l, m, n))
            inner_fold += 1
        async_results = submit_gbrf_tasks(client_url, tasks)

        for index, async_result in zip(indices, async_results) :
            inner_fold, i, j, k, l, m, n = index
            fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
            feature_names.append(fold_feature_names)
            all_true_y[i, j, k, l, m, n].extend(fold_true_y)
            all_predicted_y[i, j, k, l, m, n].extend(fold_predicted_y)
            dataset_size[inner_fold] = len(train_index) + len(test_index)
            n_features[inner_fold] = fold_n_features
            for score in scoring :
                train_score[score][inner_fold, i, j, k, l, m, n, :] = fold_train_score[score]
                test_score[score][inner_fold, i, j, k, l, m, n, :] = fold_test_score[score]
            times[inner_fold, i, j, k, l, m, n] = fold_time
    
    for score in scoring :
        for i, learning_rate in enumerate(setting_learning_rate) :
            for j, max_depth in enumerate(setting_max_depth) :
                for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                    for l, max_features in enumerate(setting_max_features) :
                        for m, subsample in enumerate(setting_subsample) :
                            for n, huber_alpha in enumerate(setting_huber_alpha) :
                                overall_test_score[score][i, j, k, l, m, n] = gbrf.calculate_regression_score(all_true_y[i, j, k, l, m, n], all_predicted_y[i, j, k, l, m, n], score)
    
    del all_predicted_y
    del all_true_y
    
    result.use_aux = use_aux
    result.dataset_name = dataset_name
    result.features = features
    result.log2 = log2
    result.train_score = train_score
    result.test_score = test_score
    result.overall_test_score = overall_test_score
    result.dataset_size = dataset_size
    result.n_features = n_features
    result.feature_names = feature_names
    result.time = times
    result.learn_intercept = learn_intercept
    result.n_estimators = n_estimators
    result.estimator_recording_frequency = gbrf.ESTIMATOR_RECORDING_FREQUENCY
    result.setting_learning_rate = setting_learning_rate
    result.setting_max_depth = setting_max_depth
    result.setting_min_samples_leaf = setting_min_samples_leaf
    result.setting_max_features = setting_max_features
    result.setting_subsample = setting_subsample
    result.setting_huber_alpha = setting_huber_loss
    return result

def train_svr_inner_cv(dataset_name, features, log2 = True, use_aux = False, grid_search = _svr_grid_search_settings, scoring = gbrf.REGRESSION_METRICS, cv_folds = None, outer_fold = None, client_url = None) :
    '''
    Compute SVR regression on a given dataset - full dataset or outer fold of CV (variable pre-selection is done on the inner fold dataset, then training is performed).
    :param features : features to be used when constructing the regression matrix.
    :param log2 : whether a log2 transformation should be applied to the response variable.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param grid_search : a dictionary of settings to be probed in grid search.
    :param scoring : scoring function to be used.
    :param cv_folds : an iterator (or list) of CV index pairs or a number of folds to be used.
    :param client_url : URL of an IPython client to be used for parallel computing.
    '''
    if not isinstance(scoring, list) :
        scoring = [scoring]

    result = SimpleNamespace()
    if isinstance(cv_folds, int) :
        n_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        values = np.array([seq.ires_activity for seq in dataset])
        cv_folds = crossvalidation.stratified_KFold(values, n_folds)
    else :
        n_folds = len(cv_folds)
   
    setting_C = grid_search.get('C', [1])
    setting_epsilon = grid_search.get('epsilon', [0.1])
    n_C = len(setting_C)
    n_epsilon = len(setting_epsilon)

    train_score, test_score = {}, {}
    overall_test_score = {}
    for score in scoring :
        test_score[score] = np.zeros((n_folds, n_C, n_epsilon), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, n_C, n_epsilon), dtype = np.float16)
        overall_test_score[score] = np.zeros((n_C, n_epsilon), dtype = np.float16)
    n_features, dataset_size = np.zeros((n_folds, ), dtype = np.int), np.zeros((n_folds, ), dtype = np.int)
    feature_names = []
    times = np.zeros((n_folds, n_C, n_epsilon), dtype = np.float16)
    all_true_y, all_predicted_y = np.zeros((n_C, n_epsilon), dtype = np.object), np.zeros((n_C, n_epsilon), dtype = np.object)
    for i, C in enumerate(setting_C) :
        for j, epsilon in enumerate(setting_epsilon) :
            all_true_y[i, j] = []
            all_predicted_y[i, j] = []
    if client_url is None :
        inner_fold = 0
        for train_index, test_index in cv_folds :
            if use_aux :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = (outer_fold, inner_fold))
            else :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
            fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
            feature_names.append(fold_feature_names)
            X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)
            del Xs
            n_features[inner_fold] = X.shape[1]
            dataset_size[inner_fold] = len(train_index) + len(test_index)
            for i, C in enumerate(setting_C) :
                for j, epsilon in enumerate(setting_epsilon) :
                    fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = svr.train_svr_cv_fold(X, y, train_index, test_index, C=C, epsilon=epsilon, normalize=True, scoring = scoring, return_regressor = False)
                    all_true_y[i, j].extend(fold_true_y)
                    all_predicted_y[i, j].extend(fold_predicted_y)
                    for score in scoring :
                        train_score[score][inner_fold, i, j] = fold_train_score[score]
                        test_score[score][inner_fold, i, j] = fold_test_score[score]
                    times[inner_fold, i, j] = fold_time
            del X
            del y
            inner_fold += 1
    else :
        tasks, indices = [], []
        inner_fold = 0
        for train_index, test_index in cv_folds :
            for i, C in enumerate(setting_C) :
                for j, epsilon in enumerate(setting_epsilon) :
                    tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, C, epsilon, True, scoring, (outer_fold, inner_fold), False))
                    indices.append((inner_fold, i, j))
            inner_fold += 1
        async_results = submit_svr_tasks(client_url, tasks)

        for index, async_result in zip(indices, async_results) :
            inner_fold, i, j = index
            fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
            feature_names.append(fold_feature_names)
            all_true_y[i, j].extend(fold_true_y)
            all_predicted_y[i, j].extend(fold_predicted_y)
            dataset_size[inner_fold] = len(train_index) + len(test_index)
            n_features[inner_fold] = fold_n_features
            for score in scoring :
                train_score[score][inner_fold, i, j] = fold_train_score[score]
                test_score[score][inner_fold, i, j] = fold_test_score[score]
            times[inner_fold, i, j] = fold_time
    
    for score in scoring :
        for i, learning_rate in enumerate(setting_C) :
            for j, max_depth in enumerate(setting_epsilon) :
                overall_test_score[score][i, j] = gbrf.calculate_regression_score(all_true_y[i, j], all_predicted_y[i, j], score)
    
    del all_predicted_y
    del all_true_y
    
    result.use_aux = use_aux
    result.dataset_name = dataset_name
    result.features = features
    result.log2 = log2
    result.train_score = train_score
    result.test_score = test_score
    result.overall_test_score = overall_test_score
    result.dataset_size = dataset_size
    result.n_features = n_features
    result.feature_names = feature_names
    result.time = times
    result.setting_C = setting_C
    result.setting_epsilon = setting_epsilon
    result.normalize = True
    return result

def train_gbrf_outer_cv(dataset_name, features, log2 = True, use_aux = False, learn_intercept = False, n_estimators = 500, grid_search = _grid_search_settings, scoring = gbrf.REGRESSION_METRICS, cv_folds = None, client_url = None) :
    '''
    Compute Gradient-Boosted Regression Trees.
    :param dataset_name : name of the dataset that should be predicted.
    :param features : features to be used when constructing the regression matrix.
    :param log2 : whether a log2 transformation should be applied to the response variable.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param learn_intercept : whether we should explicitly learn the intercept.
    :param n_estimators : number of estimators to be used in training.
    :param scoring : scoring function to be used.
    :param cv_folds : an iterator (or list) of CV index pairs or a number of folds to be used.
    :param client_url : URL of an IPython client to be used for parallel computing.
    '''
    results = []
    if not isinstance(scoring, list) :
        scoring = [scoring]

    if isinstance(cv_folds, SimpleNamespace) :
        n_outer_folds = len(cv_folds.outer_folds)
        cv_outer_folds = cv_folds.outer_folds
        cv_inner_folds = cv_folds.inner_folds
    elif isinstance(cv_fold, tuple) :
        n_outer_folds, n_inner_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        ires_values = np.array([seq.ires_activity for seq in dataset])
        cv_outer_folds = crossvalidation.stratified_KFold(ires_values, n_outer_folds)
        cv_inner_folds = None
    else :
        raise Exception('Unable to parse CV folds')
    
    setting_learning_rate = grid_search.get('learning_rate', [0.1])
    setting_max_depth = grid_search.get('max_depth', [5])
    setting_min_samples_leaf = grid_search.get('min_samples_leaf', [1])
    setting_max_features = grid_search.get('max_features', [0.5])
    setting_subsample = grid_search.get('subsample', [0.7])
    setting_huber_alpha = grid_search.get('huber_alpha', [0.9])
    n_learning_rate = len(setting_learning_rate)
    n_max_depth = len(setting_max_depth)
    n_min_samples_leaf = len(setting_min_samples_leaf)
    n_max_features = len(setting_max_features)
    n_subsample = len(setting_subsample)
    n_huber_alpha = len(setting_huber_alpha)
    
    if client_url is None :
        for outer_fold in range(n_outer_folds) :
            if cv_inner_folds is not None :
                actual_inner_folds = cv_inner_folds[outer_fold]
                actual_n_inner_folds = len(actual_inner_folds)
            else :
                actual_n_inner_folds = n_inner_folds
                outer_train_index = cv_outer_folds[outer_fold][0]
                outer_values = ires_values[outer_train_index]
                actual_inner_folds = crossvalidation.stratified_KFold(outer_values, actual_n_inner_folds)
                actual_inner_folds = [(outer_train_index[train_index], outer_train_index[test_index]) for train_index, test_index in actual_inner_folds]
            
            train_score, test_score = {}, {}
            overall_test_score = {}
            n_recorded_estimators = int(n_estimators / gbrf.ESTIMATOR_RECORDING_FREQUENCY)
            if n_estimators % gbrf.ESTIMATOR_RECORDING_FREQUENCY != 0 :
                n_recorded_estimators += 1
            for score in scoring :
                test_score[score] = np.zeros((actual_n_inner_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
                train_score[score] = np.zeros((actual_n_inner_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
                overall_test_score[score] = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
            n_features, dataset_size = np.zeros((actual_n_inner_folds, ), dtype = np.int), np.zeros((actual_n_inner_folds, ), dtype = np.int)
            feature_names = []
            times = np.zeros((actual_n_inner_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
            all_true_y, all_predicted_y = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object), np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object)
            for i, learning_rate in enumerate(setting_learning_rate) :
                for j, max_depth in enumerate(setting_max_depth) :
                    for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                        for l, max_features in enumerate(setting_max_features) :
                            for m, subsample in enumerate(setting_subsample) :
                                for n, huber_alpha in enumerate(setting_huber_alpha) :
                                    all_true_y[i, j, k, l, m, n] = []
                                    all_predicted_y[i, j, k, l, m, n] = []
            
            inner_fold = 0
            cnt = 0
            for train_index, test_index in actual_inner_folds :
                dataset_size[inner_fold] = len(train_index) + len(test_index)
                if use_aux :
                    Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = (outer_fold, inner_fold))
                else :
                    Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
                fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
                feature_names.append(fold_feature_names)
                X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)
                del Xs
                n_features[inner_fold] = X.shape[1]
                for i, learning_rate in enumerate(setting_learning_rate) :
                    for j, max_depth in enumerate(setting_max_depth) :
                        for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                            for l, max_features in enumerate(setting_max_features) :
                                for m, subsample in enumerate(setting_subsample) :
                                    for n, huber_alpha in enumerate(setting_huber_alpha) :
                                        cnt += 1
                                        fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = gbrf.train_gbrf_cv_fold(X, y, train_index, test_index, learn_intercept = learn_intercept, n_estimators = n_estimators, min_samples_leaf = min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, max_depth = max_depth, huber_alpha = huber_alpha, scoring = scoring, return_regressor = False)
                                        all_true_y[i, j, k, l, m, n].extend(fold_true_y)
                                        all_predicted_y[i, j, k, l, m, n].extend(fold_predicted_y)
                                        for score in scoring :
                                            train_score[score][inner_fold, i, j, k, l, m, n, :] = fold_train_score[score]
                                            test_score[score][inner_fold, i, j, k, l, m, n, :] = fold_test_score[score]
                                        times[inner_fold, i, j, k, l, m, n] = fold_time
                del X
                del y
                inner_fold += 1
            
            for score in scoring :
                for i, learning_rate in enumerate(setting_learning_rate) :
                    for j, max_depth in enumerate(setting_max_depth) :
                        for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                            for l, max_features in enumerate(setting_max_features) :
                                for m, subsample in enumerate(setting_subsample) :
                                    for n, huber_alpha in enumerate(setting_huber_alpha) :
                                        overall_test_score[score][i, j, k, l, m, n] = gbrf.calculate_regression_score(all_true_y[i, j, k, l, m, n], all_predicted_y[i, j, k, l, m, n], score)
            
            result = SimpleNamespace()
            result.use_aux = use_aux
            result.features = features
            result.log2 = log2
            result.dataset_name = dataset_name
            result.train_score = train_score
            result.test_score = test_score
            result.overall_test_score = overall_test_score
            result.dataset_size = dataset_size
            result.n_features = n_features
            result.feature_names = feature_names
            result.time = times
            result.learn_intercept = learn_intercept
            result.n_estimators = n_estimators
            result.estimator_recording_frequency = gbrf.ESTIMATOR_RECORDING_FREQUENCY
            result.setting_learning_rate = setting_learning_rate
            result.setting_max_depth = setting_max_depth
            result.setting_min_samples_leaf = setting_min_samples_leaf
            result.setting_max_features = setting_max_features
            result.setting_subsample = setting_subsample
            result.setting_huber_alpha = setting_huber_alpha
            results.append(result)
    else :
        client = Client(client_url)
        async_results_list = []
        for outer_fold in range(n_outer_folds) :
            if cv_inner_folds is not None :
                actual_inner_folds = cv_inner_folds[outer_fold]
                actual_n_inner_folds = len(actual_inner_folds)
            else :
                actual_n_inner_folds = n_inner_folds
                outer_train_index = cv_outer_folds[outer_fold][0]
                outer_values = ires_values[outer_train_index]
                actual_inner_folds = crossvalidation.stratified_KFold(outer_values, actual_n_inner_folds)
                actual_inner_folds = [(outer_train_index[train_index], outer_train_index[test_index]) for train_index, test_index in actual_inner_folds]
            
            dataset_size = np.zeros((actual_n_inner_folds, ), dtype = np.int)
            tasks, indices = [], []
            inner_fold = 0
            for train_index, test_index in actual_inner_folds :
                for i, learning_rate in enumerate(setting_learning_rate) :
                    for j, max_depth in enumerate(setting_max_depth) :
                        for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                            for l, max_features in enumerate(setting_max_features) :
                                for m, subsample in enumerate(setting_subsample) :
                                    for n, huber_alpha in enumerate(setting_huber_alpha) :
                                        tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, learn_intercept, n_estimators, min_samples_leaf, learning_rate, subsample, max_features, max_depth, huber_alpha, scoring, (outer_fold, inner_fold), False))
                                        indices.append((inner_fold, i, j, k, l, m, n))
                dataset_size[inner_fold] = len(train_index) + len(test_index)
                inner_fold += 1
            async_results = submit_gbrf_tasks(client, tasks)
            async_results_list.append(async_results)

        for outer_fold in range(n_outer_folds) :
            if cv_inner_folds is not None :
                actual_inner_folds = cv_inner_folds[outer_fold]
                actual_n_inner_folds = len(actual_inner_folds)
            else :
                actual_n_inner_folds = n_inner_folds

            train_score, test_score = {}, {}
            overall_test_score = {}
            n_recorded_estimators = int(n_estimators / gbrf.ESTIMATOR_RECORDING_FREQUENCY)
            if n_estimators % gbrf.ESTIMATOR_RECORDING_FREQUENCY != 0 :
                n_recorded_estimators += 1
            for score in scoring :
                test_score[score] = np.zeros((actual_n_inner_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
                train_score[score] = np.zeros((actual_n_inner_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
                overall_test_score[score] = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
            n_features = np.zeros((actual_n_inner_folds, ), dtype = np.int)
            feature_names = []
            times = np.zeros((actual_n_inner_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
            all_true_y, all_predicted_y = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object), np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object)
            for i, learning_rate in enumerate(setting_learning_rate) :
                for j, max_depth in enumerate(setting_max_depth) :
                    for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                        for l, max_features in enumerate(setting_max_features) :
                            for m, subsample in enumerate(setting_subsample) :
                                for n, huber_alpha in enumerate(setting_huber_alpha) :
                                    all_true_y[i, j, k, l, m, n] = []
                                    all_predicted_y[i, j, k, l, m, n] = []
            
            async_results = async_results_list[outer_fold]
            for index, async_result in zip(indices, async_results) :
                inner_fold, i, j, k, l, m, n = index
                fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
                feature_names.append(fold_feature_names)
                all_true_y[i, j, k, l, m, n].extend(fold_true_y)
                all_predicted_y[i, j, k, l, m, n].extend(fold_predicted_y)
                for score in scoring :
                    train_score[score][inner_fold, i, j, k, l, m, n, :] = fold_train_score[score]
                    test_score[score][inner_fold, i, j, k, l, m, n, :] = fold_test_score[score]
                times[inner_fold, i, j, k, l, m, n] = fold_time
                n_features[inner_fold] = fold_n_features
            
            for score in scoring :
                for i, learning_rate in enumerate(setting_learning_rate) :
                    for j, max_depth in enumerate(setting_max_depth) :
                        for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                            for l, max_features in enumerate(setting_max_features) :
                                for m, subsample in enumerate(setting_subsample) :
                                    for n, huber_alpha in enumerate(setting_huber_alpha) :
                                        overall_test_score[score][i, j, k, l, m, n] = gbrf.calculate_regression_score(all_true_y[i, j, k, l, m, n], all_predicted_y[i, j, k, l, m, n], score)
            
            result = SimpleNamespace()
            result.use_aux = use_aux
            result.features = features
            result.log2 = log2
            result.dataset_name = dataset_name
            result.train_score = train_score
            result.test_score = test_score
            result.overall_test_score = overall_test_score
            result.dataset_size = dataset_size
            result.n_features = n_features
            result.feature_names = feature_names
            result.time = times
            result.learn_intercept = learn_intercept
            result.n_estimators = n_estimators
            result.estimator_recording_frequency = gbrf.ESTIMATOR_RECORDING_FREQUENCY
            result.setting_learning_rate = setting_learning_rate
            result.setting_max_depth = setting_max_depth
            result.setting_min_samples_leaf = setting_min_samples_leaf
            result.setting_max_features = setting_max_features
            result.setting_subsample = setting_subsample
            result.setting_huber_alpha = setting_huber_alpha
            results.append(result)
        client.close()
        del client
    return results

def train_svr_outer_cv(dataset_name, features, log2=True, use_aux=False, grid_search=_svr_grid_search_settings, scoring=gbrf.REGRESSION_METRICS, cv_folds=None, client_url=None) :
    '''
    Compute SVR.
    :param dataset_name : name of the dataset that should be predicted.
    :param features : features to be used when constructing the regression matrix.
    :param log2 : whether a log2 transformation should be applied to the response variable.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param scoring : scoring function to be used.
    :param cv_folds : an iterator (or list) of CV index pairs or a number of folds to be used.
    :param client_url : URL of an IPython client to be used for parallel computing.
    '''
    results = []
    if not isinstance(scoring, list) :
        scoring = [scoring]

    if isinstance(cv_folds, SimpleNamespace) :
        n_outer_folds = len(cv_folds.outer_folds)
        cv_outer_folds = cv_folds.outer_folds
        cv_inner_folds = cv_folds.inner_folds
    elif isinstance(cv_fold, tuple) :
        n_outer_folds, n_inner_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        ires_values = np.array([seq.ires_activity for seq in dataset])
        cv_outer_folds = crossvalidation.stratified_KFold(ires_values, n_outer_folds)
        cv_inner_folds = None
    else :
        raise Exception('Unable to parse CV folds')
    
    setting_C = grid_search.get('C', [1])
    setting_epsilon = grid_search.get('epsilon', [0.1])
    n_C = len(setting_C)
    n_epsilon = len(setting_epsilon)
    
    if client_url is None :
        for outer_fold in range(n_outer_folds) :
            if cv_inner_folds is not None :
                actual_inner_folds = cv_inner_folds[outer_fold]
                actual_n_inner_folds = len(actual_inner_folds)
            else :
                actual_n_inner_folds = n_inner_folds
                outer_train_index = cv_outer_folds[outer_fold][0]
                outer_values = ires_values[outer_train_index]
                actual_inner_folds = crossvalidation.stratified_KFold(outer_values, actual_n_inner_folds)
                actual_inner_folds = [(outer_train_index[train_index], outer_train_index[test_index]) for train_index, test_index in actual_inner_folds]
            
            train_score, test_score = {}, {}
            overall_test_score = {}
            for score in scoring :
                test_score[score] = np.zeros((actual_n_inner_folds, n_C, n_epsilon), dtype = np.float16)
                train_score[score] = np.zeros((actual_n_inner_folds, n_C, n_epsilon), dtype = np.float16)
                overall_test_score[score] = np.zeros((n_C, n_epsilon), dtype = np.float16)
            n_features, dataset_size = np.zeros((actual_n_inner_folds, ), dtype = np.int), np.zeros((actual_n_inner_folds, ), dtype = np.int)
            feature_names = []
            times = np.zeros((actual_n_inner_folds, n_C, n_epsilon), dtype = np.float16)
            all_true_y, all_predicted_y = np.zeros((n_C, n_epsilon), dtype = np.object), np.zeros((n_C, n_epsilon), dtype = np.object)
            for i, C in enumerate(setting_C) :
                for j, epsilon in enumerate(setting_epsilon) :
                    all_true_y[i, j] = []
                    all_predicted_y[i, j] = []
            
            inner_fold = 0
            cnt = 0
            for train_index, test_index in actual_inner_folds :
                dataset_size[inner_fold] = len(train_index) + len(test_index)
                if use_aux :
                    Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = (outer_fold, inner_fold))
                else :
                    Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
                fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
                feature_names.append(fold_feature_names)
                X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)
                del Xs
                n_features[inner_fold] = X.shape[1]
                for i, C in enumerate(setting_C) :
                    for j, epsilon in enumerate(setting_epsilon) :
                        cnt += 1
                        fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = svr.train_svr_cv_fold(X, y, train_index, test_index, C=C, epsilon=epsilon, normalize=True, scoring = scoring, return_regressor = False)
                        all_true_y[i, j].extend(fold_true_y)
                        all_predicted_y[i, j].extend(fold_predicted_y)
                        for score in scoring :
                            train_score[score][inner_fold, i, j] = fold_train_score[score]
                            test_score[score][inner_fold, i, j] = fold_test_score[score]
                        times[inner_fold, i, j] = fold_time
                del X
                del y
                inner_fold += 1
            
            for score in scoring :
                for i, C in enumerate(setting_C) :
                    for j, epsilon in enumerate(setting_epsilon) :
                        overall_test_score[score][i, j] = gbrf.calculate_regression_score(all_true_y[i, j], all_predicted_y[i, j], score)
            
            result = SimpleNamespace()
            result.use_aux = use_aux
            result.features = features
            result.log2 = log2
            result.dataset_name = dataset_name
            result.train_score = train_score
            result.test_score = test_score
            result.overall_test_score = overall_test_score
            result.dataset_size = dataset_size
            result.n_features = n_features
            result.feature_names = feature_names
            result.time = times
            result.setting_C = setting_C
            result.setting_epsilon = setting_epsilon
            result.normalize = normalize
            results.append(result)
    else :
        client = Client(client_url)
        async_results_list = []
        for outer_fold in range(n_outer_folds) :
            if cv_inner_folds is not None :
                actual_inner_folds = cv_inner_folds[outer_fold]
                actual_n_inner_folds = len(actual_inner_folds)
            else :
                actual_n_inner_folds = n_inner_folds
                outer_train_index = cv_outer_folds[outer_fold][0]
                outer_values = ires_values[outer_train_index]
                actual_inner_folds = crossvalidation.stratified_KFold(outer_values, actual_n_inner_folds)
                actual_inner_folds = [(outer_train_index[train_index], outer_train_index[test_index]) for train_index, test_index in actual_inner_folds]
            
            dataset_size = np.zeros((actual_n_inner_folds, ), dtype = np.int)
            tasks, indices = [], []
            inner_fold = 0
            for train_index, test_index in actual_inner_folds :
                for i, C in enumerate(setting_C) :
                    for j, epsilon in enumerate(setting_epsilon) :
                            tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, C, epsilon, True, scoring, (outer_fold, inner_fold), False))
                            indices.append((inner_fold, i, j))
                dataset_size[inner_fold] = len(train_index) + len(test_index)
                inner_fold += 1
            async_results = submit_svr_tasks(client, tasks)
            async_results_list.append(async_results)

        for outer_fold in range(n_outer_folds) :
            if cv_inner_folds is not None :
                actual_inner_folds = cv_inner_folds[outer_fold]
                actual_n_inner_folds = len(actual_inner_folds)
            else :
                actual_n_inner_folds = n_inner_folds

            train_score, test_score = {}, {}
            overall_test_score = {}
            for score in scoring :
                test_score[score] = np.zeros((actual_n_inner_folds, n_C, n_epsilon), dtype = np.float16)
                train_score[score] = np.zeros((actual_n_inner_folds, n_C, n_epsilon), dtype = np.float16)
                overall_test_score[score] = np.zeros((n_C, n_epsilon), dtype = np.float16)
            n_features = np.zeros((actual_n_inner_folds, ), dtype = np.int)
            feature_names = []
            times = np.zeros((actual_n_inner_folds, n_C, n_epsilon), dtype = np.float16)
            all_true_y, all_predicted_y = np.zeros((n_C, n_epsilon), dtype = np.object), np.zeros((n_C, n_epsilon), dtype = np.object)
            for i, C in enumerate(setting_C) :
                for j, epsilon in enumerate(setting_epsilon) :
                    all_true_y[i, j] = []
                    all_predicted_y[i, j] = []
            
            async_results = async_results_list[outer_fold]
            for index, async_result in zip(indices, async_results) :
                inner_fold, i, j = index
                fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
                feature_names.append(fold_feature_names)
                all_true_y[i, j].extend(fold_true_y)
                all_predicted_y[i, j].extend(fold_predicted_y)
                for score in scoring :
                    train_score[score][inner_fold, i, j] = fold_train_score[score]
                    test_score[score][inner_fold, i, j] = fold_test_score[score]
                times[inner_fold, i, j] = fold_time
                n_features[inner_fold] = fold_n_features
            
            for score in scoring :
                for i, C in enumerate(setting_C) :
                    for j, epsilon in enumerate(setting_epsilon) :
                        overall_test_score[score][i, j] = gbrf.calculate_regression_score(all_true_y[i, j], all_predicted_y[i, j], score)
            
            result = SimpleNamespace()
            result.use_aux = use_aux
            result.features = features
            result.log2 = log2
            result.dataset_name = dataset_name
            result.train_score = train_score
            result.test_score = test_score
            result.overall_test_score = overall_test_score
            result.dataset_size = dataset_size
            result.n_features = n_features
            result.feature_names = feature_names
            result.time = times
            result.setting_C = setting_C
            result.setting_epsilon = setting_epsilon
            result.normalize = True
            results.append(result)
        client.close()
        del client
    return results

def train_gbrf_outer_cv_final(inner_results, can_choose_n_estimators = False, return_regressor = False, use_aux = False, scoring = gbrf.REGRESSION_METRICS, cv_folds = None, client_url = None) :
    '''
    Compute Gradient-Boosted Regression Trees based on inner-fold CV results.
    :param inner_results : inner CV results.
    :param can_choose_n_estimators : a boolean flag determining whether the regressor is allowed to use its own number of estimators.
    :param return_regressor : whether trained fold regressors should be returned.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param scoring : scoring function to be used.
    :param cv_folds : CV folds to be used.
    :param client_url : URL used for parallel computing with IPython.
    '''
    if not isinstance(scoring, list) :
        scoring = [scoring]
    
    dataset_name = inner_results[0].dataset_name
    features = inner_results[0].features
    learn_intercept = inner_results[0].learn_intercept
    log2 = inner_results[0].log2
    n_estimators = inner_results[0].n_estimators
    estimator_recording_frequency = inner_results[0].estimator_recording_frequency

    if isinstance(cv_folds, SimpleNamespace) :
        cv_folds = cv_folds.outer_folds
        n_folds = len(cv_folds)
    elif isinstance(cv_folds,  list) :
        n_folds = len(cv_folds)
    elif ininstance(cv_folds, int) :
        n_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        ires_values = np.array([seq.ires_activity for seq in dataset])
        cv_folds = crossvalidation.stratified_KFold(ires_values, n_folds)
    else :
        raise Exception('Unable to parse CV folds')
        cv_folds = None
    
    train_score, test_score = {}, {}
    overall_test_score = {}  
    n_recorded_estimators = int(n_estimators / estimator_recording_frequency)
    if n_estimators % gbrf.ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    if not isinstance(can_choose_n_estimators, bool) :
        score_size = can_choose_n_estimators / estimator_recording_frequency
    else :
        score_size = n_recorded_estimators
    for score in scoring :
        test_score[score] = np.zeros((n_folds, score_size), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, score_size), dtype = np.float16)
    n_features, dataset_size = np.zeros((n_folds, ), dtype = np.int), np.zeros((n_folds, ), dtype = np.int)
    times = np.zeros((n_folds, ), dtype = np.float16)
    used_learning_rate, used_max_depth, used_min_samples_leaf, used_max_features, used_subsample, used_n_estimators, used_huber_alpha = [], [], [], [], [], [], []
    regressors = []
    feature_names = []
    all_true_y, all_predicted_y = [], []
    if client_url is None :
        for fold in range(n_folds) :
            inner_result = inner_results[fold]
            scores = np.mean(inner_result.test_score['r2'], axis = 0)
            if can_choose_n_estimators == True :
                index = np.nanargmax(scores)
                i, j, k, l, m, n, o = np.unravel_index(index, scores.shape)
            else :
                scores = scores[:, :, :, :, :, :, -1]
                index = np.nanargmax(scores)
                i, j, k, l, m, n = np.unravel_index(index, scores.shape)
                if isinstance(can_choose_n_estimators, bool) :
                    o = n_recorded_estimators - 1
                else :
                    o = can_choose_n_estimators / estimator_recording_frequency - 1
            learning_rate = inner_result.setting_learning_rate[i]
            max_depth = inner_result.setting_max_depth[j]
            min_samples_leaf = inner_result.setting_min_samples_leaf[k]
            max_features = inner_result.setting_max_features[l]
            subsample = inner_result.setting_subsample[m]
            huber_alpha = inner_result.setting_huber_alpha[n]
            best_n_estimators = int((o + 1) * estimator_recording_frequency)
            used_learning_rate.append(learning_rate)
            used_max_depth.append(max_depth)
            used_min_samples_leaf.append(min_samples_leaf)
            used_max_features.append(max_features)
            used_subsample.append(subsample)
            used_n_estimators.append(best_n_estimators)
            used_huber_alpha.append(huber_alpha)

            train_index, test_index = cv_folds[fold]
            if use_aux :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = fold)
            else :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
            fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
            feature_names.append(fold_feature_names)
            X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)
            del Xs
            n_features[fold] = X.shape[1]
            dataset_size[fold] = len(train_index) + len(test_index)
            fold_result = gbrf.train_gbrf_cv_fold(X, y, train_index, test_index, learn_intercept = learn_intercept, n_estimators = best_n_estimators, min_samples_leaf = min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, max_depth = max_depth, huber_alpha = huber_alpha, scoring = scoring, return_regressor = return_regressor)
            if not return_regressor :
                fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = fold_result
            else :
                fold_regressor, fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = fold_result
                regressors.append(fold_regressor)
            all_true_y.extend(fold_true_y)
            all_predicted_y.extend(fold_predicted_y)
            for score in scoring :
                n_scores = len(fold_train_score[score])
                train_score[score][fold, : n_scores] = fold_train_score[score]
                test_score[score][fold, : n_scores] = fold_test_score[score]
            times[fold] = fold_time
            del X, y
    else :
        tasks = []
        for fold in range(n_folds) :
            inner_result = inner_results[fold]
            scores = np.mean(inner_result.test_score['r2'], axis = 0)
            if can_choose_n_estimators == True :
                index = np.nanargmax(scores)
                i, j, k, l, m, n, o = np.unravel_index(index, scores.shape)
            else :
                scores = scores[:, :, :, :, :, :, -1]
                index = np.nanargmax(scores)
                i, j, k, l, m, n = np.unravel_index(index, scores.shape)
                if isinstance(can_choose_n_estimators, bool) :
                    o = n_recorded_estimators - 1
                else :
                    o = can_choose_n_estimators / estimator_recording_frequency - 1
            learning_rate = inner_result.setting_learning_rate[i]
            max_depth = inner_result.setting_max_depth[j]
            min_samples_leaf = inner_result.setting_min_samples_leaf[k]
            max_features = inner_result.setting_max_features[l]
            subsample = inner_result.setting_subsample[m]
            huber_alpha = inner_result.setting_huber_alpha[n]
            best_n_estimators = (o + 1) * estimator_recording_frequency
            used_learning_rate.append(learning_rate)
            used_max_depth.append(max_depth)
            used_min_samples_leaf.append(min_samples_leaf)
            used_max_features.append(max_features)
            used_subsample.append(subsample)
            used_huber_alpha.append(huber_alpha)
            used_n_estimators.append(best_n_estimators)

            train_index, test_index = cv_folds[fold]
            tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, learn_intercept, best_n_estimators, min_samples_leaf, learning_rate, subsample, max_features, max_depth, huber_alpha, scoring, fold, return_regressor))

        async_results = submit_gbrf_tasks(client_url, tasks)
        for fold, async_result in enumerate(async_results) :
            if not return_regressor :
                fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
            else :
                fold_regressor, fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
                regressors.append(fold_regressor)
            feature_names.append(fold_feature_names)
            dataset_size[fold] = len(train_index) + len(test_index)
            n_features[fold] = fold_n_features
            all_true_y.extend(fold_true_y)
            all_predicted_y.extend(fold_predicted_y)
            for score in scoring :
                n_scores = len(fold_train_score[score])
                train_score[score][fold, : n_scores] = fold_train_score[score]
                test_score[score][fold, : n_scores] = fold_test_score[score]
            times[fold] = fold_time
    
    for score in scoring :
        overall_test_score[score] = gbrf.calculate_regression_score(all_true_y, all_predicted_y, score)
    
    result = SimpleNamespace()
    result.use_aux = use_aux
    result.features = features
    result.log2 = log2
    result.dataset_name = dataset_name
    result.train_score = train_score
    result.test_score = test_score
    result.overall_test_score = overall_test_score
    result.dataset_size = dataset_size
    result.n_features = n_features
    result.feature_names = feature_names
    result.time = times
    result.learn_intercept = learn_intercept
    result.n_estimators = n_estimators
    result.estimator_recording_frequency = gbrf.ESTIMATOR_RECORDING_FREQUENCY
    result.used_learning_rate = used_learning_rate
    result.used_max_depth = used_max_depth
    result.used_min_samples_leaf = used_min_samples_leaf
    result.used_max_features = used_max_features
    result.used_subsample = used_subsample
    result.used_huber_alpha = used_huber_alpha
    result.used_n_estimators = used_n_estimators
    result.can_choose_n_estimators = can_choose_n_estimators
    if return_regressor :
        result.regressors = regressors
    return result

def train_svr_outer_cv_final(inner_results, return_regressor=False, use_aux=False, scoring=gbrf.REGRESSION_METRICS, cv_folds=None, client_url=None) :
    '''
    Compute SVR based on inner-fold CV results.
    :param inner_results : inner CV results.
    :param return_regressor : whether trained fold regressors should be returned.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param scoring : scoring function to be used.
    :param cv_folds : CV folds to be used.
    :param client_url : URL used for parallel computing with IPython.
    '''
    if not isinstance(scoring, list) :
        scoring = [scoring]
    
    dataset_name = inner_results[0].dataset_name
    features = inner_results[0].features
    log2 = inner_results[0].log2

    if isinstance(cv_folds, SimpleNamespace) :
        cv_folds = cv_folds.outer_folds
        n_folds = len(cv_folds)
    elif isinstance(cv_folds,  list) :
        n_folds = len(cv_folds)
    elif ininstance(cv_folds, int) :
        n_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        ires_values = np.array([seq.ires_activity for seq in dataset])
        cv_folds = crossvalidation.stratified_KFold(ires_values, n_folds)
    else :
        raise Exception('Unable to parse CV folds')
        cv_folds = None
    
    train_score, test_score = {}, {}
    overall_test_score = {}  
    for score in scoring :
        test_score[score] = np.zeros((n_folds, ), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, ), dtype = np.float16)
    n_features, dataset_size = np.zeros((n_folds, ), dtype = np.int), np.zeros((n_folds, ), dtype = np.int)
    times = np.zeros((n_folds, ), dtype = np.float16)
    used_C, used_epsilon = [], []
    regressors, scaling = [], []
    feature_names = []
    all_true_y, all_predicted_y = [], []
    if client_url is None :
        for fold in range(n_folds) :
            inner_result = inner_results[fold]
            scores = np.mean(inner_result.test_score['r2'], axis = 0)
            index = np.nanargmax(scores)
            i, j = np.unravel_index(index, scores.shape)

            C = inner_result.setting_C[i]
            epsilon = inner_result.setting_epsilon[j]
            used_C.append(C)
            used_epsilon.append(epsilon)

            train_index, test_index = cv_folds[fold]
            if use_aux :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux=use_aux, fold=fold)
            else :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux=use_aux, fold=train_index)
            fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
            feature_names.append(fold_feature_names)
            X, y = build_regression_matrix(Xs, dataset_name, log2=log2)
            del Xs
            n_features[fold] = X.shape[1]
            dataset_size[fold] = len(train_index) + len(test_index)
            fold_result = svr.train_svr_cv_fold(X, y, train_index, test_index, C=C, epsilon=epsilon, normalize=True, scoring=scoring, return_regressor=return_regressor)
            if not return_regressor :
                fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = fold_result
            else :
                fold_regressor, fold_scaling, fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = fold_result
                regressors.append(fold_regressor)
                scaling.append(fold_scaling)
            all_true_y.extend(fold_true_y)
            all_predicted_y.extend(fold_predicted_y)
            for score in scoring :
                train_score[score][fold] = fold_train_score[score]
                test_score[score][fold] = fold_test_score[score]
            times[fold] = fold_time
            del X, y
    else :
        tasks = []
        for fold in range(n_folds) :
            inner_result = inner_results[fold]
            scores = np.mean(inner_result.test_score['r2'], axis = 0)
            index = np.nanargmax(scores)
            i, j = np.unravel_index(index, scores.shape)            
            C = inner_result.setting_C[i]
            epsilon = inner_result.setting_epsilon[j]
            used_C.append(C)
            used_epsilon.append(epsilon)
            train_index, test_index = cv_folds[fold]
            tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, C, epsilon, True, scoring, fold, return_regressor))
        async_results = submit_svr_tasks(client_url, tasks)
        for fold, async_result in enumerate(async_results) :
            if not return_regressor :
                fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
            else :
                fold_regressor, fold_scaling, fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
                regressors.append(fold_regressor)
                scaling.append(fold_scaling)
            feature_names.append(fold_feature_names)
            dataset_size[fold] = len(train_index) + len(test_index)
            n_features[fold] = fold_n_features
            all_true_y.extend(fold_true_y)
            all_predicted_y.extend(fold_predicted_y)
            for score in scoring :
                train_score[score][fold] = fold_train_score[score]
                test_score[score][fold] = fold_test_score[score]
            times[fold] = fold_time
    
    for score in scoring :
        overall_test_score[score] = gbrf.calculate_regression_score(all_true_y, all_predicted_y, score)
    
    result = SimpleNamespace()
    result.use_aux = use_aux
    result.features = features
    result.log2 = log2
    result.dataset_name = dataset_name
    result.train_score = train_score
    result.test_score = test_score
    result.overall_test_score = overall_test_score
    result.dataset_size = dataset_size
    result.n_features = n_features
    result.feature_names = feature_names
    result.time = times
    result.used_C = used_C
    result.used_epsilon = used_epsilon
    result.normalise = True
    if return_regressor :
        result.regressors = regressors
        result.scaling = scaling
    return result

def train_gbrf_no_cv(dataset_name, features, log2 = True, use_aux = False, learn_intercept = False, can_choose_n_estimators = False, n_estimators = 500, grid_search = _grid_search_settings, scoring = gbrf.REGRESSION_METRICS, cv_folds = None, client_url = None) :
    '''
    Compute Gradient-Boosted Regression Trees on the complete datasets.
    :param dataset_name : name of the dataset to be used.
    :param features : features to be used when constructing the regression matrix.
    :param log2 : whether a log2 transformation should be applied to the response variable.
    :param use_aux : a boolean flag determining whether precomputed auxiliary pickles should be used.
    :param learn_intercept : whether we should explicitly learn the intercept.
    :param can_choose_n_estimators : a boolean flag determining whether the regressor is allowed to use its own number of estimators.
    :param n_estimators : number of estimators to train.
    :param grid_search : a dictionary of settings to be probed in grid search.
    :param scoring : scoring function to be used.
    :param cv_folds : CV folds to be used.
    :param client_url : URL used for parallel computing with IPython.
    '''
    if not isinstance(scoring, list) :
        scoring = [scoring]
    
    if isinstance(cv_folds, SimpleNamespace) :
        cv_folds = cv_folds.outer_folds
        n_folds = len(cv_folds)
    elif isinstance(cv_folds,  list) :
        n_folds = len(cv_folds)
    elif ininstance(cv_folds, int) :
        n_folds = cv_folds
        dataset = persistent.datasets[dataset_name]
        ires_values = np.array([seq.ires_activity for seq in dataset])
        cv_folds = crossvalidation.stratified_KFold(ires_values, n_folds)
    else :
        raise Exception('Unable to parse CV folds')

    setting_learning_rate = grid_search.get('learning_rate', [0.1])
    setting_max_depth = grid_search.get('max_depth', [5])
    setting_min_samples_leaf = grid_search.get('min_samples_leaf', [1])
    setting_max_features = grid_search.get('max_features', [0.5])
    setting_subsample = grid_search.get('subsample', [0.7])
    setting_huber_alpha = grid_search.get('huber_alpha', [0.9])
    n_learning_rate = len(setting_learning_rate)
    n_max_depth = len(setting_max_depth)
    n_min_samples_leaf = len(setting_min_samples_leaf)
    n_max_features = len(setting_max_features)
    n_subsample = len(setting_subsample)
    n_huber_alpha = len(setting_huber_alpha)

    train_score, test_score = {}, {}
    overall_test_score = {}
    n_recorded_estimators = int(n_estimators / gbrf.ESTIMATOR_RECORDING_FREQUENCY)
    if n_estimators % gbrf.ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    for score in scoring :
        test_score[score] = np.zeros((n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
        train_score[score] = np.zeros((n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha, n_recorded_estimators), dtype = np.float16)
        overall_test_score[score] = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
    n_features, dataset_size = np.zeros((n_folds, ), dtype = np.int), np.zeros((n_folds, ), dtype = np.int)
    feature_names = []
    times = np.zeros((n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.float16)
    all_true_y, all_predicted_y = np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object), np.zeros((n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_huber_alpha), dtype = np.object)
    for i, learning_rate in enumerate(setting_learning_rate) :
        for j, max_depth in enumerate(setting_max_depth) :
            for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                for l, max_features in enumerate(setting_max_features) :
                    for m, subsample in enumerate(setting_subsample) :
                        for n, huber_alpha in enumerate(setting_huber_alpha) :
                            all_true_y[i, j, k, l, m, n] = []
                            all_predicted_y[i, j, k, l, m, n] = []
    if client_url is None :
        fold = 0
        for train_index, test_index in cv_folds :
            if use_aux :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = fold)
            else :
                Xs, fold_feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = train_index)
            fold_feature_names = [name for name_group in fold_feature_names for name in name_group]
            feature_names.append(fold_feature_names)
            X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)
            del Xs
            n_features[fold] = X.shape[1]
            dataset_size[fold] = len(train_index) + len(test_index)
            print '[i] Progress: ',
            for i, learning_rate in enumerate(setting_learning_rate) :
                for j, max_depth in enumerate(setting_max_depth) :
                    for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                        for l, max_features in enumerate(setting_max_features) :
                            for m, subsample in enumerate(setting_subsample) :
                                for n, huber_alpha in enumerate(setting_huber_alpha) :
                                    fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_time = gbrf.train_gbrf_cv_fold(X, y, train_index, test_index, learn_intercept = learn_intercept, n_estimators = n_estimators, min_samples_leaf = min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, max_depth = max_depth, huber_alpha = huber_alpha, scoring = scoring, return_regressor = False)
                                    all_true_y[i, j, k, l, m, n].extend(fold_true_y)
                                    all_predicted_y[i, j, k, l, m, n].extend(fold_predicted_y)
                                    for score in scoring :
                                        train_score[score][fold, i, j, k, l, m, n, :] = fold_train_score[score]
                                        test_score[score][fold, i, j, k, l, m, n, :] = fold_test_score[score]
                                    times[fold, i, j, k, l, m, n] = fold_time
                                    print '+',
            del X
            del y
            fold += 1
            print ''
    else :
        tasks, indices = [], []
        fold = 0
        for train_index, test_index in cv_folds :
            for i, learning_rate in enumerate(setting_learning_rate) :
                for j, max_depth in enumerate(setting_max_depth) :
                    for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                        for l, max_features in enumerate(setting_max_features) :
                            for m, subsample in enumerate(setting_subsample) :
                                for n, huber_alpha in enumerate(setting_huber_alpha) :
                                    tasks.append((dataset_name, features, log2, train_index, test_index, use_aux, learn_intercept, n_estimators, min_samples_leaf, learning_rate, subsample, max_features, max_depth, huber_alpha, scoring, fold, False))
                                    indices.append((fold, i, j, k, l, m, n))
            fold += 1
        async_results = submit_gbrf_tasks(client_url, tasks)

        for index, async_result in zip(indices, async_results) :
            fold, i, j, k, l, m, n = index
            fold_train_score, fold_test_score, fold_true_y, fold_predicted_y, fold_n_features, fold_feature_names, fold_time = async_result
            feature_names.append(fold_feature_names)
            all_true_y[i, j, k, l, m, n].extend(fold_true_y)
            all_predicted_y[i, j, k, l, m, n].extend(fold_predicted_y)
            dataset_size[fold] = len(train_index) + len(test_index)
            n_features[fold] = fold_n_features
            for score in scoring :
                train_score[score][fold, i, j, k, l, m, n, :] = fold_train_score[score]
                test_score[score][fold, i, j, k, l, m, n, :] = fold_test_score[score]
            times[fold, i, j, k, l, m, n] = fold_time
    
    for score in scoring :
        for i, learning_rate in enumerate(setting_learning_rate) :
            for j, max_depth in enumerate(setting_max_depth) :
                for k, min_samples_leaf in enumerate(setting_min_samples_leaf) :
                    for l, max_features in enumerate(setting_max_features) :
                        for m, subsample in enumerate(setting_subsample) :
                            for n, huber_alpha in enumerate(setting_huber_alpha) :
                                overall_test_score[score][i, j, k, l, m, n] = gbrf.calculate_regression_score(all_true_y[i, j, k, l, m, n], all_predicted_y[i, j, k, l, m, n], score)
    del all_predicted_y
    del all_true_y
   
    result = SimpleNamespace()
    result.use_aux = use_aux
    result.features = features
    result.log2 = log2
    result.dataset_name = dataset_name
    result.train_score = train_score
    result.test_score = test_score
    result.overall_test_score = overall_test_score
    result.dataset_size = dataset_size
    result.n_features = n_features
    result.feature_names = feature_names
    result.time = times
    result.learn_intercept = learn_intercept
    result.n_estimators = n_estimators
    result.estimator_recording_frequency = gbrf.ESTIMATOR_RECORDING_FREQUENCY
    result.setting_learning_rate = setting_learning_rate
    result.setting_max_depth = setting_max_depth
    result.setting_min_samples_leaf = setting_min_samples_leaf
    result.setting_max_features = setting_max_features
    result.setting_subsample = setting_subsample
    result.setting_huber_alpha = setting_huber_alpha

    estimator_recording_frequency = result.estimator_recording_frequency
    n_recorded_estimators = int(n_estimators / estimator_recording_frequency)
    scores = np.mean(result.test_score['r2'], axis = 0)
    if can_choose_n_estimators == True :
        index = np.nanargmax(scores)
        i, j, k, l, m, n, o = np.unravel_index(index, scores.shape)
    else :
        scores = scores[:, :, :, :, :, :,  -1]
        index = np.nanargmax(scores)
        i, j, k, l, m, n = np.unravel_index(index, scores.shape)
        if isinstance(can_choose_n_estimators, bool) :
            o = n_recorded_estimators - 1
        else :
            o = can_choose_n_estimators / estimator_recording_frequency - 1

    '''print i, j, k, l, m, n, o
    print setting_learning_rate
    print setting_max_depth
    print setting_min_samples_leaf
    print setting_max_features
    print setting_subsample
    print setting_huber_alpha'''
    learning_rate = setting_learning_rate[i]
    max_depth = setting_max_depth[j]
    min_samples_leaf = setting_min_samples_leaf[k]
    max_features = setting_max_features[l]
    subsample = setting_subsample[m]
    huber_alpha = setting_huber_alpha[n]
    best_n_estimators = (o + 1) * estimator_recording_frequency

    Xs, feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, fold = None)
    feature_names = [name for name_group in feature_names for name in name_group]
    X, y = build_regression_matrix(Xs, dataset_name, log2 = log2)

    train_score, overall_train_score, elapsed_time, regressor, intercept = gbrf.train_gbrf(X, y, max_depth = max_depth, n_estimators = best_n_estimators, learn_intercept = learn_intercept, min_samples_leaf = min_samples_leaf, learning_rate = learning_rate, subsample = subsample, max_features = max_features, scoring = scoring)
    
    result_final = SimpleNamespace()
    result_final.use_aux = use_aux
    result_final.dataset_name = dataset_name
    result_final.features = features
    result_final.dataset_size = dataset_size
    result_final.log2 = log2
    result_final.n_features = n_features
    result_final.feature_names = feature_names
    result_final.learning_rate = learning_rate
    result_final.used_max_depth = max_depth
    result_final.used_min_samples_leaf = min_samples_leaf
    result_final.used_max_features = max_features
    result_final.used_subsample = subsample
    result_final.used_huber_alpha = huber_alpha
    result_final.train_score = train_score
    result_final.overall_train_score = overall_train_score
    result_final.elapsed_time = elapsed_time
    result_final.learn_intercept = learn_intercept
    result_final.n_estimators = n_estimators
    result_final.used_n_estimators = best_n_estimators
    result_final.can_choose_n_estimators = can_choose_n_estimators
    result_final.regressor = regressor
    result_final.intercept = intercept

    return result, result_final

def evaluate_nocv_predictor(result, evaluate_dataset_name, scoring = gbrf.REGRESSION_METRICS) :
    '''
    Evaluate a no cv (full dataset) predictor on another dataset. Used for evaluating predictors on leaveout sets.
    :param result : result data structure with the no cv predictor.
    :param evaluate_dataset_name : name of the dataset that should be used for evaluating the predictor.
    '''
    regressor = result.regressor
    intercept = result.intercept
    dataset_name = result.dataset_name
    log2 = result.log2
    features = result.features
    use_aux = result.use_aux
    Xs, feature_names = build_regression_matrix_list(dataset_name, features, use_aux = use_aux, compute_dataset_name = evaluate_dataset_name)
    X, y = build_regression_matrix(Xs, evaluate_dataset_name, log2 = log2)
    X = np.asfortranarray(X.toarray(), dtype = np.float32)
    true_y = y
    predicted_y = regressor.predict(X) + intercept
    #predicted_y = predicted_y[:, 0]
    overall_test_score = {}
    for score in scoring :
        overall_test_score[score] = gbrf.calculate_regression_score(true_y, predicted_y, score)
    return overall_test_score

def get_random_pr_auc(dataset_name, n_permutations=10000) :
    '''
    Obtains random PR-AUC for a given dataset using sampling.
    :param dataset_name: name of the dataset for which the PR-AUC should be computed.
    :param int n_permutations: number of permutations to consider.
    '''
    seqs = persistent.datasets[dataset_name]
    y_true = np.array([seq.ires_activity for seq in seqs])
    y_true_label = np.ones(y_true.shape)
    y_true_label[y_true == tools.MINIMAL_IRES_ACTIVITY] = 0
    n_samples = len(y_true)
    random_ranking = np.array(range(n_samples), dtype = np.int)
    samples = []
    for i in xrange(n_permutations) :
        np.random.shuffle(random_ranking)
        sample = metrics.average_precision_score(y_true_label, random_ranking)
        samples.append(sample)
    return samples

def get_random_pr_curve(dataset_name) :
    '''
    Obtains random PR curve for a given dataset using sampling.
    :param dataset_name : name of the dataset for which the PR curve should be computed.
    '''
    seqs = persistent.datasets[dataset_name]
    y_true = np.array([seq.ires_activity for seq in seqs])
    y_true_label = get_labels_from_values(y_true, log2=False)
    n_samples = len(y_true)
    n_positive = np.sum(y_true_label == 1, dtype=np.int)
    fraction = n_positive / float(n_samples)
    return [fraction, fraction], [0, 1]

def get_auc(x, y):
    '''
    Get area under the curve for the given curve.
    :param np.ndarray x: x coordinates.
    :param np.ndarray y: y coordinates.
    :return: area under the curve.
    :rtype: float.
    '''
    return metrics.auc(x, y)

def make_cv_predictions(res, cv_folds):
    '''
    Make CV predictions using CV fold and regressors trains on those folds.
    :param [SimpleNamespace.SimpleNamespace] res: training result.
    :param [(np.ndarray, np.ndarray)] cv_folds: list of (train, test) cv folds.
    :returns: true and predicted values for the test CV folds.
    :rtype: (np.ndarray, np.ndarray) 
    '''
    n_folds = len(cv_folds)
    all_true_y, all_predicted_y = [], []
    for regressor, cv_fold, fold in zip(res.regressors, cv_folds, xrange(n_folds)):
        train_index, test_index = cv_fold
        fold_value = fold if res.use_aux else train_index
        Xs, feature_names = build_regression_matrix_list(res.dataset_name, res.features, use_aux=res.use_aux, fold=fold_value)
        X, y = build_regression_matrix(Xs, res.dataset_name, log2=res.log2)
        del Xs
        X, y = X[test_index, :], y[test_index]
        
        if not res.has_key('scaling'):
            X = X.toarray()
            predicted_y = regressor.predict(X)
        else:
            X_c1, X_c2, y_c1, y_c2 = res.scaling[fold]
            from svr import scale_up, rescale_X
            predicted_y = scale_up(regressor.predict(rescale_X(X, X_c1, X_c2)), y_c1, y_c2)
            
        all_true_y.extend(y)
        all_predicted_y.extend(predicted_y)
        del X, y

    return np.array(all_true_y), np.array(all_predicted_y)

def get_labels_from_values(values, log2=True):
    '''
    Construct 0/1 labels from IRES activity labels
    :param np.ndarray values: activity values.
    :param bool log2: are the considered values in log2 space?
    :return: activity labels (positive/negative).
    :rtype: np.ndarray
    '''
    labels = np.ones(values.shape)
    threshold = np.log2(tools.MINIMAL_IRES_ACTIVITY) if log2 else tools.MINIMAL_IRES_ACTIVITY
    labels[values <= threshold] = 0
    return labels

def get_roc_curve(true_values, predicted_values, log2=True):
    '''
    Compute ROC curve for a given pair of true and predicted values.
    :param np.ndarray true_values: true activity values.
    :param np.ndarray predicted_values: predicted activity values.
    :param bool log2: are the considered values in log2 space?
    :returns: false positive rates and true positive rates for various thresholds.
    :rtype: (np.ndarray, np.ndarray)
    '''
    true_labels = get_labels_from_values(true_values, log2)
    fpr, tpr, _ = metrics.roc_curve(true_labels, predicted_values, pos_label=1)
    return fpr, tpr

def get_pr_curve(true_values, predicted_values, log2=True):
    '''
    Compute PR curve for a given pair of true and predicted values.
    :param np.ndarray true_values: true activity values.
    :param np.ndarray predicted_values: predicted activity values.
    :param bool log2: are the considered values in log2 space?
    :returns: precision and recall for various thresholds.
    :rtype: (np.ndarray, np.ndarray)
    '''
    true_labels = get_labels_from_values(true_values, log2)
    precision, recall, _ = metrics.precision_recall_curve(true_labels, predicted_values, pos_label=1)
    return precision, recall

