'''
A test module containing functions relation to the interpretation of the RF models.

Alexey Gritsenko
28-08-2015
'''

import regression
import tools

import numpy as np
import pylab
import matplotlib.pyplot as plot
import matplotlib.colors

from ipyparallel import Client

def _package_partial_dependence_tasks(regressors, feature_names, value_limits, tasks, batch_size = 20) :
    '''
    Package tasks to for calculating partial dependence on variables.
    :param regressors : a list of regressors used for calculating partial dependence.
    :param feature_names : names of features to be used in regression.
    :param value_limits : a dictionary of observed feature value limits keyed by feature name.
    :param tasks : a list of feature names, for which partial dependences should be computed.
    :param batch_size : size of a single package in tasks.
    '''
    packages = []
    n_tasks = len(tasks)
    start = 0
    while start < n_tasks :
        stop = min(start + batch_size, n_tasks)
        value_limits_package = {feature_name : value_limits[feature_name] for feature_name in tasks[start:stop]}
        package = (regressors, feature_names, value_limits_package, tasks[start:stop])
        packages.append(package)
        start = stop
    return packages

def trim_pdp_vector(pdp, segment) :
    '''
    Trims partial dependence vector to remove redundant extreme values.
    :param pdp : partial dependence array.
    :param segment : starting segment, which should be trimmed further.
    '''
    len_pdp = len(pdp)
    start = 0
    while start < len_pdp - 1 and pdp[start] == pdp[start + 1] :
        start += 1
    if start > 0 :
        start -= 1
    stop = len_pdp - 1
    while stop > 0 and pdp[stop] == pdp[stop - 1] :
        stop -= 1
    stop += 1
    if stop < len_pdp :
        stop += 1
    segment_start, segment_stop = segment
    return (segment_start + start, segment_stop - (len_pdp - stop)), pdp[start : stop]

def _actual_calculate_partial_dependence(regressors, feature_names, value_limits, feature_name) :
    '''
    Function performing the actual partial dependence calculations (parallel and single-threaded).
    :param regressors : a list of regressors to be used in partial dependence calculations.
    :param feature_names : a list of regressor feature names.
    :param value_limits : an array of shape (n_folds, 2) with min and max observed values for the given feature.
    :param feature_name : name of the feature for which PDP should be calculated.
    '''
    import numpy as np
    from sklearn.ensemble.partial_dependence import partial_dependence
    from lib.interpretation import trim_pdp_vector

    overall_min_value, overall_max_value = np.nanmin(value_limits), np.nanmax(value_limits)
    overall_min_value, overall_max_value = int(overall_min_value), int(overall_max_value)
    cummulative_pdp = np.zeros((overall_max_value - overall_min_value + 1, ), dtype = np.float)
    cummulative_pdp_count = np.zeros((overall_max_value - overall_min_value + 1, ), dtype = np.float)
    n_folds = len(regressors)
    for fold in range(n_folds) :
        cur_feature_names = feature_names[fold]
        feature_id = np.where(np.array(cur_feature_names) == feature_name)[0]
        if len(feature_id) == 0 :
            continue
        feature_id = feature_id[0]
        min_value, max_value = value_limits[fold].astype(np.int)
        x = np.array(range(min_value, max_value + 1))
        pdp, axes = partial_dependence(regressors[fold], feature_id, grid = x)
        pdp = pdp[0, :]
        fold_count = np.concatenate( (np.zeros(min_value - overall_min_value, ), np.ones(pdp.shape), np.zeros((overall_max_value - max_value, ))) )
        pdp = np.concatenate((np.zeros((min_value - overall_min_value ,)), pdp, np.zeros((overall_max_value - max_value, ))))
        cummulative_pdp += pdp
        cummulative_pdp_count += fold_count
    pdp = cummulative_pdp / cummulative_pdp_count
    segment_observed = (overall_min_value, overall_max_value + 1)
    segment, pdp = trim_pdp_vector(pdp, segment_observed)
    return segment_observed, segment, pdp

def _parallel_calculate_partial_dependence(param) :
    '''
    A routine for parallel computation of partial dependences.
    :param task : parallel task to be computed given as tuple (regressors, tasks)
    '''
    from lib.interpretation import _actual_calculate_partial_dependence
    regressors, regressor_feature_names, value_limits, feature_names, = param
    del param
    results = []
    for feature_name in feature_names :
        segment_observed, segment, pdp = _actual_calculate_partial_dependence(regressors, regressor_feature_names, value_limits[feature_name], feature_name)
        results.append((segment_observed, segment, pdp))
    return results

def calculate_partial_dependence(result, cv_folds, min_n_folds = 1, min_importance = 0.0, batch_size = 20, client_url = None) :
    '''
    Calculate partial dependencies for all variables of a given result.
    :param result : result (outer CV final with regressors) for which partial dependence should be calculated.
    :parma cv_folds : CV folds to be used for computing min and max feature values in folds.
    :param min_n_folds : minimum number of folds in order for the feature to be selected for partial dependence calculations.
    :param min_importance : minimum feature importance for the feature to be selected for partial dependence calculations.
    :param batch_size : size of the batch that should be used in parallel computations.
    :param client_url : URL for IPython parallel computing.
    '''
    dataset_name = result.dataset_name
    importances = tools.get_feature_importances(result, normalize = True)
    importances = tools.average_importances(importances)
    importances_items = importances.items()
    importances_items.sort(key = lambda x : x[1][0], reverse = True)
    importances_items = [(feature_name, feature_description) for feature_name, feature_description in importances_items if feature_description[0] >= min_importance and feature_description[1] >= min_n_folds]

    feature_names = [importance_item[0] for importance_item in importances_items]
    n_folds = len(cv_folds.outer_folds)
    value_limits = {feature_name : np.zeros((n_folds, 2)) * float('NaN') for feature_name in feature_names}
    for fold in range(n_folds) :
        train_indices, _ = cv_folds.outer_folds[fold]
        Xs, fold_feature_names = regression.build_regression_matrix_list(dataset_name, result.features, fold = fold, use_aux = result.use_aux, client_url = client_url)
        X, _ = regression.build_regression_matrix(Xs, dataset_name, log2 = result.log2)
        X = X[train_indices, :]
        fold_feature_names = [feature_name for feature_name_group in fold_feature_names for feature_name in feature_name_group]
        for col, feature_name in enumerate(fold_feature_names) :
            if feature_name in feature_names :
                values = X[:, col].toarray()
                min_value, max_value = np.min(values), np.max(values)
                value_limits[feature_name][fold, :] = [min_value, max_value]
    results = {}
    if client_url is None :
        for feature_name in feature_names :
            feature_importance = importances[feature_name][0]
            segment_observed, segment, pdp = _actual_calculate_partial_dependence(result.regressors, result.feature_names, value_limits[feature_name], feature_name)
            results[feature_name] = (segment_observed, segment, pdp)
    else :
        packages = _package_partial_dependence_tasks(result.regressors, result.feature_names, value_limits, feature_names)
        if isinstance(client_url, basestring) :
            client = Client(client_url)
        else :
            client = client_url
        view = client.load_balanced_view()
        view.retries = 0
        async_results = view.map(_parallel_calculate_partial_dependence, packages, block = False, ordered = True)
        for package, package_result in zip(packages, async_results) :
            _, _, _, package_tasks = package 
            for task, result in zip(package_tasks, package_result) :
                feature_name = task
                segment_observed, segment, pdp = result
                results[feature_name] = (segment_observed, segment, pdp)
        del view
        client.close()
        del client
    return results

def manually_classify_partial_dependence(pdp, no_window_features = True) :
    '''
    Manually classify partial dependence plots (PDPs) into those with a positive, a negative and unknown effect.
    :param pdp : dictionary of calculated PDPs.
    :param no_window_features : a boolean flag determining whether window features should be also classified.
    '''
    feature_names = pdp.keys()
    if no_window_features :
        feature_names = [feature_name for feature_name in feature_names if not 'window' in feature_name]
    
    def format_feature_title(feature_name) :
        '''
        Format a printable feature name for use in the plots.
        :param feature_name : name of the feature (key) to be formatted.
        '''
        args = feature_name.split('-')
        kmer = args[1]
        if 'window' in feature_name :
            args = args[-1].split('_')
            window_start, window_stop = int(args[1]), int(args[2])
            return '%s in [%d, %d]' % (kmer, window_start, window_stop)
        else :
            return '%s' % kmer

    class status() :
        def __init__(self, feature_names) :
            self.feature_names = feature_names
            self.pos, self.n_features = 0, len(feature_names)
            self.assignment = {feature_name : None for feature_name in feature_names}
            f, ax = plot.subplots(1, 1)
            self.f, self.ax = f, ax
            self.replot()
            f.set_size_inches(7, 5, forward = True)
            f.canvas.mpl_connect('key_press_event', self.on_key_press)
        
        def replot(self) :
            '''
            '''
            print 'in replot'
            f, ax = self.f, self.ax
            current_feature = self.feature_names[self.pos]
            current_assignment = self.assignment[current_feature]
            feature_title = format_feature_title(current_feature)
            observed_segment, segment, current_pdp = pdp[current_feature]
            ax.clear()
            start, stop = segment
            x = np.array(range(start, stop))
            ax.plot(x, current_pdp, linewidth = 1.5)
            if current_assignment is not None :
                ax.text(0.8, 0.8, current_assignment, ha = 'left', va = 'bottom', transform = ax.transAxes, fontsize = 14, fontweight = 'bold')
            ax.set_xlim(start, stop - 1)
            ax.set_title(feature_title)
            ax.set_xlabel('Feature value', fontsize = 14)
            ax.set_ylabel('Partial dependence', fontsize = 14)
            f.tight_layout()
            f.canvas.draw()

        def on_key_press(self, event) :
            '''
            '''
            should_replot = False
            print event.key
            if event.key == 'right' or event.key == ' ':
                if self.pos < self.n_features - 1 :
                    self.pos += 1
                    should_replot = True
            elif event.key == 'left' :
                if self.pos > 0 :
                    self.pos -= 1
                    should_replot = True
            elif event.key == 'up' :
                current_feature_name = self.feature_names[self.pos]
                self.assignment[current_feature_name] = 'Inc'
                if self.pos < self.n_features - 1 :
                    self.pos += 1
                    should_replot = True
            elif event.key == 'down' :
                current_feature_name = self.feature_names[self.pos]
                self.assignment[current_feature_name] = 'Dec'
                if self.pos < self.n_features - 1 :
                    self.pos += 1
                    should_replot = True

            if should_replot :
                self.replot()
        
        def show(self) :
            plot.show()

    #f.tight_layout()
    stat = status(feature_names)
    stat.show()

    return stat.assignment

def classify_partial_dependence_using_derivative(pdp) :
    '''
    Automatically classify partial dependence plots (PDPs) into those with a positive and negative based on the average derivative of the PDP.
    :param pdp : dictionary of calculated PDPs.
    '''
    assignment = {}
    feature_names = pdp.keys()
    for feature_name in feature_names :
        _, _, cur_pdp = pdp[feature_name]
        length = cur_pdp.shape[0]
        diff = np.zeros((length - 1, ), dtype = np.float)
        for i in range(1, length) :
            diff[i - 1] = cur_pdp[i] - cur_pdp[i - 1]
        avg_diff = np.mean(diff)
        effect = 'Dec' if avg_diff < 0 else 'Inc'
        assignment[feature_name] = effect
    return assignment
