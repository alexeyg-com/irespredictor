'''
Module contains various functions related to SA construction.
Copied from the elongation speed project.

Alexey Gritsenko
04-02-2015
'''

import numpy as np
import persistent
import pysais

def construct_SA(dataset) :
    ''' 
    A routine for constructing suffix array for all sequences in a dataset.
    :param dataset : dataset of IRES sequences.
    ''' 
    n_strings = len(dataset)
    str_length = len(dataset[0].sequence)
    length = n_strings * (str_length + 1)
    assignment = np.zeros((length, ), np.int32)
    for i in range(n_strings) :
        start, stop = i * (str_length + 1), (i + 1) * (str_length + 1)
        assignment[start:stop] = i
    seqs = [seq.sequence for seq in dataset] 
    seqs.append('')
    sequence = '$'.join(seqs)
    sa = pysais.sais(sequence)
    lcp, lcp_left, lcp_right = pysais.lcp(sequence, sa) 
    return sequence, assignment, sa, lcp, lcp_left, lcp_right

def construct_structure_SA(dataset, structures = None) :
    ''' 
    A routine for constructing suffix array for all structures a dataset.
    :param dataset : dataset of IRES sequences.
    :param structures : a dictionary of structures (id -> MFE structure).
    '''
    if structures is None :
        structures = persistent.structures
    n_strings = len(dataset)
    example_structures = structures[dataset[0].index]
    str_length = len(example_structures[0])
    n_structures_per_sequence = len(example_structures)
    length = n_strings * n_structures_per_sequence * (str_length + 1)
    assignment = np.zeros((length, ), np.int32)
    for i in range(n_strings) :
        start, stop = i * (str_length + 1) * n_structures_per_sequence, (i + 1) * (str_length + 1) * n_structures_per_sequence
        assignment[start:stop] = i
    structs = [struct for seq in dataset for struct in structures[seq.index]] 
    structs.append('')
    structure = '$'.join(structs)
    sa = pysais.sais(structure)
    lcp, lcp_left, lcp_right = pysais.lcp(structure, sa) 
    return structure, assignment, sa, lcp, lcp_left, lcp_right

