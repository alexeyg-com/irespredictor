'''
Implements various useful routines for the project.

Alexey Gritsenko
01-01-2015
'''

import math
import copy
import persistent
import scipy.stats
import numpy as np
import copy

EPS = 1e-6
MINIMAL_IRES_ACTIVITY = 206.29

_lib_set = {'CDS_screen',
           'Genome_Wide_Sceen_Elements',
           'HIV_Clinical_Samples',
           'High_Priority_Genes_Blocks',
           'High_Priority_Viruses_Blocks',
           'HumanSNP_IRESite',
           'HumanSNP_rRNA_matching_5UTRs',
           'Human_5UTR_Screen',
           'IRESite_blocks',
           'Multiple_10nt_barcode_synthetic',
           'Multiple_8nt_barcodes',
           'Riboswitches',
           'Scanning_Mut_CDS_IRESs',
           'Scanning_Mut_High_Priority_Genes_5UTR',
           'Scanning_Mut_High_Priority_Viruses_5UTR',
           'Scanning_Mut_IRESite',
           'Scanning_Mut_rRNA_double_mutations',
           'Scanning_Mut_rRNA_matching_5UTR',
           'Scrambled_IRESite_Seq',
           'Synthetic_IRES_Combinations',
           'Synthetic_IRES_Doublet_Mutations',
           'Synthetic_IRES_Orientation',
           'Synthetic_IRES_Single_Mutations',
           'Synthetic_IRES_backgrounds_multiplicity_no_comb',
           'Synthetic_IRES_distance',
           'Synthetic_IRES_multiplicity_all_comb',
           'Synthetic_IRES_rRNA_comp',
           'Viral_5UTR_Screen',
           'rRNA_Matching_5UTRs',
           'rRNAs_revcomp_blocks'}

predefined_groups = {
    'native' : {
        'lib' : set(['CDS_screen', 'Genome_Wide_Sceen_Elements', 'High_Priority_Genes_Blocks', 'High_Priority_Viruses_Blocks', 'Human_5UTR_Screen', 'IRESite_blocks', 'Viral_5UTR_Screen', 'rRNA_Matching_5UTRs'])
        },
    'synthetic' : {
        'lib' : set(['Synthetic_IRES_Combinations', 'Synthetic_IRES_Doublet_Mutations', 'Synthetic_IRES_Orientation', 'Synthetic_IRES_Single_Mutations', 'Synthetic_IRES_backgrounds_multiplicity_no_comb', 'Synthetic_IRES_distance', 'Synthetic_IRES_multiplicity_all_comb', 'Synthetic_IRES_rRNA_comp', 'rRNAs_revcomp_blocks', 'Scrambled_IRESite_Seq'])
        },
    'scanning_mut' : {
        'lib' : set(['Scanning_Mut_CDS_IRESs', 'Scanning_Mut_High_Priority_Genes_5UTR', 'Scanning_Mut_High_Priority_Viruses_5UTR', 'Scanning_Mut_IRESite', 'Scanning_Mut_rRNA_double_mutations', 'Scanning_Mut_rRNA_matching_5UTR'])
        },
    'clinical' : {
        'lib' : set(['HIV_Clinical_Samples'])
        },
    'snp' : {
        'lib' : set(['HumanSNP_IRESite', 'HumanSNP_rRNA_matching_5UTRs'])
        },
    'riboswitches' : {
         'lib' : set(['Riboswitches'])
         },
    'native_5utr' : {
        'lib' : set(['CDS_screen', 'Genome_Wide_Sceen_Elements', 'High_Priority_Genes_Blocks', 'High_Priority_Viruses_Blocks', 'Human_5UTR_Screen', 'IRESite_blocks', 'Viral_5UTR_Screen', 'rRNA_Matching_5UTRs']),
        'location' : set(['5UTR'])
        },
    'native_cds' : {
        'lib' : set(['CDS_screen', 'Genome_Wide_Sceen_Elements', 'High_Priority_Genes_Blocks', 'High_Priority_Viruses_Blocks', 'Human_5UTR_Screen', 'IRESite_blocks', 'Viral_5UTR_Screen', 'rRNA_Matching_5UTRs']),
        'location' : set(['CDS'])
        },
    'native_3utr' : {
        'lib' : set(['CDS_screen', 'Genome_Wide_Sceen_Elements', 'High_Priority_Genes_Blocks', 'High_Priority_Viruses_Blocks', 'Human_5UTR_Screen', 'IRESite_blocks', 'Viral_5UTR_Screen', 'rRNA_Matching_5UTRs']),
        'location' : set(['3UTR'])
        },
    'human_native' : {
             'lib' : set(['CDS_screen', 'High_Priority_Genes_Blocks', 'Human_5UTR_Screen', 'rRNA_Matching_5UTRs'])
         },
    'viral_native' : {
             'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen'])
         },
    'human_native_5utr' : {
             'lib' : set(['CDS_screen', 'High_Priority_Genes_Blocks', 'Human_5UTR_Screen', 'rRNA_Matching_5UTRs']),
             'location' : set(['5UTR'])
         },
    'human_native_cds' : {
             'lib' : set(['CDS_screen', 'High_Priority_Genes_Blocks', 'Human_5UTR_Screen', 'rRNA_Matching_5UTRs']),
             'location' : set(['CDS'])
         },
    'human_native_3utr' : {
             'lib' : set(['CDS_screen', 'High_Priority_Genes_Blocks', 'Human_5UTR_Screen', 'rRNA_Matching_5UTRs']),
             'location' : set(['3UTR'])
         },
    'viral_native_RTV' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['Retro-transcribingviruses'])
        },
    'viral_native_dsRNA_noRNA' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['dsDNAviruses,noRNAstage'])
        },
    'viral_native_dsRNA' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['unclassifiedarchaealdsDNAviruses'])
        },
    'viral_native_ssRNA' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['ssDNAviruses'])
        },
    'viral_native_ssRNA_negative' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['ssRNAnegative-strandviruses'])
        },
    'viral_native_ssRNA_positive' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['ssRNApositive-strandviruses,noDNAstage'])
        },
    'viral_native_ssRNA_positive_uncapped' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['Picornaviridae', 'Hepacivirus', 'Pegivirus', 'Pestivirus'])
        },
    'viral_native_ssRNA_positive_capped' : {
                'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
                'virus_type' : set(['Flavivirus'])
        },
    'viral_native_ssRNA_all' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['ssRNApositive-strandviruses,noDNAstage', 'ssRNAnegative-strandviruses'])
        },
    'viral_native_dsRNA_all' : {
            'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
            'virus_type' : set(['dsDNAviruses,noRNAstage', 'unclassifiedarchaealdsDNAviruses'])
        },
    'viral_native_5utr' : {
             'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
             'location' : set(['5UTR'])
         },
    'viral_native_cds' : {
             'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
             'location' : set(['CDS'])
         },
    'viral_native_3utr' : {
             'lib' : set(['High_Priority_Viruses_Blocks', 'Viral_5UTR_Screen']),
             'location' : set(['3UTR'])
         },
    'mult_all_comb' : {
            'lib' : set(['Synthetic_IRES_multiplicity_all_comb'])
        },
    'distance' : {
            'lib' : set(['Synthetic_IRES_distance'])
        },
    'combinations' : {
            'lib' : set(['Synthetic_IRES_Combinations'])
        },
    'backgrounds' : {
            'lib' : set(['Synthetic_IRES_backgrounds_multiplicity_no_comb'])
        }
    }

def get_group_sequences(seqs, group_name) :
    '''
    Select sequences belonging to a given predefined group.
    :param seqs : a list of sequences from which the selection should be performed.
    :param group_name : name of the group that should be selected.
    '''
    filters = predefined_groups[group_name]
    for criterion, value in filters.iteritems() :
        selected = []
        for seq in seqs :
            should_add = False
            if criterion == 'lib' :
                sequence_libs = get_sequence_libs(seq)
                for lib in sequence_libs :
                    if lib in value :
                        should_add = True
                        break
            elif criterion == 'location' :
                sequence_locations = get_sequence_location(seq)
                for location in sequence_locations :
                    if location in value :
                        should_add = True
                        break
            elif criterion == 'virus_type' :
                sequence_types = get_sequence_virus_type(seq)
                for type in sequence_types :
                    if type in value :
                        should_add = True
                        break
            if should_add :
                selected.append(seq)
        #print '%s : from %d to %d' % (criterion, len(seqs), len(selected))
        #print value
        seqs = selected
    return seqs

def get_sequence_lib_set(sequences) :
    '''
    Returns a set of sequence libraries that can be used for selection based on the library.
    :param sequences : a list of sequences to be used when determining libraries.
    '''
    lib_groups = [seq.description.split('|') for seq in sequences]
    libs = set()
    for lib_group in lib_groups :
        for i, lib in enumerate(lib_group) :
            if i == 0 :
                libs.add(lib.split(';')[1])
            else :
                libs.add(lib.split(';')[0])
    return libs

def get_sequence_virus_type(seq) :
    '''
    seq : sequence for which virus type should be determined.
    '''
    lib_group = seq.description.split('|')
    found_types = []
    for i, lib in enumerate(lib_group) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        lib = args[0]
        if lib == 'High_Priority_Viruses_Blocks' :
            id = args[1]
        elif lib == 'Viral_5UTR_Screen' :
            id = args[2]
        else :
            found_types.append(None)
            continue
        type = persistent.virus_types.get(id, [None])
        if type[0] == 'None' :
            print seq
        found_types.extend(type)
    return found_types

def get_sequence_combination_info(seq) :
    '''
    seq : sequence for which combination info should be obtained.
    '''
    lib_group = seq.description.split('|')
    infos = []
    for i, lib in enumerate(lib_group) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        lib = args[0]
        if lib == 'Synthetic_IRES_multiplicity_all_comb' :
            type, background = args[1], args[2]
            n_sites = int(args[3])
            site_ids = [int(id) for id in args[4].split(',') if id != '-']
            site_positions = [int(id) for id in args[5].split(',') if id != '-']
            info = {'type' : type, 'background' : background, 'n_sites' : n_sites, 'site_ids' : site_ids, 'site_positions' : site_positions}
            infos.append(info)
    return infos

def get_sequence_background_info(seq) :
    '''
    seq : sequence for which combination info should be obtained.
    '''
    lib_group = seq.description.split('|')
    infos = []
    for i, lib in enumerate(lib_group) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        lib = args[0]
        if lib == 'Synthetic_IRES_backgrounds_multiplicity_no_comb' :
            type, background = args[1], args[2]
            site_length = int(args[3])
            n_sites = int(args[4])
            site_ids = [int(id) for id in args[5].split(',') if id != '-' and id != '']
            site_positions = [int(id) for id in args[6].split(',') if id != '-' and id != '']
            info = {'type' : type, 'background' : background, 'site_length' : site_length, 'n_sites' : n_sites, 'site_ids' : site_ids, 'site_positions' : site_positions}
            infos.append(info)
    return infos

def get_sequence_distance_info(seq) :
    '''
    seq : sequence for which distance info should be obtained.
    '''
    lib_group = seq.description.split('|')
    infos = []
    for i, lib in enumerate(lib_group) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        lib = args[0]
        if lib == 'Synthetic_IRES_distance' :
            type, background = args[1], args[2]
            n_sites = int(args[3])
            site_positions = [int(id) for id in args[4].split(',') if id != '-']
            site_length = int(args[5])
            info = {'type' : type, 'background' : background, 'n_sites' : n_sites, 'site_positions' : site_positions, 'site_length' : site_length}
            infos.append(info)
    return infos

def get_sequence_IRESite_source(seq) :
    '''
    seq : sequence for which IRESite source (human/viral) should be determined.
    '''
    lib_group = seq.description.split('|')
    found_types = []
    for i, lib in enumerate(lib_group) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        lib = args[0]
        if lib == 'IRESite_blocks' :
            type = args[1]
            found_types.append(type)
        else :
            found_types.append(None)
            continue
    return found_types

def trim_ires_activity(seqs, quantile = 0.95) :
    '''
    Trim high IRES activity values.
    :param seqs : sequences in which IRES activity should be trimmed.
    :param quantile : quantile to which the activity should be trimmed.
    '''
    ires_activity = np.array([seq.ires_activity for seq in seqs])
    quantile = scipy.stats.mstats.mquantiles(ires_activity, [quantile])[0]
    seqs = copy.deepcopy(seqs)
    for seq in seqs :
        if seq.ires_activity > quantile :
            seq.ires_activity = quantile
    return seqs

def merge_ires_activity_replicates(seqs) :
    '''
    Merge IRES activity replicates into a single number.
    :param seqs : a list of sequences for which replicated measurements should be merged.
    '''
    seqs = copy.deepcopy(seqs)
    for seq in seqs :
        if seq.ires_activity_replicate1 == seq.ires_activity_replicate2 :
            seq.ires_activity = seq.ires_activity_replicate1
        else :
            seq.ires_activity = scipy.stats.mstats.gmean([seq.ires_activity_replicate1, seq.ires_activity_replicate2])
    return seqs

def filter_replicates(sequences) :
    '''
    Filter sequences based on agreement of the IRES replicate measurements.
    :param sequences : a list of sequences to be filtered.
    '''
    results = []
    for seq in sequences :
        value1, value2 = seq.ires_activity_replicate1, seq.ires_activity_replicate2
        bg_cnt = 0
        if value1 == MINIMAL_IRES_ACTIVITY and not math.isnan(value1) :
            bg_cnt += 1
        if value2 == MINIMAL_IRES_ACTIVITY and not math.isnan(value2) :
            bg_cnt += 1
        if bg_cnt == 1 :
            continue
        results.append(seq)
    return results

def filter_sequences(sequences, ires_threshold = -float('inf'), promoter_threshold = 0.2, splicing_threshold = -2.5, remove_nan_splicing = False, remove_nan_promoter = False) :
    '''
    Filters out sequences that do not pass the threshold cutoffs.
    :param sequences : a list of sequences to be filtered.
    :param ires_threshold : minimum IRES activity threshold.
    :param promoter_threshold : maximum promoter threshold.
    :param splicing_threshold : minimum splicing score threshold.
    :param remove_nan_splicing : whether oligos with NaN splicing score should be removed.
    :param remove_nan_promoter : whether oligos with NaN promoter activity should be removed.
    '''
    results = []
    for seq in sequences :
        if seq.ires_activity < ires_threshold or math.isnan(seq.ires_activity) :
            continue
        if seq.promoter_activity >= promoter_threshold and not math.isnan(seq.promoter_activity) :
            continue
        if remove_nan_promoter and math.isnan(seq.promoter_activity) :
            continue
        if seq.splicing_score <= splicing_threshold and not math.isnan(seq.splicing_score) :
            continue
        if remove_nan_splicing and math.isnan(seq.splicing_score) and seq.ires_activity != MINIMAL_IRES_ACTIVITY :
            continue
        results.append(seq)
    return results

def select_sequences_by_id(sequences, ids) :
    '''
    Selects sequences from a list based on their IDs.
    :param sequences : a list of sequences to be selected from.
    :param ids : a list of IDs that should be selected.
    '''
    indexed = {}
    for seq in sequences :
        indexed[seq.index] = seq

    selected, not_found = [], []
    for id in ids :
        if indexed.has_key(id) :
            selected.append(indexed[id])
        else :
            not_found.append(id)
    not_found = list(set(not_found))
    print '[!] Missing IDs: %s' % ' '.join([str(id) for id in not_found])
    print '[!] Total missing: %d' % len(not_found)
    return selected

def get_sequences_by_lib(sequences, libs) :
    '''
    Selects sequences from a list based on their libraries.
    :param sequences : a list of sequences to be selected from.
    :param libraries : a list of libraries that should be selected.
    '''
    if not isinstance(libs, list) and not isinstance(libs, set) :
        libs = [libs]
    libs = set(libs)
    selected = []
    for seq in sequences :
        lib_group = seq.description.split('|')
        if not isinstance(lib_group, list) :
            lib_group = [lib_group]
        for i, lib in enumerate(lib_group) :
            actual_lib = lib.split(';')[1] if i == 0 else lib.split(';')[0]
            if actual_lib in libs :
                selected.append(seq)
                break
    return selected

def get_sequences_by_location(sequences, locations) :
    '''
    Selects sequences from a list based on their location.
    :param sequences : a list of sequences to be selected from.
    :param locations : a list of locations to be selected.
    '''
    if not isinstance(locations, list) and not isinstance(locations, set) :
        locations = [locations]
    locations = set(locations)
    selected = []
    for seq in sequences :
        seq_locations = get_sequence_location(seq)
        should_add = False
        for location in seq_locations :
            if location in locations :
                should_add = True
                break
        if should_add :
            selected.append(seq)
    return selected

def get_sequences_by_virus_type(sequences, types) :
    '''
    Selects sequences from a list based on their virus type.
    :param sequences : a list of sequences to be selected from.
    :param locations : a list of types to be selected.
    '''
    if not isinstance(types, list) and not isinstance(types, set) :
        types = [types]
    types = set(types)
    selected = []
    for seq in sequences :
        seq_types = get_sequence_virus_type(seq)
        should_add = False
        for type in seq_types :
            if type in types :
                should_add = True
                break
        if should_add :
            selected.append(seq)
    return selected

def trim_sequences(sequences, ltrim = 18, rtrim = 20) :
    '''
    Trim sequences by removing some bases from their beginning and end.
    :param ltrim : number of sequences to remove on the left.
    :param rtrim : number of sequences to remove on the right.
    '''
    results = []
    for seq in sequences :
        new_seq = copy.deepcopy(seq)
        new_seq.sequence = new_seq.sequence[ltrim:-rtrim]
        results.append(new_seq)
    return results

def grow_sequences(sequences, left_seq = 'AACAGTACGAGCGCGCCGAGGGCCGCCACTCCACCGGCGCCGTGGAGGTGGCTCTAGACGTTTAAA', right_seq = 'CAAGGGCGAGGAGCTGTTCACCGGGGTGGTGCCCATCCTGGTCGAGCTGGAC') :
    '''
    Grow sequences by adding constant context sequences at their beginning and end.
    :param left_seq : left sequence to be appended.
    :param right_seq : right sequence to be appended.
    '''
    results = []
    for seq in sequences :
        new_seq = copy.deepcopy(seq)
        new_seq.sequence = left_seq + new_seq.sequence + right_seq
        results.append(new_seq)
    return results

def get_sequence_libs(seq) :
    '''
    Return libraries to which a particular sequence belongs.
    :param seq : sequence from which libraries should be extracted.
    '''
    libs = seq.description.split('|')
    actual_libs = []
    for i, lib in enumerate(libs) :
        actual_lib = lib.split(';')[1] if i == 0 else lib.split(';')[0]
        actual_libs.append(actual_lib)
    return actual_libs

def get_sequence_sources(seq) :
    '''
    Return libraries to which a particular sequence belongs.
    :param seq : sequence from which libraries should be extracted.
    '''
   
    libs = seq.description.split('|')
    sources = set()
    for i, lib in enumerate(libs) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        for j, arg in enumerate(args) :
            if arg in _lib_set :
                sources.add(args[j + 1])
                break
    return sources

def get_sequence_location(seq) :
    '''
    Return location(s) of a particular sequence.
    :param seq : sequence for which location should be determined.
    '''
    oligo_length = 212 - 18 - 20
    mid_offset = oligo_length / 2
    libs = seq.description.split('|')
    locations = []
    for i, lib in enumerate(libs) :
        args = lib.split(';')
        if i == 0 :
            args = args[1:]
        lib = args[0]
        if lib == 'High_Priority_Genes_Blocks' :
            cds_start, cds_stop = int(args[3]) - 1, int(args[4])
            position = int(args[7])
            mid = position + mid_offset
            if mid < cds_start :
                location = '5UTR'
            elif mid < cds_stop :
                location = 'CDS'
            else :
                location = '3UTR'
        elif lib == 'CDS_screen' :
            cds_start, cds_stop = int(args[3]) - 1, int(args[4])
            position = max(0, cds_start - oligo_length)
            mid = position + mid_offset
            if mid < cds_start :
                location = '5UTR'
            elif mid < cds_stop :
                location = 'CDS'
            else :
                location = '3UTR'
        elif lib == 'Human_5UTR_Screen' :
            cds_start, cds_stop = int(args[3]) - 1, int(args[4])
            position = max(0, cds_start - oligo_length)
            mid = position + mid_offset
            if mid < cds_start :
                location = '5UTR'
            elif mid < cds_stop :
                location = 'CDS'
            else :
                location = '3UTR'
        elif lib == 'Viral_5UTR_Screen' :
            cds_start, cds_stop = int(args[5]) - 1, int(args[6])
            type = args[1]
            if type == 'CDS_centered' :
                position = cds_start - oligo_length / 2
            elif type == 'upstream_CDS' :
                position = max(0, cds_start - oligo_length)
            mid = position + mid_offset
            if mid < cds_start :
                location = '5UTR'
            elif mid < cds_stop :
                location = 'CDS'
            else :
                location = '3UTR'
        elif lib == 'rRNA_Matching_5UTRs' :
            location = '5UTR'
        elif lib == 'High_Priority_Viruses_Blocks' :
            position = int(args[5]) - 1
            index = seq.index
            if index in persistent.virus_cds_coordinates :
                cds_start, cds_stop = persistent.virus_cds_coordinates[index]
                mid = position + mid_offset
                if mid < cds_start :
                    location = '5UTR'
                elif mid < cds_stop :
                    location = 'CDS'
                else :
                    location = '3UTR'
            else :
                location = None # We don't have any good way of determining location for oligos in this sub-library
        else :
            location = None
        locations.append(location)
        #print args
    return locations

def get_feature_importances(result, normalize = False) :
    '''
    Returns feature importances dictionary for a given result.
    :param result : result to be used for extracting feature importances.
    :param normalize : whether feature importances should be normalized.
    '''
    features = {}
    for regressor, feature_names in zip(result.regressors, result.feature_names) :
        importances = regressor.feature_importances_
        if normalize :
            imp_max = np.max(importances)
            if abs(imp_max) < 1e-10 :
                imp_max = 1.0
            importances = importances / imp_max
        for feature, importance in zip(feature_names, importances) :
            if not feature in features :
                features[feature] = []
            features[feature].append(importance)
    return features

def process_importances(result) :
    '''
    Separate feature importances into three categories: presence-window, count-windows and count.
    :param result : result to be processed.
    '''
    importances = get_feature_importances(result)
    kmers, kmers_window = {}, {}
    for feature in importances :
        args = feature.split('-')
        type = args[0]
        if not type in ['kmers'] :
            continue
        kmer = args[1]
        if 'window' in feature :
            args = args[-1].split('_')
            window = (int(args[-2]), int(args[-1]))
        else :
            window = None
        if window is None :
            if type == 'kmers' :
                kmers[kmer] = importances[feature]
        else :  
            if type == 'kmers' :
                if not kmer in count_window :
                    kmers_window[kmer] = {} 
                kmers_window[kmer][window] = importances[feature]

    return kmers, kmers_window

def average_importances(importances, n_folds = 10, flag = False) :
    '''
    Average importances per feature.
    :param importances : dictionary of importances to be averaged.
    :param n_folds : number of folds to be used/assumed for averaging.
    '''
    new_importances = {}
    for feature in importances :
        if flag :
            if 'CAG' in feature :
                print feature, feature_importances, n_folds
        feature_importances = np.array(importances[feature])
        new_importances[feature] = (np.nansum(feature_importances) / n_folds, len(feature_importances[~np.isnan(feature_importances)]))
    return new_importances

