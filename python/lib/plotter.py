'''
A set of routines for visualizing various aspects of the IRES predictor project and data.

Alexey Gritsenko
04-02-2015
'''

import pkg_resources
pkg_resources.require("matplotlib") 

import matplotlib
import numpy as np
import pylab
import matplotlib.pyplot as plot
import matplotlib.colors
import scipy.stats
import math
import os
import copy

import tools
import reader
import prepare

def plot_kmer_windows_profile(kmers, max_length = 8, other_reading_frames = True, window_size = 20, window_step = 10, cutoff = 0.05, sequence_length = 174, filename = None) :
    '''
    Plot k-mer p-values for a given list of kmers.
    :param kmers : a list of kmers that should be processed.
    :param max_length : maximum allowed k-mer length.
    :param other_reading_frames : whether we should plot other reading frames.
    :param window_size : size of the moving window.
    :param window_step : step size of the moving window.
    :param cutoff : a p-value cutoff for k-mers. K-mers with a p-value lower than the cutoff are not included in the list.
    :param sequence_length : length of the sequence.
    :param filename : name of the file to which the image should be written.
    '''
    window_to_offset = {}
    start, k = 0, 0
    x = []
    while start < sequence_length :
        window_to_offset[start] = k
        x.append(start)
        k += 1
        start += window_step
    n_windows = k
    
    if other_reading_frames :
        n_rows, n_cols = 8, 7
        reading_frames = ['0', '1', '2', '0+1', '0+2', '1+2', 'all']
    else :
        n_rows, n_cols = 4, 2
        reading_frames = ['all']
    f, ax = plot.subplots(n_rows, n_cols, sharex = True, sharey = True)
    row, col = 0, 0
    cur_max_length = 1
    while cur_max_length <= max_length :
        for cur_frame in reading_frames :
            cur = ax[row, col]
            kmer_dict = {}
            for kmer, window, frame, coef_r, pvalue in kmers :
                if len(kmer) > cur_max_length :
                    continue
                if str(frame) != cur_frame :
                    continue
                window_start, window_stop = window
                offset = window_to_offset[window_start]
                if not kmer in kmer_dict :
                    kmer_dict[kmer] = np.zeros((n_windows, )) * float('nan')
                if math.isnan(kmer_dict[kmer][offset]) :
                    kmer_dict[kmer][offset] = -np.log(pvalue)

            #for kmer in kmer_dict :
            #    kmer_dict[kmer][np.isnan(kmer_dict[kmer])] = -np.log(cutoff)
            kmer_array = np.atleast_2d([kmer_dict[kmer] for kmer in kmer_dict])

            #min_pvalue = np.min(kmer_array[indices], axis = 0)
            #max_pvalue = np.max(kmer_array[indices], axis = 0)
            #avg_pvalue = np.mean(kmer_array[indices], axis = 0)
            min_pvalue, max_pvalue, avg_pvalue = np.zeros((n_windows, )) * float('nan'), np.zeros((n_windows, )) * float('nan'), np.zeros((n_windows,)) * float('nan')
            if len(kmer_dict) > 0 :
                for i in range(n_windows) :
                    vals = kmer_array[:, i]
                    vals = vals[np.logical_not(np.isnan(vals))]
                    if len(vals) == 0 :
                        continue
                    min_pvalue[i] = np.min(vals)
                    max_pvalue[i] = np.max(vals)
                    avg_pvalue[i] = np.mean(vals)

            cur.plot(x, max_pvalue, marker = 'o', markersize = 5, markerfacecoloralt = None, label = 'Max')
            cur.plot(x, min_pvalue, marker = 'o', markersize = 5, markerfacecoloralt = None, label = 'Min')
            cur.plot(x, avg_pvalue, marker = 'o', markersize = 5, markerfacecoloralt = None, label = 'Mean')
            cur.set_title('$k\\leq%d$, %s RFs' % (cur_max_length, cur_frame), fontsize = 12)
            cur.set_xlim(0, sequence_length - 1)
            for tick in cur.xaxis.get_major_ticks():
                tick.label.set_fontsize(8)
            for tick in cur.yaxis.get_major_ticks():
                tick.label.set_fontsize(6)
            if col == 0 :
                cur.set_ylabel('$-\\log_{10} P$', fontsize = 12)
            if row == n_rows - 1 :
                cur.set_xlabel('Position', fontsize = 12)
            col += 1
            if col >= n_cols :
                row += 1
                col = 0
        cur_max_length += 1

    ax[0, 0].legend(ncol = 3, prop = {'size' : 8})
    f.set_size_inches(18, 13, forward = True)
    f.tight_layout()
    if filename is not None :
        f.savefig(filename, bbox_inches = 'tight')
    else :
        plot.show()

def plot_ires_activity_histogram(sequences, ires_threshold = tools.MINIMAL_IRES_ACTIVITY, n_bins = 30, log2 = True) :
    '''
    Plot a histogram of IRES activity.
    :param sequences : a list of measured sequences.
    :param ires_threshold : threshold of background IRES activity.
    :param n_bins : number of bins to use when plotting.
    '''
    ires_activity = [seq.ires_activity for seq in sequences]
    ires_activity = np.array(ires_activity)
    if log2 :
        ires_activity = np.log2(ires_activity)
        ires_threshold = np.log2(ires_threshold)

    mi, ma = np.min(ires_activity), np.max(ires_activity)
    n_low, n_high = int(n_bins * (ires_threshold + tools.EPS - mi) / (ma - mi)), int(n_bins * (ma - ires_threshold - tools.EPS) / (ma - mi))
    if n_low + n_high != n_bins :
        n_high += 1
    print n_low, n_high
    if n_low == 0 :
        n_low += 1
        n_high -= 1
    bins_high = np.linspace(ires_threshold + tools.EPS, ma, n_high, endpoint = True)
    width = bins_high[1] - bins_high[0]
    low_start = mi
    if n_low == 1 :
        low_start = ires_threshold - width
    bins_low = np.linspace(low_start, ires_threshold + tools.EPS, n_low, endpoint = False)
    bins = np.concatenate((bins_low, bins_high))
    print bins
    counts = np.zeros((n_bins - 1, ), dtype = np.int)
    for i in range(n_bins - 1) :
        counts[i] = np.sum(np.logical_and(ires_activity >= bins[i], ires_activity < bins[i + 1]), dtype = np.int)
    f, ax = plot.subplots(1, 1)
    #x = (bins[1: ] + bins[ :-1]) / 2.0
    x = bins[:-1]
    
    n, bins_plotted, patches = ax.hist(x, bins, weights = counts, normed = False)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    for thisbin, thispatch in zip(bins_plotted, patches) :
        if thisbin <= ires_threshold :
            thispatch.set_facecolor('r')
    ax.legend(patches, ['Inactive', 'Active'], ncol = 2, prop = {'size' : 14}, loc = 'upper right')
    if not log2 :
        ax.set_xlabel('IRES activity', fontsize = 16)
    else :
        ax.set_xlabel('IRES activity, $log_2$', fontsize = 16)
    ax.set_ylabel('Number of sequences', fontsize = 16)
    
    ax.set_xlim(np.min(bins), np.max(bins) + width)
    positions = np.linspace(low_start, ma, 5)
    positions_str = ['%.2f' % pos for pos in positions]
    ax.set_xticks(positions)
    ax.set_xticklabels(positions_str)

    from mpl_toolkits.axes_grid1.inset_locator import inset_axes
    from mpl_toolkits.axes_grid1.inset_locator import mark_inset
    zoom = inset_axes(ax, 5.5, 4, loc = 2, bbox_to_anchor = (0.1, 0.90), bbox_transform = ax.transAxes)
    n, bins_plotted, patches = zoom.hist(x[1:], bins[1:], weights = counts[1:], normed = False)
    pylab.setp(patches, 'facecolor', 'b', 'alpha', 0.45)
    zoom.set_xlim(ires_threshold, np.max(bins) + width)
    pp, p1, p2 = mark_inset(ax, zoom, loc1 = 3, loc2 = 4, fc = 'none', ec = '0.5')
    pp.set_ec('none')
    
    positions = np.linspace(ires_threshold, ma, 5)
    positions_str = ['%.2f' % pos for pos in positions]
    zoom.set_xticks(positions)
    zoom.set_xticklabels(positions_str)

    
    #f.set_size_inches(5, 5, forward = True)
    f.tight_layout()
    plot.show()

def plot_mfe_correlation_matrix(matrix, title = 'MFE correlation', filename = None) :
    '''
    Plot segment correlation matrix.
    :param matrix : correlation matrix to be plotted.
    '''
    f, ax = plot.subplots(1, 1)
    matrix = np.ma.masked_array(matrix)
    non_nan = np.logical_not(np.isnan(matrix))
    if np.sum(non_nan, dtype = np.int) > 0 :
        ma = np.max(np.abs(matrix[non_nan]))
    else :
        ma = 0
    cax = ax.imshow(matrix.transpose(), interpolation='nearest', cmap = 'RdBu', vmin = -ma, vmax = ma)
    f.colorbar(cax)
    ax.set_title(title, fontsize = 16)
    ax.set_xlabel('Start position', fontsize = 16)
    ax.set_ylabel('Stop position', fontsize = 16)
    f.set_size_inches(10, 10, forward = True)
    f.tight_layout()
    if filename is not None :
        f.savefig(filename, bbox_inches = 'tight')
    else :
        plot.show()

def plot_positive_and_negative(seqs, log2 = True) :
    '''
    Plot positive and negative sequences together.
    :param seqs : oligos for which IRES activity should be plotted.
    :param log2 : whether we should plot log2 activity.
    '''
    spacing_max = 20
    positive_seqs, negative_seqs = tools.get_positive_sequences(seqs)
    all_ids, positive_ids, negative_ids = set(), set(), set()
    id2seq = {}
    for seq in seqs :
        all_ids.add(seq.index)
        id2seq[seq.index] = seq
    for seq in positive_seqs :
        positive_ids.add(seq.index)
    for seq in negative_seqs :
        negative_ids.add(seq.index)

    if all_ids != positive_ids | negative_ids :
        print '[!] Error: all indices does not equal to positive indices + negative indices.'
    if positive_ids & negative_ids != set() :
        print '[!] Error: positive and negative indices overlap.'

    offsets, colors = {}, {}
    for index in all_ids :
        offsets[index] = np.random.uniform(spacing_max)
        colors[index] = 0
        if index in positive_ids :
            colors[index] = 1
            if math.isnan(id2seq[index].splicing_score) :
                colors[index] = 2
                print id2seq[index]
        if index in negative_ids :
            colors[index] = 3
            if math.isnan(id2seq[index].splicing_score) :
                colors[index] = 4
                print id2seq[index]

    x = np.array([id2seq[index].ires_activity for index in all_ids])
    if log2 :
        x = np.log2(x)
    y = np.array([offsets[index] for index in all_ids])
    colors = np.array([colors[index] for index in all_ids])

    color_types = [0, 1, 2, 3, 4]
    color_type_colors = ['r', 'b', 'g', 'y', 'k']
    color_type_labels = ['Unknown', 'Positive', 'Positive (splicing NaN)', 'Negative', 'Negative (splicing NaN)']

    f, ax = plot.subplots(1, 1)
    for color_type, color_type_color, color_type_label in zip(color_types, color_type_colors, color_type_labels) :
        indices = colors == color_type
        ax.scatter(x[indices], y[indices], color = color_type_color, alpha = 0.4, label = color_type_label)
    ax.legend()
    if log2 :
        ax.set_xlabel('IRES activity', fontsize = 16)
    else :
        ax.set_xlabel('IRES activity, $\\log_2$', fontsize = 16)
    ax.set_ylabel('Random offset', fontsize = 16)
    ax.set_ylim(0, spacing_max + 1)
    f.set_size_inches(8, 8, forward = True)
    f.tight_layout()
    plot.show()

def get_dataset_display_name(dataset_name) :
    is_positive = False
    if dataset_name[-9:] == '-positive' :
        dataset_name = dataset_name[:-9]
        is_positive = True
    dataset_name = ' '.join(dataset_name.split('_'))
    if is_positive :
        return '%s (+)' % dataset_name
    return dataset_name


