'''
A module containing routines used for preparing datasets for analysis (e.g. pre-computing regressions).

23-02-2015
Alexey Gritsenko
'''

import persistent
import regression
import crossvalidation
import kmers
import structure
import reader
import writer
import os
import numpy as np

# CV1 : 1337357
# CV2 : 1337357 + 100
def create_crossvalidation_folds(datasets, n_folds = 10, random_seed = 1337357) :
    '''
    Creates CV folds for our datasets.
    :param dataset : a list of datasets for which CV should be predicted.
    :param n_folds : number of CV folds per dataset.
    '''
    for dataset_name in datasets :
        print '[i] Processing: %s' % dataset_name
        cv_folds = crossvalidation.create_folds(dataset_name, n_folds_outer = n_folds, n_folds_inner = n_folds, random_seed = random_seed)
        path = '../results/regression/current/%s' % dataset_name
        if not os.path.exists(path) :
            os.makedirs(path)
        writer.write_pickle(cv_folds, '%s/crossvalidation_folds.out' % path)

def create_random_crossvalidation_folds(dataset_name_prefix = 'random', target_dataset_names = ['viral_native_dsRNA_all', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_negative', 'human_native_5utr', 'human_native_3utr', 'human_native_cds', 'viral_native_RTV'], n_random_samples = 10, n_folds = 10, random_seed = 1337357) :
    '''
    Creates CV folds for random sample datasets.
    :param dataset_name_prefix : prefix used for saving the subsampled datasets.
    :param target_dataset_names : names of the dataset that should be mimicked.
    :param n_random_samples : number of random dataset samples to take.
    :param dataset_name_prefix : prefix that was used for saving the subsampled datasets.
    :param source_dataset_name : name of the source dataset.
    :param n_folds : number of CV folds per dataset.
    '''
    import tools
    dataset_names = []
    for dataset_name in target_dataset_names :
        for rep in xrange(1, n_random_samples + 1) :
            new_dataset_name = '%s_rep%d_%s' % (dataset_name_prefix, rep, dataset_name)
            dataset_names.append(new_dataset_name)
    create_crossvalidation_folds(dataset_names, n_folds = n_folds, random_seed = random_seed)

def create_permuted_crossvalidation_folds(dataset_name_prefix = 'permutation', target_dataset_names = ['viral_native_dsRNA_all', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_negative', 'human_native_5utr', 'human_native_3utr', 'human_native_cds', 'viral_native_RTV'], n_permutations = 10, n_folds = 10, random_seed = 1337357) :
    '''
    Creates CV folds for random permutation datasets.
    :param dataset_name_prefix : prefix used for saving the subsampled datasets.
    :param target_dataset_names : names of the dataset that should be mimicked.
    :param source_dataset_name : name of the dataset to sample from. 
    :param n_permutations : number of permutations to perform.
    :param dataset_name_prefix : prefix that was used for saving the subsampled datasets.
    :param n_folds : number of CV folds per dataset.
    '''
    import tools
    dataset_names = []
    for dataset_name in target_dataset_names :
        for rep in xrange(1, n_permutations + 1) :
            new_dataset_name = '%s_rep%d_%s' % (dataset_name_prefix, rep, dataset_name)
            dataset_names.append(new_dataset_name)
    create_crossvalidation_folds(dataset_names, n_folds = n_folds, random_seed = random_seed)

def create_subsample_crossvalidation_folds(dataset_name_suffix, source_dataset_name = 'viral_native_dsRNA_all', destination_dataset_name = 'viral_native_ssRNA_negative', n_steps = 10, n_folds = 10, random_seed = 1337357) :
    '''
    Creates CV folds for subsample datasets.
    :param dataset_name_prefix : prefix that was used for saving the subsampled datasets.
    :param source_dataset_name : name of the source dataset.
    :param destination_dataset_name : name of the destination dataset.
    :param n_steps : number of subsampling steps.
    :param dataset : a list of datasets for which CV should be predicted.
    :param n_folds : number of CV folds per dataset.
    '''
    import tools
    source_ds, destination_ds = persistent.datasets[source_dataset_name], persistent.datasets[destination_dataset_name]
    n_source, n_destination = len(source_ds), len(destination_ds)
    source_activity, destination_activity = np.array([seq.ires_activity for seq in source_ds]), np.array([seq.ires_activity for seq in destination_ds])
    source_positive_indices, destination_positive_indices = np.where(source_activity != tools.MINIMAL_IRES_ACTIVITY), np.where(destination_activity != tools.MINIMAL_IRES_ACTIVITY)
    source_positive_indices, destination_positive_indices = source_positive_indices[0], destination_positive_indices[0]
    source_negative_indices = np.where(source_activity == tools.MINIMAL_IRES_ACTIVITY)
    source_negative_indices = source_negative_indices[0]
    n_source_positive, n_destination_positive = len(source_positive_indices), len(destination_positive_indices)
    n_source_negative, n_destination_negative = n_source - n_source_positive, n_destination - n_destination_positive
    source_percent, destination_percent = n_source_positive / float(n_source), n_destination_positive / float(n_destination)
   
    print '[i] Destination: %s' % destination_dataset_name
    print '    [i] Positive: %d' % n_destination_positive
    print '    [i] Negative: %d' % n_destination_negative
    print '    [i] Relative: %.2f%%' % (destination_percent * 100)
    print '[i] Source: %s' % source_dataset_name
    print '    [i] Positive: %d' % n_source_positive
    print '    [i] Negative: %d' % n_source_negative
    print '    [i] Relative: %.2f%%' % (source_percent * 100)
    if n_destination_positive > n_source_positive or destination_percent > source_percent :
        print ''
        print '[-] Error: destination dataset has more positives than the source.'
        return
    wanted_n_source_positive = int(destination_percent * n_source_negative / (1.0 - destination_percent))
    print '    [i] Want positive: %d' % wanted_n_source_positive
    print ''
    subsample_sizes = np.linspace(n_source_positive, wanted_n_source_positive, n_steps).astype(np.int)

    dataset_names = []
    for sample_size in subsample_sizes[1:] :
        new_dataset_name = '%s_subsample_%d%s' % (source_dataset_name, sample_size, dataset_name_suffix)
        dataset_names.append(new_dataset_name)
    create_crossvalidation_folds(dataset_names, n_folds = n_folds, random_seed = random_seed) 

def kmers_pass_check(filename, type = 'kmers', max_motif_length = 5) :
    '''
    Checks whether existing k-mer file satisfies the requested data.
    :param filename : name of the file that needs to be checked.
    :param type : type of the kmers to be checked.
    :param max_kmer_length : length of the k-mer that should be present in the file.
    '''
    if not os.path.exists(filename) or not os.path.isfile(filename) :
        return False
    try :
        kmers = reader.read_pickle(filename)
    except :
        return False

    found_lengths = set()
    if type == 'bppm-windows' or type == 'bppm-pair-windows' :
        return True
    elif type == 'kmers' or type == 'accessible_kmers' :
        for kmer in kmers :
            if len(kmer) != 5 :
                return False
            length = len(kmer[0])
            found_lengths.add(length)
    elif type == 'kmers-windows' or type == 'accessible_kmers-windows' :
        for kmer in kmers :
            if len(kmer) != 6 :
                return False
            length = len(kmer[0])
            found_lengths.add(length)

    for k in range(1, max_motif_length + 1) :
        if not k in found_lengths :
            return False

    return True

def generate_cv_filename(name, dataset_name, fold) :
    '''
    Generates precomputed CV name for a given feature name.
    :param name : name of the feature.
    :param dataset_name : name of the dataset.
    :param fold : CV fold to be used.
    '''
    if fold is None :
        file_path = 'datasets/current/%s' % dataset_name
    else :
        file_path = 'datasets/current/%s/cv' % dataset_name
    if fold is None :
        return '%s/%s.out' % (file_path, name)
    elif isinstance(fold, int) :
        return '%s/fold%d-%s.out' % (file_path, fold, name)
    elif isinstance(fold, tuple) :
        inner_fold, outer_fold = fold
        return '%s/fold%d-%d-%s.out' % (file_path, inner_fold, outer_fold, name)
    else :
        return None

def _precompute_kmer_features_base(dataset_name, indices = None, fold = None, window_step = 10, window_size = 20, max_motif_length = 5, cutoff = 0.05, overwrite_files = False, client_url = None) :
    '''
    Runs pre-computation of k-mer correlations for a given dataset. Precomputed correlations are saved to disk.
    :param dataset_name: name of the dataset for which data should be precomputed.
    :param indices : indices that should be used for precomputing.
    :param fold : CV fold that the precomputing is performed for.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param max_motif_length : maximum kmer length (minimum is 1).
    :param overwrite_files : whether existing files should be overwritten.
    :param client_url : URL to be used by IPython parallel client when computing in parallel.
    '''
    pad = '    '
    offset = 1
    if isinstance(fold, tuple) :
        offset = 2

    filename = generate_cv_filename('kmers-windows%d_step%d' % (window_size, window_step), dataset_name, fold)
    if not kmers_pass_check(filename, type = 'kmers-windows', max_motif_length = max_motif_length) or overwrite_files :
        found_kmers_windows = kmers.look_for_kmers_in_windows(dataset_name, indices = indices, min_motif_length = 1, max_motif_length = max_motif_length, window_size = window_size, window_step = window_step, test_type = 'both', cutoff = cutoff, client_url = client_url)
        found_kmers_windows.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found k-mers in windows (%d): %d' % (window_size, len(found_kmers_windows))
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_kmers_windows, filename)
        del found_kmers_windows
    else :
        print pad * offset,
        print '[i] Skipping k-mers in windows (%d)' % window_size
    
    filename = generate_cv_filename('kmers', dataset_name, fold)
    if not kmers_pass_check(filename, type = 'kmers', max_motif_length = max_motif_length) or overwrite_files :
        found_kmers = kmers.look_for_kmers(dataset_name, indices = indices, min_motif_length = 1, max_motif_length = max_motif_length, test_type = 'both', cutoff = cutoff, client_url = client_url)
        found_kmers.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found k-mers: %d' % len(found_kmers)
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_kmers, filename)
        del found_kmers
    else :
        print pad * offset,
        print '[i] Skipping k-mers'

    # Premature termination (we are not using other features)
    return

    filename = generate_cv_filename('kmers_presence-windows%d_step%d' % (window_size, window_step), dataset_name, fold)
    if not kmers_pass_check(filename, type = 'kmers-windows', max_motif_length = max_motif_length) or overwrite_files :
        found_kmers_windows = kmers.look_for_kmers_in_windows(dataset_name, indices = indices, min_motif_length = 1, max_motif_length = max_motif_length, window_size = window_size, window_step = window_step, test_type = 'utest', cutoff = cutoff, client_url = client_url)
        found_kmers_windows.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found k-mers in windows (presence, %d): %d' % (window_size, len(found_kmers_windows))
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_kmers_windows, filename)
        del found_kmers_windows
    else :
        print pad * offset,
        print '[i] Skipping k-mers in windows (presence, %d)' % window_size
    
    filename = generate_cv_filename('kmers_presence', dataset_name, fold)
    if not kmers_pass_check(filename, type = 'kmers', max_motif_length = max_motif_length) or overwrite_files :
        found_kmers = kmers.look_for_kmers(dataset_name, indices = indices, min_motif_length = 1, max_motif_length = max_motif_length, test_type = 'utest', cutoff = cutoff, client_url = client_url)
        found_kmers.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found k-mers (presence): %d' % len(found_kmers)
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_kmers, filename)
        del found_kmers
    else :
        print pad * offset,
        print '[i] Skipping k-mers (presence)'
    
    filename = generate_cv_filename('accessible_kmers-windows%d_step%d' % (window_size, window_step), dataset_name, fold)
    if not kmers_pass_check(filename, type = 'accessible_kmers-windows', max_motif_length = max_motif_length) or overwrite_files :
        found_kmers_windows = kmers.look_for_accessible_kmers_in_windows(dataset_name, indices = indices, min_motif_length = 1, max_motif_length = max_motif_length, window_size = window_size, window_step = window_step, test_type = 'both', cutoff = cutoff, client_url = client_url)
        found_kmers_windows.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found acessible k-mers in windows (%d): %d' % (window_size, len(found_kmers_windows))
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_kmers_windows, filename)
        del found_kmers_windows
    else :
        print pad * offset,
        print '[i] Skipping k-mers in windows (%d)' % window_size
    
    filename = generate_cv_filename('accessible_kmers', dataset_name, fold)
    if not kmers_pass_check(filename, type = 'accessible_kmers', max_motif_length = max_motif_length) or overwrite_files :
        found_kmers = kmers.look_for_accessible_kmers(dataset_name, indices = indices, min_motif_length = 1, max_motif_length = max_motif_length, test_type = 'both', cutoff = cutoff, client_url = client_url)
        found_kmers.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found accessible k-mers: %d' % len(found_kmers)
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_kmers, filename)
        del found_kmers
    else :
        print pad * offset,
        print '[i] Skipping k-mers'

def _precompute_bppm_features_base(dataset_name, indices = None, fold = None, window_step = 5, window_size = 10, cutoff = 0.05, overwrite_files = False) :
    '''
    Runs pre-computation of bppm correlations for a given dataset. Precomputed correlations are saved to disk.
    :param dataset_name: name of the dataset for which data should be precomputed.
    :param indices : indices that should be used for precomputing.
    :param fold : CV fold that the precomputing is performed for.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param overwrite_files : whether existing files should be overwritten.
    '''
    pad = '    '
    offset = 1
    if isinstance(fold, tuple) :
        offset = 2

    filename = generate_cv_filename('bppm-windows%d_step%d' % (window_size, window_step), dataset_name, fold)
    if not kmers_pass_check(filename, type = 'bppm-windows') or overwrite_files :
        found_bppm_windows = structure.look_for_bppm_features(dataset_name, indices = indices, window_size = window_size, window_step = window_step, cutoff = cutoff)
        found_bppm_windows.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found BPPM structure in windows (%d): %d' % (window_size, len(found_bppm_windows))
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_bppm_windows, filename)
        del found_bppm_windows
    else :
        print pad * offset,
        print '[i] Skipping BPPM in windows (%d)' % window_size

    filename = generate_cv_filename('bppm-pair-windows%d_step%d' % (window_size, window_step), dataset_name, fold)
    if not kmers_pass_check(filename, type = 'bppm-pair-windows') or overwrite_files :
        found_bppm_pair_windows = structure.look_for_bppm_pair_features(dataset_name, indices = indices, window_size = window_size, window_step = window_step, cutoff = cutoff)
        found_bppm_pair_windows.sort(key = lambda x : x[-2])
        print pad * offset,
        print '[i] Found BPPM pair structure in windows (%d): %d' % (window_size, len(found_bppm_pair_windows))
        if not os.path.exists(os.path.dirname(filename)) :
            os.makedirs(os.path.dirname(filename))
        writer.write_pickle(found_bppm_pair_windows, filename)
        del found_bppm_pair_windows
    else :
        print pad * offset,
        print '[i] Skipping BPPM pair in windows (%d)' % window_size

def precompute_kmer_features(datasets = None, window_step = 10, window_size = 20, max_motif_length = 5, cutoff = 0.05, overwrite_files = False, client_url = None) :
    '''
    Runs pre-computation of k-mer correlations for a given list of datasets. Precomputed correlations are saved to disk.
    :param datasets : a list of datasets for which data should be precomputed.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param max_motif_length : maximum kmer length (minimum is 1).
    :param overwrite_files : whether existing files should be overwritten.
    :param client_url : URL to be used by IPython parallel client when computing in parallel.
    '''
    if datasets is None :
        datasets = persistent._dataset_mappings.keys()

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        _precompute_kmer_features_base(dataset_name, indices = None, fold = None, window_step = window_step, window_size = window_size, max_motif_length = max_motif_length, overwrite_files = overwrite_files, cutoff = cutoff, client_url = client_url)

def precompute_bppm_features(datasets = None, window_step = 5, window_size = 10, overwrite_files = False, cutoff = 0.05) :
    '''
    Runs pre-computation of BPPM correlations for a given list of datasets. Precomputed correlations are saved to disk.
    :param datasets : a list of datasets for which data should be precomputed.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param overwrite_files : whether existing files should be overwritten.
    '''
    if datasets is None :
        datasets = persistent._dataset_mappings.keys()

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        _precompute_bppm_features_base(dataset_name, indices = None, fold = None, window_step = window_step, window_size = window_size, overwrite_files = overwrite_files, cutoff = cutoff)

def precompute_outer_fold_kmer_features(datasets = None, window_step = 10, window_size = 20, max_motif_length = 5, cutoff = 0.05, overwrite_files = False, client_url = None) :
    '''
    Runs pre-computation of k-mer correlations using outer CV folds for a given list of datasets. Precomputed correlations are saved to disk.
    :param datasets : a list of datasets for which data should be precomputed.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param max_motif_length : maximum kmer length (minimum is 1).
    :param overwrite_files : whether existing files should be overwritten.
    :param client_url : URL to be used by IPython parallel client when computing in parallel.
    '''
    if datasets is None :
        datasets = persistent._dataset_mappings.keys()

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        folds = reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
        n_folds = len(folds.outer_folds)
        fold = 0
        for train_indices, test_indices in folds.outer_folds :
            print '  [i] Fold %d / %d' % (fold + 1, n_folds)
            _precompute_kmer_features_base(dataset_name, indices = train_indices, fold = fold, window_step = window_step, window_size = window_size, max_motif_length = max_motif_length, cutoff = cutoff, overwrite_files = overwrite_files, client_url = client_url)
            fold += 1

def precompute_outer_fold_bppm_features(datasets = None, window_step = 5, window_size = 10, overwrite_files = False, cutoff = 0.05) :
    '''
    Runs pre-computation of BPPM correlations using outer CV folds for a given list of datasets. Precomputed correlations are saved to disk.
    :param datasets : a list of datasets for which data should be precomputed.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param overwrite_files : whether existing files should be overwritten.
    '''
    if datasets is None :
        datasets = persistent._dataset_mappings.keys()

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        folds = reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
        n_folds = len(folds.outer_folds)
        fold = 0
        for train_indices, test_indices in folds.outer_folds :
            print '  [i] Fold %d / %d' % (fold + 1, n_folds)
            _precompute_bppm_features_base(dataset_name, indices = train_indices, fold = fold, window_step = window_step, window_size = window_size, cutoff = cutoff, overwrite_files = overwrite_files)
            fold += 1

def precompute_inner_fold_kmer_features(datasets = None, window_step = 10, window_size = 20, max_motif_length = 5, cutoff = 0.05, overwrite_files = False, randomized_order = False, client_url = None) :
    '''
    Runs pre-computation of k-mer correlations using inner CV folds for a given list of datasets. Precomputed correlations are saved to disk.
    :param datasets : a list of datasets for which data should be precomputed.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param max_motif_length : maximum kmer length (minimum is 1).
    :param overwrite_files : whether existing files should be overwritten.
    :param randomized_order : whether the order in which correlations are computed should be randomized.
    :param client_url : URL to be used by IPython parallel client when computing in parallel.
    '''
    if datasets is None :
        datasets = persistent._dataset_mappings.keys()

    if randomized_order :
        np.random.shuffle(datasets)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        folds = reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
        n_folds = len(folds.outer_folds)
        range_folds = range(n_folds)
        if randomized_order :
            np.random.shuffle(range_folds)
        for fold in range_folds :
            n_inner_folds = len(folds.inner_folds[fold])
            order_indices = range(n_inner_folds)
            if randomized_order :
                np.random.shuffle(order_indices)
            for inner_fold in order_indices :
                train_indices, test_indices = folds.inner_folds[fold][inner_fold]
                print '    [i] Fold %d / %d - %d / %d' % (fold + 1, n_folds, inner_fold + 1, n_inner_folds)
                _precompute_kmer_features_base(dataset_name, indices = train_indices, fold = (fold, inner_fold), window_step = window_step, window_size = window_size, max_motif_length = max_motif_length, cutoff = cutoff, overwrite_files = overwrite_files, client_url = client_url)

def precompute_inner_fold_bppm_features(datasets = None, window_step = 5, window_size = 10, cutoff = 0.05, overwrite_files = False, randomized_order = False) :
    '''
    Runs pre-computation of BPPM correlations using inner CV folds for a given list of datasets. Precomputed correlations are saved to disk.
    :param datasets : a list of datasets for which data should be precomputed.
    :param window_step : step for moving the correlation window (used in windowed correlations).
    :param window_size : size of the moving window.
    :param overwrite_files : whether existing files should be overwritten.
    :param randomized_order : whether the order in which correlations are computed should be randomized.
    '''
    if datasets is None :
        datasets = persistent._dataset_mappings.keys()

    if randomized_order :
        np.random.shuffle(datasets)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        folds = reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
        n_folds = len(folds.outer_folds)
        range_folds = range(n_folds)
        if randomized_order :
            np.random.shuffle(range_folds)
        for fold in range_folds :
            n_inner_folds = len(folds.inner_folds[fold])
            order_indices = range(n_inner_folds)
            if randomized_order :
                np.random.shuffle(order_indices)
            for inner_fold in order_indices :
                train_indices, test_indices = folds.inner_folds[fold][inner_fold]
                print '    [i] Fold %d / %d - %d / %d' % (fold + 1, n_folds, inner_fold + 1, n_inner_folds)
                _precompute_bppm_features_base(dataset_name, indices = train_indices, fold = (fold, inner_fold), window_step = window_step, window_size = window_size, cutoff = cutoff, overwrite_files = overwrite_files)

