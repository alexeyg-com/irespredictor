#!/usr/bin/env python
import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

client_url =  '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'

window_step = 10
window_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90]

def run_inner(dataset_name) :
    #datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    for window_size in window_sizes :
        lib.prepare.precompute_correlations(datasets = [dataset_name], client_url = client_url, window_step = window_step, window_size = window_size)
    #lib.prepare.precompute_outer_fold_correlations(datasets = [dataset_name], client_url = client_url, max_motif_length = 7)
    #lib.prepare.precompute_inner_fold_correlations(datasets = [dataset_name], client_url = client_url, randomized_order = True, max_motif_length = 7)

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        window_sizes = [int(sys.argv[2])]
    run_inner(dataset_name)
