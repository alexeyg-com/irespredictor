#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

correction_method = 'fdr_bh'
cutoff = 0.05
min_kmer_length = 1
kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['kmers', 'kmers-windows']
#feature_types = ['kmers_presence', 'kmers_presence-windows']
minimum_fraction = 0.1

client_url =  '/home/nfs/alexeygritsenk/.ipython/profile_ires_stripped/security/ipcontroller-client.json'
client_url = None

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which the regression should be computed.
    '''
    n_feature_types = len(feature_types)
    n_combinations = 2 ** n_feature_types
    configurations = range(n_combinations)

    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for kmer_length in kmer_lengths :
        for configuration in configurations :
            names = []
            for i in range(n_feature_types) :
                if configuration % 2 == 1 :
                    feature_type = feature_types[i]
                    names.append(feature_type)
                configuration /= 2
            if len(names) == 0 :
                continue
            feature_type = '_'.join(names)
            file_path = '../results/regression/current/%s/svr' % dataset_name
            filename = '%s/%s-min_kmer_%d-max_kmer_%d-grid_search_settings_pcb-minimum_fraction_%s-cutoff_%s-correction_method_%s.out' % (file_path, '_'.join(names), min_kmer_length, kmer_length, str(minimum_fraction), str(cutoff), correction_method)
            filename_final = '%s/%s-min_kmer_%d-max_kmer_%d-grid_search_settings_pcb-minimum_fraction_%s-cutoff_%s-correction_method_%s-final.out' % (file_path, '_'.join(names), min_kmer_length, kmer_length, str(minimum_fraction), str(cutoff), correction_method)
            if os.path.exists(filename_final) :
                print '    [i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, ', '.join(names))
                continue
            if not os.path.exists(filename) :
                print '    [i] Skipping for %s : k = %d, type = %s (missing)' % (dataset_name, kmer_length, ', '.join(names))
                continue
            print '    [i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, ', '.join(names))
            results = lib.reader.read_pickle(filename)
            res = lib.regression.train_svr_outer_cv_final(results, use_aux = True, cv_folds = folds, return_regressor = True, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename_final)
            print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        kmer_length = int(sys.argv[2])
        kmer_lengths = [kmer_length]
    run_inner(dataset_name)

