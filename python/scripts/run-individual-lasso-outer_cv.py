#!/usr/bin/env python
#import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['independent', 'independent-windows', 'positional', 'positional-windows']

def run_inner(dataset_name) :
    #datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    client_url = '/home/nfs/alexeygritsenk/src/IRES_Predictor/python/profile-grid/security/ipcontroller-client.json'
    #lib.prepare.precompute_inner_fold_correlations(datasets = [dataset], client_url = client_url, randomized_order = True)
    #lib.prepare.precompute_correlations(datasets = [dataset], client_url = client_url)
    folds = lib.reader.read_pickle('../results/%s/crossvalidation_folds.out' % dataset_name)
    for kmer_length in kmer_lengths :
        for feature_type in feature_types :
            file_path = '../results/%s/glmnet' % dataset_name
            filename = '%s/%s-max_kmer_%d.out' % (file_path, feature_type, kmer_length)
            filename_final = '%s/%s-max_kmer_%d-final.out' % (file_path, feature_type, kmer_length)
            if not os.path.exists(filename) :
                print '[i] Skipping for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
                continue
            results = lib.reader.read_pickle(filename)
            print '[i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
            features = [{'feature_type' : feature_type, 'max_motif_length' : kmer_length}]
            res = lib.regression.train_glment_alphas_outer_cv_final(dataset_name, features, results, log2 = True, use_aux = True, cv_folds = folds, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename_final)
            print '    [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)
