import os
import shutil

def do_copy(start_dir, dest_dir, skip_list = ['glmnet', 'mfe-arrays.out']) :
    '''
    Copy folder contents while ignoring some folders.
    :param start_dir : directory which should be copied.
    :param dest_dir : directory to which things should be copied.
    :param skip_list : a list of folders that should be removed from the copying process.
    '''
    skip_list = set(skip_list)
    for root, subdirs, files in os.walk(start_dir) :
        root_name = os.path.basename(root)
        if root_name in skip_list :
            continue
        rel_path = os.path.relpath(root, start_dir)
        to_path = os.path.join(dest_dir, rel_path)
        if not os.path.exists(to_path) :
            os.makedirs(to_path)
        for file in files :
            if file in skip_list :
                continue
            from_file = os.path.join(root, file)
            to_file = os.path.join(to_path, file)
            print from_file, ' - > ', to_file
            shutil.copyfile(from_file, to_file)
