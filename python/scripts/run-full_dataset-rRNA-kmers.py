#!/usr/bin/env python
import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import lib.kmers
import sys
import os

client_url = '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    rRNA = lib.reader.read_ribosomal_rna_seq()
    kmers = lib.kmers.generate_kmers([rRNA['18S']])
    kmers = list(kmers)
    lib.prepare.precompute_correlations(datasets = [dataset_name], kmer_source = kmers, client_url = client_url, max_motif_length = 10)
    #datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    #lib.prepare.precompute_outer_fold_correlations(datasets = [dataset_name], client_url = client_url, max_motif_length = 7)
    #lib.prepare.precompute_inner_fold_correlations(datasets = [dataset_name], client_url = client_url, randomized_order = True, max_motif_length = 7)

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)
