#!/usr/bin/env python
import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

client_url = '/home/nfs/alexeygritsenk/.ipython/profile_ires/security/ipcontroller-client.json'
client_url = None

def run_inner(dataset_name) :
    #datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    lib.prepare.precompute_kmer_features(datasets = [dataset_name], client_url = client_url, cutoff = 1.1)
    #lib.prepare.precompute_outer_fold_correlations(datasets = [dataset_name], client_url = client_url, max_motif_length = 7)
    #lib.prepare.precompute_inner_fold_correlations(datasets = [dataset_name], client_url = client_url, randomized_order = True, max_motif_length = 7)

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)
