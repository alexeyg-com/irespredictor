#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['structure_count', 'structure_presence', 'ws_structure_count', 'ws_structure_presence', 'structure_count-windows', 'structure_presence-windows', 'ws_structure_count-windows', 'ws_structure_presence-windows']
minimum_fraction_settings = [0.0, 0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064, 0.128, 0.256, 0.512]
client_url =  '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'
client_url =  None

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which the regression should be computed.
    '''

    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for kmer_length in kmer_lengths :
        for feature_type in feature_types :
            for minimum_fraction in minimum_fraction_settings :
                file_path = '../results/regression/current/%s/gbrf' % dataset_name
                filename = '%s/%s-max_kmer_%d-sqrt_eran_refined2-3k-minimum_fraction_%s.out' % (file_path, feature_type, kmer_length, str(minimum_fraction))
                filename_final = '%s/%s-max_kmer_%d-sqrt_eran_refined2-3k-minimum_fraction_%s-final.out' % (file_path, feature_type, kmer_length, str(minimum_fraction))
                if os.path.exists(filename_final) :
                    print '    [i] Skipping for %s : k = %d, type = %s, minimum fraction = %s (exists)' % (dataset_name, kmer_length, feature_type, str(minimum_fraction))
                    continue
                if not  os.path.exists(filename) :
                    print '    [i] Skipping for %s : k = %d, type = %s, minimum fraction = %s (not found)' % (dataset_name, kmer_length, feature_type, str(minimum_fraction))
                    continue
                print '    [i] Starting for %s : k = %d, type = %s, minimum fraction = %s' % (dataset_name, kmer_length, feature_type, str(minimum_fraction))
                results = lib.reader.read_pickle(filename)
                res = lib.regression.train_gbrf_outer_cv_final(results, can_choose_n_estimators = False, use_aux = True, cv_folds = folds, client_url = client_url)
                if not os.path.exists(file_path) :
                    os.makedirs(file_path)
                lib.writer.write_pickle(res, filename_final)
                print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        minimum_fraction = sys.argv[2]
        minimum_fraction_settings = [minimum_fraction]
    if len(sys.argv) > 3 :
        feature_type = sys.argv[3]
        feature_types = [feature_type]
    if len(sys.argv) > 4 :
        kmer_length = int(sys.argv[4])
        kmer_lengths = [kmer_length]
    run_inner(dataset_name)

