#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np

window_feature_types = ['fmm-windows', ['fmm', 'fmm-windows']]
feature_types = ['fmm']
aggregators = ['sum', 'max', 'both']
window_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90]
window_step = 10

client_url =  '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'
#client_url = None

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which computations should be performed.
    '''
    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for aggregator in aggregators :
        for feature_type in window_feature_types :
            for window_size in window_sizes :
                file_path = '../results/regression/current/%s/gbrf' % dataset_name
                if isinstance(feature_type, list) :
                    feature_type_str = '_'.join(feature_type)
                else :
                    feature_type_str = feature_type
                filename = '%s/%s%d_step%d-aggregator_%s-sqrt.out' % (file_path, feature_type_str, window_size, window_step, aggregator)
                filename_final = '%s/%s%d_step%d-aggregator_%s-sqrt-final.out' % (file_path, feature_type_str, window_size, window_step, aggregator)
                if os.path.exists(filename_final) :
                    print '    [i] Skipping for %s : window size = %d, type = %s, aggregator = %s (exists)' % (dataset_name, window_size, feature_type_str, aggregator)
                    continue
                if not os.path.exists(filename) :
                    print '    [i] Skipping for %s : window size = %d, type = %s, aggregator = %s' % (dataset_name, window_size, feature_type_str, aggregator)
                    continue
                print '    [i] Starting for %s : window size = %d, type = %s, aggregator = %s' % (dataset_name, window_size, feature_type_str, aggregator)
                results = lib.reader.read_pickle(filename)
                res = lib.regression.train_gbrf_outer_cv_final(results, use_aux = True, cv_folds = folds, client_url = client_url)
                if not os.path.exists(file_path) :
                    os.makedirs(file_path)
                lib.writer.write_pickle(res, filename_final)
                print '        [+] Done.'
        for feature_type in feature_types :
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            if isinstance(feature_type, list) :
                feature_type_str = '_'.join(feature_type)
            else :
                feature_type_str = feature_type
            filename = '%s/%s-aggregator_%s-sqrt.out' % (file_path, feature_type_str, aggregator)
            filename_final = '%s/%s-aggregator_%s-sqrt-final.out' % (file_path, feature_type_str, aggregator)
            if os.path.exists(filename_final) :
                print '    [i] Skipping for %s : type = %s, aggregator = %s (exists)' % (dataset_name, feature_type_str, aggregator)
                continue
            print '    [i] Starting for %s : type = %s, aggregator = %s' % (dataset_name, feature_type_str, aggregator)
            if not os.path.exists(filename) :
                print '    [i] Skipping for %s : type = %s, aggregator = %s' % (dataset_name, feature_type_str, aggregator)
                continue
            results = lib.reader.read_pickle(filename)
            res = lib.regression.train_gbrf_outer_cv_final(results, use_aux = True, cv_folds = folds, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename_final)
            print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        window_size = int(sys.argv[2])
        window_sizes = [window_size]
    run_inner(dataset_name)

