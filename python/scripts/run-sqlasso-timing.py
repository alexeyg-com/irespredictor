#!/usr/bin/env python

import lib.sqlasso

output_filename = './timing.out'
n_gammas = 1000
n_samples_max = 2000

lib.sqlasso.runtime_tests(n_gammas = n_gammas, n_samples_max = n_samples_max, random_seed = 15, write_progress = output_filename)
