#!/usr/bin/env python
#import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os
import numpy as np

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['independent', 'independent-windows', 'positional', 'positional-windows']

ESTIMATOR_RECORDING_FREQUENCY = 100

def convert_score(scores) :
    n_folds, n_learning_rate, n_max_depth, n_min_samples_leaf, n_max_features, n_subsample, n_estimators = scores.shape
    n_recorded_estimators = int(n_estimators / ESTIMATOR_RECORDING_FREQUENCY)
    if n_estimators % ESTIMATOR_RECORDING_FREQUENCY != 0 :
        n_recorded_estimators += 1
    indices = [i for i in range(n_estimators) if (i + 1) % ESTIMATOR_RECORDING_FREQUENCY == 0 or i == n_estimators - 1]
    indices = np.array(indices)
    compressed_scores = scores[:, :, :, :, :, :, indices]
    compressed_scores = compressed_scores.astype(np.float16)
    return compressed_scores

def convert(res) :
    scoring = res.test_score.keys()
    n_estimators = res.n_estimators
    for score in scoring :
        res.test_score[score] = convert_score(res.test_score[score])
        res.train_score[score] = convert_score(res.train_score[score])
    time = res.time[:, :, :, :, :, :, 0]
    time = time.astype(np.float16)
    res.time = time
    res.estimator_recording_frequency = ESTIMATOR_RECORDING_FREQUENCY
    return res

def run() :
    datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    for dataset_name in datasets :
        for kmer_length in kmer_lengths :
            for feature_type in feature_types :
                file_path = '../results/%s/gbrf' % dataset_name
                filename = '%s/%s-max_kmer_%d.out' % (file_path, feature_type, kmer_length)
                if not os.path.exists(filename) :
                    print '[i] Skipping for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
                    continue
                print '[i] Converting for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
                results = lib.reader.read_pickle(filename)
                if results[0].test_score['r2'].shape[-1] < 5000 :
                    print '    [i] Skipping - already processed.'
                    continue
                for i in range(len(results)) :
                    results[i] = convert(results[i])
                print '    [+] Done'
                lib.writer.write_pickle(results, filename)

if __name__ == '__main__' :
    run()
