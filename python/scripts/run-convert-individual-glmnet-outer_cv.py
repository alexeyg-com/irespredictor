#!/usr/bin/env python
#import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os
import numpy as np
import scipy.sparse

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['independent', 'independent-windows', 'positional', 'positional-windows']

def convert_score(scores) :
    return scores.astype(np.float16)

def convert_solution(solution) :
    n_folds, n_alphas = solution.shape
    for i in range(n_folds) :
        for j in range(n_alphas) :
            solution[i, j] = scipy.sparse.csr_matrix(solution[i, j])

def convert(res) :
    scoring = res.test_score.keys()
    for score in scoring :
        res.test_score[score] = convert_score(res.test_score[score])
        res.train_score[score] = convert_score(res.train_score[score])
    res.time = res.time.astype(np.float16)
    res.solution = convert_solution(res.solution)
    return res

def run() :
    datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    for dataset_name in datasets :
        for kmer_length in kmer_lengths :
            for feature_type in feature_types :
                file_path = '../results/%s/glmnet' % dataset_name
                filename = '%s/%s-max_kmer_%d.out' % (file_path, feature_type, kmer_length)
                if not os.path.exists(filename) :
                    print '[i] Skipping for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
                    continue
                print '[i] Converting for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
                results = lib.reader.read_pickle(filename)
                for i in range(len(results)) :
                    results[i] = convert(results[i])
                print '    [+] Done'
                lib.writer.write_pickle(results, filename)

if __name__ == '__main__' :
    run()
