#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

kmer_lengths = [1, 2, 3, 4, 5]
#feature_types = ['independent', 'independent-windows', 'positional-windows', 'positional']
feature_types = ['independent', 'independent-windows', 'positional-windows']
minimum_fraction = 0.1
client_url =  './profile/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which the regression should be computed.
    '''
    n_feature_types = len(feature_types)
    n_combinations = 2 ** n_feature_types
    configurations = range(n_combinations)

    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for kmer_length in kmer_lengths :
        for configuration in configurations :
            names = []
            for i in range(n_feature_types) :
                if configuration % 2 == 1 :
                    feature_type = feature_types[i]
                    names.append(feature_type)
                configuration /= 2
            if len(names) == 0 :
                continue
            feature_type = '_'.join(names)
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename = '%s/%s-max_kmer_%d-sqrt_eran_refined2-1k-all_orf-minimum_fraction_%s.out' % (file_path, '_'.join(names), kmer_length, str(minimum_fraction))
            filename_final = '%s/%s-max_kmer_%d-sqrt_eran_refined2-1k-all_orf-minimum_fraction_%s-final.out' % (file_path, '_'.join(names), kmer_length, str(minimum_fraction))
            if os.path.exists(filename_final) :
                print '    [i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, ', '.join(names))
                continue
            if not os.path.exists(filename) :
                print '    [i] Skipping for %s : k = %d, type = %s (missing)' % (dataset_name, kmer_length, ', '.join(names))
                continue
            print '    [i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, ', '.join(names))
            results = lib.reader.read_pickle(filename)
            res = lib.regression.train_gbrf_outer_cv_final(results, can_choose_n_estimators = 1000, use_aux = True, cv_folds = folds, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename_final)
            print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        kmer_length = int(sys.argv[2])
        kmer_lengths = [kmer_length]
    run_inner(dataset_name)

