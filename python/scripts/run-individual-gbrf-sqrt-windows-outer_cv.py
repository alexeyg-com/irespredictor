#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np

kmer_lengths = [1, 2, 3, 4, 5]
window_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90]
window_step = 10
feature_types = ['independent-windows', 'positional-windows']
client_url =  './profile/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset, for which the computing should be performed.
    '''
    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)
    for kmer_length in kmer_lengths :
        for feature_type in feature_types :
            for window_size in window_sizes :
                file_path = '../results/regression/current/%s/gbrf' % dataset_name
                filename = '%s/%s%d_step%d-max_kmer_%d-sqrt-n_estimators.out' % (file_path, feature_type, window_size, window_step, kmer_length)
                filename_final = '%s/%s%d_step%d-max_kmer_%d-sqrt-final.out' % (file_path, feature_type, window_size, window_step, kmer_length)
                if os.path.exists(filename_final) :
                    print '    [i] Skipping for %s : k = %d, type = %s, window size = %d (exists)' % (dataset_name, kmer_length, feature_type, window_size)
                    continue
                if not os.path.exists(filename) :
                    print '    [i] Skipping for %s : k = %d, type = %s, window size = %d' % (dataset_name, kmer_length, feature_type, window_size)
                    continue
                results = lib.reader.read_pickle(filename)
                print '    [i] Starting for %s : k = %d, type = %s, window size %d' % (dataset_name, kmer_length, feature_type, window_size)
                res = lib.regression.train_gbrf_outer_cv_final(results, can_choose_n_estimators = False, use_aux = True, cv_folds = folds, client_url = client_url)
                if not os.path.exists(file_path) :
                    os.makedirs(file_path)
                lib.writer.write_pickle(res, filename_final)
                print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)
