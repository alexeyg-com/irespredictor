#!/usr/bin/env python
#import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['independent', 'independent-windows', 'positional', 'positional-windows']
client_url = '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    folds = lib.reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
    for kmer_length in kmer_lengths :
        for feature_type in feature_types :
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename = '%s/%s-max_kmer_%d-log-5k.out' % (file_path, feature_type, kmer_length)
            filename_final = '%s/%s-max_kmer_%d-log-5k-final.out' % (file_path, feature_type, kmer_length)
            if os.path.exists(filename_final) :
                print '[i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, feature_type)
                continue
            if not os.path.exists(filename) :
                print '[i] Skipping for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
                continue
            results = lib.reader.read_pickle(filename)
            print '[i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
            res = lib.regression.train_gbrf_outer_cv_final(results, can_choose_n_estimators = False, use_aux = True, cv_folds = folds, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename_final)
            print '    [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        kmer_length = int(sys.argv[2])
        kmer_lengths = [kmer_length]
    run_inner(dataset_name)
