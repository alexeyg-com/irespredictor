#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

correction_method = 'fdr_bh'
cutoff = 0.05
min_kmer_length = 1
kmer_lengths = [4]
feature_types = ['kmers', 'kmers-windows']
minimum_fraction = 0.1

#client_url =  './profile-grid/security/ipcontroller-client.json'
client_url = None
client_url =  '/home/nfs/alexeygritsenk/.ipython/profile_ires_stripped/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which the regression should be computed.
    '''
    n_feature_types = len(feature_types)
    n_combinations = 2 ** n_feature_types
    configurations = range(n_combinations)

    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for kmer_length in kmer_lengths :
        for configuration in configurations :
            names, features = [], []
            for i in range(n_feature_types) :
                if configuration % 2 == 1 :
                    feature_type = feature_types[i]
                    feature = {'feature_type' : feature_type, 'min_motif_length' : min_kmer_length, 'max_motif_length' : kmer_length, 'minimum_fraction' : minimum_fraction, 'cutoff' : cutoff, 'correction_method' : correction_method}
                    features.append(feature)
                    names.append(feature_type)
                configuration /= 2
            if len(names) == 0 :
                continue
            feature_type = '_'.join(names)
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename_nocv = '%s/%s-min_kmer_%d-max_kmer_%d-sqrt_eran_refined2-1k-minimum_fraction_%s-cutoff_%s-correction_method_%s-no_cv.out' % (file_path, '_'.join(names), min_kmer_length, kmer_length, str(minimum_fraction), str(cutoff), correction_method)
            if os.path.exists(filename_nocv) :
                print '    [i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, ', '.join(names))
                continue
            print '    [i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, ', '.join(names))
            res = lib.regression.train_gbrf_no_cv(dataset_name, features, log2 = True, n_estimators = 1000, use_aux = True, cv_folds = folds, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename_nocv)
            print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        kmer_length = int(sys.argv[2])
        kmer_lengths = [kmer_length]
    run_inner(dataset_name)

