#!/usr/bin/env python
#import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

window_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90]
client_url = './profile/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    folds = lib.reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
    for window_size in window_sizes :
        file_path = '../results/regression/current/%s/gbrf' % dataset_name
        filename = '%s/mfe-windows-window_size_%d-sqrt-n_estimators.out' % (file_path, window_size)
        filename_final = '%s/mfe-windows-window_size_%d-sqrt-n_estimators-final.out' % (file_path, window_size)
        if os.path.exists(filename_final) :
            print '[i] Skipping for %s : window size = %d (exists)' % (dataset_name, window_size)
            continue
        if not os.path.exists(filename) :
            print filename
            print '[i] Skipping for %s : window size = %d' % (dataset_name, window_size)
            continue
        results = lib.reader.read_pickle(filename)
        print '[i] Starting for %s : window size = %d' % (dataset_name, window_size)
        features = [{'feature_type' : 'mfe-windows', 'window_size' : window_size}]
        res = lib.regression.train_gbrf_outer_cv_final(results, use_aux = True, cv_folds = folds, client_url = client_url)
        if not os.path.exists(file_path) :
            os.makedirs(file_path)
        lib.writer.write_pickle(res, filename_final)
        print '    [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2:
        window_size = int(sys.argv[2])
        window_sizes = [window_sizes]
    run_inner(dataset_name)
