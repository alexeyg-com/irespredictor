#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np

window_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90]
client_url = './profile/security/ipcontroller-client.json'

def run_inner(positive_only = True, randomized_order = True) :
    '''
    Runs inner fold CV for the GBRFR.
    :param positive_only : whether to run it only for the positive datasets.
    :param randomized_order : whether to randomize the order in which things are computed.
    '''
    datasets = ['native-positive', 'viral_native_ssRNA_all-positive', 'viral_native_dsRNA_all-positive']
    datasets = [dataset_name for dataset_name in datasets if (dataset_name[-9:] == '-positive') == positive_only]

    if randomized_order :
        np.random.shuffle(datasets)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        if not os.path.exists(cv_filename) :
            print '[i] Skipping dataset: %s (CV fold missing)' % dataset_name
            continue
        folds = lib.reader.read_pickle(cv_filename)
        if randomized_order :
            np.random.shuffle(window_sizes)

        for window_size in window_sizes :
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename = '%s/mfe-windows-window_size_%d-sqrt-n_estimators.out' % (file_path, window_size)
            if os.path.exists(filename) :
                print '    [i] Skipping for %s : window size = %d, type = %s (exists)' % (dataset_name, window_size, 'mfe-windows')
                continue
            print '    [i] Starting for %s : window size = %d, type = %s' % (dataset_name, window_size, 'mfe-windows')
            features = [{'feature_type' : 'mfe-windows', 'window_size' : window_size}]
            res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, use_aux = True, cv_folds = folds, client_url = client_url)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename)
            print '        [+] Done.'

if __name__ == '__main__' :
    if len(sys.argv) > 1:
        window_size = int(sys.argv[1])
        window_sizes = [window_sizes]
    run_inner(positive_only = True)
