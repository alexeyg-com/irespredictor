#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

cutoff = 0.05
correction_method = 'fdr_bh'
min_kmer_length = 1
kmer_lengths = [3]
features = [{'feature_type' : 'kmers', 'min_motif_length' : min_kmer_length, 'minimum_fraction' : 0.1, 'cutoff' : cutoff, 'correction_method' : correction_method}, {'feature_type' : 'kmers-windows', 'min_motif_length' : min_kmer_length, 'minimum_fraction' : 0.1, 'cutoff' : cutoff, 'correction_method' : correction_method}, {'feature_type' : 'structures-windows', 'min_motif_length' : min_kmer_length, 'minimum_fraction' : 0.9, 'cutoff' : cutoff, 'correction_method' : correction_method}]

client_url =  '/home/nfs/alexeygritsenk/.ipython/profile_ires/security/ipcontroller-client.json'
#client_url =  './profile-grid/security/ipcontroller-client.json'

#datasets = ['human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_positive_uncapped', 'viral_native_ssRNA_positive_capped', 'native']
datasets = ['human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_positive_uncapped', 'viral_native_ssRNA_positive_capped']

datasets = ['viral_native_dsRNA_all']

def run_inner(randomized_order = True) :
    '''
    Runs inner fold CV for the GBRFR.
    :param randomized_order : whether to randomize the order in which things are computed.
    '''

    if randomized_order :
        np.random.shuffle(datasets)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        if not os.path.exists(cv_filename) :
            print '[i] Skipping dataset: %s (CV fold missing)' % dataset_name
            continue
        folds = lib.reader.read_pickle(cv_filename)

        names = [feature['feature_type'] for feature in features]
        for kmer_length in kmer_lengths :
            for feature in features :
                feature['max_motif_length'] = kmer_length
                if feature['feature_type'] == 'structures-windows' :
                    feature['max_motif_length'] = 1
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename = '%s/%s-min_kmer_%d-max_kmer_%d-sqrt_eran_refined2-1k_structure_1-correction_method_%s.out' % (file_path, '_'.join(names), min_kmer_length, kmer_length, correction_method)
            if os.path.exists(filename) :
                print '    [i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, ', '.join(names))
                continue
            print '    [i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, ', '.join(names))
            grid_search = copy.deepcopy(lib.regression._grid_search_settings_eran_refined2)
            res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, n_estimators = 1000, use_aux = True, cv_folds = folds, client_url = client_url, grid_search = grid_search)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename)
            print '        [+] Done.'

if __name__ == '__main__' :
    run_inner(randomized_order = True)

