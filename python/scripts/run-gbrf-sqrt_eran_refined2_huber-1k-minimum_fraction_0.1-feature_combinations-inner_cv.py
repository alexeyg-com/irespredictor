#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

correction_method = 'fdr_bh'
cutoff = 0.05
min_kmer_length = 1
kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['kmers', 'kmers-windows']
minimum_fraction = 0.1

client_url =  '/home/nfs/alexeygritsenk/.ipython/profile_ires/security/ipcontroller-client.json'
#client_url =  './profile-grid/security/ipcontroller-client.json'

datasets = ['human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_positive_uncapped', 'viral_native_ssRNA_positive_capped', 'native']
#datasets = ['human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_positive_uncapped', 'viral_native_ssRNA_positive_capped']
datasets = ['viral_native_dsRNA_all']

def run_inner(randomized_order = True) :
    '''
    Runs inner fold CV for the GBRFR.
    :param randomized_order : whether to randomize the order in which things are computed.
    '''

    n_feature_types = len(feature_types)
    n_combinations = 2 ** n_feature_types
    configurations = range(n_combinations)
    if randomized_order :
        np.random.shuffle(datasets)
        np.random.shuffle(kmer_lengths)
        np.random.shuffle(configurations)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        if not os.path.exists(cv_filename) :
            print '[i] Skipping dataset: %s (CV fold missing)' % dataset_name
            continue
        folds = lib.reader.read_pickle(cv_filename)

        for kmer_length in kmer_lengths :
            for configuration in configurations :
                names, features = [], []
                for i in range(n_feature_types) :
                    if configuration % 2 == 1 :
                        feature_type = feature_types[i]
                        feature = {'feature_type' : feature_type, 'min_motif_length' : min_kmer_length, 'max_motif_length' : kmer_length, 'minimum_fraction' : minimum_fraction, 'cutoff' : cutoff, 'correction_method' : correction_method}
                        features.append(feature)
                        names.append(feature_type)
                    configuration /= 2
                if len(names) == 0 :
                    continue
                feature_type = '_'.join(names)
                file_path = '../results/regression/current/%s/gbrf' % dataset_name
                filename = '%s/%s-min_kmer_%d-max_kmer_%d-sqrt_eran_refined2_huber-1k-minimum_fraction_%s-cutoff_%s-correction_method_%s.out' % (file_path, '_'.join(names), min_kmer_length, kmer_length, str(minimum_fraction), str(cutoff), correction_method)
                if os.path.exists(filename) :
                    print '    [i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, ', '.join(names))
                    continue
                print '    [i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, ', '.join(names))
                grid_search = copy.deepcopy(lib.regression._grid_search_settings_eran_refined2_huber)
                res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, n_estimators = 1000, use_aux = True, cv_folds = folds, client_url = client_url, grid_search = grid_search)
                if not os.path.exists(file_path) :
                    os.makedirs(file_path)
                lib.writer.write_pickle(res, filename)
                print '        [+] Done.'

if __name__ == '__main__' :
    run_inner(randomized_order = True)

