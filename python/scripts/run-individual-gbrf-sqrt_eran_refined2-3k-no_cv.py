#!/usr/bin/env python
#import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['independent', 'independent-windows', 'positional', 'positional-windows']
client_url = './profile/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    folds = lib.reader.read_pickle('../results/regression/current/%s/crossvalidation_folds.out' % dataset_name)
    for kmer_length in kmer_lengths :
        for feature_type in feature_types :
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename_nocv = '%s/%s-max_kmer_%d-sqrt_eran_refined2-3k-no_cv.out' % (file_path, feature_type, kmer_length)
            if os.path.exists(filename_nocv) :
                print '[i] Skipping for %s : k = %d, type = %s (exists)' % (dataset_name, kmer_length, feature_type)
                continue
            print '[i] Starting for %s : k = %d, type = %s' % (dataset_name, kmer_length, feature_type)
            features = [{'feature_type' : feature_type, 'max_motif_length' : kmer_length}]
            grid_search = lib.regression._grid_search_settings_eran_refined2
            res = lib.regression.train_gbrf_no_cv(dataset_name, features, n_estimators = 3000, log2 = True, use_aux = True, cv_folds = folds, client_url = client_url, grid_search = grid_search)
            lib.writer.write_pickle(res, filename_nocv)
            print '    [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        kmer_length = int(sys.argv[2])
        kmer_lengths = [kmer_length]
    run_inner(dataset_name)
