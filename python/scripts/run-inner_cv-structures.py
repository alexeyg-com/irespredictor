#!/usr/bin/env python
import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

def run_inner(dataset_name) :
    lib.prepare.precompute_inner_fold_bppm_features(datasets = [dataset_name], randomized_order = True, cutoff = 1.1)

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)
