#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

kmer_lengths = [1, 2, 3, 4, 5]
feature_types = ['independent', 'independent-windows', 'positional-windows', 'positional']
minimum_fraction_settings = [0.0, 0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064, 0.128, 0.256, 0.512]
client_url =  './profile-grid/security/ipcontroller-client.json'

def run_inner(positive_only = True, randomized_order = True) :
    '''
    Runs inner fold CV for the GBRFR.
    :param positive_only : whether to run it only for the positive datasets.
    :param randomized_order : whether to randomize the order in which things are computed.
    '''
    #datasets = lib.reader.read_pickle('../results/regression/current/selected-dataset-names.out')
    #datasets = [dataset_name for dataset_name in datasets if not '-train' in dataset_name]
    datasets = ['viral_native_ssRNA_all-positive', 'viral_native_dsRNA_all-positive', 'native-positive']
    datasets = [dataset_name for dataset_name in datasets if (dataset_name[-9:] == '-positive') == positive_only]

    if randomized_order :
        np.random.shuffle(datasets)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        if not os.path.exists(cv_filename) :
            print '[i] Skipping dataset: %s (CV fold missing)' % dataset_name
            continue
        folds = lib.reader.read_pickle(cv_filename)
        if randomized_order :
            np.random.shuffle(kmer_lengths)
            np.random.shuffle(feature_types)
            np.random.shuffle(minimum_fraction_settings)

        for kmer_length in kmer_lengths :
            for feature_type in feature_types :
                for minimum_fraction in minimum_fraction_settings :
                    file_path = '../results/regression/current/%s/gbrf' % dataset_name
                    filename = '%s/%s-max_kmer_%d-sqrt_eran_refined2-3k-minimum_fraction_%s.out' % (file_path, feature_type, kmer_length, str(minimum_fraction))
                    if os.path.exists(filename) :
                        print '    [i] Skipping for %s : k = %d, type = %s, minimum fraction = %s (exists)' % (dataset_name, kmer_length, feature_type, str(minimum_fraction))
                        continue
                    print '    [i] Starting for %s : k = %d, type = %s, minimum fraction = %s' % (dataset_name, kmer_length, feature_type, str(minimum_fraction))
                    features = [{'feature_type' : feature_type, 'max_motif_length' : kmer_length, 'minimum_fraction' : minimum_fraction}]
                    grid_search = lib.regression._grid_search_settings_eran_refined2
                    res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, n_estimators = 3000, use_aux = True, cv_folds = folds, client_url = client_url, grid_search = grid_search)
                    if not os.path.exists(file_path) :
                        os.makedirs(file_path)
                    lib.writer.write_pickle(res, filename)
                    print '        [+] Done.'

if __name__ == '__main__' :
    run_inner(positive_only = True, randomized_order = True)
