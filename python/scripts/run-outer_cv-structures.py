#!/usr/bin/env python
import lib.prepare
import lib.regression
import lib.persistent
import lib.reader
import sys
import os

def run_inner(dataset_name) :
    #datasets = lib.reader.read_pickle('../results/selected-dataset-names.out')
    lib.prepare.precompute_outer_fold_bppm_features(datasets = [dataset_name], cutoff = 1.1)

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)
