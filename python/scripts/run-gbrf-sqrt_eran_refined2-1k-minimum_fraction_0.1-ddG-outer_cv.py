#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

correction_method = 'fdr_bh'
cutoff = 0.05
feature_type = 'ddG-open-windows'
window_setups = [(10, 5), (20, 10), (30, 10), (40, 10), (50, 10), (60, 10), (70, 10), (80, 10)]
minimum_fraction = 0.1

#client_url = None
client_url =  '/home/nfs/alexeygritsenk/.ipython/profile_ires/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which the regression should be computed.
    '''
    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for window_size, window_step in window_setups :
        feature_name = 'ddG_size%d_step%d' % (window_size, window_step)
        file_path = '../results/regression/current/%s/gbrf' % dataset_name
        filename = '%s/%s-sqrt_eran_refined2-1k-minimum_fraction_%s-cutoff_%s-correction_method_%s.out' % (file_path, feature_name, str(minimum_fraction), str(cutoff), correction_method)
        filename_final = '%s/%s-sqrt_eran_refined2-1k-minimum_fraction_%s-cutoff_%s-correction_method_%s-final.out' % (file_path, feature_name, str(minimum_fraction), str(cutoff), correction_method)
        if os.path.exists(filename_final) :
            print '    [i] Skipping for %s (%d, %d) : type = %s (exists)' % (dataset_name, window_size, window_step, feature_type)
            continue
        if not os.path.exists(filename) :
            print '    [i] Skipping for %s (%d, %d) : type = %s (missing)' % (dataset_name, window_size, window_step, feature_type)
            continue
        print '    [i] Starting for %s (%d, %d) : type = %s' % (dataset_name, window_size, window_step, feature_type)
        results = lib.reader.read_pickle(filename)
        res = lib.regression.train_gbrf_outer_cv_final(results, can_choose_n_estimators = 1000, use_aux = True, cv_folds = folds, return_regressor = True, client_url = client_url)
        if not os.path.exists(file_path) :
            os.makedirs(file_path)
        lib.writer.write_pickle(res, filename_final)
        print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    run_inner(dataset_name)

