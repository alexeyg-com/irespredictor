#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

kmer_lengths = [4]
feature_types = ['independent', 'independent-windows', 'positional-windows', 'positional']
max_features_settings = ['log2', 'sqrt', 10, 20, 30, 40, 50, 60, 70, 80, 90, 0.001, 0.002, 0.004, 0.008, 0.016, 0.032]
client_url =  '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'

def run_inner(dataset_name) :
    '''
    Runs inner fold CV for the GBRFR.
    :param dataset_name : name of the dataset for which the regression should be computed.
    '''

    print '[i] Processing dataset: %s' % dataset_name
    cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
    folds = lib.reader.read_pickle(cv_filename)

    for kmer_length in kmer_lengths :
        for feature_type in feature_types :
            for max_features in max_features_settings :
                file_path = '../results/regression/current/%s/gbrf' % dataset_name
                filename = '%s/%s-max_kmer_%d-sqrt_eran_refined2-3k-max_features_%s.out' % (file_path, feature_type, kmer_length, str(max_features))
                filename_final = '%s/%s-max_kmer_%d-sqrt_eran_refined2-3k-max_features_%s-final.out' % (file_path, feature_type, kmer_length, str(max_features))
                if os.path.exists(filename_final) :
                    print '    [i] Skipping for %s : k = %d, type = %s, max features = %s' % (dataset_name, kmer_length, feature_type, str(max_features))
                    continue
                if not  os.path.exists(filename) :
                    print '    [i] Skipping for %s : k = %d, type = %s, max features = %s' % (dataset_name, kmer_length, feature_type, str(max_features))
                    continue
                print '    [i] Starting for %s : k = %d, type = %s, max features = %s' % (dataset_name, kmer_length, feature_type, str(max_features))
                results = lib.reader.read_pickle(filename)
                res = lib.regression.train_gbrf_outer_cv_final(results, can_choose_n_estimators = False, use_aux = True, cv_folds = folds, client_url = client_url)
                if not os.path.exists(file_path) :
                    os.makedirs(file_path)
                lib.writer.write_pickle(res, filename_final)
                print '        [+] Done.'

if __name__ == '__main__' :
    dataset_name = sys.argv[1]
    if len(sys.argv) > 2 :
        max_features = sys.argv[2]
        max_features_settings = [max_features]
    run_inner(dataset_name)

