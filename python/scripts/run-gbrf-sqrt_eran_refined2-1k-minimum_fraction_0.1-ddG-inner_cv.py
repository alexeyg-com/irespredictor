#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np
import copy

correction_method = 'fdr_bh'
cutoff = 0.05
feature_type = 'ddG-open-windows'
minimum_fraction = 0.1
window_setups = [(10, 5), (20, 10), (30, 10), (40, 10), (50, 10), (60, 10), (70, 10), (80, 10)]

client_url = '/home/nfs/alexeygritsenk/.ipython/profile_ires/security/ipcontroller-client.json'

#datasets = ['human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'viral_native_ssRNA_positive_uncapped', 'viral_native_ssRNA_positive_capped', 'native']
datasets = ['human_native_5utr', 'human_native_cds', 'viral_native_ssRNA_negative', 'viral_native_dsRNA_all', 'human_native_3utr', 'viral_native_RTV', 'viral_native_ssRNA_positive', 'native']

def run_inner(randomized_order = True) :
    '''
    Runs inner fold CV for the GBRFR.
    :param randomized_order : whether to randomize the order in which things are computed.
    '''

    if randomized_order :
        np.random.shuffle(datasets)
        np.random.shuffle(window_setups)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        if not os.path.exists(cv_filename) :
            print '[i] Skipping dataset: %s (CV fold missing)' % dataset_name
            continue
        folds = lib.reader.read_pickle(cv_filename)

        for window_size, window_step in window_setups :
            feature = {'feature_type' : feature_type, 'minimum_fraction' : minimum_fraction, 'cutoff' : cutoff, 'correction_method' : correction_method, 'window_size' : window_size, 'window_step' : window_step}
            features = [feature]
            feature_name = 'ddG_size%d_step%d' % (window_size, window_step)
            file_path = '../results/regression/current/%s/gbrf' % dataset_name
            filename = '%s/%s-sqrt_eran_refined2-1k-minimum_fraction_%s-cutoff_%s-correction_method_%s.out' % (file_path, feature_name, str(minimum_fraction), str(cutoff), correction_method)
            if os.path.exists(filename) :
                print '    [i] Skipping for %s (%d, %d) : type = %s (exists)' % (dataset_name, window_size, window_step, feature_type)
                continue
            print '    [i] Starting for %s (%d, %d) : type = %s' % (dataset_name, window_size, window_step, feature_type)
            grid_search = copy.deepcopy(lib.regression._grid_search_settings_eran_refined2)
            res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, n_estimators = 1000, use_aux = True, cv_folds = folds, client_url = client_url, grid_search = grid_search)
            if not os.path.exists(file_path) :
                os.makedirs(file_path)
            lib.writer.write_pickle(res, filename)
            print '        [+] Done.'

if __name__ == '__main__' :
    run_inner(randomized_order = True)

