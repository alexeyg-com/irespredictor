#!/usr/bin/env python
import lib.regression
import lib.persistent
import lib.reader
import lib.writer
import sys 
import os
import numpy as np

window_feature_types = ['fmm-windows', ['fmm', 'fmm-windows']]
feature_types = ['fmm']
aggregators = ['sum', 'max', 'both']
window_sizes = [10, 20, 30, 40, 50, 60, 70, 80, 90]
window_step = 10

client_url =  '/home/alexeyg/.ipython/profile_default/security/ipcontroller-client.json'
#client_url = None

def run_inner(positive_only = True, randomized_order = True) :
    '''
    Runs inner fold CV for the GBRFR.
    :param positive_only : whether to run it only for the positive datasets.
    :param randomized_order : whether to randomize the order in which things are computed.
    '''
    #datasets = lib.reader.read_pickle('../results/regression/current/selected-dataset-names.out')
    #datasets = [dataset_name for dataset_name in datasets if not '-train' in dataset_name]
    #datasets = ['viral_native_ssRNA_all-positive', 'viral_native_dsRNA_all-positive', 'native-positive']
    datasets = ['native-positive']
    datasets = [dataset_name for dataset_name in datasets if (dataset_name[-9:] == '-positive') == positive_only]

    if randomized_order :
        np.random.shuffle(datasets)

    for dataset_name in datasets :
        print '[i] Processing dataset: %s' % dataset_name
        cv_filename = '../results/regression/current/%s/crossvalidation_folds.out' % dataset_name
        if not os.path.exists(cv_filename) :
            print '[i] Skipping dataset: %s (CV fold missing)' % dataset_name
            continue
        folds = lib.reader.read_pickle(cv_filename)
        if randomized_order :
            np.random.shuffle(window_sizes)
            np.random.shuffle(feature_types)
            np.random.shuffle(aggregators)

        for aggregator in aggregators :
            for feature_type in window_feature_types :
                for window_size in window_sizes :
                    file_path = '../results/regression/current/%s/gbrf' % dataset_name
                    if isinstance(feature_type, list) :
                        feature_type_str = '_'.join(feature_type)
                    else :
                        feature_type_str = feature_type
                    filename = '%s/%s%d_step%d-aggregator_%s-sqrt.out' % (file_path, feature_type_str, window_size, window_step, aggregator)
                    if os.path.exists(filename) :
                        print '    [i] Skipping for %s : window size = %d, type = %s, aggregator = %s (exists)' % (dataset_name, window_size, feature_type_str, aggregator)
                        continue
                    print '    [i] Starting for %s : window size = %d, type = %s, aggregator = %s' % (dataset_name, window_size, feature_type_str, aggregator)
                    features = []
                    if not isinstance(feature_type, list) :
                        feature_type = [feature_type]
                    for feature in feature_type :
                        features.append({'feature_type' : feature, 'window_size' : window_size, 'window_step' : window_step, 'aggregator' : aggregator})
                    res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, use_aux = True, cv_folds = folds, client_url = client_url)
                    if not os.path.exists(file_path) :
                        os.makedirs(file_path)
                    lib.writer.write_pickle(res, filename)
                    print '        [+] Done.'
            for feature_type in feature_types :
                file_path = '../results/regression/current/%s/gbrf' % dataset_name
                if isinstance(feature_type, list) :
                    feature_type_str = '_'.join(feature_type)
                else :
                    feature_type_str = feature_type
                filename = '%s/%s-aggregator_%s-sqrt.out' % (file_path, feature_type_str, aggregator)
                if os.path.exists(filename) :
                    print '    [i] Skipping for %s : type = %s, aggregator = %s (exists)' % (dataset_name, feature_type_str, aggregator)
                    continue
                print '    [i] Starting for %s : type = %s, aggregator = %s' % (dataset_name, feature_type_str, aggregator)
                features = []
                if not isinstance(feature_type, list) :
                    feature_type = [feature_type]
                for feature in feature_type :
                    features.append({'feature_type' : feature, 'aggregator' : aggregator})
                res = lib.regression.train_gbrf_outer_cv(dataset_name, features, log2 = True, use_aux = True, cv_folds = folds, client_url = client_url)
                if not os.path.exists(file_path) :
                    os.makedirs(file_path)
                lib.writer.write_pickle(res, filename)
                print '        [+] Done.'

if __name__ == '__main__' :
    run_inner(positive_only = True)

