'''
A module defining the various experiments that were ran.

Alexey Gritsenko
02-01-2015
'''

import lib.tools
import lib.reader
import lib.writer

import numpy as np

def create_RNApromo_input_files(n_reps = 5) :
    '''
    Creates a bunch of input files to be used with RNApromo.
    :param n_reps : number of repetitions for the random sample from the inactive sequences.
    '''
    native_ids = lib.reader.read_sequence_ids(filename = '../data/ires_lib_oligos_for_fmm_analysis.tab')
    seqs = lib.reader.read_sequences(filename = '../data/55k_oligos_sequence_and_expression_measurements.tab')
    seqs = lib.tools.filter_sequences(seqs)
    native_seqs = lib.tools.select_sequences_by_id(seqs, native_ids)
    # Sort sequences by decreasing IRES activity
    native_seqs.sort(key = lambda x : -x.ires_activity)
    native_seqs_trimmed = lib.tools.trim_sequences(native_seqs)
    for cur_seqs, cur_name in zip([native_seqs, native_seqs_trimmed], ['native_seqs', 'native_seqs-trimmed']) :
        print '[i] Processing %s (%d seqs)' % (cur_name, len(cur_seqs))
        positive, negative  = lib.tools.get_positive_sequences(cur_seqs)
        print '   [i] Positive: %d' % len(positive)
        print '   [i] Negative: %d' % len(negative)
        lib.writer.write_sequences_fasta(positive, '%s-positive.fa' % cur_name)
        lib.writer.write_sequences_fasta(negative, '%s-negative.fa' % cur_name)
        structured, unstructured = lib.tools.get_structured_sequences(cur_seqs)
        structured_positive, structured_negative = lib.tools.get_positive_sequences(structured)
        unstructured_positive, unstructured_negative = lib.tools.get_positive_sequences(unstructured)
        print '   [i] Structured: %d' % len(structured)
        print '       [i] Positive: %d' % len(structured_positive)
        print '       [i] Negative: %d' % len(structured_negative)
        print '   [i] Unstructured: %d' % len(unstructured)
        print '       [i] Positive: %d' % len(unstructured_positive)
        print '       [i] Negative: %d' % len(unstructured_negative)
        lib.writer.write_sequences_fasta(structured_positive, '%s-structured-positive.fa' % cur_name)
        lib.writer.write_sequences_fasta(structured_negative, '%s-structured-negative.fa' % cur_name)
        lib.writer.write_sequences_fasta(unstructured_positive, '%s-unstructured-positive.fa' % cur_name)
        lib.writer.write_sequences_fasta(unstructured_negative, '%s-unstructured-negative.fa' % cur_name)
        top10percent_num = int(len(positive) * 0.1)
        top10percent = positive[:top10percent_num]
        lib.writer.write_sequences_fasta(top10percent, '%s-top10_percent-positive.fa' % cur_name)
        print '   [i] Top 10%%: %d' % len(top10percent)
        for i in range(n_reps) :
            random_inactive = np.random.choice(negative, top10percent_num, replace = False)
            print '      [i] Random negative (sample %d): %d' % (i + 1, len(random_inactive))
            lib.writer.write_sequences_fasta(random_inactive, '%s-random_negative_%d.fa' % (cur_name, i + 1))

def create_RNApromo_origin_input_files() :
    '''
    Create a bunch of input files to be used with RNApromo when applied to subsets of sequences separated by origin.
    Only trimmed sequences are used.
    '''
    seqs = lib.reader.read_sequences(filename = '../data/55k_oligos_sequence_and_expression_measurements.tab')
    seqs = lib.tools.filter_sequences(seqs)
    origins = lib.tools.get_sequence_origins(seqs)
    origins = list(origins)
    seqs = lib.tools.trim_sequences(seqs)
    seqs.sort(key = lambda x : -x.ires_activity)
    for origin in origins :
        origin_seqs = lib.tools.get_sequences_by_origin(seqs, origin)
        print '[i] Processing origin %s (%d seqs)' % (origin, len(origin_seqs))
        positive, negative  = lib.tools.get_positive_sequences(origin_seqs)
        print '   [i] Positive: %d' % len(positive)
        print '   [i] Negative: %d' % len(negative)
        lib.writer.write_sequences_fasta(positive, '%s-trimmed-positive.fa' % origin)
        lib.writer.write_sequences_fasta(negative, '%s-trimmed-negative.fa' % origin)
        structured, unstructured = lib.tools.get_structured_sequences(origin_seqs)
        structured_positive, structured_negative = lib.tools.get_positive_sequences(structured)
        unstructured_positive, unstructured_negative = lib.tools.get_positive_sequences(unstructured)
        print '   [i] Structured: %d' % len(structured)
        print '       [i] Positive: %d' % len(structured_positive)
        print '       [i] Negative: %d' % len(structured_negative)
        print '   [i] Unstructured: %d' % len(unstructured)
        print '       [i] Positive: %d' % len(unstructured_positive)
        print '       [i] Negative: %d' % len(unstructured_negative)
        lib.writer.write_sequences_fasta(structured_positive, '%s-trimmed-structured-positive.fa' % origin)
        lib.writer.write_sequences_fasta(structured_negative, '%s-trimmed-structured-negative.fa' % origin)
        lib.writer.write_sequences_fasta(unstructured_positive, '%s-trimmed-unstructured-positive.fa' % origin)
        lib.writer.write_sequences_fasta(unstructured_negative, '%s-trimmed-unstructured-negative.fa' % origin)

def write_rnapromo_structure(structures, filename) :
    '''
    Writes RNA secondary structures to a file.
    :param structures : a dictionary of structures that should be written into a file.
    :param filename : name of the file to which RNA structures should be written.
    '''
    fout = open(filename, 'wb')
    for index, struct in structures.iteritems() :
        fout.write('Seq%d\t%d\t%s\n' % (index, 0, struct))
    fout.close()
