#!/usr/bin/env python

import sys
import os
import copy
from Bio import SeqIO
from sklearn.cross_validation import KFold
import numpy as np
import sklearn.metrics
import scipy.stats
from rpy2 import robjects

'''
Default number of CV folds.
'''
_default_n_folds = 5

'''
Default number of repetitions.
'''
_default_n_reps = 1000

'''
Default negative sequence factor.
'''
_default_negative_factor = 1

'''
RNApromo path.
'''
_rnapromo_path = '/net/mraid08/export/genie/Runs/HumanLibraries/IRES/Analysis/16_expression_bins_eGFP/IRES_Predictor/tools/RNApromo'

def parse_arguments(args, parsed) :
    '''
    Parse arguments.
    :param args : a list of command line arguments.
    :param parsed : a return dictionary with parse results.
    '''
    args = args[1:]
    if len(args) < 3 :
        return False
    parsed['positive_filename'] = args[0]
    parsed['negative_filename'] = args[1]
    parsed['struct_filename'] = args[2]
    if len(args) >= 4 :
        try :
            arg = int(args[3])
            parsed['n_folds'] = arg
        except :
            print '[-] Error: number of folds must be an integer.'
            return False
    if len(args) >= 5 :
        try :
            arg = int(args[4])
            parsed['n_reps'] = arg
        except :
            print '[-] Error: number of repetitions must be an integer.'
            return False
    if len(args) >= 6 :
        try :
            arg = int(args[5])
            parsed['negative_factor'] = arg
        except :
            print '[-] Error: negative sequence factor must be an integer.'
            return False

    return True

def usage(exec_str) :
    '''
    Print execution statement.
    :param exec_str : name of the executable.
    '''
    print '[i] Usage: %s <positive_filename> <negative_filename> <struct_filename> [# CV folds = %d] [# repetitions = %d] [negative factor = %d]' % (exec_str, _default_n_folds, _default_n_reps, _default_negative_factor)

def read_fasta(filename) :
    '''
    Read sequences from a FASTA file.
    :param filename : name of the file to be read.
    '''
    fin = open(filename, 'rb')
    records = SeqIO.parse(fin, 'fasta')
    records = list(records)
    fin.close()
    return records

def read_structs(filename) :
    '''
    Read RNA structures from the structure file.
    :param filename : name of the file from which the structures should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    result = {}
    for line in lines :
        args = line.strip().split('\t')
        id, struct = args[0], args[2]
        result[id] = struct
    return result

def read_scores(filename) :
    '''
    Read RNApromo scores output by a call to rnamotifs08_motif_match.pl
    :param filename : name of the file from which the scores should be read.
    '''
    fin = open(filename, 'rb')
    lines = fin.readlines()
    fin.close()
    result = {}
    for line in lines :
        args = line.strip().split('\t')
        id = args[0]
        score = float(args[1])
        result[id] = score
    return result

def write_fasta(seqs, filename, sort = False) :
    '''
    Write sequences to a file in FASTA format.
    :param seqs : a list of sequences that should be saved.
    :param filename : name of the file to which the sequences should be saved.
    :param sort : should we sort sequences based on ID prior to saving?
    '''
    if sort :
        seqs = copy.deepcopy(seqs)
        seqs.sort(key = lambda x : int(x.id[3:]))
    fout = open(filename, 'wb')
    for seq in seqs :
        fout.write('>%s\n' % seq.id)
        fout.write('%s\n' % str(seq.seq))
    fout.close()

def sample_sequences(seqs, n_samples) :
    '''
    Sample sequences from a list of sequences.
    :param seqs : list of sequences that should be sampled (without replacement).
    :param n_samples : number of sequences to be sampled.
    '''
    n_seqs = len(seqs)
    indices = np.random.choice(n_seqs, size = n_samples, replace = False)
    selected = []
    for index in indices :
        selected.append(seqs[index])
    return selected

def sample_mixed_sequences(seqs_a, seqs_b, n_samples) :
    '''
    Sample a mixture of sequences from two lists.
    :param seqs_a : first list of sequences that should be sampled (without replacement).
    :param seqs_b : second list of sequences that should be sampled (without replacement).
    :param n_samples : number of sequences that should be sampled.
    '''
    n_seqs_a, n_seqs_b = len(seqs_a), len(seqs_b)
    n_seqs = n_seqs_a + n_seqs_b
    indices = np.random.choice(n_seqs, size = n_samples, replace = False)
    selected = []
    for index in indices :
        if index < n_seqs_a :
            selected.append(seqs_a[index])
        else :
            selected.append(seqs_b[index - n_seqs_a])
    return selected

def generate_cv_folds(n_samples, n_folds) :
    '''
    Generate CV sets.
    :param n_samples : number of samples in the set.
    :param n_folds : number of folds for CV.
    '''
    sets = []
    cv_folds = KFold(n_samples, n_folds = n_folds)
    for train_set, test_set in cv_folds :
        sets.append((train_set, test_set))
    return sets

def make_rnapromo_database(seq_filename, struct_filename, output_filename) :
    seqs = read_fasta(seq_filename)
    structs = read_structs(struct_filename)
    fout = open(output_filename, 'wb')
    for seq in seqs :
        id = seq.id
        struct = structs[id]
        actual_seq = str(seq.seq)
        fout.write('%s\t%s\t%s\n' % (id, actual_seq, struct))
    fout.close()

def write_script(lines, filename) :
    fout = open(filename, 'wb')
    fout.writelines([''.join([line, '\n']) for line in lines])
    fout.close()
    os.system('chmod +x %s' % filename)

def execute_script(filename) :
    os.system(filename)

def run_rnapromo(subdir, train_filename, negative_filename, struct_filename, test_filename, script_prefix = '') :
    train_filename = os.path.relpath(train_filename, subdir)
    negative_filename = os.path.relpath(negative_filename, subdir)
    test_filename = os.path.relpath(test_filename, subdir)
    rnapromo_path = _rnapromo_path
    model_basename, _ = os.path.splitext(os.path.basename(train_filename))
    test_database_filename = '%s.db' % os.path.splitext(os.path.basename(test_filename))[0]
    negative_database_filename = '%s.db' % os.path.splitext(os.path.basename(negative_filename))[0]
    test_score_filename = '%s_score.tab' % os.path.splitext(os.path.basename(test_filename))[0]
    negative_score_filename = '%s_score.tab' % os.path.splitext(os.path.basename(negative_filename))[0]
    make_rnapromo_database(os.path.join(subdir, test_filename), struct_filename, os.path.join(subdir, test_database_filename))
    make_rnapromo_database(os.path.join(subdir, negative_filename), struct_filename, os.path.join(subdir, negative_database_filename))
    lines = ['#!/usr/bin/env bash',
             'cd %s' % subdir,
             '%s/rnamotifs08_motif_finder.pl -positive_seq %s -negative_seq %s -positive_struct %s -negative_struct %s -n 1' % (rnapromo_path, train_filename, negative_filename, os.path.relpath(struct_filename, subdir), os.path.relpath(struct_filename, subdir)),
             'mv cm_1.tab %s.tab' % model_basename,
             'mv alignment_1.tab %s_alignment.tab' % model_basename,
             'mv cons_1.tab %s_cons.tab' % model_basename,
             'mv scores_1.tab %s_scores.tab' % model_basename,
             '%s/lib/RNAmodel_plot.pl %s.tab -format pdf -output %s' % (rnapromo_path, model_basename, model_basename),
             '%s/rnamotifs08_motif_match.pl %s -cm %s.tab > %s' % (rnapromo_path, test_database_filename, model_basename, test_score_filename), # check where the output gets?
             '%s/rnamotifs08_motif_match.pl %s -cm %s.tab > %s' % (rnapromo_path, negative_database_filename, model_basename, negative_score_filename)]
    script_filename = os.path.join(subdir, '%s_script.sh' % script_prefix)
    write_script(lines, script_filename)
    execute_script(script_filename)
    return os.path.join(subdir, test_score_filename), os.path.join(subdir, negative_score_filename)

def compute_auc(scores) :
    scores = copy.deepcopy(scores)
    scores.sort(key = lambda x : -x[1]) # what is the proper sort order???
    labels = [label for label, score in scores]
    n_positive, n_negative = np.sum([label == '+' for label in labels], dtype = np.int), np.sum([label == '-' for label in labels], dtype = np.int)
    cur_positive, cur_negative = 0, 0
    x, y = [], []
    for label, score in scores :
        if label == '+' :
            cur_positive += 1
        else :
            cur_negative += 1
        true_positive_rate = cur_positive / float(n_positive)
        false_positive_rate = cur_negative / float(n_negative)
        x.append(false_positive_rate)
        y.append(true_positive_rate)
    auc = sklearn.metrics.auc(x, y)
    return auc

def process_score_files(positive_score_filename, negative_score_filename) :
    positive_scores = read_scores(positive_score_filename)
    negative_scores = read_scores(negative_score_filename)
    scores = []
    for id, score in positive_scores.iteritems() :
        scores.append(('+', score))
    for id, score in negative_scores.iteritems() :
        scores.append(('-', score))
    auc = compute_auc(scores)
    return auc

def perform_cv(positive_seqs, negative_seqs, struct_filename, n_folds, subdir = '', prefix = '') :
    '''
    Perform CV for the given positive and negative sets.
    :param positive_seqs : a list of positive sequences.
    :param negative_seqs : a list of negative sequences.
    :param struct_filename : name of the file containing predicted RNA secondary structures.
    :param subdir : name of the subdirectory in which we should store input files.
    :param prefix : name of the prefix that should be used for the filenames.
    '''
    n_positive = len(positive_seqs)
    cv_folds = generate_cv_folds(n_positive, n_folds)
    if subdir != '' and subdir is not None :
        if not os.path.exists(subdir) :
            os.mkdir(subdir)
    negative_filename = '%s/%s_negative.fa' % (subdir, prefix)
    write_fasta(negative_seqs, negative_filename, sort = True)
    auc = []
    fold = 0
    for train_indices, test_indices in cv_folds :
        train_seqs = [positive_seqs[index] for index in train_indices]
        test_seqs = [positive_seqs[index] for index in test_indices]
        train_filename = '%s/%s_fold%d_train.fa' % (subdir, prefix, fold)
        test_filename = '%s/%s_fold%d_test.fa' % (subdir, prefix, fold)
        write_fasta(train_seqs, train_filename, sort = True)
        write_fasta(test_seqs, test_filename, sort = True)
        script_prefix = '%s_fold%d' % (prefix, fold)
        test_score_filename, negative_score_filename = run_rnapromo(subdir, train_filename, negative_filename, struct_filename, test_filename, script_prefix)
        fold_auc = process_score_files(test_score_filename, negative_score_filename)
        auc.append(fold_auc)
        fold += 1
    return auc

def write_auc(fout, cv_auc) :
    '''
    Write AUC results to an output file.
    :param fout : handle of the output file.
    :param cv_auc : array of output CV folds.
    '''
    n = len(cv_auc)
    for i in range(n) :
        fout.write('%.10f\t' % cv_auc[i])
    fout.write('\t%.10f\n' % np.mean(cv_auc))
    fout.flush()

def kstest(x, y) :
    '''
    Perform two sample one-sided KS test on given samples.
    :param x : sample one (should be smaller).
    :param y : sample two (should be larger)
    '''
    func = robjects.r['ks.test']
    x = robjects.FloatVector(x)
    y = robjects.FloatVector(y)
    res = func(x, y, alternative = 'greater')
    statistic, pvalue = res[0][0], res[1][0]
    return statistic, pvalue

def main(positive_filename, negative_filename, struct_filename, n_folds = _default_n_folds, n_reps = _default_n_reps, negative_factor = _default_negative_factor, output_filename = 'auc.txt') :
    '''
    Main control function.
    :param positive_filename : name of the positive sequence set.
    :param negative_filename : name of the negative sequence set.
    :param struct_filename : name of the file containing predicted RNA secondary structures.
    :param n_folds : number of CV folds.
    :param n_repetitions : number of sampling repetitions.
    :param negative_factor : ratio of negative to positive sequences in each of the random samples.
    :param output_filename : name of the output file (for AUC and CV results).
    '''
    positive_seqs = read_fasta(positive_filename)
    negative_seqs = read_fasta(negative_filename)
    n_positive, n_negative = len(positive_seqs), len(negative_seqs)
    negative_factor = (n_folds - 1.0) / float(n_folds) * negative_factor
    if n_positive * negative_factor > n_negative * 0.7 :
        print '[!] Warning: too few negative sequences (n_positive = %d, n_negative = %d, negative factor = %d)' % (n_positive, n_negative, negative_factor)
    fout = open(output_filename, 'wb')
    auc_positive_means, auc_control_means, auc_control2_means = [], [], []
    for rep in range(n_reps) :
        positive_sample = positive_seqs
        negative_sample = sample_sequences(negative_seqs, n_positive * negative_factor)
        control_sample = sample_mixed_sequences(positive_seqs, negative_seqs, n_positive)
        control_sample2 = sample_mixed_sequences(positive_seqs, negative_seqs, n_positive)
        n_positive_sample, n_negative_sample = len(positive_sample), len(negative_sample)
        n_control_sample, n_control_sample2 = len(control_sample), len(control_sample2)
        print '[i] Sample %d (n_positive = %d, n_negative = %d, n_control = %d, n_control2 = %d)' % (rep + 1, n_positive_sample, n_negative_sample, n_control_sample, n_control_sample2)
        fout.write('%d\n' % rep)
        subdir = 'rep%d' % rep
        auc_positive = perform_cv(positive_sample, negative_sample, struct_filename, n_folds = n_folds, subdir = subdir, prefix = 'positive')
        auc_control = perform_cv(control_sample, negative_sample, struct_filename, n_folds = n_folds, subdir = subdir, prefix = 'control')
        auc_control2 = perform_cv(control_sample2, negative_sample, struct_filename, n_folds = n_folds, subdir = subdir, prefix = 'control2')
        write_auc(fout, auc_positive)
        write_auc(fout, auc_control)
        write_auc(fout, auc_control2)
        print '   [+] AUC positive: %.5f' % np.mean(auc_positive)
        print '   [+] AUC control: %.5f' % np.mean(auc_control)
        print '   [+] AUC control2: %.5f' % np.mean(auc_control2)
        auc_positive_means.append(np.mean(auc_positive))
        auc_control_means.append(np.mean(auc_control))
        auc_control2_means.append(np.mean(auc_control2))
    auc_positive_means = np.array(auc_positive_means)
    auc_control_means = np.array(auc_control_means)
    auc_control2_means = np.array(auc_control2_means)

    print '[+] Positive > Control: p = %g' % kstest(auc_control_means, auc_positive_means)[1]
    print '[+] Positive > Control2: p = %g' % kstest(auc_control2_means, auc_positive_means)[1]
    print '[+] Positive - Control > Control2 - Control: p = %g' % kstest(auc_control2_means - auc_control_means, auc_positive_means - auc_control_means)[1]
    print '[+] Control2 > Control: p = %g' % kstest(auc_control_means, auc_control2_means)[1]
    
    fout.write('Positive > Control: p = %g\n' % kstest(auc_control_means, auc_positive_means)[1])
    fout.write('Positive > Control2: p = %g\n' % kstest(auc_control2_means, auc_positive_means)[1])
    fout.write('Positive - Control > Control2 - Control: p = %g\n' % kstest(auc_control2_means - auc_control_means, auc_positive_means - auc_control_means)[1])
    fout.write('Control2 > Control: p = %g\n' % kstest(auc_control_means, auc_control2_means)[1])
    fout.close()

if __name__ == '__main__' :
    parsed = {}
    if not parse_arguments(sys.argv, parsed) :
        print '[-] Error: too few arguments.'
        usage(sys.argv[0])
    else :
        main(**parsed)

