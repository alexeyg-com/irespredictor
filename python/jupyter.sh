#!/usr/bin/bash

#SBATCH --mem 30720
#SBATCH --cpus-per-task 1
#SBATCH --qos bigmem 
#SBATCH --mem-per-cpu 30720
##SBATCH --time 5760
#SBATCH --time 10000
#SBATCH --partition bigmem

export XDG_RUNTIME_DIR=""
echo `pwd`

node=`uname --nodename`

killall ssh
ssh -g -N -R 0.0.0.0:8888:$node:8888 alexeygritsenk@linux-bastion.tudelft.nl &
auks -a
jupyter-notebook
