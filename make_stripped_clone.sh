#!/usr/bin/env bash
# A script for making a stripped clone of the complete project directory.
# When stripping dataset-specific directories are removed.

if [ "$#" -ne 1 ]; then
    echo "[-] Illegal number of parameters"
    exit 1
fi

DEST=$1

if [ ! -d "$DEST" ];
then
    mkdir $DEST
fi

paths="python/datasets
figures
results/regression
results/classification"

excludes="--exclude=results/mfe-arrays.out --exclude=results/ensemble-structure.out --exclude=results/pf-structure.out --exclude=results/mfe-structure.out"
for path in $paths;
do
    for exclude in `ls $path`;
    do
        file="$path/$exclude"
        if [ ! -f $file ] && [ ! -L $file ];
        then
            echo "[i] Going to skip: $file"
            excludes="$excludes --exclude=$file"
        fi
    done
done

#echo $excludes

rsync -a $excludes --exclude=.git ./ $DEST

echo "[i] Starting to create symlinks"
for path in $paths;
do
    for exclude in `ls $path`;
    do
        file="$path/$exclude"
        if [ ! -f $file ] && [ ! -L $file ];
        then
            fullpath=`readlink -m $file`
            ln --symbolic $fullpath $DEST/$path/$exclude
        fi
    done
done
